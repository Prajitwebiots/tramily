<?php $__env->startSection('title'); ?> Supplier | Profile <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- begin::Body -->

<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<!-- BEGIN: Left Aside -->
	<button class="m-aside-left-close m-aside-left-close--skin-light" id="m_aside_left_close_btn">
	<i class="la la-close"></i>
	</button>
	
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					My Profile
					</h3>
				</div>
				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-lg-4">
					<div class="m-portlet m-portlet--full-height  ">
						<div class="m-portlet__body">
							<div class="m-card-profile">
								<div class="m-card-profile__title m--hide">
									Your Profile
								</div>
								<div class="m-card-profile__pic">
									<div class="m-card-profile__pic-wrapper">
										<img src="<?php echo e(URL::asset('assets/images/user')); ?>/<?php echo e(Sentinel::getUser()->profile); ?>" alt="" style="height: 150px;width: 150px"/>
									</div>
								</div>
								<div class="m-card-profile__details">
									<span class="m-card-profile__name">
										<?php echo e(Sentinel::getUser()->first_name); ?> <?php echo e(Sentinel::getUser()->last_name); ?>

									</span>
									<a href="" class="m-card-profile__email m-link">
										<?php echo e(Sentinel::getUser()->email); ?>

									</a>
								</div>
							</div>
							<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
								<li class="m-nav__separator m-nav__separator--fit"></li>
								<li class="m-nav__section m--hide">
									<span class="m-nav__section-text">
										Section
									</span>
								</li>
								<li class="m-nav__item">
									<a href="<?php echo e(url('supplier/profile')); ?>" class="m-nav__link">
										<i class="m-nav__link-icon flaticon-profile-1"></i>
										<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-nav__link-text">
													My Profile
												</span>
												
											</span>
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="<?php echo e(url('supplier/changePass')); ?>" class="m-nav__link">
										<i class="m-nav__link-icon flaticon-lock"></i>
										<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-nav__link-text">
													Change Password
												</span>
												
											</span>
										</span>
									</a>
								</li>
								<!-- <li class="m-nav__item">
										<a href="header/profile&amp;demo=default.html" class="m-nav__link">
												<i class="m-nav__link-icon flaticon-share"></i>
												<span class="m-nav__link-text">
														Activity
												</span>
										</a>
								</li> -->
								
							</ul>
							
							
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
						<div class="m-portlet__head">
							<div class="m-portlet__head-tools">
								<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
											<i class="flaticon-share m--hide"></i>
											Update Profile
										</a>
									</li>
									
								</ul>
							</div>
							
						</div>
						
						<div class="tab-content">
							<div class="tab-pane active" id="m_user_profile_tab_1">
								<form  action=" <?php echo e(url('supplier/profile')); ?>/<?php echo e($userdata->id); ?>" method="post" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
									<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
									<input type="hidden" name="user_id" value="<?php echo e($userdata->id); ?>">
									<br>
									<?php if(session('error')): ?>
									
									<div class="alert alert-danger">
										<?php echo e(session('error')); ?>

									</div>
									<?php endif; ?>
									<?php if(session('success')): ?>
									
									<div class="alert alert-success">
										<?php echo e(session('success')); ?>

									</div>
									<?php endif; ?>
									<div class="m-portlet__body">
										<!-- <div class="form-group m-form__group m--margin-top-10 m--hide">
												<div class="alert m-alert m-alert--default" role="alert">
														The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
												</div>
										</div> -->
										
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												Profile  Picture
											</label>
											<div class="col-8">
												<input class="form-control m-input" type="file" name="photo">
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												Agency Name
											</label>
											<div class="col-8">
												<input class="form-control m-input" autocomplete="off" name="agency_name" type="text" value="<?php echo e($userdata->agency_name); ?>">
											</div>
											 <?php if($errors->has('agency_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('agency_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												Agency Address
											</label>
											<div class="col-8">
												 <textarea class="form-control m-input m-input--solid" autocomplete="off" value="<?php echo e(old('agency_address')); ?>" id="exampleTextarea" name="agency_address" autocomplete="off" placeholder="Agency Address" rows="3"><?php echo e($userdata->agency_address); ?></textarea>
											</div>
											 <?php if($errors->has('agency_address')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('agency_address')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												Concerned Person Name
											</label>
											<div class="col-2">
												<select class="form-control m-select  " id="" name="title_name">
                                                <option <?php if('' == $userdata->title_name): ?> selected <?php endif; ?> value="">TITLE *</option>
                                                <option  <?php if("Mr."== $userdata->title_name): ?> selected <?php endif; ?> value="Mr.">Mr.</option>
                                                <option <?php if( 'Ms.' == $userdata->title_name): ?> selected <?php endif; ?> value="Ms.">Ms.</option>
                                                <option <?php if('Mrs.' == $userdata->title_name): ?> selected <?php endif; ?> value="Mrs.">Mrs.</option>
                                            </select>
											</div>
											<div class="col-3">
												<input class="form-control m-input" type="text" value="<?php echo e($userdata->first_name); ?>" placeholder="First name *" name="first_name" autocomplete="off">
											</div>
											<div class="col-3">
											<input class="form-control m-input" type="text" value="<?php echo e($userdata->last_name); ?>" placeholder="Last name *" name="last_name" autocomplete="off">
											</div>
											<?php if($errors->has('title_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('title_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                             <?php if($errors->has('first_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('first_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                             <?php if($errors->has('last_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('last_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
										
                                       
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												Email
											</label>
											<div class="col-8">
												<input disabled="" class="form-control m-input" name="email" autocomplete="off" type="email" value="<?php echo e($userdata->email); ?>">
											</div>
											<?php if($errors->has('email')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('email')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
										
										
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Phone No.
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="mobile" autocomplete="off" type="text" value="<?php echo e($userdata->mobile); ?>">
											</div>
											<?php if($errors->has('mobile')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('mobile')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>

										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Office No.
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="office_no" autocomplete="off" type="text" value="<?php echo e($userdata->office_no); ?>">
											</div>
											<?php if($errors->has('office_no')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('office_no')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
										
                                        <div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Country
											</label>
											<div class="col-8">
												<select class="form-control m-select  " id="country" name="country">
                                                
                                                <?php $__currentLoopData = $countrylist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option <?php if($userdata->country == $country->id ): ?> selected <?php endif; ?> value="<?php echo e($country->id); ?>"><?php echo e($country->country_name .' / '.$country->country_alphacode); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </select>
											</div>
											<?php if($errors->has('country')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('country')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												State
											</label>
											<div class="col-8">
												<select class="form-control m-select  " id="state" name="state">
                                                
                                                <?php $__currentLoopData = $statelist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option <?php if($userdata->state == $state->id ): ?> selected  <?php endif; ?>  value="<?php echo e($state->id); ?>"><?php echo e($state->state_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </select>
											</div>
											<?php if($errors->has('state')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('state')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												City
											</label>
											<div class="col-8">
												<select class="form-control m-select  " id="city" name="city">
                                               
                                                <?php $__currentLoopData = $citylist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                   <option  <?php if($city->id == $userdata->city): ?>  selected  <?php endif; ?>  value="<?php echo e($city->id); ?>"><?php echo e($city->city_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </select>
											</div>
											<?php if($errors->has('city')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('city')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Pincode
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="pincode" autocomplete="off" type="text" value="<?php echo e($userdata->pincode); ?>">
											</div>
											<?php if($errors->has('pincode')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('pincode')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>

										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												PAN no
											</label>
											<div class="col-8">
												 <input class="form-control m-input" type="text" value="<?php echo e($userdata->pan_no); ?>" placeholder="PAN No" name="pan_no" autocomplete="off">
											</div>
											<?php if($errors->has('pan_no')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('pan_no')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>

										

										<div class="m--padding-15"></div><strong style="font-weight: 600;margin-left: 28px;">Agency GST Details</strong><div class="m--padding-15"></div>
									<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Name
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="gst_full_name" autocomplete="off" type="text" value="<?php echo e($userdata->gst_name); ?>">
											</div>
											<?php if($errors->has('gst_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('gst_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div><div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												State
											</label>
											<div class="col-8">
											<select class="form-control m-select  " id="state" name="gst_state">
                                                <option value="">State *</option>
                                                <?php $__currentLoopData = $statelist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                  <option  <?php if( $state->id == $userdata->gst_state): ?>  selected <?php endif; ?> value="<?php echo e($state->id); ?>"><?php echo e($state->state_name); ?></option> 
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </select>
												
											</div>
											<?php if($errors->has('gst_state')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('gst_state')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												GST Address
											</label>
											<div class="col-8">
												 <textarea class="form-control m-input m-input--solid" autocomplete="off"  id="exampleTextarea" name="gst_address" autocomplete="off" placeholder="GST Address" rows="3"><?php echo e($userdata->gst_address); ?></textarea>
											</div>
											 <?php if($errors->has('agency_address')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('agency_address')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Phone Number
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="gst_phone" autocomplete="off" type="text" value="<?php echo e($userdata->gst_phone); ?>">
											</div>
											<?php if($errors->has('gst_phone')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('gst_phone')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>

										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Registeres email id
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="regi_gmail" autocomplete="off" type="text" value="<?php echo e($userdata->regi_gmail); ?>">
											</div>
											<?php if($errors->has('regi_gmail')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('regi_gmail')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>

										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												GSTIN No
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="gst_no" autocomplete="off" type="text" value="<?php echo e($userdata->gst_no); ?>">
											</div>
											<?php if($errors->has('gst_no')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('gst_no')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
										
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions">
											<div class="row">
												<div class="col-2"></div>
												<div class="col-8">
													<button type="submit" class="btn btn-primary m-btn m-btn--air m-btn--custom">
													Save changes
													</button>
													&nbsp;&nbsp;
													<button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
													Cancel
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane active" id="m_user_profile_tab_2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- end::Body -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<!--begin::Page Vendors -->
<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="<?php echo e(asset('assets/app/js/dashboard.js')); ?>" type="text/javascript"></script>
<!--end::Page Snippets -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>