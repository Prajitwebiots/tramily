<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style type="text/css">
        * {color: #707070;font-family: verdana;font-size: 14px;margin: 0 auto;padding: 0;vertical-align: middle;line-height: 150%;}
        a {text-decoration: none;}
        a:hover {text-decoration: underline;}
    </style>
</head>
<body style="margin:0 auto;padding:0; background-color:#fff;">
<table style="width:730px;  margin: 0 auto;" cellspacing="3" cellpadding="0">
    <tr>
        <td>
            <div style="font-size:12px;padding: 15px 3%;width: 93.8%;background-color:#262734;float:left;">
                <div style='float:left;text-align:left;display: inline-table;'>
                    <img src="<?php echo asset('assets/app/media/img/logos/logo-2.png'); ?>" alt="Tour Tickets" style="width:100px; height: 100%" />
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div style="font-size:12px;color: #707070;border: 1px solid #E5E5E5;padding: 6%;width: 87.7%;background-color:#FFF;float:left;font-family: verdana;">
                <h1 style="color:#000000c7;font-size:  20px;margin-bottom: 0px;">Reset Password</h1>
                <br>
                <div style="border-bottom: 2px solid #272735;margin-bottom:  30px;"></div>
                <h2 style="color:#000000c7">
                     <?php echo e($user->title_name.' '.$user->first_name.' '.$user->last_name); ?> <br /><br />
</h2>

                    <p style="color:#000000c7">We have received a request to reset the password for your account. If you did not request to reset your passwod, please ignore this email.<a href="<?php echo e(url('/')); ?>/reset/<?php echo e($user->username); ?>/<?php echo e($code); ?>">Reset Password</a><br /><br /><br />

                 
                 <b style="color:#000000c7">Thanks & Regards,</b><br>
                <span>Team Tramily</span>
                </p>
            </div>
        </td>
    </tr>
    <tr>
        <!--<td style='background-color:#262734;'>-->
        <!--    <div style="font-size:12px;color: #707070;border-width: 1px; border-style: solid; border-color: #e5e5e5;padding: 15px 3%;width: 93.8%;float:left;text-align: center;">-->
        <!--        <p style='margin: 0;font-family: verdana;font-size:12px;display: block;color: #ffffff;'><?php echo 'copyright'; ?></p>-->
        <!--        <p style='display: block;margin: 0;'>-->
        <!--            <a target="_blank" href="<?php echo url(''); ?>termsconditions" style='font-family: verdana;font-size:12px;color:#707070;display: inline-table;'>Terms &amp; Conditions</a><span style="color:#707070"> | </span>-->
        <!--            <a target="_blank" href="<?php echo url(''); ?>privacypolicy" style='font-family: verdana;font-size:12px;color:#707070;display: inline-table;'>Privacy Policy</a>-->
        <!--        </p>-->
        <!--    </div>-->
        <!--</td>-->
    </tr>
</table>
</body>
</html>