<!-- begin::Header -->
<header class="m-grid__item		m-header "  data-minimize="minimize" data-minimize-offset="200" data-minimize-mobile-offset="200" >
	<div class="m-header__top">
		<div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
			<div class="m-stack m-stack--ver m-stack--desktop">
				<!-- begin::Brand -->
				<div class="m-stack__item m-brand">
					<div class="m-stack m-stack--ver m-stack--general m-stack--inline">
						<div class="m-stack__item m-stack__item--middle m-brand__logo">
							<?php if(Sentinel::getUser()->roles()->first()->slug == '2'): ?>
							<a href="<?php echo e(url('user/dashboard')); ?>" class="m-brand__logo-wrapper">
								<img alt="" src="<?php echo e(URL::asset('assets/app/media/img/logos/logo-2.png')); ?>" style="width: 50%;"/>
							</a>
							<?php else: ?>
							<a href="<?php echo e(url('supplier/dashboard')); ?>" class="m-brand__logo-wrapper">
								<img alt="" src="<?php echo e(URL::asset('assets/app/media/img/logos/logo-2.png')); ?>" style="width: 50%;"/>
							</a>
							<?php endif; ?>

						</div>
						<div class="m-stack__item m-stack__item--middle m-brand__tools">

							<!-- begin::Responsive Header Menu Toggler-->
							<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
								<span></span>
							</a>
							<!-- end::Responsive Header Menu Toggler-->
							<!-- begin::Topbar Toggler-->
							<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
								<i class="flaticon-more"></i>
							</a>
							<!--end::Topbar Toggler-->
						</div>
					</div>
				</div>
				<!-- end::Brand -->
				<!-- begin::Topbar -->

				<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
					<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
						<div class="m-stack__item m-topbar__nav-wrapper">
							<ul class="m-topbar__nav m-nav m-nav--inline">
								<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
									<a href="#" class="m-nav__link m-dropdown__toggle">
										<span class="m-topbar__userpic m--hide">
											<img src="<?php echo e(URL::asset('assets/images/user')); ?>/<?php echo e(Sentinel::getUser()->profile); ?>" class="m--img-rounded m--marginless m--img-centered" alt=""/>
										</span>
										<span class="m-topbar__welcome">
											Hello,&nbsp;
										</span>
										<span class="m-topbar__username">
											<?php echo e(Sentinel::getUser()->first_name); ?>


										</span>
										<?php  $sesion = Session::get('adminlogin', 'default')?>
										<?php if( $sesion == 1): ?>
											<div class="fixed" style=" position: fixed;top: 40px;left: 100px;z-index: 3;">
												<a href="<?php echo e(url('back-to-admin')); ?>"  class="btn btn-danger">
													Back to admin
												</a>
											</div>
										<?php endif; ?>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__header m--align-center" style="background: url(<?php echo e(URL::asset('assets/app/media/img/misc/user_profile_bg.jpg')); ?>); background-size: cover;">
												<div class="m-card-user m-card-user--skin-dark">
													<div class="m-card-user__pic">
														<img src="<?php echo e(URL::asset('assets/images/user')); ?>/<?php echo e(Sentinel::getUser()->profile); ?>" class="m--img-rounded m--marginless" style="width: 60px;height:60px; " alt=""/>
													</div>
													<div class="m-card-user__details">
														<span class="m-card-user__name m--font-weight-500">
															<?php echo e(Sentinel::getUser()->first_name); ?>

														</span>
														<a href="" class="m-card-user__email m--font-weight-300 m-link">
															<?php echo e(Sentinel::getUser()->email); ?>

														</a>

													</div>
												</div>
											</div>
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav m-nav--skin-light">
														<li class="m-nav__section m--hide">
															<span class="m-nav__section-text">
																Section
															</span>
														</li>
														<?php if(Sentinel::getUser()->roles()->first()->slug == '2'): ?>
														<li class="m-nav__item">
															<a href="<?php echo e(url('user/profile')); ?>" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-profile-1"></i>
																<span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			My Profile
																		</span>

																	</span>
																</span>
															</a>
															</li>
															<li class="m-nav__item">

															<a  class="m-nav__link">
																<i class="m-nav__link-icon fa fa-inr"></i>
																<span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			Wallet balance : <?php echo e(Sentinel::getUser()->wallet_balance); ?>

																		</span>

																	</span>
																</span>
															</a>

														</li>
															<li class="m-nav__item">

																<a href="<?php echo e(url('history')); ?>" class="m-nav__link">
																	<i class="m-nav__link-icon flaticon-signs-1"></i>
																	<span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			Booking history
																		</span>

																	</span>
																</span>
																</a>

															</li>
														<?php else: ?>
														<li class="m-nav__item">
															<a href="<?php echo e(url('supplier/profile')); ?>" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-profile-1"></i>
																<span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			My Profile
																		</span>
																		
																	</span>
																</span>
															</a>
														</li>
															<li class="m-nav__item">
																<a href="<?php echo e(route('hello')); ?>" class="m-nav__link">
																	<i class="m-nav__link-icon flaticon-signs-1"></i>
																	<span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			Booking history
																		</span>

																	</span>
																</span>
																</a>
															</li>
														<?php endif; ?>
														
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<form id="form-submit" method="post" action="<?php echo url('/');?>/logout">
																<?php echo e(csrf_field()); ?>

																<a href="#" onclick="document.getElementById('form-submit').submit()" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
																	Logout
																</a>
															</form>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
				<!-- end::Topbar -->
			</div>
		</div>
	</div>
	<div class="m-header__bottom">
		<div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
			<div class="m-stack m-stack--ver m-stack--desktop">
				<!-- begin::Horizontal Menu -->
				<div class="m-stack__item m-stack__item--middle m-stack__item--fluid">

					<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

					<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">
						<ul id="header_menu" class=" nav nav-tabs m-tabs m-tabs-line m-menu__nav  m-menu__nav--submenu-arrow " role="tablist">


							<?php if(Sentinel::getUser()->roles()->first()->slug == '2'): ?>


								<?php if(Request::path() == 'user/dashboard'): ?>
									<li class="m-menu__item  m-menu__item--active"  aria-haspopup="true">
										<a class="m-menu__link " data-toggle="tab" href="#m_tabs_7_0" role="tab" aria-expanded="false">
											<span class="m-menu__item-here"></span>
											<span class="m-menu__link-text">
										Dashboard
									</span>
										</a>
									</li>
									
										
											
											
										
									
										
									


									
										
											
											
										
										
										
									
									
										
											
											
										
											
										
									

								<?php else: ?>
									<li class="m-menu__item  m-menu__item--active"  aria-haspopup="true">
										<a  href="<?php echo e(url('user/dashboard')); ?>" class="m-menu__link ">
											<span class="m-menu__item-here"></span>
											<span class="m-menu__link-text">
										Dashboard
									</span>
										</a>
									</li>


								<?php endif; ?>
							<?php else: ?>
								<li class="m-menu__item  <?php echo Request::path() == 'supplier/dashboard'? 'm-menu__item--active':'' ?>"  aria-haspopup="true">
									<a  href="<?php echo e(url('supplier/dashboard')); ?>" class="m-menu__link ">
										<span class="m-menu__item-here"></span>
										<span class="m-menu__link-text">
										Dashboard
									</span>
									</a>
								</li>
								<li class="m-menu__item  <?php echo Request::path() == 'supplier-FlightOneWay'? 'm-menu__item--active':'' ?>"  aria-haspopup="true">
									<a class="m-menu__link "  href="<?php echo e(url('supplier-FlightOneWay')); ?>" >
										<span class="m-menu__item-here"></span>
										<span class="m-menu__link-text">
										One way
									</span>
									</a>
								</li>


								<li class="m-menu__item   <?php echo Request::path() == 'supplier-FlightRoundTrip'? 'm-menu__item--active':'' ?>"  aria-haspopup="true">
									<a class="m-menu__link"  href="<?php echo e(url('supplier-FlightRoundTrip')); ?>" >
										<span class="m-menu__item-here"></span>
										<span class="m-menu__link-text">
										Round trip
										</span>
									</a>
								</li>
								<li class="m-menu__item   <?php echo Request::path() == 'supplier-FlightMultiCity'? 'm-menu__item--active':'' ?>"  aria-haspopup="true">
									<a class="m-menu__link"  href="<?php echo e(url('supplier-FlightMultiCity')); ?>" >
										<span class="m-menu__item-here"></span>
										<span class="m-menu__link-text">
										Multi city
											</span>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>

				<!-- end::Horizontal Menu -->
				<!--begin::Search-->
				<div class="m-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-light m-header-search m-header-search--expandable m-header-search--skin-" id="m_quicksearch" data-search-type="default">
					<!--begin::Search Form -->
					<div >

						<?php if(Sentinel::getUser()->roles()->first()->slug == '2'): ?>
							<label class="m--font-light m--block-inline">

										Wallet balance : <i class="la la-inr m--block-inline"></i>  <?php echo e(Sentinel::getUser()->wallet_balance); ?>

							</label>

						<?php endif; ?>
					</div>
					<!--end::Search Form -->
					<!--begin::Search Results -->
					<div class="m-dropdown__wrapper">
						<div class="m-dropdown__arrow m-dropdown__arrow--center"></div>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-max-height="300" data-mobile-max-height="200">
									<div class="m-dropdown__content m-list-search m-list-search--skin-light"></div>
								</div>
							</div>
						</div>
					</div>
					<!--end::Search Results -->
				</div>

				<!--end::Search-->
			</div>
		</div>

	</div>

</header>


<!-- end::Header -->