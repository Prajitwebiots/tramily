<?php $__env->startSection('title'); ?> Supplier | One Way Flight <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
	<style type="text/css">
		.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
			border-color: #5767de!important;
			color: #fff!important;
			background-color: #5767de!important;
		}
	</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
	<!-- begin::Body -->
	<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
		<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
	<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title m-subheader__title--separator">
						One way import
					</h3>
					<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
						<li class="m-nav__item m-nav__item--home">
							<a href="#" class="m-nav__link m-nav__link--icon">
								<i class="m-nav__link-icon la la-home"></i>
							</a>
						</li>
						<li class="m-nav__separator">
							-
						</li>
						<li class="m-nav__item">
							<a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                One way import
                            </span>
							</a>
						</li>
					</ul>
				</div>

			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<?php if(session('error')): ?><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
			<?php if(session('success')): ?><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
			<div class="m-portlet m-portlet--mobile">
				<!--begin::Form-->
				<form  action="<?php echo e(url('one_way_import')); ?>" method="post" class="m-form m-form--fit m-form--label-align-right row" enctype="multipart/form-data">
					<?php echo e(csrf_field()); ?>

						<div class="form-group m-form__group col-md-12 m--align-center">
							<a  href="<?php echo e(url('assets/file/OneWays.csv')); ?>">
							Click here to download sample csv file.
							</a>
							<br><br>
							<h5 for="exampleInputEmail1">
								File
							</h5>
							<div></div>
								<input type="file" name="file" >

							<?php if($errors->has('file')): ?>
								<span class="help-block text-danger">
									<br>
									<strong><?php echo e($errors->first('file')); ?></strong>
								</span>
							<?php endif; ?>
						</div>
						<div class="form-group m-form__group col-md-12 m--align-center">

							<button type="submit" class="btn btn-primary">
								Submit
							</button>
						</div>
				</form>
				<!--end::Form-->
				<div class="m--padding-20">
				<?php if(count($data) >= 1): ?>
					<!--begin: Datatable -->
					<table class="m-datatable " id="html_table" width="100%">
						<thead>
						<tr>
							<th>sr.no</th>
							<th>Flight name</th>
							<th>Flight number</th>
							<th>flight source</th>
							<th>flight_destination</th>
							<th>flight_departure</th>
							<th>flight_departure_time</th>
							<th>flight_arrival</th>
							<th>flight_price</th>
							<th>flight_pnr_no</th>
							<th>flight_via</th>
							<th>sold</th>
							<th>seat</th>

						</tr>
						</thead>
						<tbody>
						<?php $i = 1;$city[] = ""; ?>
						<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<td><?php echo e($i++); ?></td>
								<td><?php echo e($list['flight_name']); ?></td>
								<td><?php echo e($list['flight_number']); ?></td>
								<td><?php echo e($list['flight_source']); ?></td>
								<td><?php echo e($list['flight_destination']); ?></td>
								<td><?php echo e($list['flight_departure']); ?></td>
								<td><?php echo e($list['flight_departure_time']); ?></td>
								<td><?php echo e($list['flight_arrival']); ?></td>
								<td><?php echo e($list['flight_price']); ?></td>
								<td><?php echo e($list['flight_pnr_no']); ?></td>
								<td><?php echo e($list['flight_via']); ?></td>
								<td><?php echo e($list['sold']); ?></td>
								<td><?php echo e($list['seat']); ?></td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</tbody>
					</table>
					<!--end: Datatable -->

						<form  action="<?php echo e(url('onewaysave')); ?>" method="post" class="m-form m-form--fit m-form--label-align-right row" enctype="multipart/form-data">
							<?php echo e(csrf_field()); ?>

							<div class="form-group m-form__group col-md-12 m--align-center m--padding-bottom-50">
								<input type="hidden" name="onewayfile" value="<?php echo e($name); ?>" >
								<button type="submit" class="btn btn-primary">
									Save data
								</button>
							</div>
						</form>
				<?php endif; ?>
			</div></div>
		</div>
			</div>
		</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
	<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>