<!-- head -->
<?php $__env->startSection('title'); ?> 
   Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?php echo e(URL::asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" /> 
     <link href="<?php echo e(URL::asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <style>
        .team-modal-main-div{
            padding-right: 30em;
        }
    </style>
<?php $__env->stopSection(); ?>
    

<?php $__env->startSection('content'); ?>

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="min-height: 1489px;">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Users Bets</h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo e(url('admin/dashboard')); ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span class="active">Users Bets</span>
            </li>

           <!--  <span class="set-right"><a href="<?php echo e(route('admin-team.create')); ?>" class="btn btn-primary "  >Add New</a></span> -->
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <div class="row">

        </div>
        <br>
        <?php if(session('error')): ?><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
        <?php if(session('success')): ?><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
        <br>
        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Users Bets</span>
                        </div>
                        <div class="tools"></div>
                    </div>
                    <div class="portlet-body">
                        <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>User Name</th>
                                <th>Bet Details</th>
                                <th>Team Details</th>
                                <th>Type</th>
                                <th>Bet On</th>
                                <th>Odd</th>
                                <th>Stake</th>
                                <th>Win</th>
                                <th>Date / Time</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody><?php $i = 1; ?>
                            <?php $__currentLoopData = $bet; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bets): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($i++); ?></td>
                                <td><?php echo e($bets->user->first_name); ?></td>
                                <td><?php echo e($bets->event->master->event_name); ?></td>
                                <td><?php echo e($bets->team->name); ?></td>
                                 
                                 <?php if($bets->type == 0): ?>
                                <td><span class="label label-sm label-success"> Single </span></td>
                                 <?php else: ?>
                                <td><span class="label label-sm label-success"> Multiple </span></td>
                                 <?php endif; ?>
                                 
                                 <?php if($bets->bet_on == 0): ?>
                                <td><span class="label label-sm label-warning"> Odds2 </span></td>
                                 <?php elseif($bets->bet_on == 1): ?>
                                <td><span class="label label-sm label-warning"> Draw </span></td>
                                 <?php else: ?>
                                <td><span class="label label-sm label-warning"> Odds2 </span></td>
                                 <?php endif; ?>
                                
                                <td><?php echo e($bets->odds); ?></td>
                                <td></td>
                                
                                 <?php if($bets->win == 0): ?>
                                <td><span class="label label-sm label-success"> Win </span></td>
                                 <?php else: ?>
                                <td><span class="label label-sm label-danger"> Loss </span></td>
                                 <?php endif; ?>
                                <td><?php echo e($bets->date_time); ?></td>
                              
                                 <?php if($bets->status == 0): ?>
                                <td><span class="label label-sm label-success"> Completed </span></td>
                                 <?php else: ?>
                                <td><span class="label label-sm label-info"> Pending </span></td>
                                 <?php endif; ?>

                            </tr>
                            
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('script'); ?>
     <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-buttons.min.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
      
    </script>
    <?php $__env->stopSection(); ?>
   
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>