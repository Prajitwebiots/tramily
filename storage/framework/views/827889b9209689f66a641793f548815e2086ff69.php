<?php $__env->startSection('title'); ?> Admin |  Edit Users <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Edit User
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Edit User
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        
        <div class="row">
            <div class="col-md-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                Edit User
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" action="<?php echo e(url('updateUserProfile',$user->id)); ?>" method="post" >
                        <?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div> <br><?php endif; ?>
                        <?php if(session('success')): ?><br><div class="alert alert-success"><?php echo e(session('success')); ?></div> <br><?php endif; ?>
                        <div class="m-portlet__body">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Agency Name
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->agency_name); ?>" name="agency_name"  >
                                        <?php if($errors->has('agency_name')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('agency_name')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Concerned Person Name
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->first_name); ?>" name="full_name"  >
                                        <?php if($errors->has('full_name')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('full_name')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Agency Address
                                        </label>
                                        
                                        <textarea class="form-control m-input m-input--solid" value="<?php echo e(old('agency_address')); ?>" id="exampleTextarea" name="agency_address" autocomplete="off" placeholder="Agency Address" rows="6"><?php echo e($user->agency_address); ?></textarea>
                                        <?php if($errors->has('agency_address')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('agency_address')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Mobile No
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->mobile); ?>" name="mobile"  >
                                        <?php if($errors->has('mobile')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('mobile')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    
                                    
                                    
                                    
                                </div>
                                <div class="col-md-6">
                                    
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            City
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->city); ?>" name="city"  >
                                        <?php if($errors->has('city')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('city')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            State
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->state); ?>" name="state"  >
                                        <?php if($errors->has('state')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('state')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Country
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->country); ?>" name="country"  >
                                        <?php if($errors->has('country')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('country')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Pincode
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->pincode); ?>" name="pincode"  >
                                        <?php if($errors->has('pincode')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('pincode')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Office No
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->office_no); ?>" name="office_no"  >
                                        <?php if($errors->has('office_no')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('office_no')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="m--padding-15"></div><strong style="font-weight: 600;margin-left: 28px;">Agency GST Details</strong><div class="m--padding-15"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Name
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->gst_name); ?>" name="gst_full_name"  >
                                        <?php if($errors->has('gst_full_name')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('gst_full_name')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            State
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->gst_state); ?>" name="gst_state"  >
                                        <?php if($errors->has('gst_state')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('gst_state')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            GSTIN No
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->gst_no); ?>" name="gst_no"  >
                                        <?php if($errors->has('gst_no')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('gst_no')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button type="submit" class="btn btn-primary">
                                Submit
                                </button>
                                <button type="reset" class="btn btn-secondary">
                                Cancel
                                </button>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                
                
                
            </div>
            
            <!--end::Portlet-->
            
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>