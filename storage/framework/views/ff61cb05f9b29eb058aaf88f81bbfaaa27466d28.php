<?php $__env->startSection('title'); ?>Admin | City <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                City
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                City
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <?php if(session('error')): ?><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
        <?php if(session('success')): ?><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
        <div class="m-portlet m-portlet--mobile">
        
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                            <span>
                                                                <i class="la la-search"></i>
                                                            </span>
                                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <a href="<?php echo e(route('admin-city.create')); ?>" class="btn m-btn--pill    btn-primary" >Add New</a>
                        </h3>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                             </div>
                        </div>
                
                <!--begin: Datatable -->
                <table class="m-datatable" id="html_table" width="100%">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            
                            <th>Country Name</th>
                          <!--   <th>State Name</th> -->
                            <th>City Name</th>
                            <th>Status</th>
                            <th>Actions</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php $__currentLoopData = $city; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cities): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>          
                        <tr>
                            <td><?php echo e($i++); ?></td>
                            <td><?php echo e($cities->country->country_name); ?></td>
                           <!--  <td><?php echo e($cities->state->state_name); ?></td> -->
                            <td><?php echo e($cities->city_name); ?></td>
                            <td>
                                <?php if($cities->status == 0): ?>
                                <?php echo e('<span><span class="m-badge  m-badge--danger m-badge--wide">Deactivated</span></span>'); ?>

                                <?php else: ?>
                                <?php echo e('<span><span class="m-badge  m-badge--primary m-badge--wide">Activated</span></span>'); ?>

                                <?php endif; ?>
                            </td>
                             <td>
                                 <?php echo e('<a href="'.route('admin-city.edit',$cities->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit "><i class="la la-edit"></i></a>'); ?>

                                 <?php echo e('<a href="'.url('cityIsDelete').'/'.$cities->id.'/0" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete " ><i class="la la-trash-o"></i></a>'); ?>

                            </td>
                            <td>
                                <?php if($cities->status == 0): ?>
                                <?php echo e('<a href="'.url('cityStatus').'/'.$cities->id.'/1" class="btn m-btn--pill    btn-primary" >Active</a>'); ?>

                                <?php else: ?>
                                <?php echo e('<a href="'.url('cityStatus').'/'.$cities->id.'/0" class="btn m-btn--pill    btn-danger" >Block</a>'); ?>

                                <?php endif; ?>
                            </td>
                            
                        </tr>
                        
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>