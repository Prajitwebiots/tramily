<?php $__env->startSection('title'); ?> Admin | Add City <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                City
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                City
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
     
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                   
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" action="<?php echo e(route('admin-city.store')); ?>" method="post">
                        <div class="m-portlet__body">
                            <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                            <div class="form-group m-form__group">
                                <label for="exampleInputEmail1">
                                    Country Name
                                </label>
                                <select class="form-control m-input" id="country" name="country_id">
                                    <option value="">Select Country</option>
                                    <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $countries): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($countries->id); ?>"><?php echo e($countries->country_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <?php if($errors->has('country_id')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('country_id')); ?></strong>
                                                </span>
                                            <?php endif; ?> 
                            </div>
                             <div class="form-group m-form__group">
                                <label for="exampleInputEmail1">
                                    State Name
                                </label>
                                <select class="form-control m-input" id="state" name="state_id">
                                    <option value="">Select State</option>
                                    <?php $__currentLoopData = $state; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $states): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($states->id); ?>"><?php echo e($states->state_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                
                            </div>
                            <div class="form-group m-form__group">
                                <label for="exampleInputEmail1">
                                    City Name
                                </label>
                                <input type="text" class="form-control m-input" id="exampleInputText" autocomplete="off" name="city_name"  placeholder="Enter State Name">
                                 <?php if($errors->has('city_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('city_name')); ?></strong>
                                                </span>
                                            <?php endif; ?> 
                            </div>
                            
                            
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button type="submit" class="btn btn-primary">
                                Submit
                                </button>
                              <a href="<?php echo e(url('admin-city')); ?>" class="btn btn-secondary">Cancel</a>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                
                
                
            </div>
            
            <!--end::Portlet-->
            
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>