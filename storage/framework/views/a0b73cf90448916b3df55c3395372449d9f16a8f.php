<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="">
             <!--    <img src="<?php echo e(url('/')); ?>/assets/layouts/layout4/img/logo-light.png" alt="logo" class="logo-default" /> --> <h2><b>BETTING</b></h2></a>
            <div class="menu-toggler sidebar-toggler">
            </div>
        </div>
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
         <?php
       $slug = Sentinel::getUser()->roles()->first()->slug;
     ?>
        <div class="page-top">
            <form id="form-submit" method="post" action="<?php echo url('/');?>/logout">
                <?php echo e(csrf_field()); ?>

            <div class="top-menu">
            <?php if($slug=='superadmin'): ?>
            <!--   Here here -->
                <ul class="nav navbar-nav pull-rig`ht">
                    <li class="separator hide"> </li>
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username username-hide-on-mobile"> <?php echo e(Sentinel::getUser()->first_name); ?> </span>
                            <?php if(Sentinel::getUser()->profile): ?>
                                <img alt="" class="img-circle" src="<?php echo e(url('/')); ?>/assets/images/profile/<?php echo e(Sentinel::getUser()->profile); ?>" /> </a>
                            <?php else: ?>
                                <img alt="" class="img-circle" src="<?php echo e(url('/')); ?>/assets/images/profile/default.jpg" /> </a>
                            <?php endif; ?>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="<?php echo e(url('superadmin-profile')); ?>">
                                    <i class="icon-user"></i> My Profile </a>
                            </li> 
                            <li>
                                <a href="#" onclick="document.getElementById('form-submit').submit()" >
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-quick-sidebar-toggler">
                        <a href="#" onclick="document.getElementById('form-submit').submit()"  class="dropdown-toggle">
                            <i class="icon-logout"></i>
                        </a>
                    </li>
                </ul>
            <?php elseif($slug=='admin'): ?>
                <ul class="nav navbar-nav pull-right">
                    <li class="separator hide"> </li>
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username username-hide-on-mobile"> <?php echo e(Sentinel::getUser()->first_name); ?> </span>
                            <img alt="" class="img-circle" src="../assets/images/profile/<?php echo e(Sentinel::getUser()->profile); ?>" /> </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                       <li>
                                <a href="<?php echo e(url('admin-profile')); ?>">
                                    <i class="icon-user"></i> My Profile </a>
                            </li> 
                            <li>
                                <a href="#" onclick="document.getElementById('form-submit').submit()" >
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-quick-sidebar-toggler">
                        <a href="#" onclick="document.getElementById('form-submit').submit()"  class="dropdown-toggle">
                            <i class="icon-logout"></i>
                        </a>
                    </li>
                </ul>
            <?php else: ?>
                <ul class="nav navbar-nav pull-right">
                    <li class="separator hide"> </li>
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username username-hide-on-mobile"><?php echo e(Sentinel::getUser()->first_name); ?></span>
                            <img alt="" class="img-circle" src="../assets/images/profile/<?php echo e(Sentinel::getUser()->profile); ?>" /> </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="<?php echo e(url('profile')); ?>">
                                    <i class="icon-user"></i> My Profile </a>
                            </li>
                            <li>
                                <a href="#" onclick="document.getElementById('form-submit').submit()" >
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-quick-sidebar-toggler">
                        <a href="#" onclick="document.getElementById('form-submit').submit()"  class="dropdown-toggle">
                            <i class="icon-logout"></i>
                        </a>
                    </li>
                </ul>
            <?php endif; ?>

            </div>
            </form>

        </div>
        <!-- END PAGE TOP -->
    </div>

    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->