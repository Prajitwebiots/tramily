  

<?php $__env->startSection('content'); ?>
 <div class="center-content">
        <div class="row">
            <div class="col-12  col-md-4 col-lg-3 nopadding">
                <div class="site-sidebar">
               <?php echo $__env->make('frontend.layouts.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
<div class="col-12  col-md-8 col-lg-9 nopadding">
                <div class="inner-page">
                    <div class="content">
                    	<div class="title-container">
                            <h1>BETTING HISTORY</h1>
                        </div>
                    	<table class="table">
    <thead>
      <tr>
        <th>Type</th>
        <!-- <th>Status</th> -->
        <th>Date / Time</th>
        <th>Bet Details</th>
        <th>Bet On</th>
        <th>Odds</th>
        <th>Stake</th>
        <th>Win</th>
        

      </tr>
    </thead>
    <tbody>
      <tr>
      	<?php $__currentLoopData = $bet; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bets): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        
        <?php if($bets->type == 0): ?>
        <td>Single</td>
        <?php else: ?>
        <td>Multiple</td>
        <?php endif; ?>

      


        <td><?php echo e($bets->date_time); ?></td>
        <td><?php echo e($bets->team->name); ?></td>

        <?php if($bets->bet_on == 0): ?>
        <td>odds1</td>
        <?php elseif($bets->bet_on == 1): ?>
        <td>Draw</td>
        <?php else: ?>
        <td>Odds2</td>
        <?php endif; ?>

        <td><?php echo e($bets->odds); ?></td>
        <td></td>
        <td></td>
        
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </tr>
     
    </tbody>
  </table>
                    </div>
                </div>
 </div>

<?php $__env->stopSection(); ?>			
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>