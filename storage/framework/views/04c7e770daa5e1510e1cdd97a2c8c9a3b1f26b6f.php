<!-- head -->
<?php $__env->startSection('title'); ?>
    Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
 
<?php $__env->stopSection(); ?>
  
<?php $__env->startSection('content'); ?>
  <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
      <div class="page-content" style="min-height: 1603px;">
          <!-- BEGIN PAGE HEAD-->
          <div class="page-head">
              <!-- BEGIN PAGE TITLE -->
              <div class="page-title">
                  <h1>Insert Odd

                  </h1>
              </div>
              <!-- END PAGE TITLE -->

          </div>
          <!-- END PAGE HEAD-->
          <!-- BEGIN PAGE BREADCRUMB -->
          <ul class="page-breadcrumb breadcrumb">
              <li>
                  <a href="index.html">Home</a>
                  <i class="fa fa-circle"></i>
              </li>
              <li>
                  <span class="active">Insert Odd</span>
              </li>
          </ul>
          <!-- END PAGE BREADCRUMB -->
          <!-- BEGIN PAGE BASE CONTENT -->
          <div class="row">
              <div class="col-md-10 ">
                  <!-- BEGIN SAMPLE FORM PORTLET-->
                  <div class="portlet light bordered">
                      <div class="portlet-title">
                          <div class="caption font-blue-sunglo">
                              <i class="icon-settings font-blue-sunglo"></i>
                              <span class="caption-subject bold uppercase"> Insert Odd for <?php echo e($event->master->event_name); ?></span>
                          </div>

                      </div>

                      <?php if($errors->any()): ?>
                      <div class="alert alert-danger">
                          <ul class="alert alert-danger">
                          <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <li> <?php echo e($error); ?></li><br>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </ul>
                      </div>
                      <?php endif; ?>

                      <?php if(session('success')): ?>
                      <div class="alert alert-success">
                          <?php echo e(session('success')); ?>

                      </div>
                      <?php endif; ?>


                      <div class="portlet-body form">
                         <?php echo e(Form::model($event, array('route' => array('admin-odd.update', $event->id), 'method' => 'PUT'))); ?>

                              <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                              <div class="form-body">
                              <?php $j=1; ?>
                              <?php $__currentLoopData = $odds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $odd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                               <div class="row" id="removeThis<?php echo $j; ?>">
                               <div><i class="fa fa-times" aria-hidden="true" style="color:red; cursor: pointer;" onclick="remove(<?php echo $j; ?>);"> Remove </i></div> 
                                  <div class="form-group <?php echo e($errors->has('odd_master') ? ' has-error' : ''); ?> col-md-4">
                                      <label>Odd Master</label> 
                                      <select name="odd[<?php echo $j; ?>][odd_master]" id="odd_master"  class="form-control">
                                        <option value="">Select Odd Master</option>  
                                        <?php $__currentLoopData = $odd_masters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $odd_master): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($odd_master->id); ?>" <?php if($odd_master->id==$odd->odd_id): ?> selected <?php endif; ?>><?php echo e($odd_master->odd_title); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      </select>
                                      <?php if($errors->has('odd_master')): ?>
                                         <span class="help-block">
                                             <strong><?php echo e($errors->first('odd_master')); ?></strong>
                                         </span>
                                     <?php endif; ?>
                                  </div>
                                 
                                    <div class="form-group <?php echo e($errors->has('title') ? ' has-error' : ''); ?> col-md-4">
                                        <label>Odd Title</label>
                                        <input type="text" class="form-control" placeholder="Enter Odd title" name="odd[<?php echo $j; ?>][title]" value="<?php echo e($odd->name); ?>">
                                        <?php if($errors->has('title')): ?>
                                           <span class="help-block">
                                               <strong><?php echo e($errors->first('title')); ?></strong>
                                           </span>
                                       <?php endif; ?>
                                    </div>
                                    <div class="form-group <?php echo e($errors->has('odd') ? ' has-error' : ''); ?> col-md-4">
                                        <label>Odd</label>
                                        <input type="text" class="form-control" placeholder="Enter Odd " name="odd[<?php echo $j; ?>][odd_val]" value="<?php echo e($odd->odd); ?>">
                                        <?php if($errors->has('odd')): ?>
                                           <span class="help-block">
                                               <strong><?php echo e($errors->first('odd')); ?></strong>
                                           </span>
                                       <?php endif; ?>
                                    </div>
                                  </div>
                                    <?php $j++ ; ?>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                  <div id="new-period">

                                  </div>
                                  <div class="form-group" style="float:right;">
                                      <button type="button" class="btn btn-info" onclick="addOdds();">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Odd
                                      </button>
                                  </div>
                              </div>
                              <div class="form-actions">
                                  <button type="submit" class="btn blue">Submit</button>
                                   <a  class="btn default" href="<?php echo e(route('admin-team.index')); ?>">Cancel</a>
                              </div>
                          </form>
                      </div>
                  </div>

              </div>

          </div>

          <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
  </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
  <script>
   var i = 1000;
    function addOdds()
    {
        var odd_masters='';
        var pausecontent = new Array();
        <?php foreach($odd_masters as $key => $val){ ?>
           odd_masters+=' <option value="<?php echo $val['id'] ?>"><?php echo $val['odd_title'] ?></option>';
        <?php } ?>
      
       var odd='';
         odd +='<div class="row" id="removeThis'+i+'" ><div><i class="fa fa-times" aria-hidden="true" style="color:red; cursor: pointer;" onclick="remove('+i+');"> Remove </i></div> <div class="form-group col-md-4"><label>Odd Master</label><select name="odd['+i+'][odd_master]" id="odd_master" class="form-control">  <option value="">Select Odd Master</option>'+odd_masters+'</select></div><div class="form-group  col-md-4">   <label>Odd Title</label>   <input type="text" class="form-control" placeholder="Enter Odd title" name="odd['+i+'][title]" ></div> <div class="form-group  col-md-4"> <label>Odd</label> <input type="text" class="form-control" placeholder="Enter Odd " name="odd['+i+'][odd_val]" >  </div></div>';
        $('#new-period').append(odd);
        i = i+1;
    }
     function remove($id){
        $('#removeThis'+$id).remove();
      }
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>