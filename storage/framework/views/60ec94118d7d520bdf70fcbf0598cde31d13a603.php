<?php $__env->startSection('title'); ?> Tramily | A Travel Family <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<link href="<?php echo e(URL::asset('assets/pages/css/login-5.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<style>
.m-login.m-login--1 .m-login__wrapper {
overflow: hidden;
padding: 0% 2rem 2rem 2rem !important;
}
.m-login.m-login--1 .m-login__aside {
width: 50%;
}
</style>
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--singin" id="m_login">
        <!-- <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside"> -->
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="m-stack m-stack--hor m-stack--desktop">
                <div class="m-stack__item m-stack__item--fluid">
                    <div class="m-login__wrapper">
                        <div class="m--padding-15"></div>
                        <div class="m-login__logo">
                            <a href="#">
                                <img src="<?php echo e(URL::asset('assets/app/media/img/logos/logo-2.png')); ?>"  style="width: 30%;">
                            </a>
                        </div>
                        
                       
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                Supplier Sign Up
                                </h3>
                                <div class="m-login__desc">
                                    Enter your details to create your account:
                                </div>
                            </div>
                            <?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
                            <?php if(session('success')): ?><br><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>

                            <form class="m-login__form m-form" action="<?php echo e(url('/supplierRegister')); ?>" method="post" id="supp">
                               <!--  <div class="m-alert m-alert--outline alert alert-danger alert-dismissible errorsignup" role="alert">            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>           <span></span></div> -->
                               <!-- <div class="m-alert m-alert--outline alert alert-danger alert-dismissible errorsignuphide" style="display:none"><ul class="errorsignup" ></ul></div> -->
                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                <div class="row">
                                    <span><br><b>Personal Details</b><br><br></span>
                                    <div class="col-xl-12">
                                        

                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" value="<?php echo e(old('agency_name')); ?>" placeholder="Agency Name" name="agency_name" autocomplete="off">
                                          <?php if($errors->has('agency_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('agency_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>


                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" value="<?php echo e(old('full_name')); ?>" placeholder="Concerned Person Name" name="full_name" autocomplete="off">
                                        <?php if($errors->has('full_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('full_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                         <div class="form-group m-form__group">
                                            <textarea class="form-control m-input m-input--solid" value="<?php echo e(old('agency_address')); ?>" id="exampleTextarea" name="agency_address" autocomplete="off" placeholder="Agency Address" rows="3"></textarea>
                                        <?php if($errors->has('agency_address')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('agency_address')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text"  value="<?php echo e(old('email')); ?>" placeholder="Email" name="email" autocomplete="off">
                                         <?php if($errors->has('email')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('email')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                         <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text"  value="<?php echo e(old('city')); ?>" placeholder="City" name="city" autocomplete="off">
                                        <?php if($errors->has('city')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('city')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text"  value="<?php echo e(old('state')); ?>" placeholder="State" name="state" autocomplete="off">
                                         <?php if($errors->has('state')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('state')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" value="<?php echo e(old('country')); ?>" placeholder="Country" name="country" autocomplete="off">
                                         <?php if($errors->has('country')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('country')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" value="<?php echo e(old('pincode')); ?>"  placeholder="Pin Code" name="pincode" autocomplete="off">
                                         <?php if($errors->has('pincode')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('pincode')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="number" value="<?php echo e(old('mobile')); ?>" placeholder="Mobile No" name="mobile" autocomplete="off">
                                        <?php if($errors->has('mobile')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('mobile')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="number" value="<?php echo e(old('office_no')); ?>" placeholder="Office No" name="office_no" autocomplete="off">
                                        <?php if($errors->has('office_no')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('office_no')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="password" placeholder="Password" name="password" autocomplete="off">
                                        <?php if($errors->has('password')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('password')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="password" placeholder="Confirm Password" name="confirm_password" autocomplete="off">
                                        <?php if($errors->has('confirm_password')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('confirm_password')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        
                                    </div>


                                    <span><br><b>Agency GST Details</b><br><br></span>
                                    
                                    
                                     <div class="col-xl-12">
                                  <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Name" value="<?php echo e(old('gst_full_name')); ?>" name="gst_full_name" autocomplete="off">
                                        <?php if($errors->has('gst_full_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('gst_full_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                
                                     <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" value="<?php echo e(old('gst_state')); ?>" placeholder="State Name" name="gst_state" autocomplete="off">
                                        <?php if($errors->has('gst_state')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('gst_state')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                          <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" value="<?php echo e(old('gst_no')); ?>" placeholder="GSTIN No" name="gst_no" autocomplete="off">
                                         <?php if($errors->has('gst_no')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('gst_no')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                     </div>
                                   
                                </div>
                                <div class="row form-group m-form__group m-login__form-sub">
                                    <div class="col m--align-left">
                                        <label class="m-checkbox m-checkbox--focus">
                                            <input type="checkbox" name="agree">
                                            I Agree the
                                            <a href="#" class="m-link m-link--focus" required>
                                                terms and conditions
                                            </a>
                                            .
                                            <span></span>
                                        </label>
                                        <span class="m-form__help"></span>
                                    </div>
                                </div>
                                <div class="m-login__form-action">
                                    <button id="m_login_signup_submitt" type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                    Sign Up
                                    </button>
                                    <a href="/supplier/login" id="m_login_signup_cancel1" class="btn btn-outline-focus  m-btn m-btn--pill m-btn--custom">
                                  Cancel                              
                              </a>
                                </div>
                            </form>
                        </div>
                      
                    </div>
                </div>
                
            </div>
            </div>
    
     <!--    </div> -->
     <!--    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1  m-login__content" style="background-image: url(<?php echo e(URL::asset('assets/app/media/img/bg/bg-4.jpg')); ?>)">
            <div class="m-grid__item m-grid__item--middle">
                <h3 class="m-login__welcome">
                WELCOME TO TRAMILY
                </h3>
                <p class="m-login__msg">
                    The world is a book and those who do not travel read only one page
                    <br>
                    
                </p>
            </div>
        </div> -->

</div>
<!-- end:: Page -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/snippets/pages/user/login.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/snippets/pages/user/select2.js')); ?>" type="text/javascript"></script>

<?php if(@$register): ?>
<script type="text/javascript">
$(document).ready(function(){
$("#m_login_signup").trigger('click');
});
</script>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>