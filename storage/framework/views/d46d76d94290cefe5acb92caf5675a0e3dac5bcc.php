<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Tramily | A Travel Family </title>
     <style type="text/css">
        * {color: #707070;font-family: verdana;font-size: 14px;margin: 0 auto;padding: 0;vertical-align: middle;line-height: 150%;}
        a {text-decoration: none;}
        a:hover {text-decoration: underline;}
    </style>
</head>
<body style="margin:0 auto;padding:0; background-color:#fff;">
<table style="width:730px;  margin: 0 auto;" cellspacing="3" cellpadding="0">
    <tr>
        <td>
            <div style="font-size:12px;color: #707070;padding: 15px 3%;width: 93.8%;background-color:#262734;float:left;">
                <div style='float:left;text-align:left;display: inline-table;'>
                    <img src="<?php echo asset('assets/app/media/img/logos/logo-2.png'); ?>" alt="Tour Tickets" style="width: 100px;height: 100px" />
                </div>
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <div style="font-size:12px;color: #707070;border: 1px solid #E5E5E5;padding: 6%;width: 87.7%;background-color:#FFF;float:left;font-family: verdana;">
                 <h1 style="color:#000000c7;font-size:  20px;margin-bottom: 0px;">Multicity Flight Search  </h1>
                <br>
                <div style="border-bottom: 2px solid #272735;margin-bottom:  30px;"></div>
                <h2 style="color:#000000c7">
                    Dear Admin <br /><br />
</h2>

                  <p style="color:#000000c7">  User find ticket but not found !<br /><br>  </p>

                   <b style="color:#000000c7">Client Details:</b>
                    <br>
                    <br>
                <table style="width: 100%; margin-left: 5%; margin-bottom: 5%">
                    <tr>
                        <td style="width: 25%">Name</td> <td>: <?php echo $user->first_name.' '.$user->last_name; ?></td>
                    </tr>
                    <tr>
                        <td>Email</td> <td>: <?php echo $user->email; ?></td>
                    </tr>
                    <tr>
                        <td>Mobile No: </td> <td>: <?php echo $user->mobile; ?></td>
                    </tr>
                    <tr>
                        <td>Company Name: </td> <td>: <?php echo $user->agency_name; ?></td>
                    </tr>
                </table>
                 <b style="color:#000000c7">Search For:</b>
                    <br>
                    <br>
                <table style="width: 100%; margin-left: 5%; margin-bottom: 5%">

                    <tr>
                        <td style="width: 25%">Flying Source</td> <td>: <?php echo e(isset($flightsource) ? $flightsource : ' '); ?></td>
                    </tr>
                    <tr>
                        <td>Flying Destination: </td> <td>: <?php echo e(isset($flightdestination) ? $flightdestination : ' '); ?></td>
                    </tr>
                    <tr>
                        <td>Departure: </td> <td>: <?php echo e(isset($request['departure']) ? $request['departure'] : ' '); ?></td>
                    </tr>
                    <tr><td></td></tr>


                    <tr>
                        <td>Flying Source 2</td> <td>: <?php echo e(isset($flightsource2) ? $flightsource2 : ' '); ?></td>
                    </tr>
                    <tr>
                        <td>Flying Destination 2</td> <td>: <?php echo e(isset($flightdestination2) ? $flightdestination2 : ""); ?></td>
                    </tr>
                    <tr>
                        <td>Departure: </td> <td>: <?php echo $request['departure2'] ?></td>
                    </tr>
                    <tr><td></td></tr>

                    <tr>
                        <td>Flying Source 3</td> <td>: <?php echo e(isset($flightsource3) ? $flightsource3 : " "); ?></td>
                    </tr>
                    <tr>
                        <td>Flying Destination 3 </td> <td>: <?php echo e(isset($flightdestination3) ? $flightdestination3 : ' '); ?></td>
                    </tr>
                    <tr>
                        <td>Departure: </td> <td>: <?php echo $request['departure3'] ?></td>
                    </tr>

                    <tr>
                        <td>Adults: </td> <td>: <?php echo $request['adults'] ?></td>
                    </tr>


                </table>

                <b style="color:#000000c7">Thanks & Regards,</b><br>
                <span>Team Tramily</span>
              
            </div>
        </td>
    </tr>
    <tr>
        <!--<td style='background-color:#262734;'>-->
        <!--    <div style="font-size:12px;color: #707070;border-width: 1px; border-style: solid; border-color: #e5e5e5;padding: 15px 3%;width: 93.8%;float:left;text-align: center;">-->
        <!--        <p style='margin: 0;font-family: verdana;font-size:12px;display: block;color: #ffffff;'><?php echo 'copyright'; ?></p>-->
        <!--        <p style='display: block;margin: 0;'>-->
        <!--            <a target="_blank" href="<?php echo url(''); ?>termsconditions" style='font-family: verdana;font-size:12px;color:#707070;display: inline-table;'>Terms &amp; Conditions</a><span style="color:#707070"> | </span>-->
        <!--            <a target="_blank" href="<?php echo url(''); ?>privacypolicy" style='font-family: verdana;font-size:12px;color:#707070;display: inline-table;'>Privacy Policy</a>-->
        <!--        </p>-->
        <!--    </div>-->
        <!--</td>-->
    </tr>
</table>
</body>
</html>
