
<?php $__env->startSection('title'); ?> Admin Dashboard <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">
			Dashboard
			</h3>
		</div>
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<!--Begin::Main Portlet-->
	
	<!--End::Main Portlet-->
	<!--Begin::Main Portlet-->
	
	<!--End::Main Portlet-->
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<!--begin::Page Vendors -->
<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="<?php echo e(asset('assets/app/js/dashboard.js')); ?>" type="text/javascript"></script>
<!--end::Page Snippets -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>