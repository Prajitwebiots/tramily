<!-- head -->
<?php $__env->startSection('title'); ?>
   Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
	<!-- <link href="<?php echo e(URL::asset('backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')); ?>" rel="stylesheet" type="text/css" /> -->
	<link rel="stylesheet" href="<?php echo e(URL::asset('assets/global/plugins/ckeditor/css/samples.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(URL::asset('assets/global/plugins/ckeditor/toolbarconfigurator/lib/codemirror/neo.css')); ?>">
<?php $__env->stopSection(); ?>
	
<?php $__env->startSection('content'); ?>
	<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
	    <div class="page-content" style="min-height: 1603px;">
	        <!-- BEGIN PAGE HEAD-->
	        <div class="page-head"> 
	            <!-- BEGIN PAGE TITLE -->
	            <div class="page-title">
	                <h1>Insert Sub-Admin

	                </h1>
	            </div>
	            <!-- END PAGE TITLE -->

	        </div>
	        <!-- END PAGE HEAD-->
	        <!-- BEGIN PAGE BREADCRUMB -->
	        <ul class="page-breadcrumb breadcrumb">
	            <li>
	                <a href="index.html">Home</a>
	                <i class="fa fa-circle"></i>
	            </li>
	            <li>
	                <span class="active">Insert Sub-Admin</span>
	            </li>
	        </ul>
	        <!-- END PAGE BREADCRUMB -->
	        <!-- BEGIN PAGE BASE CONTENT -->
	        <div class="row">
	            <div class="col-md-10 ">
	                <!-- BEGIN SAMPLE FORM PORTLET-->
	                <div class="portlet light bordered">
	                    <div class="portlet-title">
	                        <div class="caption font-blue-sunglo">
	                            <i class="icon-settings font-blue-sunglo"></i>
	                            <span class="caption-subject bold uppercase"> Insert Sub-Admin</span>
	                        </div>

	                    </div>

	                    <?php if($errors->any()): ?>
	                    <div class="alert alert-danger">
	                        <ul class="class="alert alert-danger">
	                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                        <li> <?php echo e($error); ?></li><br>
	                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                        </ul>
	                    </div>
	                    <?php endif; ?>

	                    <?php if(session('success')): ?>
	                    <div class="alert alert-success">
	                        <?php echo e(session('success')); ?>

	                    </div>
	                    <?php endif; ?>


	                    <div class="portlet-body form">
	                        <form role="form" method="post" action="<?php echo e(route('sub-admin.store')); ?>" enctype="multipart/form-data">
	                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
	                            <div class="form-body">
	                                <div class="form-group <?php echo e($errors->has('firstname') ? ' has-error' : ''); ?>">
						                <label class="control-label visible-ie8 visible-ie9">Firts Name</label>
						                <div class="input-icon">
						                    <i class="fa fa-font"></i>
						                    <input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="firstname"  value="<?php echo e(old('firstname')); ?>" /> 
						                </div>
						                <?php if($errors->has('firstname')): ?>
					                       <span class="help-block">
					                           <strong><?php echo e($errors->first('firstname')); ?></strong>
					                       </span>
					                    <?php endif; ?>
						            </div>
						            <div class="form-group <?php echo e($errors->has('lastname') ? ' has-error' : ''); ?>">
						                <label class="control-label visible-ie8 visible-ie9">Last Name</label>
						                <div class="input-icon">
						                    <i class="fa fa-font"></i>
						                    <input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="lastname"   value="<?php echo e(old('lastname')); ?>" /> </div>
						                <?php if($errors->has('lastname')): ?>
					                       <span class="help-block">
					                           <strong><?php echo e($errors->first('lastname')); ?></strong>
					                       </span>
					                    <?php endif; ?>
						            </div>

						            <div class="form-group <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
						                <label class="control-label visible-ie8 visible-ie9">Email</label>
						                <div class="input-icon">
						                    <i class="fa fa-envelope"></i>
						                    <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email"  value="<?php echo e(old('email')); ?>" /> </div>
						                 <?php if($errors->has('email')): ?>
					                       <span class="help-block">
					                           <strong><?php echo e($errors->first('email')); ?></strong>
					                       </span>
					                    <?php endif; ?>
						            </div>
						            <div class="form-group <?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
						                <label class="control-label visible-ie8 visible-ie9">Password</label>
						                <div class="input-icon">
						                    <i class="fa fa-lock"></i>
						                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" /> </div>

						                <?php if($errors->has('password')): ?>
					                       <span class="help-block">
					                           <strong><?php echo e($errors->first('password')); ?></strong>
					                       </span>
					                    <?php endif; ?>
						            </div>
						            <div class="form-group <?php echo e($errors->has('rpassword') ? ' has-error' : ''); ?>">
						                <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
						                <div class="controls">
						                    <div class="input-icon">
						                        <i class="fa fa-check"></i>
						                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="rpassword" /> </div>
						                </div>
						                 <?php if($errors->has('rpassword')): ?>
					                       <span class="help-block">
					                           <strong><?php echo e($errors->first('rpassword')); ?></strong>
					                       </span>
					                    <?php endif; ?>
						            </div>
	                            </div>
	                            <div class="form-actions">
	                                <button type="submit" class="btn blue">Submit</button>
	                                <a  class="btn default" href="<?php echo e(route('sub-admin.index')); ?>">Cancel</a>
	                            </div>
	                        </form>
	                    </div>
	                </div>

	            </div>

	        </div>

	        <!-- END PAGE BASE CONTENT -->
	    </div>
	    <!-- END CONTENT BODY -->
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script src="<?php echo e(URL::asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/ckeditor/ckeditor.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/global/plugins/ckeditor/js/sample.js')); ?>"></script>
    <script>
        initSample();
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>