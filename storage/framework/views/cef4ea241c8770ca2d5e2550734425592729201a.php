<?php $__env->startSection('title'); ?> Supplier  |booking history   <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
	.quMemberue2b,.que3a,.que3b{
		display: none;
	}
	.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- begin::Body -->

<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
						Booking history
					</h3>
				</div>
			</div>
		</div>
		<div class="m-content">

			<?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div>   <br><?php endif; ?>
			<?php if(session('success')): ?>   <br><div class="alert alert-success"><?php echo e(session('success')); ?></div>   <br><?php endif; ?>

                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <h4>One Way Bookings</h4>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <table class="m-datatable" id="html_table" width="100%">
                            <thead>
                            <tr>
                                <th>Sr .no</th>
                                <th>Booking Date</th>
                                <th>Sector</th>
                                <th>Date</th>
                                <th>Flight Details</th>
                                <th>No.  of Seats</th>
                                <th>PNR</th>
                                <th>Fare</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1; ?>

                            <?php $__currentLoopData = $history_oneway; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($i++); ?></td>
                                    <td><?php echo e(date_format($key->created_at,' D d-M-Y h:i A')); ?></td>
                                    <td><?php echo e($key->oneway->flightSource->flight_source.' - '.$key->oneway->flightDestination->flight_destination.'-'.$key->oneway->flight->flight_code); ?></td>
                                    <td><?php echo e(date_format(date_create($key->oneway->flight_departure),' D d-M-Y h:i A')); ?></td>
                                    <td><?php echo e($key->oneway->flight->flight_name.'-'.$key->oneway->flight->flight_code.'-'.$key->oneway->flight_number); ?></td>
                                    <td><?php echo e($key->number_of_seat); ?></td>
                                    <td><?php echo e($key->oneway->flight_pnr_no); ?></td>
                                    <td><?php echo e('<i class="la la-inr"></i>'.$key->amount); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <h4>Round Trip Bookings</h4>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search_r">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <table class="m-datatable_r" id="html_table" width="100%">
                            <thead>
                            <tr>
                                <th>Sr .no</th>
                                <th>Booking Date</th>
                                <th>Sector</th>
                                <th>Date</th>
                                <th>Flight Details</th>
                                <th>No.  of Seats</th>
                                <th>PNR</th>
                                <th>Fare</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1; ?>

                            <?php $__currentLoopData = $history_round_trip; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($i++); ?></td>
                                    <td><?php echo e(date_format($key->created_at,' D d-M-Y h:i A')); ?></td>
                                    <td><?php echo e($key->round_trip->flightSource->flight_source.' - '.$key->round_trip->flightDestination->flight_destination.' - '.$key->round_trip->flight->flight_code); ?>

                                    <?php echo e(e('<span>-------------------------------------</span>')); ?>

                                     <?php echo e($key->round_trip->flightSourceReturn->flight_destination.' - '.$key->round_trip->flightDestinationReturn->flight_source.' - '.$key->round_trip->flightReturn->flight_code); ?>

                                    </td>
                                    <td><?php echo e(date_format(date_create($key->round_trip->flight_departure),' D d-M-Y h:i A')); ?>

                                        <?php echo e(e('<span>------------------</span>')); ?>

                                        <?php echo e(date_format(date_create($key->round_trip->return_departure_date),' D d-M-Y h:i A')); ?>

                                    </td>
                                    <td><?php echo e($key->round_trip->flightReturn->flight_name.'-'.$key->round_trip->flightReturn->flight_code.'-'.$key->round_trip->return_flight_number); ?>

                                        <?php echo e(e('<span>------------------</span>')); ?>

                                        <?php echo e($key->round_trip->flight->flight_name.'-'.$key->round_trip->flight->flight_code.'-'.$key->round_trip->flight_number); ?>

                                    </td>
                                    <td><?php echo e($key->number_of_seat); ?></td>
                                    <td><?php echo e($key->round_trip->flight_pnr_no.'-'.$key->round_trip->flight_pnr_no2); ?></td>
                                    <td><?php echo e('<i class="la la-inr"></i>'.$key->amount); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </tbody>
                        </table>
                        <!--end: Datatable -->

                    </div>
                </div>
                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <h4>Multi city Trip Bookings</h4>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search_t">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <table class="m-datatable_t"  width="100%">
                            <thead>
                            <tr>
                                <th>Sr .no</th>
                                <th>Booking Date</th>
                                <th>Sector</th>
                                <th>Date</th>
                                <th>Flight Details</th>
                                <th>No.  of Seats</th>
                                <th>PNR</th>
                                <th>Fare</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1; ?>

                            <?php $__currentLoopData = $history_multicity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <tr>
                                        <td><?php echo e($i++); ?></td>
                                        <td><?php echo e(date_format($key->created_at,' D d-M-Y h:i A')); ?></td>
                                        <td>
                                        <?php echo e($key->multicity->flightSourceF->flight_source.' - '.$key->multicity->flightDestinationF->flight_destination.' - '.$key->multicity->flightF->flight_code); ?>

                                            <?php echo e(e('<span>-------------------------------------</span>')); ?>

                                        <?php echo e($key->multicity->flightSourceS->flight_source.' - '.$key->multicity->flightDestinationS->flight_destination.' - '.$key->multicity->flightS->flight_code); ?>

                                        <?php echo e(e('<span>-------------------------------------</span>')); ?>

                                        <?php echo e($key->multicity->flightSourceT->flight_source.' - '.$key->multicity->flightDestinationT->flight_destination.' - '.$key->multicity->flightT->flight_code); ?>

                                        
                                        <td>
                                        <?php echo e(date_format(date_create($key->multicity->flight_departure_date_f),' D d-M-Y h:i A')); ?>

                                        <?php echo e(e('<span>---------------------------</span>')); ?>

                                        <?php echo e(date_format(date_create($key->multicity->flight_departure_date_s),' D d-M-Y h:i A')); ?>

                                        <?php echo e(e('<span>---------------------------</span>')); ?>

                                        <?php echo e(date_format(date_create($key->multicity->flight_departure_date_t),' D d-M-Y h:i A')); ?>

                                        </td>
                                        <td><?php echo e($key->multicity->flightF->flight_name.'-'.$key->multicity->flightF->flight_code.'-'.$key->multicity->flight_number_f); ?>

                                            <?php echo e(e('<span>------------------</span>')); ?>

                                            <?php echo e($key->multicity->flightS->flight_name.'-'.$key->multicity->flightS->flight_code.'-'.$key->multicity->flight_number_s); ?>

                                            <?php echo e(e('<span>------------------</span>')); ?>

                                            <?php echo e($key->multicity->flightT->flight_name.'-'.$key->multicity->flightT->flight_code.'-'.$key->multicity->flight_number_t); ?>

                                        </td>
                                        <td><?php echo e($key->number_of_seat); ?></td>
                                        <td><?php echo e($key->multicity->flight_pnr_no.'-'.$key->multicity->flight_pnr_no2.'-'.$key->multicity->flight_pnr_no3); ?></td>
                                        <td><?php echo e('<i class="la la-inr"></i>'.$key->amount); ?></td>
                                    </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </tbody>
                        </table>
                        <!--end: Datatable -->
                    <?php $__currentLoopData = $history_multicity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <!--begin::Modal-->
                            <div class="modal fade" id="m_modal_t<?php echo e($loop->iteration); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                Members information
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <?php $__currentLoopData = $key->booking_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="m-list-timeline__item">
                                                    <span class="m-list-timeline__badge m-list-timeline__badge--brand"></span>
                                                    <span class="m-list-timeline__text">
																<?php echo e($loop->iteration.') '.$info->title .' '.$info->firstname .' '.$info->lastname); ?>

															</span>
                                                </div>
                                                <br>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Modal-->
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php $__currentLoopData = $history_multicity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <!--begin::Modal-->
                            <div class="modal fade" id="m_modal_ticket_t<?php echo e($loop->iteration); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                Ticket Information

                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row m--padding-top-10"></div>
                                            <h5> First Flight information</h5>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <p>
											<span class="m--font-bolder">
												Flying Source :<br> <?php echo e($key->multicity->flightSourceF->flight_source); ?>

											</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>
											<span class="m--font-bolder">
												Flying Destination :<br> <?php echo e($key->multicity->flightDestinationF->flight_destination); ?>

											</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>
											<span class="m--font-bolder">
												Flight Name :<br> <?php echo e($key->multicity->flightF->flight_name); ?>

											</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>
											<span class="m--font-bolder">
												Flight Number :<br> <?php echo e($key->multicity->flightF->flight_code.'-'.$key->multicity->flight_number_f); ?>

											</span>
                                                    </p>
                                                </div>

                                            </div>

                                            <div class="row m--padding-top-40">
                                                <div class="col-md-3">
                                                    <p>
											<span class="m--font-bolder">
												Departure Date :<br> <?php echo e(date("d-M-Y D", strtotime($key->multicity->flight_departure_date_f))); ?>

											</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>
											<span class="m--font-bolder">
												Departure Time :<br> <?php echo e(date("h:i A", strtotime($key->multicity->flight_departure_time_f))); ?>

											</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p>
											<span class="m--font-bolder">
												Arrival Time :<br> <?php echo e(date("h:i A", strtotime($key->multicity->flight_arrival_time_f))); ?>

											</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="row m--padding-top-40"></div>
                                            <h5>Second Flight information</h5>
                                            <hr>

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <p>
											<span class="m--font-bolder">
												Flying Source :<br> <?php echo e($key->multicity->flightSourceS->flight_source); ?>

											</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>
											<span class="m--font-bolder">
												Flying Destination :<br> <?php echo e($key->multicity->flightDestinationS->flight_destination); ?>

											</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>
											<span class="m--font-bolder">
												Flight Name :<br> <?php echo e($key->multicity->flightS->flight_name); ?>

											</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>
											<span class="m--font-bolder">
												Flight Number :<br> <?php echo e($key->multicity->flightS->flight_code.'-'.$key->multicity->flight_number_s); ?>

											</span>
                                                    </p>
                                                </div>

                                            </div>

                                            <div class="row m--padding-top-40">
                                                <div class="col-md-3">
                                                    <p>
												<span class="m--font-bolder">
													Departure Date :<br> <?php echo e(date("d-M-Y D", strtotime($key->multicity->flight_departure_date_s))); ?>

												</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>
												<span class="m--font-bolder">
													Departure Time :<br> <?php echo e(date("h:i A", strtotime($key->multicity->flight_departure_time_s))); ?>

												</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p>
												<span class="m--font-bolder">
													Arrival Time :<br> <?php echo e(date("h:i A", strtotime($key->multicity->flight_arrival_time_s))); ?>

												</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="row m--padding-top-40"></div>
                                            <h5> Third Flight information</h5>
                                            <hr>

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <p>
												<span class="m--font-bolder">
													Flying Source :<br> <?php echo e($key->multicity->flightSourceT->flight_source); ?>

												</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>
												<span class="m--font-bolder">
													Flying Destination :<br> <?php echo e($key->multicity->flightDestinationT->flight_destination); ?>

												</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>
												<span class="m--font-bolder">
													Flight Name :<br> <?php echo e($key->multicity->flightT->flight_name); ?>

												</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>
												<span class="m--font-bolder">
													Flight Number :<br> <?php echo e($key->multicity->flightT->flight_code.'-'.$key->multicity->flight_number_t); ?>

												</span>
                                                    </p>
                                                </div>

                                            </div>

                                            <div class="row m--padding-top-40">
                                                <div class="col-md-3">
                                                    <p>
												<span class="m--font-bolder">
													Departure Date :<br> <?php echo e(date("d-M-Y D", strtotime($key->multicity->flight_departure_date_t))); ?>

												</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>
												<span class="m--font-bolder">
													Departure Time :<br> <?php echo e(date("h:i A", strtotime($key->multicity->flight_departure_time_t))); ?>

												</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p>
												<span class="m--font-bolder">
													Arrival Time :<br> <?php echo e(date("h:i A", strtotime($key->multicity->flight_arrival_time_t))); ?>

												</span>
                                                    </p>
                                                </div>
                                            </div>


                                            <hr>
                                            <div class="row m--padding-top-40">
                                                <?php if($key->multicity->flight_pnr_no): ?>
                                                    <div class="col-md-2 ">
                                                        <p>
												<span class="m--font-bolder">
													PNR No :<br> <?php echo e($key->multicity->flight_pnr_no); ?>

												</span>

                                                        </p>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if($key->multicity->flight_pnr_no2): ?>
                                                    <div class="col-md-2 ">
                                                        <p>
													<span class="m--font-bolder">
														PNR No 2 :<br> <?php echo e($key->multicity->flight_pnr_no2); ?>

													</span>

                                                        </p>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if($key->multicity->flight_pnr_no3): ?>
                                                    <div class="col-md-2 ">
                                                        <p>
													<span class="m--font-bolder">
														PNR No 3 :<br> <?php echo e($key->multicity->flight_pnr_no3); ?>

													</span>

                                                        </p>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="col-md-3">
                                                    <p>
														<span class="m--font-bolder">
															Via  :<br> <?php echo e($key->multicity->flight_via); ?>

														</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                Close
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <!--end::Modal-->

                    </div>

                </div>
			</div>
		<!--end:: Widgets/Stats-->

	</div>
</div>
<!-- end::Body -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>

<!--begin::Page Vendors -->
<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="<?php echo e(asset('assets/app/js/dashboard.js')); ?>" type="text/javascript"></script>
<!--end::Page Snippets -->
<script>
$( document ).ready(function() {
$(".m-checkbox").click(function(){
$(".collapse").addClass("show");
});
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>