
<?php $__env->startSection('title'); ?> User Dashboard <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
	<link href="<?php echo e(asset('assets/demo/default/base/dashboard.css')); ?>" rel="stylesheet" type="text/css" />
	<style>
		ul.dropdown-menu.inner {
			max-height: 217px !important;
		}
	</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
						Book Flight Tickets . . .
					</h3>
				</div>

			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-lg-12">
					<!-- <h4 class="m-portlet__head-text m--padding-bottom-20 ">What would you like to search Today! </h4> -->
					<!--begin::Portlet-->
					<?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div>   <br><?php endif; ?>
					<?php if(session('success')): ?>   <br><div class="alert alert-success"><?php echo e(session('success')); ?></div>   <br><?php endif; ?>
					<div class="m-portlet m-portlet--tabs m-portlet--brand m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
						<div class="m-portlet__head">

							<div class="m-portlet__head-tools">
								<ul class="nav nav-tabs m-tabs m-tabs-line" role="tablist">

									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_7_1" role="tab" aria-expanded="false">
											One way
										</a>
									</li>
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_7_2" role="tab" aria-expanded="false">
											Round trip
										</a>
									</li>
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link " data-toggle="tab" href="#m_tabs_7_3" role="tab" aria-expanded="true">
											Multi city
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="tab-content">
								
								<div class="tab-pane active " id="m_tabs_7_1" role="tabpanel" aria-expanded="false">
									<!--begin::one way-->
										<!-- <h4 class="m-portlet__head-text">Search Flight</h4><hr> -->
										<!--begin::Form-->
									<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"  METHOD="post" action="<?php echo e(url('flights/one-way-search')); ?>" id="m_form_2" novalidate="novalidate">
										<?php echo e(csrf_field()); ?>

										<div class="m-portlet__body">
												<div class="form-group m-form__group row">
													<div class="col-md-3">
														<select required class="form-control m-bootstrap-select m_selectpicker" name="flying_source"  title="Flying Source" placeholder="Flying Source" data-live-search="true">

															<?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<option value="<?php echo e($fligth->id); ?>"><?php echo e($fligth->flight_source); ?></option>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

														</select>
														<span class="m-form__help">
																Select your flying source
														</span>
													</div>
													<div class="col-md-3">
														<select required class="form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination" data-live-search="true">
															<?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<option value="<?php echo e($fligth->id); ?>"><?php echo e($fligth->flight_destination); ?></option>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</select>
														<span class="m-form__help">
																Select your flying destination
														</span>
													</div>
													<div class="col-md-3">
														<input type="text" required class="form-control" id="m_datepicker_1" placeholder="Departure" name="departure" readonly="" >
														<span class="m-form__help">
														Please select Departure date
													</span>
													</div>
													<div class="col-md-3">
														<select  name="adults" required size="5" class="size_5 form-control m-bootstrap-select m_selectpicker">
															<option value="1" selected > Adults </option>
															<?php
															for($i=1;$i<=20;$i++)
																echo '<option value="'.$i.'">'. $i.' </option>';
															?>

														</select>
														<span class="m-form__help">
														Please select number of Adults
													</span>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-md-11 m--align-right">
														<button type="submit" class="btn btn-primary">
															Search
														</button>
														<button type="reset" class="btn btn-secondary">
															Cancel
														</button>
													</div>
													<div class="col-md-1"></div>
												</div>
											</div>
										</form>
										<!--end::Form-->
									</div>
									<!--end::one way-->


								
								
								<div class="tab-pane" id="m_tabs_7_2" role="tabpanel" aria-expanded="false">
									<!--begin::one way-->
									<!-- <h4 class="m-portlet__head-text">Flight</h4><hr> -->
									<!--begin::Form-->
									<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo e(url('flights/round-trip-search')); ?>" id="m_form_2" method="post" novalidate="novalidate">
                                       <?php echo e(csrf_field()); ?>

										<div class="m-portlet__body">
											<div class="form-group m-form__group row">
												<div class="col-md-6 m--padding-10">

													<select class="form-control m-bootstrap-select m_selectpicker" name="flying_source" aria-describedby="option-error" aria-invalid="true"  title="Flying Source" placeholder="Flying Source" data-live-search="true">
															<?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<option value="<?php echo e($fligth->id); ?>"><?php echo e($fligth->flight_source); ?></option>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</select>
													<span class="m-form__help">
																Select your flying source
														</span>
												</div>
												<div class="col-md-6 m--padding-10">
													<select class="form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination" data-live-search="true">
														<?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<option value="<?php echo e($fligth->id); ?>"><?php echo e($fligth->flight_destination); ?></option>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</select>
													<span class="m-form__help">
																Select your flying destination
														</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<input type="text" class="form-control" id="m_datepicker_2" placeholder="Departure Date" name="departure" readonly="" >
													<span class="m-form__help">
														Please select Departure date
													</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<input type="text" class="form-control" id="m_datepicker_3" placeholder="Return Date" name="returnDate" readonly="" >
													<span class="m-form__help">
														Please select Return date
													</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<select  name="adults"  class="form-control m-bootstrap-select m_selectpicker">
														<option value="1" selected > Adults </option>
                                                        <?php
                                                        for($i=1;$i <=20;$i++)
                                                            echo '<option value="'.$i.'">'. $i.' </option>';
                                                        ?>

													</select>
													<span class="m-form__help">
														Please select number of Adults
													</span>
												</div>
											</div>
											<div class="form-group m-form__group row">
												<div class="col-md-11 m--align-right">
													<button type="submit" class="btn btn-primary">
														Search
													</button>
													<button type="reset" class="btn btn-secondary">
														Cancel
													</button>
												</div>
												<div class="col-md-1"></div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
									<!--end::one way-->


								
								
								<div class="tab-pane" id="m_tabs_7_3" role="tabpanel" aria-expanded="false">
									<!--begin::one way-->
									<!-- <h4 class="m-portlet__head-text">Flight</h4><hr> -->
									<!--begin::Form-->
									<form method="post" action="<?php echo e(url('flights/multi-city-search')); ?>" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">

										<?php echo e(csrf_field()); ?>

										<div class="m-portlet__body">
											<div class="form-group m-form__group row">
												<div class="col-md-4 m--padding-10">
													<select class="form-control m-bootstrap-select m_selectpicker" name="flying_source"  title="Flying Source" placeholder="Flying Source" data-live-search="true">
														<?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value="<?php echo e($fligth->id); ?>"><?php echo e($fligth->flight_source); ?></option>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</select>
													<span class="m-form__help">
																Select your flying source
														</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<select class="form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination" data-live-search="true">
														<?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value="<?php echo e($fligth->id); ?>"><?php echo e($fligth->flight_destination); ?></option>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</select>
													<span class="m-form__help">
																Select your flying destination
														</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<input type="text" class="form-control" id="m_datepicker_4" placeholder="Departure" name="departure" readonly="" >
													<span class="m-form__help">
														Please select Departure date
													</span>
												</div>

												
												<div class="col-md-4 m--padding-10">
													<select class="form-control m-bootstrap-select m_selectpicker" name="flying_source2"  title="Flying Source" placeholder="Flying Source" data-live-search="true">
														<?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value="<?php echo e($fligth->id); ?>"><?php echo e($fligth->flight_source); ?></option>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</select>
													<span class="m-form__help">
																Select your flying source
														</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<select class="form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination2" data-live-search="true">
														<?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value="<?php echo e($fligth->id); ?>"><?php echo e($fligth->flight_destination); ?></option>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</select>
													<span class="m-form__help">
																Select your flying destination
														</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<input type="text" class="form-control" id="m_datepicker_5" placeholder="Departure" name="departure2" readonly="" >
													<span class="m-form__help">
														Please select Departure date
													</span>
												</div>

												

												
												<div class="col-md-4 m--padding-10 hide  hide-block" >
													<select class=" form-control m-bootstrap-select m_selectpicker" name="flying_source3"  title="Flying Source" placeholder="Flying Source" data-live-search="true">
														<?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value="<?php echo e($fligth->id); ?>"><?php echo e($fligth->flight_source); ?></option>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</select>
													<span class="m-form__help">
																Select your flying source
														</span>
												</div>
												<div class="col-md-4 m--padding-10 hide  hide-block">
													<select class=" form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination3" data-live-search="true">
														<?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<option value="<?php echo e($fligth->id); ?>"><?php echo e($fligth->flight_destination); ?></option>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</select>
													<span class="m-form__help">
																Select your flying destination
														</span>
												</div>
												<div class="col-md-4 m--padding-10 hide hide-block">
													<input type="text" class=" form-control" id="m_datepicker_6" placeholder="Departure" name="departure3" readonly="" >
													<span class="m-form__help">
														Please select Departure date
													</span>
												</div>

												
												<div class="col-md-4 m--padding-10">
													<button type="button" class=" bth-remove m--pull-left bth-sm hide-block btn btn-secondary remove_city" ><i class="fa fa fa-minus"></i> Remove City</button>
													<button type="button" class=" bth-add btn m--pull-left show-block bth-sm btn-secondary add_city" ><i class="fa fa-plus"></i> Add City</button>
												</div>

												<div class="col-md-4 m--padding-10">
													<select  name="adults"  class="form-control m-bootstrap-select m_selectpicker">
														<option value="1" selected > Adults </option>
                                                        <?php
                                                        for($i=1;$i <=20;$i++)
                                                            echo '<option value="'.$i.'">'. $i.' </option>';
                                                        ?>

													</select>
													<span class="m-form__help">
														Please select number of Adults
													</span>
												</div>
											</div>
											<div class="m--padding-10 row">
												<div class="col-md-11 m--align-right">
													<button type="submit" class="btn btn-primary">
														Search
													</button>
													<button type="reset" class="btn btn-secondary">
														Cancel
													</button>
												</div>
												<div class="col-md-1"></div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
									<!--end::one way-->


								


							</div>
						</div>
					</div>
					<!--end::Portlet-->

				</div>
			</div>
		</div>
	</div>
</div>
<!-- end::Body -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>

<!--begin::Page Vendors -->
<script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js')); ?>" type="text/javascript"></script>

<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="<?php echo e(asset('assets/app/js/dashboard.js')); ?>" type="text/javascript"></script>
<!--end::Page Snippets -->
<script>



    $('#m_datepicker_1').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
        orientation: "bottom left",
		autoclose:true,
    });

    $('#m_datepicker_2').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		orientation: "bottom left",
		autoclose:true,
    });

    $('#m_datepicker_3').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate: '04-05-2018',
		orientation: "bottom left",
		autoclose:true,
    });

    $('#m_datepicker_4').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		orientation: "bottom left",
		autoclose:true,
    });

    $('#m_datepicker_5').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		orientation: "bottom left",
		autoclose:true,
    });

    $('#m_datepicker_6').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		orientation: "bottom left",
		autoclose:true,
    });

    $(".add_city").click(function(){
        $(".hide").removeClass('hide-block');
        $(".hide").addClass('show-block');
        $(".bth-remove").removeClass('hide-block');
        $(".bth-remove").addClass('show-block');
        $(".bth-add").removeClass('show-block');
        $(".bth-add").addClass('hide-block');
    });
    $(".bth-remove").click(function(){
        $(".hide").removeClass('show-block');
        $(".hide").addClass('hide-block');
        $(".bth-remove").removeClass('show-block');
        $(".bth-remove").addClass('hide-block');
        $(".bth-add").removeClass('hide-block');
        $(".bth-add").addClass('show-block');
    });
	$("#header_menu .m-menu__link").click(function(){
		$("#header_menu>li").removeClass("m-menu__item--active");
		$(this).parent('li').addClass('m-menu__item--active');
    });
</script>
	<script>// for round trip
        var start = new Date();
        var end = new Date(new Date().setYear(start.getFullYear()+1));


        $('#m_datepicker_2').datepicker({
            startDate : start,
            endDate   : end
        }).on('changeDate', function(){

            
//           $('#m_datepicker_3').datepicker('setStartDate',  $(this).val());
           $('#m_datepicker_3').datepicker('setStartDate', $(this).val());
        });

        $('#m_datepicker_3').datepicker({
            startDate : start,
            endDate   : end
        }).on('changeDate', function(){

            $('#m_datepicker_2').datepicker('setEndDate',  $(this).val());
        });
	</script>
	<script> // for multicity
		var start = new Date();
		var end = new Date(new Date().setYear(start.getFullYear()+1));

		$('#m_datepicker_4').datepicker({
			startDate : start,
			endDate   : end
		}).on('changeDate', function(){
			$('#m_datepicker_5').datepicker('setStartDate',  $(this).val());
			$('#m_datepicker_6').datepicker('setStartDate',  $(this).val());

		});

		$('#m_datepicker_5').datepicker({
			startDate : start,
			endDate   : end
		}).on('changeDate', function(){
			$('#m_datepicker_4').datepicker('setEndDate',  $(this).val());
			$('#m_datepicker_5').datepicker('setStartDate',  $(this).val());
			$('#m_datepicker_6').datepicker('setStartDate',  $(this).val());

		});
		$('#m_datepicker_6').datepicker({
			startDate : start,
			endDate   : end
		}).on('changeDate', function(){
			$('#m_datepicker_5').datepicker('setStartDate',  $(this).val());
			$('#m_datepicker_5').datepicker('setEndDate',  $(this).val());
			$('#m_datepicker_4').datepicker('setEndDate',  $(this).val());
		});

	</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>