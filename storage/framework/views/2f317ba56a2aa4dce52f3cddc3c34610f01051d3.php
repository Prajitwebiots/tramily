<?php $__env->startSection('title'); ?> Tramily | Admin Login <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url(../assets//images/bg-2.jpg);">
	<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
		<div class="m-login__container">
			<div class="m-login__logo">
				<a href="#">
					<img src="./assets/app/media/img/logos/logo-2.png" style="width: 50%;">
				</a>
			</div>
			<div class="m-login__signin">
				<div class="m-login__head">
					<h3 class="m-login__title">
					Sign In To Admin
					</h3>
				</div>
				<?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
				<?php if(session('success')): ?><br><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
				<form class="m-login__form m-form" action="<?php echo e(url('admin/login-post')); ?>" method="post">
					<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
					<div class="form-group m-form__group">
						<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off">
					</div>
					<div class="form-group m-form__group">
						<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
					</div>
					<div class="row m-login__form-sub">
					</div>
					<div class="m-login__form-action">
						<button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
						  Sign In
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/snippets/pages/user/login.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.loginMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>