<?php $__env->startSection('content'); ?>
 <div class="center-content">
        <div class="row">
            <div class="col-12  col-md-4 col-lg-3 nopadding">
                <div class="site-sidebar">
               <?php echo $__env->make('frontend.layouts.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
            <div class="col-12  col-md-8 col-lg-9 nopadding">
                <div class="inner-page">
                    <div class="content">

                        <div class="home-promo-box fadeIn">

                            <a href="#"><img src="https://s3.amazonaws.com/cdn.coingaming.io/sportsbet/images/Promotions/2017/virtual-sportsbet-main.jpg" style="width: 100%"></a>

                        </div>


                        <div class="live">

                            <div class="title-container">
                                <h1><div>Live</div> Right now</h1>
                            </div>

                            <div class="live-category">
                                <div class="header">
                                    <div class="category">
                                        <i class="hd-sport-icon tennis"></i>Tennis - ITF Men: Indonesia F7, Singles
                                    </div>
                                    <div class="one">1</div>
                                    <div class="x">X</div>
                                    <div class="two">2</div>
                                </div>
                                <div class="body">
                                    <div class="info">
                                        <div class="time">
                                            &nbsp;
                                        </div>
                                        <div class="status">
                                            <div>0</div>
                                            <div>1</div>
                                        </div>
                                        <div class="teams">
                                            <div>Puodziunas, Scott</div>
                                            <div>Haliak, Mikalai</div>
                                        </div>
                                        <div>
                                        </div>
                                        <div class="market">
                                            <div class="selection-container">
                                                <button class="selection enabled">
                                                    <div class="odds">4.00</div>
                                                </button>
                                                <button class="selection disabled">
                                                    <div class="odds">-</div>
                                                </button>
                                                <button class="selection enabled">
                                                    <div class="odds">1.22</div>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="extra-odds">+3</div>
                                    </div>
                                </div>
                            </div>

                            <div class="live-category">
                                <div class="header">
                                    <div class="category">
                                        <i class="hd-sport-icon tennis"></i>Tennis - ITF Men: Vietnam F3, Doubles
                                    </div>
                                    <div class="one">1</div>
                                    <div class="x">X</div>
                                    <div class="two">2</div>
                                </div>
                                <div class="body">
                                    <div class="info">
                                        <div class="time">

                                            &nbsp;

                                        </div>
                                        <div class="status">
                                            <div>0</div>
                                            <div>0</div>
                                        </div>
                                        <div class="teams">

                                            <div>Lu T / Stevenson K</div>

                                            <div>Ito Y / Matsuzaki Y</div>

                                        </div>
                                        <div>

                                        </div>
                                        <div class="market">
                                            <div class="selection-container">
                                                <button class="selection disabled">
                                                    <div class="odds">N/A</div>
                                                </button>
                                                <button class="selection disabled">
                                                    <div class="odds">N/A</div>
                                                </button>
                                                <button class="selection disabled">
                                                    <div class="odds">N/A</div>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="extra-odds">+2</div>
                                    </div>
                                </div>
                            </div>

                            <div class="live-category">
                                <div class="header">
                                    <div class="category">
                                        <i class="hd-sport-icon tennis"></i>Tennis - ITF Men: Vietnam F3, Doubles
                                    </div>
                                    <div class="one">1</div>
                                    <div class="x">X</div>
                                    <div class="two">2</div>
                                </div>
                                <div class="body">
                                    <div class="info">
                                        <div class="time">
                                            &nbsp;
                                        </div>
                                        <div class="status">
                                            <div>0</div>
                                            <div>0</div>
                                        </div>
                                        <div class="teams">

                                            <div>Thinh H / Tu N N</div>

                                            <div>Diaz M / Vu A</div>

                                        </div>
                                        <div>

                                        </div>
                                        <div class="market">
                                            <div class="selection-container">


                                                <button class="selection enabled">
                                                    <div class="odds">13.75</div>
                                                </button>


                                                <button class="selection disabled">
                                                    <div class="odds">-</div>
                                                </button>


                                                <button class="selection enabled">
                                                    <div class="odds">1.01</div>
                                                </button>


                                            </div>
                                        </div>
                                        <div class="extra-odds">+3</div>
                                    </div>
                                </div>
                            </div>



                        </div>
                        <div style="margin-top: 20px"></div>
                        <div class="title-container">
                            <h1>Upcoming Events</h1>
                        </div>


                        <div style="margin-top: 20px"></div>    
                    <?php $__currentLoopData = $eventmaster; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $master): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="title-container">
                            <h1><?php echo e($master->event_name); ?></h1>
                        </div>

                        <div class="events-header">
                            <div class="header">
                                <div class="start">Start Time</div>
                                <div class="event">Event</div>
                                <div class="one">1</div>
                                <div class="x">X</div>
                                <div class="two">2</div>
                            </div>
                            <?php $__currentLoopData = $master->events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="event-container">
                                <div class="start">
                                <?php $now = \Carbon\Carbon::parse();
                                $date = \Carbon\Carbon::parse($event->start_date);
                                $length = $date->diffForHumans($now); ?>
                                    <div><?php echo e($date->toDateString()); ?> <b><?php echo e($date->format('h:i')); ?></b></div>
                                    <div class="timer">
                                        <?php echo e($length); ?>

                                    </div>
                                </div>
                                <div class="icon"><i class="hd-sport-icon <?php echo e($event->cat->icon); ?>"></i></div>
                                <div class="event">
                                    <div class="competition">
                                        <?php echo e($event->subcat->name); ?>: <?php echo e(@$event->subsubcat->name); ?>, <?php echo e($master->event_name); ?>

                                    </div>
                                    <div class="competitors"><a href="<?php echo e(url('event-odds')); ?>" ><?php echo e($event->team1->name); ?> V <?php echo e($event->team2->name); ?></a></div>
                                </div>
                                <div class="markets">
                                    <div class="selection-container">
                                        <button class="selection enabled">
                                            <div class="odds"><?php echo e($event->odds); ?></div>
                                        </button>
                                        <?php if($event->draw): ?>
                                        <button class="selection enabled">
                                            <div class="odds"><?php echo e($event->draw); ?></div>
                                        </button>
                                        <?php else: ?>
                                        <button class="selection disabled">
                                            <div class="odds">-</div>
                                        </button>
                                        <?php endif; ?>
                                        <button class="selection enabled">
                                            <div class="odds"><?php echo e($event->odds2); ?></div>
                                        </button>
                                    </div>
                                </div>
                                <div class="extra-odds">+<?php echo e($event->odd->count()); ?></div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
               
            </div>
        </div>

    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>