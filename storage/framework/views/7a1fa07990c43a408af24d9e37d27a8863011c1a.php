<!-- head -->
<?php $__env->startSection('title'); ?>
    Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
 <meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
 <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>
  
<?php $__env->startSection('content'); ?>
  <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
      <div class="page-content" style="min-height: 1603px;">
          <!-- BEGIN PAGE HEAD-->
          <div class="page-head">
              <!-- BEGIN PAGE TITLE -->
              <div class="page-title">
                  <h1>Insert Odd

                  </h1>
              </div>
              <!-- END PAGE TITLE -->

          </div>
          <!-- END PAGE HEAD-->
          <!-- BEGIN PAGE BREADCRUMB -->
          <ul class="page-breadcrumb breadcrumb">
              <li>
                  <a href="index.html">Home</a>
                  <i class="fa fa-circle"></i>
              </li>
              <li>
                  <span class="active">Insert Odd</span>
              </li>
          </ul>
          <!-- END PAGE BREADCRUMB -->
          <!-- BEGIN PAGE BASE CONTENT -->
          <div class="row">
              <div class="col-md-10 ">
                  <!-- BEGIN SAMPLE FORM PORTLET-->
                  <div class="portlet light bordered">
                      <div class="portlet-title">
                          <div class="caption font-blue-sunglo">
                              <i class="icon-settings font-blue-sunglo"></i>
                              <span class="caption-subject bold uppercase"> Odd for <?php echo e($event->master->event_name); ?></span>
                          </div>

                      </div>

                    

                      <?php if(session('success')): ?>
                      <div class="alert alert-success">
                          <?php echo e(session('success')); ?>

                      </div>
                      <?php endif; ?>


                      <div class="portlet-body form">
                          <form role="form" method="post" action="<?php echo e(route('admin-odd.store')); ?>" >
                              <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                              <input type="hidden" name="event_id" value="<?php echo e($event->id); ?>">
                              <div class="form-body">
                                   <div class="row">
                                  <div class="form-group" style="float:right;">
                                    <span> <a class="btn blue btn-outline sbold" data-toggle="modal" href="#oddmaster"> <i class="fa fa-plus" aria-hidden="true"></i> Add Odd Master</a></span> 
                                      
                                  </div>
                                   </div>
                               <div class="row">
                                  <div class="form-group <?php echo e($errors->has('odd_master') ? ' has-error' : ''); ?> col-md-4">
                                      <label>Odd Master</label> 
                                      <select name="odd[1][odd_master]" id="odd_master"  class="form-control">
                                        <option value="">Select Odd Master</option>  
                                        <?php $__currentLoopData = $odd_masters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $odd_master): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($odd_master->id); ?>"><?php echo e($odd_master->odd_title); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      </select>
                                      <?php if($errors->has('odd_master')): ?>
                                         <span class="help-block">
                                             <strong><?php echo e($errors->first('odd_master')); ?></strong>
                                         </span>
                                     <?php endif; ?>
                                  </div>
                                 
                                    <div class="form-group <?php echo e($errors->has('title') ? ' has-error' : ''); ?> col-md-4">
                                        <label>Odd Title</label>
                                        <input type="text" class="form-control" placeholder="Enter Odd title" name="odd[1][title]" value="<?php echo e(old('title')); ?>">
                                        <?php if($errors->has('title')): ?>
                                           <span class="help-block">
                                               <strong><?php echo e($errors->first('title')); ?></strong>
                                           </span>
                                       <?php endif; ?>
                                    </div>
                                    <div class="form-group <?php echo e($errors->has('odd') ? ' has-error' : ''); ?> col-md-4">
                                        <label>Odd</label>
                                        <input type="text" class="form-control" placeholder="Enter Odd " name="odd[1][odd_val]" value="<?php echo e(old('odd')); ?>">
                                        <?php if($errors->has('odd')): ?>
                                           <span class="help-block">
                                               <strong><?php echo e($errors->first('odd')); ?></strong>
                                           </span>
                                       <?php endif; ?>
                                    </div>
                                  </div>

                                  <div id="new-period">

                                  </div>
                                  <div class="form-group" style="float:right;">
                                      <button type="button" class="btn btn-info" onclick="addOdds();">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Odd
                                      </button>
                                  </div>
                              </div>
                              <div class="form-actions">
                                  <button type="submit" class="btn blue">Submit</button>
                                   <a  class="btn default" href="<?php echo e(route('admin-team.index')); ?>">Cancel</a>
                              </div>
                          </form>
                      </div>
                  </div>

              </div>

          </div>

          <!-- END PAGE BASE CONTENT -->
      </div>
      <!-- END CONTENT BODY -->
  </div>

  <!-- Odd master model -->
<div class="modal fade" id="oddmaster" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Odd Master</h4>
            </div>
            <div class="modal-body"> 
           <form action="" method="post" id="oddmstr">
            <div class="form-group">
              <label>Odd Title</label>
                 <input type="text" class="form-control" placeholder="Enter Odd  Name" name="oddtitle">                       
            </div>
          </form>
          </div>
       <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <button type="button" id="Submit-oddmaster" class="btn green">Save</button>
        </div>
    </div>                                 
    </div>                                       
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
  <script>
   var i = 1000;
    function addOdds()
    {
        var odd_masters='';
        var pausecontent = new Array();
        <?php foreach($odd_masters as $key => $val){ ?>
           odd_masters+=' <option value="<?php echo $val['id'] ?>"><?php echo $val['odd_title'] ?></option>';
        <?php } ?>
      
       var odd='';
         odd +='<div class="row" id="removeThis'+i+'" ><div class="form-group col-md-4"><label>Odd Master</label>&nbsp;&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true" style="color:red; cursor: pointer;" onclick="remove('+i+');"> Remove </i> <select name="odd['+i+'][odd_master]" id="odd_master'+i+'" class="form-control">  <option value="">Select Odd Master</option>'+odd_masters+'</select></div><div class="form-group  col-md-4">   <label>Odd Title</label>   <input type="text" class="form-control" placeholder="Enter Odd title" name="odd['+i+'][title]" ></div> <div class="form-group  col-md-4"> <label>Odd</label> <input type="text" class="form-control" placeholder="Enter Odd " name="odd['+i+'][odd_val]" >  </div></div>';
        $('#new-period').append(odd);
        i = i+1;
    }
     function remove($id){
        $('#removeThis'+$id).remove();
      }
  </script>
  <script src="<?php echo e(asset('js/custome.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>