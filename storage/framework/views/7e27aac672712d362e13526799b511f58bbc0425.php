<?php $__env->startSection('content'); ?>
  
<div class="center-content">
        <div class="row">
            <div class="col-12  col-md-4 col-lg-3 nopadding">
                <div class="site-sidebar">

                <?php echo $__env->make('frontend.layouts.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="_token">
            <div class="col-12  col-md-8 col-lg-9 nopadding">
                <div class="inner-page">
                    <div class="event-toggle">
                        <div class="event-first">
                            <div class="row">
                            <div class="col-md-9">Andria, Italy | Semifinals</div>
                            <div class="col-md-3 text-right">
                                Indoor
                            </div>
                            </div>
                        </div>
                        <div class="event-second">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="country-ul">
                                        <li>Basic, Mirza</li>
                                        <li> Heyman, Christopher</li>
                                    </ul>
                                </div>
                                <div class="col-md-6 pull-right">
                                    <table style="float:right;color: #fff">
                                        <tr>
                                            <td>3</td>
                                            <td>3</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>2</td>
                                            <td>0</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="event-third">
                            <div class="match_img" style="display: none">
                                <img src="http://e2.365dm.com/17/10/768x432/skysports-romelu-lukaku-joe-root-lewis-hamilton-owen-farrell-britt-assombalonga_4136046.jpg?20171023121519" width="100%">
                            </div>
                            <div class="bottom-btn">
                            <button class="show_img"><i class="fa fa-angle-down" aria-hidden="true"></i></button>
                            </div>
                            <div class="bottom-img">
                                <img src="https://www.nassm.com/sites/default/files/randomslide/15.jpg" width="100%">
                            </div>
                        </div>
                    </div>
                    <div class="content">
                        <div class="event-details-market">
                            <div class="ttl-event-detail"><h1>Match Winner</h1></div>
                            <div class="selections-container">

                                <div class="cell col-md-6 enabled">
                                    <div class="selection-name"><?php echo e($team_data->Team1->name); ?></div>
                                    <button class="selection">
                                        <div><?php echo e($team_data->odds); ?></div>
                                    </button>
                                </div>

                                <div class="cell col-md-6 enabled">
                                    <div class="selection-name"><?php echo e($team_data->Team2->name); ?></div>
                                    <button class="selection">
                                        <div><?php echo e($team_data->odds2); ?></div>
                                    </button>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div id="show_more">
                          
                        </div>

                     
                    <button class="btn btn-success" style="float: right;" id="moreodds" onclick="show_more(<?php echo e($team_data->id); ?>)">Show More Odds</button>   

                </div>
              </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script_bottom'); ?>

<script>
    $(".show_img").click(function(){
        $(".match_img").toggle("slow");
    })
</script>


<?php $__env->stopSection(); ?>
<script type="text/javascript">
  function  show_more(id) 
  {
   
    var _token=$("#_token").val();
      var url = '<?php echo e(url("more_show")); ?>';

             $.ajax({
                url: url,   
                type:'post',
                data: { '_token' : _token, 'event_id':id },
                success: function(result)
                { 
                    console.log(result);
                    $('#moreodds').hide();
                    $('#show_more').html(result);
                 } 
             });
    
  }
</script>



<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>