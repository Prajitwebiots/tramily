
<?php $__env->startSection('title'); ?> User | Search One Way Flight <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
	.que2a,.que2b,.que3a,.que3b{
		display: none;
	}
	ul.dropdown-menu.inner {
		max-height: 217px !important;
	}
</style>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					Flight Search - One Way
					</h3>
				</div>
				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-md-12">
					
					<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tabs m-portlet--primary m-portlet--head-solid-bg m-portlet--head-sm">
						<?php
							$request = Session::get('oneway', 'default');
						?>
						<div class="m-portlet__body">
							<div class="tab-content">
								
								<div class="tab-pane active  " id="m_tabs_7_1" role="tabpanel" aria-expanded="false">

									<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-primary alert-dismissible fade show" role="alert">
										<div class="m-alert__icon">
											<i class="la la-warning"></i>
										</div>
										<div class="m-alert__text">
											<strong>
											Search Result For,
											</strong>
											Flying Source: <?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($fligth->id == $request['flying_source']): ?> <?php echo e($fligth->flight_source); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											| Flying Destination:  <?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($fligth->id == $request['flying_destination']): ?> <?php echo e($fligth->flight_destination); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> |
											Departure:  <?php echo e($request['departure']); ?> | Adults: <?php echo e($request['adults']); ?>

										</div>

										<div class="m-alert__close">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
										</div>
									</div>
									<!--begin::one way-->
									<!--begin::Form-->
								<?php if(($showalert == 1) ): ?>
									
									<!-- <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-primary alert-dismissible fade show" role="alert">
											<div class="m-alert__icon">
													<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
													Sorry, We couldn't find blocks as per your Requirement, So Near by dates are Offered for your Reference
											</div>
											<div class="m-alert__close">
													<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
											</div>
									</div> -->
										<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
											<strong>
												Sorry ! We couldn't find blocks as per your Requirement, So Near by dates are Offered for your Reference..
											</strong>

										</div>
										
									<?php elseif(($showalert == 2) && count($oneway) >= 0 ): ?>
										<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
											<strong>
												Searched Result Not found.
											</strong>

										</div>
									<?php endif; ?>
									
									
									<div class="m-content">
										
										<div class="row">
							
											<div class="col-xl-12">
												<form class="m-form m-form--state m-form--fit m-form--label-align-right" method="post" action="<?php echo e(url('flights/one-way-search')); ?>" id="m_form_2" novalidate="novalidate">
													<div class="m-portlet__body">
														
														<div class="form-group m-form__group row">
															<div class="row">
																<div class="col-md-3">
																	<?php echo e(csrf_field()); ?>

																	<select class="m--padding-top-10 form-control m-bootstrap-select m_selectpicker" name="flying_source" title="Flying Source" placeholder="Flying Source" data-live-search="true">
																		<?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<option <?php if($fligth->id == $request['flying_source']): ?> selected <?php endif; ?> value="<?php echo e($fligth->id); ?>"><?php echo e($fligth->flight_source); ?></option>
																		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																	</select>
																	
																	<span class="m-form__help">
																		Select your flying source
																	</span>
																</div>
																<div class="col-md-3">
																	<select class="m--padding-top-10 form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination" data-live-search="true">
																		<?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<option <?php if($fligth->id == $request['flying_destination']): ?> selected <?php endif; ?> value="<?php echo e($fligth->id); ?>"><?php echo e($fligth->flight_destination); ?></option>
																		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																	</select>
																	<span class="m-form__help">
																		Select your flying destination
																	</span>
																</div>
																<div class="col-md-3" style="
																	margin-top: 9px;
																	">
																	<input type="text" class="m--padding-top-10 form-control" value="<?php echo e($request['departure']); ?>" id="m_datepicker_1" placeholder="Departure" name="departure" readonly="" >
																	<span class="m-form__help">
																		Please select Departure date
																	</span>
																</div>
																<div class="col-md-3">
																	<select  name="adults"  class="m--padding-top-10 form-control m-bootstrap-select m_selectpicker">
																		<option value="1" selected > Adults </option>
																		<?php for($i=1;$i <=20;$i++): ?>
																		<option <?php if($i == $request['adults']): ?> selected <?php endif; ?>  value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
																		<?php endfor; ?>
																	</select>
																	<span class="m-form__help">
																		Please select number of Adults
																	</span>
																</div>
															</div>
														</div>
														<div class="form-group m-form__group m--align-right">
															<div class="">
																<button type="submit" class="btn btn-primary">
																Search
																</button>
															</div>
														</div>
													</div>
												</form>
											</div>
											<div class="col-xl-12">
												<?php if(count($oneway) > 0): ?>
												
												<div class="m--padding-10 m--padding-top-30 ">
													<!--begin: Datatable -->
													<table class="m-datatable" id="html_table" width="100%">
														<thead class="">
															<tr >

																<th>SR .No</th>
																<th>Date</th>
																<th>Flight Departure</th>
																<th>Flight Arrival</th>
																<th>Flight Number</th>
																<th>Price </th>
																<th>Action </th>
															</tr>
														</thead>
														<tbody>
																<?php $__currentLoopData = $oneway; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filte): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<tr>
																	<td>
																		<?php echo e($loop->iteration); ?>

																	</td>
																<td>
																	<?php echo e(date_format(date_create($filte->flight_departure),"D d - M - Y ")); ?>

																</td>
																<td>
																	<?php echo e($filte->flightSource->flight_source); ?>

																	<?php echo e('<br>'); ?>

																	
																	<?php echo e('<code>'); ?><?php echo e(date_format(date_create($filte->flight_departure_time)," h:i A")); ?><?php echo e('</code>'); ?>

																</td>
																
																<td>
																	<?php echo e($filte->flightDestination->flight_destination); ?>

																	<?php echo e('<br>'); ?>

																	<?php echo e('<code>'); ?><?php echo e(date_format(date_create($filte->flight_arrival)," h:i A")); ?><?php echo e('</code>'); ?>

																</td>
																<td><?php echo e('<code>'.$filte->flight->flight_name.'-'.$filte->flight->flight_code.'-'.$filte->flight_number.'</code>'); ?></td>
																<td>
																	<?php echo e('<i class="fa fa-rupee"> '.$filte->flight_price.'</i>'.' / Per Person'); ?><?php echo e('<br>'); ?>

																	<?php echo e('[Avail. Seats: '.$filte->seat.']'); ?>

																</td>
																<td>

																	<?php echo e(e( Form::open(['method' => 'post','url' => ['flights/one-way-book'],'style'=>'display:inline']))); ?>

																	<?php echo e(e('<input type="hidden" value="'.$request['adults'].'" name="adults">')); ?>

																	<?php echo e(e('<input type="hidden" value="'.$filte->id.'" name="id">')); ?>

																	<?php echo e('<button type="submit"   class="btn btn-outline-brand m-btn m-btn--icon" title ="Book Now">
																		<span>
																			<i class="la la-envelope-o"></i>
																			<span>Book Now</span>
																		</span>
																	</button>'); ?>

																	<?php echo e(e( Form::close() )); ?>

																</td>

															</tr>
																<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

														</tbody>
													</table>
													<!--end: Datatable -->
												</div>


												
												<?php endif; ?>
												<?php if($showalert == 2  || $showalert == 1): ?>
												
												<div class="m--padding-10 m--padding-top-30 ">
													<div class="m-alert m-alert--icon alert alert-danger" role="alert">
														<div class="m-alert__icon">
															<i class="flaticon-danger"></i>
														</div>
														<div class="m-alert__text">
														<strong>If The Desired Results Doesn't Meet Your Requirement, Please Click on Send mail, Our Team shall help you to fulfill your Requirement</strong> </div>
														<div class="m-alert__actions" style="width: 220px;">
															<a href="<?php echo e(url('flights/emailoneway')); ?>" class="btn btn-outline-light btn-sm m-btn m-btn--hover-brand" >
															<i class="fa fa-envelope"></i> Send Search Mail
															</a>
														</div>
													</div>
												</div>
												
												<?php endif; ?>
											</div>
										</div>
									</div>
									<!--end::Form-->
								</div>
								<!--end::one way-->
								
							</div>
						</div>
						<!--end::Portlet-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end::Body -->
	<?php $__env->stopSection(); ?>
	<?php $__env->startSection('bottom_script'); ?>
	<!--begin::Page Vendors -->
	<script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js')); ?>" type="text/javascript"></script>
	
	<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
	<!--end::Page Vendors -->
	<!--begin::Page Snippets -->
	<script src="<?php echo e(asset('assets/app/js/dashboard.js')); ?>" type="text/javascript"></script>
	<!--end::Page Snippets -->
	<script>
	$('#m_datepicker_1').datepicker({
	format: 'dd-mm-yyyy',
	todayHighlight:	true,
	autoclose:true,
	startDate:new Date()
	});
	</script>
	<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>