<?php $__env->startSection('head'); ?>
    <link href="<?php echo e(URL::asset('assets/css/header-footer.css')); ?>" rel="stylesheet" id="style_components" type="text/css" /> 
     <link href="<?php echo e(URL::asset('assets/css/offer.css')); ?>" rel="stylesheet" id="style_components" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
 <div class="content-container">
        <div class="middle">
                    <div class="middle-content clearfix" style="padding-top: 0">

                        <article class="clearfix" style="border: none">

                            <div id="mainBlockDesign">
                                <section class="promotions">
                                    <div class="banner-image"><img src="//cdn.coingaming.io/sportsbet/images/Bitcoin-Sportsbook-Sportsbet-Promotions-Offer-Header-image-201610.jpg" width="100%"></div>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php $__currentLoopData = $promotions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $promotion): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="promotion">
                                      <div class="row">
                                        <div class="col-12 col-sm-12 col-lg-3">
                                        <div class="promotion-image"><img src="<?php echo e(url('assets/images/blog/'.$promotion->image)); ?>"></div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-lg-9">
                                            <div class="promotion-body">
                                            <h1><?php echo e($promotion->title); ?></h1><?php echo e($promotion->description); ?><a class="promotion-link" href="<?php echo e(route('single.promotions',$promotion->id)); ?>">Read more</a> </div>
                                        </div>
                                      </div>
                                    </div>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  
                                </section>
                            </div>

                            <div id="loggedInDesign"></div>

                        </article>

                    </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>