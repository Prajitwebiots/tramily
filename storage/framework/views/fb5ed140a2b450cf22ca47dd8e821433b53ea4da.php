<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<?php echo $__env->make('layouts.dashHead', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<!-- end::Body -->
	<body class="m-page--wide m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			
			<?php echo $__env->make('layouts.dashHeader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<?php echo $__env->yieldContent('content'); ?>
			</div>
			<?php echo $__env->make('layouts.dashFooter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			
			<!-- end:: Page -->
			<?php echo $__env->make('layouts.dashFooterContent', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			
		</div>
		<?php echo $__env->yieldContent('bottom_script'); ?>
	</body>
</html>