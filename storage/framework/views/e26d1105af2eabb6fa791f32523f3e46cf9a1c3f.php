        <!--begin::Base Scripts -->
        <script src="<?php echo e(asset('assets/vendors/base/vendors.bundle.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(asset('assets/demo/default/base/scripts.bundle.js')); ?>" type="text/javascript"></script>
        <!--end::Base Scripts -->
        <!--begin::Page Snippets -->
        <?php echo $__env->yieldContent('bottom_script'); ?>
        <!--end::Page Snippets -->        