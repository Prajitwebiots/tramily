<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style type="text/css">
        * {color: #707070;font-family: verdana;font-size: 12px;margin: 0 auto;padding: 0;vertical-align: middle;line-height: 150%;}
        a {text-decoration: none;}
        a:hover {text-decoration: underline;}
    </style>
</head>
<body style="margin:0 auto;padding:0; background-color:#fff;">
<table style="width:730px;  margin: 0 auto;" cellspacing="3" cellpadding="0">
    <tr>
        <td>
            <div style="font-size:12px;padding: 15px 3%;width: 93.8%;background-color:#262734;float:left;">
                <div style='float:left;text-align:left;display: inline-table;'>
                    <img src="<?php echo asset('assets/app/media/img/logos/logo-2.png'); ?>" alt="Tour Tickets" style="width:100px; height: 100%" />
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div style="font-size:12px;color: #707070;border: 1px solid #E5E5E5;padding: 6%;width: 87.7%;background-color:#FFF;float:left;font-family: verdana;">
                <p style="color:#000">
                    Dear <?php echo $user->first_name.' '.$user->last_name; ?>,<br /><br />

                    flight ticket details are as following!<br /><br>

                    Client Details:
                <table style="width: 100%; margin-left: 5%; margin-bottom: 5%">
                    <tr>
                        <td style="width: 25%">Name</td> <td>: <?php echo $user->first_name.' '.$user->last_name; ?></td>
                    </tr>
                    <tr>
                        <td>Email</td> <td>: <?php echo $user->email; ?></td>
                    </tr>
                    <tr>
                        <td>Mobile No: </td> <td>: <?php echo $user->mobile; ?></td>
                    </tr>
                    <tr>
                        <td>Company Name: </td> <td>: <?php echo $user->agency_name; ?></td>
                    </tr>
                </table>

                 Details About Flight:
                <table style="width: 100%; margin-left: 5%; margin-bottom: 5%">

                    <tr>
                        <td style="width: 25%">Source</td> <td>: <?php echo $roundtripbook->flightSource->flight_source; ?></td>
                    </tr>
                    <tr>
                        <td>Destination</td> <td>: <?php echo $roundtripbook->flightDestination->flight_destination; ?></td>
                    </tr>
                    <tr>
                        <td>Departure</td> <td>: <?php echo $roundtripbook->flight_departure.''.$roundtripbook->flight_departure_time;?></td>
                    </tr>


                    <tr>
                        <td style="width: 18%"> Return Source </td> <td>: <?php echo $roundtripbook->flightSourceReturn->flight_destination; ?></td>
                    </tr>
                    <tr>
                        <td>Destination Return</td> <td>: <?php echo $roundtripbook->flightDestinationReturn->flight_source; ?></td>
                    </tr>
                    <tr>
                        <td>Departure Return</td> <td>: <?php echo $roundtripbook->return_departure_date.''.$roundtripbook->return_departure_time;?></td>
                    </tr>

                    <tr>
                        <td>Flight Name : </td> <td>: <?php echo $roundtripbook->flight->flight_name;?></td>
                    </tr>
                    <tr>
                        <td>Flight Number : </td> <td>: <?php echo $roundtripbook->flight->flight_code; ?></td>
                    </tr>


                    <tr>
                        <td>Seat</td> <td>: <?php echo  $roundtripbook->seat; ?></td>
                    </tr>
                    <tr>
                        <td>PNR No</td> <td>: <?php echo $roundtripbook->flight_pnr_no; ?></td>
                    </tr>
                    <tr>
                        <td>PNR No .2</td> <td>: <?php echo $roundtripbook->flight_pnr_no2; ?></td>
                    </tr>
                    <tr>
                        <td>Via </td> <td>: <?php echo $roundtripbook->flight_via; ?></td>
                    </tr>

                </table>



                <strong>Regards,<br /><br />
                </strong>
                <a style='color:#0000FF;' href="<?php echo URL(''); ?>">Tramily Travel</a>
                </p>
            </div>
        </td>
    </tr>
    <tr>
        <td style='background-color:#262734;'>
            <div style="font-size:12px;color: #707070;border-width: 1px; border-style: solid; border-color: #e5e5e5;padding: 15px 3%;width: 93.8%;float:left;text-align: center;">
                <p style='margin: 0;font-family: verdana;font-size:12px;display: block;color: #ffffff;'><?php echo 'copyright'; ?></p>
                <p style='display: block;margin: 0;'>
                    <a target="_blank" href="<?php echo url(''); ?>termsconditions" style='font-family: verdana;font-size:12px;color:#707070;display: inline-table;'>Terms &amp; Conditions</a><span style="color:#707070"> | </span>
                    <a target="_blank" href="<?php echo url(''); ?>privacypolicy" style='font-family: verdana;font-size:12px;color:#707070;display: inline-table;'>Privacy Policy</a>
                </p>
            </div>
        </td>
    </tr>
</table>
</body>
</html>