<?php $__env->startSection('title'); ?>Admin | Flight Source <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Flight Source
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Flight Source
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <br>
        <?php if(session('error')): ?><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
        <?php if(session('success')): ?><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
        <br>
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                        <a href="<?php echo e(route('admin-flightSource.create')); ?>" class="btn m-btn--pill    btn-primary" >Add New</a>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                
                <!--begin: Datatable -->
                <table class="m-datatable" id="html_table" width="100%">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            
                            <th>Flight Source</th>
                            <th>Status</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php $__currentLoopData = $flight; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flights): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                        <tr>
                            <td><?php echo e($i++); ?></td>
                            <td><?php echo e($flights->flight_source); ?></td>
                            <td>
                                <?php if($flights->status == 0): ?>
                                <?php echo e('<span><span class="m-badge  m-badge--danger m-badge--wide">Deactivated</span></span>'); ?>

                                <?php else: ?>
                                <?php echo e('<span><span class="m-badge  m-badge--primary m-badge--wide">Activated</span></span>'); ?>

                                <?php endif; ?>
                            </td>
                            <td>
                                <?php echo e('<a href="'.route('admin-flightSource.edit',$flights->id).'" class="btn m-btn--pill    btn-primary" >Edit</a>'); ?>

                                <?php if($flights->status == 0): ?>
                                <?php echo e('<a href="'.url('flightSourceStatus').'/'.$flights->id.'/1" class="btn m-btn--pill    btn-primary" >Active</a>'); ?>

                                <?php else: ?>
                                <?php echo e('<a href="'.url('flightSourceStatus').'/'.$flights->id.'/0" class="btn m-btn--pill    btn-danger" >Block</a>'); ?>

                                <?php endif; ?>
                            </td>
                            
                        </tr>
                        
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>