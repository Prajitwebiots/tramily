<!-- head -->
<?php $__env->startSection('title'); ?> 
   Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?php echo e(URL::asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" /> 
     <link href="<?php echo e(URL::asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <style>
        .team-modal-main-div{
            padding-right: 30em;
        }
    </style>
<?php $__env->stopSection(); ?>
    

<?php $__env->startSection('content'); ?>

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="min-height: 1489px;">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Betting History

                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo e(url('admin/dashboard')); ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span class="active">Betting History</span>
            </li>

            <!-- <span class="set-right"><a href="<?php echo e(route('sub-admin.create')); ?>" class="btn btn-primary "  >Add New</a></span> -->
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <div class="row">

        </div>
        <br>
        <?php if(session('error')): ?><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
        <?php if(session('success')): ?><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
        <br>
        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Betting History</span>
                        </div>
                        <div class="tools"></div>
                    </div>
                    <div class="portlet-body">
                        <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>

                            <tr>
                                <th>Sr No</th>
                                <th>User Name</th>
                                <th>Event Name</th>
                                <th>Team</th>
                                <th>Odds</th>
                                <th>Date</th>
                                <th>Bet Type</th>
                                <th>Bet On</th>
                                <th>Win</th>
                                <th>Status</th>
                                <th>Created At</th>
                               <!--  <th>Action</th> -->
                            </tr>
                            </thead>
                            <tbody><?php $i = 1; ?>
                            <?php $__currentLoopData = $bet; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($user->user): ?>
                            <tr>
                                <td><?php echo e($i++); ?></td>
                                <td><?php echo e($user->user->first_name); ?> <?php echo e($user->user->last_name); ?></td>
                                <td><?php echo e($user->event->master->event_name); ?></td>
                                <td><?php echo e($user->team->name); ?></td>
                                <td><?php echo e($user->odds); ?></td>
                                <td><?php echo e($user->date_time); ?></td>
                                <td><?php echo e($user->bet_type); ?></td>
                                <td><?php echo e($user->bet_on); ?></td>
                                 <td><?php echo e($user->win); ?></td>
                                <?php if($user->user->status == 0): ?>
                                <td><span class="label label-sm label-danger"> Deactivated </span></td>
                                <?php else: ?>
                                <td><span class="label label-sm label-success"> Activated </span></td> 
                                <?php endif; ?>
                                <td> <?php echo date('j M, Y h:i:s A', strtotime($user->user->created_at)); ?></td>
                                <!-- <td width="18%">
                                    <?php if($user->user->status == 0): ?>
                                     <a href="<?php echo e(url('changeAdminStatus/'.$user->user->id.'/1')); ?>" class="btn btn-success" >Active</a>
                                    <?php else: ?>
                                      <a href="<?php echo e(url('changeAdminStatus/'.$user->user->id.'/0')); ?>" class="btn btn-danger" >Block</a>
                                    <?php endif; ?>
                                   <!--  <a href="<?php echo e(route('sub-admin.edit',$user->user->id)); ?>" class="btn btn-primary">Edit</a> -->
                                </td> -->
                            </tr>
                            <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('script'); ?>
     <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-buttons.min.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
      
    </script>
    <?php $__env->stopSection(); ?>
   
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>