<!-- head -->
<?php $__env->startSection('title'); ?>
    Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="../backend/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <style>
        .cms-modal-main-div{
            padding-right: 30em;
        }
    </style>
<?php $__env->stopSection(); ?>
    
<?php $__env->startSection('content'); ?>


<?php $__env->startSection('content'); ?>

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="min-height: 1489px;">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Category

                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo e(url('admin/dashboard')); ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span class="active">Category</span>
            </li>

            <span class="set-right"><a href="<?php echo e(route('admin-category.create')); ?>" class="btn btn-primary ">Add New Category</a></span>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <div class="row">

        </div>
        <br>
        <?php if(session('error')): ?><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
        <?php if(session('success')): ?><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
        <br>
        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Category</span>
                        </div>
                        <div class="tools"></div>
                    </div>
                    <div class="portlet-body">
                        <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Category Name</th>
                                <th>Slug</th>
                                <th>Icon</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody><?php $i = 1; ?>
                          <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($i++); ?></td>
                                <td><?php echo e($cat->name); ?></td>
                                <td><?php echo e($cat->slug); ?></td>
                                <td><?php echo e($cat->icon); ?></td>
                                <?php if($cat->status == 1): ?>
                                <td><span class="label label-sm label-danger"> Deactivated </span></td>
                                <?php else: ?>
                                <td><span class="label label-sm label-success"> Activated </span></td> 
                                <?php endif; ?>
                                  <td width="250">


                                            <a href="<?php echo e(route('admin-category.edit',$cat->id)); ?>" class="btn btn-xs blue"  ><i class="fa fa-pencil"></i></a>

                                        <?php if($cat->status == 0): ?>
                                        <a href="<?php echo e(route('admin-category.deactivestatus',$cat->id)); ?>" class="btn btn-danger" >Deactivate</a>
                                        <?php endif; ?>
                                        <?php if($cat->status == 1): ?>
                                        <a href="<?php echo e(route('admin-category.activestatus',$cat->id)); ?>" class="btn btn-success" >Active</a>
                                        <?php endif; ?>

                                       <!--   <?php echo e(Form::open(['method' => 'DELETE', 'class'=>'set-right' ,'route' => ['admin-category.destroy',$cat->id]])); ?>

                                       <?php echo e(Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs'] )); ?>

                                    <?php echo e(Form::close()); ?> -->



                                    </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                       
                           
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('script'); ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="../backend/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="../backend/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="../backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="../backend/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../backend/assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
      
    </script>
    <?php $__env->stopSection(); ?>
   
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>