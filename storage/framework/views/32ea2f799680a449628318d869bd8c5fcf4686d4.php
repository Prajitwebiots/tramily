<?php $__env->startSection('title'); ?> Supplier | Multi City Flight <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
	.que2a,.que2b,.que3a,.que3b{
		display: none;
	}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					Multi City Flight
					</h3>
				</div>
			</div>
		</div>
		<div class="m-content">
			
			<?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div>   <br><?php endif; ?>
			<?php if(session('success')): ?>   <br><div class="alert alert-success"><?php echo e(session('success')); ?></div>   <br><?php endif; ?>
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tab">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<span class="m-portlet__head-icon m--hide">
										<i class="la la-gear"></i>
									</span>
									<h3 class="m-portlet__head-text">
									Add Flight
									</h3>
								</div>
							</div>
						</div>
						<!--begin::Form-->
						<form class="m-form m-form--fit m-form--label-align-right" action="<?php echo e(route('supplier-FlightMultiCity.store')); ?>" method="post" >
							<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
							<?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div> <br><?php endif; ?>
							<?php if(session('success')): ?><br><div class="alert alert-success"><?php echo e(session('success')); ?></div> <br><?php endif; ?>
							<div class="m-portlet__body">
								<strong style="font-weight: 600;margin-left: 28px;">Flight Details 1</strong>
								<div class="m--padding-15"></div>
								<div class="row">
									<div class="col-md-6">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Source
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="source" name="source_f">
												<option value="">Select Flight Source</option>
												<?php $__currentLoopData = $flightSource; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flightSources): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option value="<?php echo e($flightSources->id); ?>"><?php echo e($flightSources->flight_source); ?></option>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
											
											<?php if($errors->has('source_f')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('source_f')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Destination
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="destination" name="destination_f">
												<option value="">Select Flight Destination</option>
												<?php $__currentLoopData = $flightDestination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flightDestinations): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option value="<?php echo e($flightDestinations->id); ?>"><?php echo e($flightDestinations->flight_destination); ?></option>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
											
											<?php if($errors->has('destination_f')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('destination_f')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Name
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="flight" name="flight_name_f">
												<option value="">Select Flight</option>
												<?php $__currentLoopData = $flight; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flights): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option value="<?php echo e($flights->id); ?>"><?php echo e($flights->flight_name); ?></option>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
											
											<?php if($errors->has('flight_name_f')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('flight_name_f')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
										
										
									</div>
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Number
											</label>
											
											<input type="text" class="form-control m-input" value="<?php echo e(old('flight_number_f')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Flight Number" name="flight_number_f"  >
											<?php if($errors->has('flight_number_f')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('flight_number_f')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
										
										
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Pnr No
											</label>
											
											<input type="text" class="form-control m-input" value="<?php echo e(old('pnr_no')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Pnr No" name="pnr_no"  >
											<?php if($errors->has('pnr_no')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('pnr_no')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Departure Date
											</label>
											
											<input type="text" class="form-control m-input" id="m_datepicker_1" name="departure_date_f" readonly="" placeholder="Select date">
											<?php if($errors->has('departure_date_f')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('departure_date_f')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Departure Time
											</label>
											<div class="input-group timepicker" >
												<input type="text" class="form-control m-input" id="m_timepicker_2" readonly="" name="departure_time_f" placeholder="Select time">
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											<?php if($errors->has('departure_time_f')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('departure_time_f')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Arrival Time
											</label>
											<div class="input-group timepicker" id="">
												<input type="text" class="form-control m-input"  id="m_timepicker_2" readonly="" name="arrival_time_f" placeholder="Select time">
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											<?php if($errors->has('arrival_time_f')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('arrival_time_f')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
								</div>
								
								<div class="m--padding-20"></div>
								<strong style="font-weight: 600;margin-left: 28px;">Flight Details 2</strong>
								<div class="m--padding-15"></div>
								<div class="row">
									<div class="col-md-6">
										<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Source
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="source" name="source_s">
												<option value="">Select Flight Source</option>
												<?php $__currentLoopData = $flightSource; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flightSources): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option value="<?php echo e($flightSources->id); ?>"><?php echo e($flightSources->flight_source); ?></option>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
											
											<?php if($errors->has('source_s')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('source_s')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Destination
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="destination" name="destination_s">
												<option value="">Select Flight Destination</option>
												<?php $__currentLoopData = $flightDestination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flightDestinations): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option value="<?php echo e($flightDestinations->id); ?>"><?php echo e($flightDestinations->flight_destination); ?></option>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
											
											<?php if($errors->has('destination_s')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('destination_s')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Name
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="flight" name="flight_name_s">
												<option value="">Select Flight</option>
												<?php $__currentLoopData = $flight; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flights): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option value="<?php echo e($flights->id); ?>"><?php echo e($flights->flight_name); ?></option>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
											
											<?php if($errors->has('flight_name_s')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('flight_name_s')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
										
										
									</div>
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Number
											</label>
											
											<input type="text" class="form-control m-input" value="<?php echo e(old('flight_number_s')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Flight Number" name="flight_number_s"  >
											<?php if($errors->has('flight_number_s')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('flight_number_s')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
										
										
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Pnr No
											</label>
											
											<input type="text" class="form-control m-input" value="<?php echo e(old('pnr_no2')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Pnr No" name="pnr_no2"  >
											<?php if($errors->has('pnr_no2')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('pnr_no2')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Departure Date
											</label>
											
											<input type="text" class="form-control m-input" id="m_datepicker_2" name="departure_date_s" readonly="" placeholder="Select date">
											<?php if($errors->has('departure_date_s')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('departure_date_s')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Departure Time
											</label>
											<div class="input-group timepicker" >
												<input type="text" class="form-control m-input" id="m_timepicker_2" readonly="" name="departure_time_s" placeholder="Select time">
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											<?php if($errors->has('departure_time_s')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('departure_time_s')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Arrival Time
											</label>
											<div class="input-group timepicker" id="">
												<input type="text" class="form-control m-input"  id="m_timepicker_2" readonly="" name="arrival_time_s" placeholder="Select time">
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											<?php if($errors->has('arrival_time_s')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('arrival_time_s')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<div class="m--padding-20"></div>
								<strong style="font-weight: 600;margin-left: 28px;">Flight Details 3</strong>
								<div class="m--padding-15"></div>
								<div class="row">
									<div class="col-md-6">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Source
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="source" name="source_t">
												<option value="">Select Flight Source</option>
												<?php $__currentLoopData = $flightSource; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flightSources): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option value="<?php echo e($flightSources->id); ?>"><?php echo e($flightSources->flight_source); ?></option>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
											
											<?php if($errors->has('source_t')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('source_t')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Destination
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="destination" name="destination_t">
												<option value="">Select Flight Destination</option>
												<?php $__currentLoopData = $flightDestination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flightDestinations): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option value="<?php echo e($flightDestinations->id); ?>"><?php echo e($flightDestinations->flight_destination); ?></option>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
											
											<?php if($errors->has('destination_t')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('destination_t')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Name
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="flight" name="flight_name_t">
												<option value="">Select Flight</option>
												<?php $__currentLoopData = $flight; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flights): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<option value="<?php echo e($flights->id); ?>"><?php echo e($flights->flight_name); ?></option>
												<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</select>
											
											<?php if($errors->has('flight_name_t')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('flight_name_t')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
										
										
									</div>
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Number
											</label>
											
											<input type="text" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" value="<?php echo e(old('flight_number_t')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Flight Number" name="flight_number_t"  >
											<?php if($errors->has('flight_number_t')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('flight_number_t')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
										
										
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Pnr No
											</label>
											
											<input type="text" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" value="<?php echo e(old('pnr_no3')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Pnr No" name="pnr_no3"  >
											<?php if($errors->has('pnr_no3')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('pnr_no3')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Departure Date
											</label>
											
											<input type="text" class="form-control m-input" id="m_datepicker_3" name="departure_date_t" readonly="" placeholder="Select date">
											<?php if($errors->has('departure_date_t')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('departure_date_t')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Departure Time
											</label>
											<div class="input-group timepicker" >
												<input type="text" class="form-control m-input" id="m_timepicker_2" readonly="" name="departure_time_t" placeholder="Select time">
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											<?php if($errors->has('departure_time_t')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('departure_time_t')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Arrival Time
											</label>
											<div class="input-group timepicker" id="">
												<input type="text" class="form-control m-input"  id="m_timepicker_2" readonly="" name="arrival_time_t" placeholder="Select time">
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											<?php if($errors->has('arrival_time_t')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('arrival_time_t')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
								</div>
								
								<br>
								<br>
								
								<div class="row">
									<div class="col-md-6">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Price
											</label>
											
											<input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" placeholder="Enter Price" name="flight_price"  >
											<?php if($errors->has('flight_price')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('flight_price')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-md-6">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Seat
											</label>
											
											<input type="text" class="form-control m-input" value="<?php echo e(old('seat')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Seat" name="seat"  >
											<?php if($errors->has('seat')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('seat')); ?></strong>
											</span>
											<?php endif; ?>
										</div>
									</div>
									
								</div>
							</div>
							<div class="m-portlet__foot m-portlet__foot--fit">
								<div class="m-form__actions">
									<button type="submit" class="btn btn-primary">
									Submit
									</button>
									<a href="<?php echo e(url('supplier-FlightMultiCity')); ?>" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
						</form>
						<!--end::Form-->
					</div>
					<!--end::Portlet-->
					
					
					
				</div>
				
				<!--end::Portlet-->
				
			</div>
		</div>
		<!--end:: Widgets/Stats-->
		
	</div>
</div>
<!-- end::Body -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js')); ?>" type="text/javascript"></script>
<script>
$('#m_datepicker_1').datepicker({
format: 'yyyy-mm-dd',
				todayHighlight:	true,
startDate:new Date(),
});
$('#m_datepicker_2').datepicker({
format: 'yyyy-mm-dd',
				todayHighlight:	true,
startDate:new Date(),
});
$('#m_datepicker_3').datepicker({
format: 'yyyy-mm-dd',
				todayHighlight:	true,
startDate:new Date(),
})
</script>
<script type="text/javascript">
</script>
<!--begin::Page Vendors -->
<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="<?php echo e(asset('assets/app/js/dashboard.js')); ?>" type="text/javascript"></script>
<!--end::Page Snippets -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>