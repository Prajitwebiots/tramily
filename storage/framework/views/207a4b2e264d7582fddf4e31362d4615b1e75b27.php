<!DOCTYPE html><!-- 
<html lang="en" >
	<!-- begin::Head -->
    <?php echo $__env->make('adminlayouts.dashHead', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<!-- end::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
		
		<?php echo $__env->make('adminlayouts.dashHeader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		<?php echo $__env->make('adminlayouts.dashSidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		</div>
		<div class="m-grid__item m-grid__item--fluid m-wrapper">
			<?php echo $__env->yieldContent('content'); ?>
		</div>
		</div>
		<?php echo $__env->make('adminlayouts.dashFooter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
		
		<!-- end:: Page -->
		<?php echo $__env->make('adminlayouts.dashFooterContent', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
		
		</div>
		<?php echo $__env->yieldContent('bottom_script'); ?>
	</body>

</html>