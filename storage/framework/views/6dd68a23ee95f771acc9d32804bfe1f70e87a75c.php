
<?php $__env->startSection('title'); ?> User Change Password <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- begin::Body -->

<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<!-- BEGIN: Left Aside -->
	<button class="m-aside-left-close m-aside-left-close--skin-light" id="m_aside_left_close_btn">
	<i class="la la-close"></i>
	</button>
	
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					My Profile
					</h3>
				</div>
				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-lg-4">
					<div class="m-portlet m-portlet--full-height  ">
						<div class="m-portlet__body">
							<div class="m-card-profile">
								<div class="m-card-profile__title m--hide">
									Your Profile
								</div>
								<div class="m-card-profile__pic">
									<div class="m-card-profile__pic-wrapper">
										<img src="<?php echo e(URL::asset('assets/images/user')); ?>/<?php echo e(Sentinel::getUser()->profile); ?>" alt=""/>
									</div>
								</div>
								<div class="m-card-profile__details">
									<span class="m-card-profile__name">
										<?php echo e(Sentinel::getUser()->first_name); ?> <?php echo e(Sentinel::getUser()->last_name); ?>

									</span>
									<a href="" class="m-card-profile__email m-link">
										<?php echo e(Sentinel::getUser()->email); ?>

									</a>
								</div>
							</div>
							<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
								<li class="m-nav__separator m-nav__separator--fit"></li>
								<li class="m-nav__section m--hide">
									<span class="m-nav__section-text">
										Section
									</span>
								</li>
								<li class="m-nav__item">
									<a href="<?php echo e(url('user/profile')); ?>" class="m-nav__link">
										<i class="m-nav__link-icon flaticon-profile-1"></i>
										<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-nav__link-text">
													My Profile
												</span>
												
											</span>
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="<?php echo e(url('user/changePass')); ?>" class="m-nav__link">
										<i class="m-nav__link-icon flaticon-lock"></i>
										<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-nav__link-text">
													Change Password
												</span>
												
											</span>
										</span>
									</a>
								</li>

								
							</ul>
							
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
						<div class="m-portlet__head">
							<div class="m-portlet__head-tools">
								<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
											<i class="flaticon-share m--hide"></i>
											Change Password
										</a>
									</li>
									
								</ul>
							</div>
							
						</div>
						<div class="tab-content">
							<div class="tab-pane active" id="m_user_profile_tab_1">
								<form  action=" <?php echo e(url('user/update-password')); ?>/<?php echo e(Sentinel::getUser()->id); ?>" method="post" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
									<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
									<input type="hidden" name="user_id" value="<?php echo e(Sentinel::getUser()->id); ?>">
									<br>
									<?php if(session('error')): ?>
									
									<div class="alert alert-danger">
										<?php echo e(session('error')); ?>

									</div>
									<?php endif; ?>
									<?php if(session('success')): ?>
									
									<div class="alert alert-success">
										<?php echo e(session('success')); ?>

									</div>
									<?php endif; ?>
									<div class="m-portlet__body">

										<div class="form-group m-form__group row">
											<div class="col-10 ml-auto">
												<h3 class="m-form__section">
												1. Change Password
												</h3>
											</div>
										</div>
										<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Old Password
										</label>
										<div class="col-7">
											<input class="form-control m-input" name="old_password" type="password" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input"  class="col-2 col-form-label" >
											New Password
										</label>
										<div class="col-7">
											<input class="form-control m-input" name="new_password" type="password" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input"  class="col-2 col-form-label" >
											Re-Type New Password
										</label>
										<div class="col-7">
											<input class="form-control m-input" name="confirm_password" type="password" >
										</div>
									</div>
									
										
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions">
											<div class="row">
												<div class="col-2"></div>
												<div class="col-7">
													<button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
													Save changes
													</button>
													&nbsp;&nbsp;
													<button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
													Cancel
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane active" id="m_user_profile_tab_2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- end::Body -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<!--begin::Page Vendors -->
<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="<?php echo e(asset('assets/app/js/dashboard.js')); ?>" type="text/javascript"></script>
<!--end::Page Snippets -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>