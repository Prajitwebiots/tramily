<?php $__env->startSection('title'); ?> Supplier | Edit Multi City Flight <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css\bootstrap-clockpicker.css')); ?>">
	<style>
		'*'{ color: red;
		}
	</style>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					Multi City Flight
					</h3>
				</div>
			</div>
		</div>
		<div class="m-content">
			
			<?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div>   <br><?php endif; ?>
			<?php if(session('success')): ?>   <br><div class="alert alert-success"><?php echo e(session('success')); ?></div>   <br><?php endif; ?>
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tab">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<span class="m-portlet__head-icon m--hide">
										<i class="la la-gear"></i>
									</span>
									<h3 class="m-portlet__head-text">
									Edit Flight
									</h3>
								</div>
							</div>
						</div>
						<!--begin::Form-->
						<?php echo e(Form::model($flightCity, array('route' => array('supplier-FlightMultiCity.update', $flightCity->id), 'method' => 'PUT' , 'class' => 'm-form m-form--fit m-form--label-align-right' ))); ?>

						<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
						<?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div> <br><?php endif; ?>
						<?php if(session('success')): ?><br><div class="alert alert-success"><?php echo e(session('success')); ?></div> <br><?php endif; ?>
						<div class="m-portlet__body">
							<strong style="font-weight: 600;margin-left: 28px;">Flight Details 1</strong>
							<div class="m--padding-15"></div>
							<div class="row">
								<div class="col-md-6">
									
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
                                            Flight Source <span class="m--font-danger"> *</span>
										</label>
										<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="source" name="source_f">
											<option value="">Select Flight Source</option>
											<?php $__currentLoopData = $flightSource; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flightSources): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($flightSources->id); ?>" <?php if($flightSources->id == $flightCity->flight_source_f): ?> selected <?php endif; ?>><?php echo e($flightSources->flight_source); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
										
										<?php if($errors->has('source_f')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('source_f')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Flight Destination  <span class="m--font-danger"> <span class="m--font-danger"> *</span></span>
										</label>
										<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="destination" name="destination_f">
											<option value="">Select Flight Destination</option>
											<?php $__currentLoopData = $flightDestination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flightDestinations): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($flightDestinations->id); ?>" <?php if($flightDestinations->id == $flightCity->flight_destination_f): ?> selected <?php endif; ?>><?php echo e($flightDestinations->flight_destination); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
										
										<?php if($errors->has('destination_f')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('destination_f')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<br>
							<br>
							<div class="row">
								<div class="col-md-4">
									
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Flight Name  <span class="m--font-danger"> *</span>
										</label>
										<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="flight" name="flight_name_f">
											<option value="">Select Flight</option>
											<?php $__currentLoopData = $flight; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flights): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($flights->id); ?>" <?php if($flights->id == $flightCity->flight_name_f): ?> selected <?php endif; ?>><?php echo e($flights->flight_name); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
										
										<?php if($errors->has('flight_name_f')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('flight_name_f')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
									
									
								</div>
								<div class="col-md-4">
									
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Flight Number  <span class="m--font-danger"> *</span>
										</label>
										
										<input type="text" class="form-control m-input"  value="<?php echo e($flightCity->flight_number_f); ?>" value="<?php echo e(old('flight_number_f')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Flight Number" name="flight_number_f"  >
										<?php if($errors->has('flight_number_f')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('flight_number_f')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
									
									
								</div>
								<div class="col-md-4">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Pnr No  <span class="m--font-danger"> *</span>
										</label>
										
										<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_pnr_no); ?>" value="<?php echo e(old('pnr_no')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Pnr No" name="pnr_no"  >
										<?php if($errors->has('pnr_no')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('pnr_no')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<br>
							<br>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Departure Date  <span class="m--font-danger"> *</span>
										</label>
										
										<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_departure_date_f); ?>" id="m_datepicker_1" name="departure_date_f" readonly="" placeholder="Select date">
										<?php if($errors->has('departure_date_f')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('departure_date_f')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Departure Time  <span class="m--font-danger"> *</span>
										</label>
										<div class="input-group timepicker" >
											<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_departure_time_f); ?>" id="m_time_1" readonly="" name="departure_time_f" placeholder="Select time">
											<span class="input-group-addon">
												<i class="la la-clock-o"></i>
											</span>
										</div>
										<?php if($errors->has('departure_time_f')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('departure_time_f')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Arrival Time  <span class="m--font-danger"> *</span>
										</label>
										<div class="input-group timepicker" id="">
											<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_arrival_time_f); ?>" id="m_time_2" readonly="" name="arrival_time_f" placeholder="Select time">
											<span class="input-group-addon">
												<i class="la la-clock-o"></i>
											</span>
										</div>
										<?php if($errors->has('arrival_time_f')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('arrival_time_f')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
							</div>
							
							<div class="m--padding-20"></div>
							<strong style="font-weight: 600;margin-left: 28px;">Flight Details 2</strong>
							<div class="m--padding-15"></div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Flight Source  <span class="m--font-danger"> *</span>
										</label>
										<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="source" name="source_s">
											<option value="">Select Flight Source</option>
											<?php $__currentLoopData = $flightSource; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flightSources): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($flightSources->id); ?>" <?php if($flightSources->id == $flightCity->flight_source_s): ?> selected <?php endif; ?>><?php echo e($flightSources->flight_source); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
										
										<?php if($errors->has('source_s')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('source_s')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Flight Destination  <span class="m--font-danger"> *</span>
										</label>
										<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="destination" name="destination_s">
											<option value="">Select Flight Destination</option>
											<?php $__currentLoopData = $flightDestination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flightDestinations): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($flightDestinations->id); ?>" <?php if($flightDestinations->id == $flightCity->flight_destination_s): ?> selected <?php endif; ?>><?php echo e($flightDestinations->flight_destination); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
										
										<?php if($errors->has('destination_s')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('destination_s')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<br>
							<br>
							<div class="row">
								<div class="col-md-4">
									
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Flight Name  <span class="m--font-danger"> *</span>
										</label>
										<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="flight" name="flight_name_s">
											<option value="">Select Flight</option>
											<?php $__currentLoopData = $flight; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flights): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($flights->id); ?>" <?php if($flights->id == $flightCity->flight_name_s): ?> selected <?php endif; ?>><?php echo e($flights->flight_name); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
										
										<?php if($errors->has('flight_name_s')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('flight_name_s')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
									
									
								</div>
								<div class="col-md-4">
									
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Flight Number  <span class="m--font-danger"> *</span>
										</label>
										
										<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_number_s); ?>" value="<?php echo e(old('flight_number_s')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Flight Number" name="flight_number_s"  >
										<?php if($errors->has('flight_number_s')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('flight_number_s')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
									
									
								</div>
								<div class="col-md-4">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Pnr No
										</label>
										
										<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_pnr_no2); ?>" value="<?php echo e(old('pnr_no2')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Pnr No" name="pnr_no2"  >
										<?php if($errors->has('pnr_no2')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('pnr_no2')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<br>
							<br>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Departure Date  <span class="m--font-danger"> *</span>
										</label>
										
										<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_departure_date_s); ?>" id="m_datepicker_2" name="departure_date_s" readonly="" placeholder="Select date">
										<?php if($errors->has('departure_date_s')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('departure_date_s')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Departure Time  <span class="m--font-danger"> *</span>
										</label>
										<div class="input-group timepicker" >
											<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_departure_time_s); ?>" id="m_time_3" readonly="" name="departure_time_s" placeholder="Select time">
											<span class="input-group-addon">
												<i class="la la-clock-o"></i>
											</span>
										</div>
										<?php if($errors->has('departure_time_s')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('departure_time_s')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Arrival Time  <span class="m--font-danger"> *</span>
										</label>
										<div class="input-group timepicker" id="">
											<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_arrival_time_s); ?>" id="m_time_4" readonly="" name="arrival_time_s" placeholder="Select time">
											<span class="input-group-addon">
												<i class="la la-clock-o"></i>
											</span>
										</div>
										<?php if($errors->has('arrival_time_s')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('arrival_time_s')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<div class="m--padding-20"></div>
							<strong style="font-weight: 600;margin-left: 28px;">Flight Details 3</strong>
							<div class="m--padding-15"></div>
							<div class="row">
								<div class="col-md-6">
									
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Flight Source  <span class="m--font-danger"> *</span>
										</label>
										<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="source" name="source_t">
											<option value="">Select Flight Source</option>
											<?php $__currentLoopData = $flightSource; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flightSources): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($flightSources->id); ?>" <?php if($flightSources->id == $flightCity->flight_source_t): ?> selected <?php endif; ?>><?php echo e($flightSources->flight_source); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
										
										<?php if($errors->has('source_t')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('source_t')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Flight Destination  <span class="m--font-danger"> *</span>
										</label>
										<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="destination" name="destination_t">
											<option value="">Select Flight Destination</option>
											<?php $__currentLoopData = $flightDestination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flightDestinations): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($flightDestinations->id); ?>" <?php if($flightDestinations->id == $flightCity->flight_destination_t): ?> selected <?php endif; ?>><?php echo e($flightDestinations->flight_destination); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
										
										<?php if($errors->has('destination_t')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('destination_t')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<br>
							<br>
							<div class="row">
								<div class="col-md-4">
									
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Flight Name  <span class="m--font-danger"> *</span>
										</label>
										<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="flight" name="flight_name_t">
											<option value="">Select Flight</option>
											<?php $__currentLoopData = $flight; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flights): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<option value="<?php echo e($flights->id); ?>" <?php if($flights->id == $flightCity->flight_name_t): ?> selected <?php endif; ?>><?php echo e($flights->flight_name); ?></option>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
										
										<?php if($errors->has('flight_name_t')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('flight_name_t')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
									
									
								</div>
								<div class="col-md-4">
									
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Flight Number  <span class="m--font-danger"> *</span>
										</label>
										
										<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_number_t); ?>" value="<?php echo e(old('flight_number_t')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Flight Number" name="flight_number_t"  >
										<?php if($errors->has('flight_number_t')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('flight_number_t')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
									
									
								</div>
								<div class="col-md-4">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Pnr No
										</label>
										
										<input type="text" class="form-control m-input"  value="<?php echo e($flightCity->flight_pnr_no3); ?>" value="<?php echo e(old('pnr_no3')); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Pnr No" name="pnr_no3"  >
										<?php if($errors->has('pnr_no3')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('pnr_no3')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<br>
							<br>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Departure Date  <span class="m--font-danger"> *</span>
										</label>
										
										<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_departure_date_t); ?>" id="m_datepicker_3" name="departure_date_t" readonly="" placeholder="Select date">
										<?php if($errors->has('departure_date_t')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('departure_date_t')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Departure Time  <span class="m--font-danger"> *</span>
										</label>
										<div class="input-group timepicker" >
											<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_departure_time_t); ?>" id="m_time_5" readonly="" name="departure_time_t" placeholder="Select time">
											<span class="input-group-addon">
												<i class="la la-clock-o"></i>
											</span>
										</div>
										<?php if($errors->has('departure_time_t')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('departure_time_t')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Arrival Time  <span class="m--font-danger"> *</span>
										</label>
										<div class="input-group timepicker" id="">
											<input type="text" class="form-control m-input" value="<?php echo e($flightCity->flight_arrival_time_t); ?>" id="m_time_6" readonly="" name="arrival_time_t" placeholder="Select time">
											<span class="input-group-addon">
												<i class="la la-clock-o"></i>
											</span>
										</div>
										<?php if($errors->has('arrival_time_t')): ?>
										<span class="help-block text-danger">
											<strong><?php echo e($errors->first('arrival_time_t')); ?></strong>
										</span>
										<?php endif; ?>
									</div>
								</div>
							</div>
							
							<br>
							<br>
							
							<div class="row">
								<div class="col-md-4">

									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Supplier Price  / Admin Price <span class="m--font-danger"> *</span><code  id="pricechange" class="m-link m-link--state m-link--warning">Change</code>
										</label>
										<div class="row">
											<input type="text"  disabled class="form-control m-input col-md-4 m--margin-right-10" value="<?php echo e($flightCity->supplier_price); ?>" autocomplete="off" id="supplier_price" placeholder="Enter Price" name="flight_price"  >
											<input type="text"  disabled class="form-control m-input col-md-4 m--margin-right-10" value="<?php echo e($flightCity->flight_price); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Price" name="admin_price"  >

										</div>
										<?php if($errors->has('flight_price')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('flight_price')); ?></strong>
											</span>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-4">

									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">
											Seat <span class="m--font-danger"> *</span> <code  id="addseat" class="m-link m-link--state m-link--warning m--margin-right-10 ">Add </code><code  id="changeminus" class="m-link m-link--state m-link--warning m--margin-right-10 ">Minus </code>
										</label>
										<br><code>Totle Seat / Sold seat /Available Seat  </code>

										<div class="row m--margin-top-10">
											<input type="text" class="form-control m-input col-md-2 m--margin-right-10" disabled="" value="<?php echo e($flightCity->seat+$flightCity->sold); ?>" autocomplete="off" id="exampleInputText" >
											<input type="text" class="form-control m-input col-md-2 m--margin-right-10"  disabled="" value="<?php echo e($flightCity->sold); ?>" autocomplete="off" id="newseat" placeholder="add new  Seat" name="seat">
											<input type="text" class="form-control m-input col-md-2 m--margin-right-10"  disabled="" value="<?php echo e($flightCity->seat); ?>" autocomplete="off" id="newseat" placeholder="add new  Seat" >
											<i  id="sign" class="fa m--margin-right-5"></i>
											<input type="hidden" value="" id="seatcount" name="seatcount">
											<input type="text" class="form-control m-input col-md-2 m--margin-right-10" disabled=""  value="" autocomplete="off" name="available" id="totoleseat" >
										</div>

										<?php if($errors->has('seat')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('seat')); ?></strong>
											</span>
										<?php endif; ?>
										<?php if($errors->has('available')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('available')); ?></strong>
											</span>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-4">

									<div class="form-group m-form__group ">
										<label for="exampleInputEmail1" >
											Via
										</label>

										<input type="text" class="form-control m-input " value="<?php echo e($flightCity->flight_via); ?>" autocomplete="off" id="exampleInputText" placeholder="Enter Via" name="via"  >
										<?php if($errors->has('via')): ?>
											<span class="help-block text-danger">
												<strong><?php echo e($errors->first('via')); ?></strong>
											</span>
										<?php endif; ?>
									</div>
								</div>
								
							</div>
						</div>
						
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" class="btn btn-primary">
								Update
								</button>
								<a href="<?php echo e(url('supplier-FlightMultiCity')); ?>" class="btn btn-secondary">Cancel</a>
							</div>
						</div>
						<?php echo Form::close();; ?>

						<!--end::Form-->
					</div>
					<!--end::Portlet-->
					
					
					
				</div>
				
				<!--end::Portlet-->
				
			</div>
		</div>
		<!--end:: Widgets/Stats-->
		
	</div>



</div>
<!-- end::Body -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/jquery-clockpicker.js')); ?>" type="text/javascript"></script>

<script>
$('#m_datepicker_1').datepicker({
format: 'dd-mm-yyyy',
			todayHighlight:	true,
startDate:new Date(),
});
$('#m_datepicker_2').datepicker({
format: 'dd-mm-yyyy',
			todayHighlight:	true,
startDate:new Date(),
});
$('#m_datepicker_3').datepicker({
format: 'dd-mm-yyyy',
			todayHighlight:	true,
startDate:new Date(),
});
$('#newseat').on('keyup', function() {
    var totel = parseInt(<?php echo e($flightCity->seat); ?>) - parseInt($(this).val());
    $('#totoleseat').val(totel);

})

$('#m_datepicker_time_2').datetimepicker({
    use24hours: true,
    format: "hh:ii",
    showMeridian: !0,
    todayHighlight: !0,
    autoclose: !0,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0,
    pickerPosition: "bottom-left"

});
$('#m_datepicker_time_3').datetimepicker({
    format: "hh:ii",
    showMeridian: !0,
    todayHighlight: !0,
    autoclose: !0,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0,
    pickerPosition: "bottom-left"

});
$('#m_datepicker_time_4').datetimepicker({
    format: "hh:ii",
    showMeridian: !0,
    todayHighlight: !0,
    autoclose: !0,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0,
    pickerPosition: "bottom-left"

});
$('#m_datepicker_time_5').datetimepicker({
    format: "hh:ii",
    showMeridian: !0,
    todayHighlight: !0,
    autoclose: !0,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0,
    pickerPosition: "bottom-left"

});
$('#m_datepicker_time_6').datetimepicker({
    format: "hh:ii",
    showMeridian: !0,
    todayHighlight: !0,
    autoclose: !0,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0,
    pickerPosition: "bottom-left"

});
$('#m_datepicker_time_7').datetimepicker({
    format: "hh:ii",
    showMeridian: !0,
    todayHighlight: !0,
    autoclose: !0,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0,
    pickerPosition: "bottom-left"

});

</script>
<script> // for multicity
    var start = new Date();
    var end = new Date(new Date().setYear(start.getFullYear()+1));

    $('#m_datepicker_1').datepicker({

        startDate : start,
        endDate   : end
    }).on('changeDate', function(){
        $('#m_datepicker_2').datepicker('setStartDate',  $(this).val());
        $('#m_datepicker_3').datepicker('setStartDate',  $(this).val());

    });

    $('#m_datepicker_2').datepicker({
        startDate : start,
        endDate   : end
    }).on('changeDate', function(){
        $('#m_datepicker_1').datepicker('setEndDate',  $(this).val());
        $('#m_datepicker_2').datepicker('setStartDate',  $(this).val());
        $('#m_datepicker_3').datepicker('setStartDate',  $(this).val());

    });
    $('#m_datepicker_3').datepicker({
        startDate : start,
        endDate   : end
    }).on('changeDate', function(){
        $('#m_datepicker_2').datepicker('setStartDate',  $(this).val());
        $('#m_datepicker_2').datepicker('setEndDate',  $(this).val());
        $('#m_datepicker_1').datepicker('setEndDate',  $(this).val());
    });
    $('#m_time_1').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('#m_time_2').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('#m_time_3').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('#m_time_4').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('#m_time_5').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('#m_time_6').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    //        available seat  change
    $('#addseat').on('click', function() {
//            $('#totoleseat').prop('disabled', true);
        $('#totoleseat').prop('disabled', function(i, v) { return !v; });
        $('#seatcount').val("add");
        $('#sign').removeClass("fa-minus");
        $('#sign').addClass("fa-plus");
    });
    $('#changeminus').on('click', function() {
//            $('#totoleseat').prop('disabled', true);
        $('#totoleseat').prop('disabled', function(i, v) { return !v; });
        $('#seatcount').val("minus");
        $('#sign').removeClass("fa-plus");
        $('#sign').addClass("fa-minus");
    });
    //        price change
    $('#pricechange').on('click', function() {
//            $('#totoleseat').prop('disabled', true);
        $('#supplier_price').prop('disabled', function(i, v) { return !v; });
    });
    $('#m_datepicker_time_1').on('click', function() {
        $('#m_datepicker_time_1').val('');

    });


</script>

<script type="text/javascript">
</script>
<!--begin::Page Vendors -->
<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="<?php echo e(asset('assets/app/js/dashboard.js')); ?>" type="text/javascript"></script>
<!--end::Page Snippets -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>