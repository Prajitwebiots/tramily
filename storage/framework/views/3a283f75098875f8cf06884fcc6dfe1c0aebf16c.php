
<?php $__env->startSection('title'); ?> Admin |  Edit Users <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
    <style>
        '<span class="m--font-danger"> *</span>'{
            color: red;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Edit User
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Edit User
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        
        <div class="row">
            <div class="col-md-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                  
                    <form class="m-form m-form--fit m-form--label-align-right" action="<?php echo e(url('updateUserProfile',$user->id)); ?>" method="post" >
                        <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                        <?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div> <br><?php endif; ?>
                        <?php if(session('success')): ?><br><div class="alert alert-success"><?php echo e(session('success')); ?></div> <br><?php endif; ?>
                        <div class="m-portlet__body">
                            <div class="row">
                                <div class="col-md-6">
                                    
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Agency Name <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->agency_name); ?>" name="agency_name"  >
                                        <?php if($errors->has('agency_name')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('agency_name')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>

                                    <div class="form-group m-form__group ">
                                        <label for="example-text-input" class="col-form-label">
                                            Concerned Person Name <span class="m--font-danger"> *</span>
                                        </label>
                                        <div class="row">
                                        <div class="col-3">
                                            <select class="form-control m-select  " id="" name="title_name">
                                                <option <?php if('' == $user->title_name): ?> selected <?php endif; ?> value="">TITLE <span class="m--font-danger"><span class="m--font-danger"> *</span></span></option>
                                                <option  <?php if("Mr."== $user->title_name): ?> selected <?php endif; ?> value="Mr.">Mr.</option>
                                                <option <?php if( 'Ms.' == $user->title_name): ?> selected <?php endif; ?> value="Ms.">Ms.</option>
                                                <option <?php if('Mrs.' == $user->title_name): ?> selected <?php endif; ?> value="Mrs.">Mrs.</option>
                                            </select>
                                        </div>
                                        <div class="col-3">
                                            <input class="form-control m-input" type="text" value="<?php echo e($user->first_name); ?>" placeholder="First name " name="first_name" autocomplete="off">
                                        </div>
                                        <div class="col-3">
                                            <input class="form-control m-input" type="text" value="<?php echo e($user->last_name); ?>" placeholder="Last name " name="last_name" autocomplete="off">
                                        </div>
                                        <?php if($errors->has('title_name')): ?>
                                            <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('title_name')); ?></strong>
                                                </span>
                                        <?php endif; ?>
                                        <?php if($errors->has('first_name')): ?>
                                            <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('first_name')); ?></strong>
                                                </span>
                                        <?php endif; ?>
                                        <?php if($errors->has('last_name')): ?>
                                            <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('last_name')); ?></strong>
                                                </span>
                                        <?php endif; ?>
                                    </div>
                                        </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Agency Address <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <textarea class="form-control"  id="m_autosize_1" name="agency_address"  placeholder="Agency Address" ><?php echo e($user->agency_address); ?></textarea>
                                        <?php if($errors->has('agency_address')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('agency_address')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Mobile No <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->mobile); ?>" name="mobile"  >
                                        <?php if($errors->has('mobile')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('mobile')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group   m-form__group">
                                        <label for="example-text-input" >
                                            PAN no
                                        </label>

                                        <input class="form-control m-input" type="text" value="<?php echo e($user->pan_no); ?>" placeholder="PAN No" name="pan_no" autocomplete="off">

                                        <?php if($errors->has('pan_no')): ?>
                                            <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('pan_no')); ?></strong>
                                                </span>
                                        <?php endif; ?>
                                    </div>
                                    
                                    
                                    
                                    
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group m-form__group ">
                                        <label for="example-text-input"  >
                                            Country <span class="m--font-danger"> *</span>
                                        </label>
                                        <div class="col-8">
                                            <select class="form-control m-select  " id="country" name="country">

                                                <?php $__currentLoopData = $countrylist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option <?php if($user->country == $country->id ): ?> selected <?php endif; ?> value="<?php echo e($country->id); ?>"><?php echo e($country->country_name .' / '.$country->country_alphacode); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </select>
                                        </div>
                                        <?php if($errors->has('country')): ?>
                                            <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('country')); ?></strong>
                                                </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group ">
                                        <label for="example-text-input"  >
                                            State <span class="m--font-danger"> *</span>
                                        </label>
                                        <div class="col-8">
                                            <select class="form-control m-select  " id="state" name="state">

                                                <?php $__currentLoopData = $statelist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option <?php if($user->state == $state->id ): ?> selected  <?php endif; ?>  value="<?php echo e($state->id); ?>"><?php echo e($state->state_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </select>
                                        </div>
                                        <?php if($errors->has('state')): ?>
                                            <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('state')); ?></strong>
                                                </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group ">
                                        <label for="example-text-input"  >
                                            City <span class="m--font-danger"> *</span>
                                        </label>
                                        <div class="col-8">
                                            <select class="form-control m-select  " id="city" name="city">

                                                <?php $__currentLoopData = $citylist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option  <?php if($city->id == $user->city): ?>  selected  <?php endif; ?>  value="<?php echo e($city->id); ?>"><?php echo e($city->city_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </select>
                                        </div>
                                        <?php if($errors->has('city')): ?>
                                            <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('city')); ?></strong>
                                                </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Pincode <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->pincode); ?>" name="pincode"  >
                                        <?php if($errors->has('pincode')): ?>
                                            <span class="help-block text-danger">
                                                <strong><?php echo e($errors->first('pincode')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Office No <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->office_no); ?>" name="office_no"  >
                                        <?php if($errors->has('office_no')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('office_no')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>


                                </div>
                                
                            </div>
                            <div class="m--padding-15"></div><strong style="font-weight: 600;margin-left: 28px;">Agency GST Details</strong><div class="m--padding-15"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Name <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->gst_name); ?>" name="gst_full_name"  >
                                        <?php if($errors->has('gst_full_name')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('gst_full_name')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            State <span class="m--font-danger"> *</span>
                                        </label>

                                        <select class="form-control m-select  " id="state" name="gst_state">
                                            <option value="">State <span class="m--font-danger"><span class="m--font-danger"> *</span></span></option>
                                            <?php $__currentLoopData = $statelist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option  <?php if( $state->id == $user->gst_state): ?>  selected <?php endif; ?> value="<?php echo e($state->id); ?>"><?php echo e($state->state_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                        </select>
                                        <?php if($errors->has('gst_state')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('gst_state')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            GSTIN No <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="<?php echo e($user->gst_no); ?>" name="gst_no"  >
                                        <?php if($errors->has('gst_no')): ?>
                                        <span class="help-block text-danger">
                                            <strong><?php echo e($errors->first('gst_no')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            GST Address
                                        </label>
                                        <div class="col-8">
                                            <textarea class="form-control m-input m-input--solid" autocomplete="off"  id="exampleTextarea" name="gst_address" autocomplete="off" placeholder="GST Address" rows="3"><?php echo e($user->gst_address); ?></textarea>
                                        </div>
                                        <?php if($errors->has('agency_address')): ?>
                                            <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('agency_address')); ?></strong>
                                                </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label" >
                                            Phone Number
                                        </label>
                                        <div class="col-8">
                                            <input class="form-control m-input" name="gst_phone" autocomplete="off" type="text" value="<?php echo e($user->gst_phone); ?>">
                                        </div>
                                        <?php if($errors->has('gst_phone')): ?>
                                            <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('gst_phone')); ?></strong>
                                                </span>
                                        <?php endif; ?>
                                    </div>

                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label" >
                                            Registeres email id
                                        </label>
                                        <div class="col-8">
                                            <input class="form-control m-input" name="regi_gmail" autocomplete="off" type="text" value="<?php echo e($user->regi_gmail); ?>">
                                        </div>
                                        <?php if($errors->has('regi_gmail')): ?>
                                            <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('regi_gmail')); ?></strong>
                                                </span>
                                        <?php endif; ?>
                                    </div></div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button type="submit" class="btn btn-primary">
                                Submit
                                </button>
                                <a href="<?php echo e(url('admin/users')); ?>" class="btn btn-secondary">
                            Cancel
                            </a>
                                
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                
                
                
            </div>
            
            <!--end::Portlet-->
            
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
    <script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/autosize.js')); ?>" type="text/javascript"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>