<?php $__env->startSection('title'); ?> User | Search One Way Flight <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
	.que2a,.que2b,.que3a,.que3b{
		display: none;
	}
	ul.dropdown-menu.inner {
		max-height: 217px !important;
	}

</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					Flight Search - Round Trip
					</h3>
				</div>
				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-md-12">

					<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tabs m-portlet--brand m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
						<?php
								$request = Session::get('multicity', 'default');

						?>
						<div class="m-portlet__body">
							<div class="tab-content">
								
								<div class="tab-pane active  " id="m_tabs_7_1" role="tabpanel" aria-expanded="false">
									<!--begin::one way-->
									<!--begin::Form-->

									<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-primary alert-dismissible fade show" role="alert">
										<div class="m-alert__icon">
											<i class="la la-warning"></i>
										</div>
										<div class="m-alert__text">
											<strong>
											Search Result For,
											</strong>
											<br>


											Flying Source: <?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($fligth->id == @$request['flying_source']): ?>
												<?php echo e(@$fligth->flight_source); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											| Flying Destination:  <?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($fligth->id == $request['flying_destination']): ?> <?php echo e($fligth->flight_destination); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> |
											Departure:  <?php echo e($request['departure']); ?>

											<br>


											Flying Source : <?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($fligth->id == $request['flying_source2']): ?> <?php echo e($fligth->flight_source); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											| Flying Destination:  <?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($fligth->id == $request['flying_destination2']): ?> <?php echo e($fligth->flight_destination); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> |
											Departure:  <?php echo e($request['departure2']); ?>

											<br>

											<?php if(($request['flying_source3'] )&&( $request['flying_destination3']) ): ?>

											Flying Source : <?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($fligth->id == $request['flying_source3']): ?> <?php echo e($fligth->flight_source); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											| Flying Destination:  <?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <?php if($fligth->id == $request['flying_destination3']): ?> <?php echo e($fligth->flight_destination); ?> <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> |
											Departure:  <?php echo e($request['departure3']); ?>

											<?php endif; ?>

											
											
											




											| Adults: <?php echo e($request['adults']); ?>

										</div>
										<div class="m-alert__close">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
										</div>


									</div>
								<?php if(($showalert == 1) ): ?>
									
									<!-- <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-primary alert-dismissible fade show" role="alert">
											<div class="m-alert__icon">
													<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
													Sorry, We couldn't find blocks as per your Requirement, So Near by dates are Offered for your Reference
											</div>
											<div class="m-alert__close">
													<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
											</div>
									</div> -->
										<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
											<strong>
												Sorry ! We couldn't find blocks as per your Requirement, So Near by dates are Offered for your Reference..
											</strong>

										</div>
									<?php elseif(($showalert == 2) && count($flight) == 0): ?>
										<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
											<strong>
												Searched Result Not found.
											</strong>

										</div>
									<?php endif; ?>
									
									
									<div class="m-content">

										<div class="row">
											<div class="col-xl-12">
												<form method="post" action="<?php echo e(url('flights/multi-city-search')); ?>" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">

													<?php echo e(csrf_field()); ?>

													<div class="m-portlet__body">
														<div class="form-group m-form__group row">
															<div class="col-md-4 m--padding-10">
																<select class="form-control m-bootstrap-select m_selectpicker" name="flying_source"  title="Flying Source" placeholder="Flying Source" data-live-search="true">
																	<?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<option value="<?php echo e($fligth->id); ?>" <?php if($fligth->id == $request['flying_source']): ?> selected <?php endif; ?>><?php echo e($fligth->flight_source); ?></option>
																	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																</select>
																<span class="m-form__help">
																Select your flying source
														</span>
															</div>
															<div class="col-md-4 m--padding-10">
																<select class="form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination" data-live-search="true">
																	<?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<option value="<?php echo e($fligth->id); ?>" <?php if($fligth->id == $request['flying_destination']): ?> selected <?php endif; ?>><?php echo e($fligth->flight_destination); ?></option>
																	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																</select>
																<span class="m-form__help">
																Select your flying destination
														</span>
															</div>
															<div class="col-md-4 m--padding-10">
																<input type="text" class="form-control" value="<?php echo e($request['departure']); ?>" id="m_datepicker_4" placeholder="Departure" name="departure" readonly="" >
																<span class="m-form__help">
																	 Please select Departure date
																</span>
															</div>

															
															<div class="col-md-4 m--padding-10">
																<select class="form-control m-bootstrap-select m_selectpicker" name="flying_source2"  title="Flying Source" placeholder="Flying Source" data-live-search="true">
																	<?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<option value="<?php echo e($fligth->id); ?>" <?php if($fligth->id == $request['flying_source2']): ?> selected <?php endif; ?>><?php echo e($fligth->flight_source); ?></option>
																	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																</select>
																<span class="m-form__help">
																Select your flying source
														</span>
															</div>
															<div class="col-md-4 m--padding-10">
																<select class="form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination2" data-live-search="true">
																	<?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<option value="<?php echo e($fligth->id); ?>" <?php if($fligth->id == $request['flying_destination2']): ?> selected <?php endif; ?>><?php echo e($fligth->flight_destination); ?></option>
																	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																</select>
																<span class="m-form__help">
																Select your flying destination
														</span>
															</div>
															<div class="col-md-4 m--padding-10">
																<input type="text" class="form-control" id="m_datepicker_5" value="<?php echo e($request['departure2']); ?>" placeholder="Departure" name="departure2" readonly="" >
																<span class="m-form__help">
														Please select Departure date
													</span>
															</div>

															

															
															<div class="col-md-4 m--padding-10 hide  m--hide" >
																<select class=" form-control m-bootstrap-select m_selectpicker" name="flying_source3"  title="Flying Source" placeholder="Flying Source" data-live-search="true">
																	<?php $__currentLoopData = $source; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<option value="<?php echo e($fligth->id); ?>" <?php if($fligth->id == $request['flying_source3']): ?> selected <?php endif; ?>><?php echo e($fligth->flight_source); ?></option>
																	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																</select>
																<span class="m-form__help">
																Select your flying source
														</span>
															</div>
															<div class="col-md-4 m--padding-10 hide  m--hide">
																<select class=" form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination3" data-live-search="true">
																	<?php $__currentLoopData = $destination; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fligth): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<option value="<?php echo e($fligth->id); ?>" <?php if($fligth->id == $request['flying_destination3']): ?> selected <?php endif; ?>><?php echo e($fligth->flight_destination); ?></option>
																	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																</select>
																<span class="m-form__help">
																Select your flying destination
														</span>
															</div>
															<div class="col-md-4 m--padding-10 hide m--hide ">
																<input type="text" class="  form-control"  value="<?php echo e($request['departure3']); ?>" id="m_datepicker_6" placeholder="Departure" name="departure3" readonly="" >
																<span class="m-form__help">
														Please select Departure date
													</span>
															</div>

															
															<div class="col-md-4 m--padding-10">
																<button type="button" class=" bth-add btn btn-secondary add_city" ><i class="fa fa-plus"></i> Add City</button>
																<button type="button" class=" bth-remove m--hide btn btn-secondary remove_city" ><i class="fa fa fa-minus"></i> Remove City</button>
															</div>

															<div class="col-md-4 m--padding-10">
																<select  name="adults"  class="form-control m-bootstrap-select m_selectpicker">
																	<option value="1" selected > Adults </option>

																	<?php for($i=1;$i <=20;$i++): ?>
																		<option <?php if($i == $request['adults']): ?> selected <?php endif; ?>  value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
																	<?php endfor; ?>

																</select>
																<span class="m-form__help">
														Please select number of Adults
													</span>
															</div>
														</div>
														<div class="m--padding-10 row">
															<div class="col-md-11 m--align-right">
																<button type="submit" class="btn btn-primary">
																	Search
																</button>
																<button type="reset" class="btn btn-secondary">
																	Cancel
																</button>
															</div>
															<div class="col-md-1"></div>
														</div>
													</div>
												</form>


											</div>
											<hr>
											<div class="col-xl-12">
												<?php if(count($flight) > 0): ?>
												
												<div class="m--padding-10 m--padding-top-30 ">
													<!--begin: Datatable -->
													<table class="m-datatable" id="html_table" width="100%">
														<thead class="">
															<tr >
																<th>Date</th>
																<th>Flight Departure</th>
																<th>Flight Arrival</th>
																<th>Flight Number</th>
																<th>Price </th>
																<th>Action </th>
															</tr>
														</thead>
														<tbody>
																<?php $__currentLoopData = $flight; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filte): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<tr>
																<td>
																	<?php echo e('<center>'); ?><?php echo e(date_format(date_create($filte->flight_departure_date_f)," d - M - Y ")); ?> <?php echo e('</center>'); ?>

																	<?php echo e('<br>'); ?><?php echo e('<center>To</center>'); ?><?php echo e('<br>'); ?>

																	<?php echo e('<center>'); ?><?php echo e(date_format(date_create($filte->flight_departure_date_s)," d - M - Y ")); ?> <?php echo e('</center>'); ?>

																	<?php if($request['flying_destination3'] ||$request['flying_source3'] || $request['departure3']): ?>
																		<?php echo e('<br>'); ?><?php echo e('<center>To</center>'); ?><?php echo e('<br>'); ?>

																		<?php echo e('<center>'); ?><?php echo e(date_format(date_create($filte->flight_departure_date_t)," d - M - Y ")); ?> <?php echo e('</center>'); ?>

																	<?php endif; ?>
																</td>
																<td>
																	<?php echo e($filte->flightSourceF->flight_source); ?>

																	<?php echo e('<br>'); ?>

																	<?php echo e('<code>'); ?><?php echo e(date_format(date_create($filte->flight_departure_time_f)," h:i A")); ?><?php echo e('</code>'); ?>

																 <?php echo e('<span>----------------------</span>'); ?>

																 <?php echo e($filte->flightSourceS->flight_source); ?>

																	<?php echo e('<br>'); ?>

																	<?php echo e('<code>'); ?><?php echo e(date_format(date_create($filte->flight_departure_time_s)," h:i A")); ?><?php echo e('</code>'); ?>

																<?php if($request['flying_destination3'] ||$request['flying_source3'] || $request['departure3']): ?>
																	<?php echo e('<span>----------------------</span>'); ?>

																		<?php echo e($filte->flightSourceT->flight_source); ?>

																	<?php echo e('<br>'); ?>

																	<?php echo e('<code>'); ?><?php echo e(date_format(date_create($filte->flight_departure_time_t)," h:i A")); ?><?php echo e('</code>'); ?>


																<?php endif; ?>
																</td>

																<td>
																	<?php echo e($filte->flightDestinationF->flight_destination); ?>

																	<?php echo e('<br>'); ?>

																	<?php echo e('<code>'); ?><?php echo e(date_format(date_create($filte->flight_arrival_time_f)," h:i A")); ?><?php echo e('</code>'); ?>

																    <?php echo e('<span>----------------------</span>'); ?>

																	<?php echo e($filte->flightDestinationS->flight_destination); ?>

																	<?php echo e('<br>'); ?>

																	<?php echo e('<code>'); ?><?php echo e(date_format(date_create($filte->flight_arrival_time_s)," h:i A")); ?><?php echo e('</code>'); ?>

																	<?php if($request['flying_destination3'] ||$request['flying_source3'] || $request['departure3']): ?>
																		<?php echo e('<span>----------------------</span>'); ?>

																		<?php echo e($filte->flightDestinationT->flight_destination); ?>

																		<?php echo e('<br>'); ?>

																		<?php echo e('<code>'); ?><?php echo e(date_format(date_create($filte->flight_arrival_time_t)," h:i A")); ?><?php echo e('</code>'); ?>


																	<?php endif; ?>

																</td>
																<td>
																	<?php echo e('<code>'.$filte->flightF->flight_name.'-'.$filte->flightF->flight_code.'-'.$filte->flight_number_f.'</code>'); ?>

																 	<?php echo e('<span>----------------------</span>'); ?>

																	<?php echo e('<code>'.$filte->flightS->flight_name.'-'.$filte->flightS->flight_code.'-'.$filte->flight_number_s.'</code>'); ?>

																	<?php if($request['flying_destination3'] ||$request['flying_source3'] || $request['departure3']): ?>
																		<?php echo e('<span>----------------------</span>'); ?>

																		<?php echo e('<code>'.$filte->flightT->flight_name.'-'.$filte->flightT->flight_code.'-'.$filte->flight_number_t.'</code>'); ?>


																	<?php endif; ?>
																</td>
																<td>
																	<?php echo e('<i class="fa fa-rupee"> '.$filte->flight_price.'</i>'.' / Per Person'); ?><?php echo e('<br>'); ?>

																	<?php echo e('[Avail. Seats: '.$filte->seat.']'); ?>

																</td>
																<td>
																	<?php echo e(e( Form::open(['method' => 'post','url' => ['flights/multi-city-book'],'style'=>'display:inline']))); ?>

																	<?php if($request['flying_destination3'] == "" || $request['flying_source3']== "" || $request['departure3']== ""): ?>
																	<?php echo e(e('<input type="hidden" value="1" name="Third">')); ?>


																		<?php else: ?>
																	<?php echo e(e('<input type="hidden" value="0" name="Third">')); ?>

																	<?php endif; ?>
																	<?php echo e(e('<input type="hidden" value="'.$request['adults'].'" name="adults">')); ?>

																	<?php echo e(e('<input type="hidden" value="'.$filte->id.'" name="id">')); ?>

																	<?php echo e('<button type="submit"   class="btn btn-outline-brand m-btn m-btn--icon" title ="Book Now">
																		<span>
																			<i class="la la-envelope-o"></i>
																			<span>Book Now</span>
																		</span>
																	</button>'); ?>

																	<?php echo e(e( Form::close() )); ?>

																</td>
															</tr>
																<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</tbody>
													</table>
													<!--end: Datatable -->
												</div>
												
												<?php endif; ?>
												<?php if($showalert == 1 || $showalert == 2 ): ?>
												
												<div class="m--padding-10 m--padding-top-30 ">
													<div class="m-alert m-alert--icon alert alert-danger" role="alert">
														<div class="m-alert__icon">
															<i class="flaticon-danger"></i>
														</div>
														<div class="m-alert__text">
														<strong>If The Desired Results Doesn't Meet Your Requirement, Please Click on Send mail, Our Team shall help you to fulfill your Requirement</strong> </div>
														<div class="m-alert__actions" style="width: 220px;">
															<a href="<?php echo e(url('flights/emailmulticity')); ?>" class="btn btn-outline-light btn-sm m-btn m-btn--hover-brand" >
															<i class="fa fa-envelope"></i> Send Search Mail
															</a>
														</div>
													</div>
												</div>
												
												<?php endif; ?>
											</div>
										</div>
									</div>
									<!--end::Form-->
								</div>
								<!--end::one way-->
								
							</div>
						</div>
						<!--end::Portlet-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- end::Body -->
	<?php $__env->stopSection(); ?>

	<?php $__env->startSection('bottom_script'); ?>
	<!--begin::Page Vendors -->
	<script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js')); ?>" type="text/javascript"></script>
	
	<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
	<!--end::Page Vendors -->
	<!--begin::Page Snippets -->
	<script src="<?php echo e(asset('assets/app/js/dashboard.js')); ?>" type="text/javascript"></script>
	<!--end::Page Snippets -->
	<script>
	 $('#m_datepicker_1').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		 autoclose:true,
    });
    $('#m_datepicker_2').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		autoclose:true,
    });
    $('#m_datepicker_3').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		autoclose:true,
    });
    $('#m_datepicker_4').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		autoclose:true,
    });
    $('#m_datepicker_5').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		autoclose:true,
    });
    $('#m_datepicker_6').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		autoclose:true,
    });
     $(".add_city").click(function(){
         $( ".hide" ).removeClass('m--hide');
         $( ".bth-remove" ).removeClass('m--hide');
         $( ".bth-add" ).addClass('m--hide');
     });
     $(".bth-remove").click(function(){
         $( ".hide" ).addClass('m--hide');
         $( ".bth-remove" ).addClass('m--hide');
         $( ".bth-add" ).removeClass('m--hide');

     });

	<?php if($request['flying_destination3'] ||$request['flying_source3'] || $request['departure3']): ?>
		$(".bth-add").trigger("click");
	 <?php endif; ?>

	</script>

	<script> // for multicity
        var start = new Date();
        var end = new Date(new Date().setYear(start.getFullYear()+1));

        $('#m_datepicker_4').datepicker({
            startDate : start,
            endDate   : end
        }).on('changeDate', function(){
            $('#m_datepicker_5').datepicker('setStartDate',  $(this).val());
            $('#m_datepicker_6').datepicker('setStartDate',  $(this).val());

        });

        $('#m_datepicker_5').datepicker({
            startDate : start,
            endDate   : end
        }).on('changeDate', function(){
            $('#m_datepicker_4').datepicker('setEndDate',  $(this).val());
            $('#m_datepicker_5').datepicker('setStartDate',  $(this).val());
            $('#m_datepicker_6').datepicker('setStartDate',  $(this).val());

        });
        $('#m_datepicker_6').datepicker({
            startDate : start,
            endDate   : end
        }).on('changeDate', function(){
            $('#m_datepicker_5').datepicker('setStartDate',  $(this).val());
            $('#m_datepicker_5').datepicker('setEndDate',  $(this).val());
            $('#m_datepicker_4').datepicker('setEndDate',  $(this).val());
        });

	</script>
	<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>