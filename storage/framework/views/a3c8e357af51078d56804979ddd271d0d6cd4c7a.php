<?php $__env->startSection('title'); ?>Admin | Booking history <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Booking history
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                               Booking history
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <?php if(session('error')): ?><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
        <?php if(session('success')): ?><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
            <div class="m-portlet m-portlet--mobile">

                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <h4>One Way bookings</h4>
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--begin: Datatable -->
                    <table class="m-datatable" id="html_table" width="100%">
                        <thead>
                        <tr>
                            <th>Sr .no</th>
                            <th>Booking Date</th>
                            <th>Name</th>
                            <th>Sector</th>
                            <th>Travel Date</th>
                            <th>Amount</th>
                            <th>No. Seat</th>

                            <th>Ticket</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $i = 1; ?>

                        <?php $__currentLoopData = $history_oneway; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($i++); ?></td>
                                <td><?php echo e(date_format($key->created_at,' D d-M-Y H:i')); ?></td>
                                <td><?php echo e($key->user->title_name.' '.$key->user->first_name.' '.$key->user->last_name.' '); ?></td>
                                <td><?php echo e(@$key->oneway->flightSource->flight_source.' - '.@$key->oneway->flightdestination->flight_destination.' - '.@$key->oneway->flight_number); ?></td>
                                <td><?php echo e(date("D d-M-Y", strtotime($key->oneway->flight_departure))); ?> - <?php echo e(date("H:i A", strtotime($key->oneway->flight_departure_time))); ?></td>
                                <td><?php echo e($key->amount); ?></td>
                                <td><?php echo e(count($key->booking_details)); ?></td>
                                <td><?php echo e(e('<a  class="btn btn-primary btn-sm"  href="'.asset('assets/ticket'.'/'.$key->pdf).'">Ticket</button>')); ?></td>
                               
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>
                    <!--end: Datatable -->

                <?php $__currentLoopData = $history_oneway; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <!--begin::Modal-->
                        <div class="modal fade" id="m_modal_ticket<?php echo e($loop->iteration); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            Ticket Information

                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                                        </button>
                                    </div>


                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flying Source : <br><?php echo e($key->oneway->flightSource->flight_source); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flying Destination :<br> <?php echo e($key->oneway->flightDestination->flight_destination); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flight Name :<br> <?php echo e($key->oneway->flight->flight_name); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flight Number :<br> <?php echo e($key->oneway->flight->flight_code); ?>

											</span>
                                                </p>
                                            </div>

                                        </div>
                                        <div class="row m--padding-top-40">
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Departure Date :<br> <?php echo e(date("Y-M-d D", strtotime($key->oneway->flight_departure))); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Departure Time : <br><?php echo e(date("H:i A", strtotime($key->oneway->flight_departure_time))); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-2">
                                                <p>
											<span class="m--font-bolder">
												Arrival Time :<br> <?php echo e(date("H:i A", strtotime($key->oneway->flight_arrival))); ?>

											</span>
                                                </p>
                                            </div>

                                            <div class="col-md-2">
                                                <p>
                                                    <span class="m--font-bolder">
                                                        PNR No  : <?php echo e($key->oneway->flight_pnr_no); ?>

                                                    </span>
                                                </p>
                                            </div>
                                            <div class="col-md-2">
                                                <p>
                                                    <span class="m--font-bolder">
                                                        Via  : <?php echo e($key->oneway->flight_via); ?>

                                                    </span>
                                                </p>
                                            </div>


                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>

                                </div>
                            </div>
                            <!--end::Modal-->
                        </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <h4>Round Trip</h4>
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search_r">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--begin: Datatable -->
                    <table class="m-datatable_r"  width="100%">
                        <thead>
                        <tr>
                            <th>Sr .no</th>
                            <th>Booking Date</th>
                            <th>Name</th>
                            <th>Sector</th>
                            <th>Travel Date</th>
                            <th>Amount</th>
                            <th>No. seat</th>

                            <th>Ticket</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $i = 1; ?>

                        <?php $__currentLoopData = $history_round_trip; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($i++); ?></td>
                                <td><?php echo e(date_format($key->created_at,' D d-M-Y H:i')); ?></td>
                                <td><?php echo e($key->user->title_name.' '.$key->user->first_name.' '.$key->user->last_name.' '); ?></td>
                                <td>
                                    <?php echo e(@$key->round_trip->flightSource->flight_source.' - '.@$key->round_trip->flightdestination->flight_destination.' - '.@$key->round_trip->flight_number); ?>

                                    <?php echo e('<span>-------------------------------------</span>'); ?>

                                    <?php echo e(@$key->round_trip->flightSourceReturn->flight_destination.' - '.@$key->round_trip->flightDestinationReturn->flight_source.' - '.@$key->round_trip->return_flight_number); ?>

                                </td>
                                <td>
                                    <?php echo e(date("D d-M-Y", strtotime($key->round_trip->flight_departure_date))); ?> - <?php echo e(date("H:i A", strtotime($key->round_trip->flight_departure_time))); ?>

                                    <?php echo e('<span>-----------------------</span>'); ?>

                                    <?php echo e(date("D d-M-Y", strtotime($key->round_trip->return_departure_date))); ?> - <?php echo e(date("H:i A", strtotime($key->round_trip->return_departure_time))); ?>

                                </td>
                                <td><?php echo e($key->amount); ?></td>
                                <td><?php echo e(count($key->booking_details)); ?></td>
                                <td><?php echo e(e('<a  class="btn btn-primary btn-sm"  href="'.asset('assets/ticket'.'/'.$key->pdf).'">Ticket</button>')); ?></td>


                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>
                    <!--end: Datatable -->
                <?php $__currentLoopData = $history_round_trip; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <!--begin::Modal-->
                        <div class="modal fade" id="m_modal_r<?php echo e($loop->iteration); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            Members information
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <?php $__currentLoopData = $key->booking_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="m-list-timeline__item">
                                                <span class="m-list-timeline__badge m-list-timeline__badge--brand"></span>
                                                <span class="m-list-timeline__text">
																<?php echo e($loop->iteration.') '.$info->title .' '.$info->firstname .' '.$info->lastname); ?>

															</span>
                                            </div>
                                            <br>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Modal-->
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php $__currentLoopData = $history_round_trip; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <!--begin::Modal-->
                        <div class="modal fade" id="m_modal_ticket_r<?php echo e($loop->iteration); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            Ticket Information

                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flying Source :<br> <?php echo e($key->round_trip->flightSource->flight_source); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flying Destination :<br> <?php echo e($key->round_trip->flightDestination->flight_destination); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flight Name :<br> <?php echo e($key->round_trip->flight->flight_name); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flight Number :<br> <?php echo e($key->round_trip->flight->flight_code); ?>

											</span>
                                                </p>
                                            </div>

                                        </div>

                                        <div class="row m--padding-top-40">
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Departure Date :<br> <?php echo e(date("Y-M-d D", strtotime($key->round_trip->flight_departure))); ?>

											</span>
                                                </p>
                                            </div><div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Departure Time :<br> <?php echo e(date("H:i A", strtotime($key->round_trip->flight_departure_time))); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-4">
                                                <p>
											<span class="m--font-bolder">
												Arrival Time :<br> <?php echo e(date("H:i A", strtotime($key->round_trip->flight_arrival))); ?>

											</span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row m--padding-top-40"></div>
                                        <h5>Return Flight information</h5>
                                        <hr>


                                        <div class="row m--padding-top-40">
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Source :<br> <?php echo e($key->round_trip->flightSourceReturn->flight_destination); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Destination :<br> <?php echo e($key->round_trip->flightDestinationReturn->flight_source); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												 Name :<br> <?php echo e($key->round_trip->flightReturn->flight_name); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												 Number :<br> <?php echo e($key->round_trip->flightReturn->flight_code); ?>

											</span>
                                                </p>
                                            </div>

                                        </div>

                                        <div class="row m--padding-top-40">
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												 Departure Date :<br> <?php echo e(date("Y-M-d D", strtotime($key->round_trip->return_departure_date))); ?>

											</span>
                                                </p>
                                            </div><div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												 Departure Time :<br> <?php echo e(date("H:i A", strtotime($key->round_trip->return_arrival_time))); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Arrival Time :<br> <?php echo e(date("H:i A", strtotime($key->round_trip->return_flight_arrival))); ?>

											</span>
                                                </p>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="row m--padding-top-40">
                                            <?php if($key->round_trip->flight_pnr_no): ?>
                                                <div class="col-md-3 ">
                                                    <p>
											<span class="m--font-bolder">
												PNR No : <?php echo e($key->round_trip->flight_pnr_no); ?>

											</span>

                                                    </p>
                                                </div>
                                            <?php endif; ?>
                                            <?php if($key->round_trip->flight_pnr_no): ?>
                                                <div class="col-md-3 ">
                                                    <p>
												<span class="m--font-bolder">
													PNR No 2 : <?php echo e($key->round_trip->flight_pnr_no2); ?>

												</span>

                                                    </p>
                                                </div>
                                            <?php endif; ?>
                                            <div class="col-md-3">
                                                <p>
														<span class="m--font-bolder">
															Via  : <?php echo e($key->round_trip->flight_via); ?>

														</span>
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>

                                </div>
                            </div>
                            <!--end::Modal-->
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile">

                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <h4>Multi city Trip</h4>
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search_t">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--begin: Datatable -->
                    <table class="m-datatable_t"  width="100%">
                        <thead>
                        <tr>
                            <th>Sr .no</th>
                            <th>Booking Date</th>
                            <th>Name</th>
                            <th>Sector</th>
                            <th>Travel Date</th>
                            <th>Amount</th>
                            <th>No. Seat</th>

                            <th>Ticket</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $i = 1; ?>

                        <?php $__currentLoopData = $history_multicity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($i++); ?></td>
                                <td><?php echo e(date_format($key->created_at,' D d-M-Y H:i')); ?></td>
                                <td><?php echo e($key->user->title_name.' '.$key->user->first_name.' '.$key->user->last_name.' '); ?></td>
                                <td>
                                    <?php echo e(@$key->multicity->flightSourceF->flight_source.' - '.@$key->multicity->flightDestinationF->flight_destination.' - '.@$key->multicity->flight_number_f); ?>

                                    <?php echo e('<span>-------------------------------------</span>'); ?>

                                    <?php echo e(@$key->multicity->flightSourceS->flight_source.' - '.@$key->multicity->flightDestinationS->flight_destination.' - '.@$key->multicity->flight_number_s); ?>

                                    <?php echo e('<span>-------------------------------------</span>'); ?>

                                    <?php echo e(@$key->multicity->flightSourceT->flight_source.' - '.@$key->multicity->flightDestinationT->flight_destination.' - '.@$key->multicity->flight_number_t); ?>

                                </td>
                                <td>
                                    <?php echo e(date("D d-M-Y", strtotime($key->multicity->flight_departure_date_f))); ?> - <?php echo e(date("H:i A", strtotime($key->multicity->flight_departure_time_f))); ?>

                                    <?php echo e('<span>---------------------------</span>'); ?>

                                    <?php echo e(date("D d-M-Y", strtotime($key->multicity->flight_departure_date_s))); ?> - <?php echo e(date("H:i A", strtotime($key->multicity->flight_departure_time_s))); ?>

                                    <?php echo e('<span>---------------------------</span>'); ?>

                                    <?php echo e(date("D d-M-Y", strtotime($key->multicity->flight_departure_date_t))); ?> - <?php echo e(date("H:i A", strtotime($key->multicity->flight_departure_time_t))); ?>


                                </td>
                                <td><?php echo e($key->amount); ?></td>
                                <td><?php echo e(count($key->booking_details)); ?></td>
                                <td><?php echo e(e('<a  class="btn btn-primary btn-sm"  href="'.asset('assets/ticket'.'/'.$key->pdf).'">Ticket</button>')); ?></td>

                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>
                    <!--end: Datatable -->
                <?php $__currentLoopData = $history_multicity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <!--begin::Modal-->
                        <div class="modal fade" id="m_modal_t<?php echo e($loop->iteration); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            Members information
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <?php $__currentLoopData = $key->booking_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="m-list-timeline__item">
                                                <span class="m-list-timeline__badge m-list-timeline__badge--brand"></span>
                                                <span class="m-list-timeline__text">
																<?php echo e($loop->iteration.') '.$info->title .' '.$info->firstname .' '.$info->lastname); ?>

															</span>
                                            </div>
                                            <br>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Modal-->
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php $__currentLoopData = $history_multicity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <!--begin::Modal-->
                        <div class="modal fade" id="m_modal_ticket_t<?php echo e($loop->iteration); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            Ticket Information

                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row m--padding-top-10"></div>
                                        <h5> First Flight information</h5>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flying Source : <?php echo e($key->multicity->flightSourceF->flight_source); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flying Destination : <?php echo e($key->multicity->flightDestinationF->flight_destination); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flight Name : <?php echo e($key->multicity->flightF->flight_name); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flight Number : <?php echo e($key->multicity->flightF->flight_code); ?>

											</span>
                                                </p>
                                            </div>

                                        </div>

                                        <div class="row m--padding-top-40">
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Departure Date : <?php echo e(date("Y-M-d D", strtotime($key->multicity->flight_departure_date_f))); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Departure Time : <?php echo e(date("H:i A", strtotime($key->multicity->flight_departure_time_f))); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-4">
                                                <p>
											<span class="m--font-bolder">
												Arrival Time : <?php echo e(date("H:i A", strtotime($key->multicity->flight_arrival_time_f))); ?>

											</span>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="row m--padding-top-40"></div>
                                        <h5>Second Flight information</h5>
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flying Source : <?php echo e($key->multicity->flightSourceS->flight_source); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flying Destination : <?php echo e($key->multicity->flightDestinationS->flight_destination); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flight Name : <?php echo e($key->multicity->flightS->flight_name); ?>

											</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
											<span class="m--font-bolder">
												Flight Number : <?php echo e($key->multicity->flightS->flight_code); ?>

											</span>
                                                </p>
                                            </div>

                                        </div>

                                        <div class="row m--padding-top-40">
                                            <div class="col-md-3">
                                                <p>
												<span class="m--font-bolder">
													Departure Date : <?php echo e(date("Y-M-d D", strtotime($key->multicity->flight_departure_date_s))); ?>

												</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
												<span class="m--font-bolder">
													Departure Time : <?php echo e(date("H:i A", strtotime($key->multicity->flight_departure_time_s))); ?>

												</span>
                                                </p>
                                            </div>
                                            <div class="col-md-4">
                                                <p>
												<span class="m--font-bolder">
													Arrival Time : <?php echo e(date("H:i A", strtotime($key->multicity->flight_arrival_time_s))); ?>

												</span>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="row m--padding-top-40"></div>
                                        <h5> Third Flight information</h5>
                                        <hr>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <p>
												<span class="m--font-bolder">
													Flying Source : <?php echo e($key->multicity->flightSourceT->flight_source); ?>

												</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
												<span class="m--font-bolder">
													Flying Destination : <?php echo e($key->multicity->flightDestinationT->flight_destination); ?>

												</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
												<span class="m--font-bolder">
													Flight Name : <?php echo e($key->multicity->flightT->flight_name); ?>

												</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
												<span class="m--font-bolder">
													Flight Number : <?php echo e($key->multicity->flightT->flight_code); ?>

												</span>
                                                </p>
                                            </div>

                                        </div>

                                        <div class="row m--padding-top-40">
                                            <div class="col-md-3">
                                                <p>
												<span class="m--font-bolder">
													Departure Date : <?php echo e(date("Y-M-d D", strtotime($key->multicity->flight_departure_date_t))); ?>

												</span>
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>
												<span class="m--font-bolder">
													Departure Time : <?php echo e(date("H:i A", strtotime($key->multicity->flight_departure_time_t))); ?>

												</span>
                                                </p>
                                            </div>
                                            <div class="col-md-4">
                                                <p>
												<span class="m--font-bolder">
													Arrival Time : <?php echo e(date("H:i A", strtotime($key->multicity->flight_arrival_time_t))); ?>

												</span>
                                                </p>
                                            </div>
                                        </div>


                                        <hr>
                                        <div class="row m--padding-top-40">
                                            <?php if($key->multicity->flight_pnr_no): ?>
                                                <div class="col-md-2 ">
                                                    <p>
												<span class="m--font-bolder">
													PNR No : <?php echo e($key->multicity->flight_pnr_no); ?>

												</span>

                                                    </p>
                                                </div>
                                            <?php endif; ?>
                                            <?php if($key->multicity->flight_pnr_no2): ?>
                                                <div class="col-md-2 ">
                                                    <p>
													<span class="m--font-bolder">
														PNR No 2 : <?php echo e($key->multicity->flight_pnr_no2); ?>

													</span>

                                                    </p>
                                                </div>
                                            <?php endif; ?>
                                            <?php if($key->multicity->flight_pnr_no3): ?>
                                                <div class="col-md-2 ">
                                                    <p>
													<span class="m--font-bolder">
														PNR No 3 : <?php echo e($key->multicity->flight_pnr_no3); ?>

													</span>

                                                    </p>
                                                </div>
                                            <?php endif; ?>
                                            <div class="col-md-3">
                                                <p>
														<span class="m--font-bolder">
															Via  : <?php echo e($key->multicity->flight_via); ?>

														</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>

                                </div>
                            </div>

                        </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <!--end::Modal-->
                </div>

            </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>