
<?php $__env->startSection('title'); ?> User |booking history   <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
	.quMemberue2b,.que3a,.que3b{
		display: none;
	}
	.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- begin::Body -->

<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
						booking history
					</h3>
				</div>
			</div>
		</div>
		<div class="m-content">

			<?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div>   <br><?php endif; ?>
			<?php if(session('success')): ?>   <br><div class="alert alert-success"><?php echo e(session('success')); ?></div>   <br><?php endif; ?>

                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <h4>One Way bookings</h4>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <table class="m-datatable" id="html_table" width="100%">
                            <thead>
                            <tr>
                                <th>Sr .no</th>
                                <th>Payment type</th>
                                <th>Amount</th>
                                <th>Seat</th>
                                <th>Date/Time</th>
                                <th>e-ticket</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1; ?>

                            <?php $__currentLoopData = $history_oneway; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($i++); ?></td>
                                    <td><?php echo e($key->payment_type == 1 ? 'Wallet balance':''); ?></td>
                                    <td><?php echo e($key->amount); ?></td>
                                    <td><?php echo e($key->number_of_seat); ?></td>
                                    <td><?php echo e(date_format($key->created_at,' D d-M-Y h:i')); ?></td>
                                    
                                    <td><?php echo e(e('<a  class="btn btn-primary btn-sm"  href="'.asset('assets/ticket'.'/'.$key->pdf).'">Ticket</button>')); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </tbody>
                        </table>
                        <!--end: Datatable -->


                    </div>
                </div>
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <h4>Round Trip</h4>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search_r">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <table class="m-datatable_r"  width="100%">
                            <thead>
                            <tr>
                                <th>Sr .no</th>
                                <th>Payment type</th>
                                <th>Amount</th>
                                <th>Seat</th>
                                <th>Date/Time</th>
                                <th>Members</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1; ?>

                            <?php $__currentLoopData = $history_round_trip; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($i++); ?></td>
                                    <td><?php echo e($key->payment_type == 1 ? 'Wallet balance':''); ?></td>
                                    <td><?php echo e($key->amount); ?></td>
                                    <td><?php echo e($key->number_of_seat); ?></td>
                                    <td><?php echo e(date_format($key->created_at,' D d-M-Y h:i')); ?></td>

                                    <td><?php echo e(e('<a  class="btn btn-primary btn-sm"  href="'.asset('assets/ticket'.'/'.$key->pdf).'">Ticket</button>')); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </tbody>
                        </table>
                        <!--end: Datatable -->

                    </div>
                </div>
                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <h4>Multi city Trip</h4>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search_t">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <table class="m-datatable_t"  width="100%">
                            <thead>
                            <tr>
                                <th>Sr .no</th>
                                <th>Payment type</th>
                                <th>Amount</th>
                                <th>Seat</th>
                                <th>Date/Time</th>
                                <th>Members</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1; ?>

                            <?php $__currentLoopData = $history_multicity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($i++); ?></td>
                                    <td><?php echo e($key->payment_type == 1 ? 'Wallet balance':''); ?></td>
                                    <td><?php echo e($key->amount); ?></td>
                                    <td><?php echo e($key->number_of_seat); ?></td>
                                    <td><?php echo e(date_format($key->created_at,' D d-M-Y h:i')); ?></td>
                                    <td><?php echo e(e('<a  class="btn btn-primary btn-sm"  href="'.asset('assets/ticket'.'/'.$key->pdf).'">Ticket</button>')); ?></td>
                                    
                                     </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </tbody>
                        </table>
                        <!--end: Datatable -->


                    </div>

                </div>
			</div>
		<!--end:: Widgets/Stats-->

	</div>
</div>
<!-- end::Body -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>

<!--begin::Page Vendors -->
<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="<?php echo e(asset('assets/app/js/dashboard.js')); ?>" type="text/javascript"></script>
<!--end::Page Snippets -->
<script>
$( document ).ready(function() {
$(".m-checkbox").click(function(){
$(".collapse").addClass("show");
});
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>