
<footer class="footer">

        <div class="logos-container">
            <div class="row">
                <div class="col-4 col-md-4">
                    <a href="#" target="_blank">
                        <div class="box">
                            <div><img src="//cdn.coingaming.io/sportsbet/images/betting-tips-icon.svg">
                            </div>
                            <div>Betting Tips</div>
                        </div>
                    </a>
                </div>
                <div class="col-4 col-md-4">
                    <a href="#">
                        <div class="box">
                            <div><img src="//cdn.coingaming.io/sportsbet/images/live-betting-icon.svg">
                            </div>
                            <div>Live Betting</div>
                        </div>
                    </a>
                </div>
                <div class="col-4 col-md-4">
                    <a href="#">
                        <div class="box">
                            <div><img src="//cdn.coingaming.io/sportsbet/images/promotions-icon.svg">
                            </div>
                            <div>Promotions</div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="logos">
                <ul>
                    <li>
                        <div class="logo-bx">
                            <img src="<?php echo e(url::asset('assets/images/bitcoin-accepted.png')); ?>">
                        </div>
                    </li>
                    <li>
                        <div class="logo-bx">
                            <img src="<?php echo e(url::asset('assets/images/bitcoin-accepted.png')); ?>">
                        </div>
                    </li>
                    <li>
                        <div class="logo-bx">
                            <img src="<?php echo e(url::asset('assets/images/bitcoin-accepted.png')); ?>">
                        </div>
                    </li>
                    <li>
                        <div class="logo-bx">
                            <img src="<?php echo e(url::asset('assets/images/bitcoin-accepted.png')); ?>">
                        </div>
                    </li>
                    <li>
                        <div class="logo-bx">
                            <img src="<?php echo e(url::asset('assets/images/bitcoin-accepted.png')); ?>">
                        </div>
                    </li>
                </ul>

            </div>
            <div class="social-container">
                <div class="social-logos">
                    <a href="#" target="_blank"><img src="//cdn.coingaming.io/sportsbet/images/footer-live-chat-icon.png"></a>
                    <a href="#" target="_blank"><img src="//cdn.coingaming.io/sportsbet/images/footer-facebook-icon.png"></a>
                    <a href="#" target="_blank"><img src="//cdn.coingaming.io/sportsbet/images/footer-twitter-icon.png"></a>
                    <a href="#" target="_blank"><img src="//cdn.coingaming.io/sportsbet/images/footer-blog-icon.png"></a>
                </div>
            </div>
            <div class="footer-info">
                The site is powered by the <a href="#" target="_blank"><img src="//cdn.coingaming.io/sportsbet/images/coingaming-icon.png"></a> Bitcoin Casino Gaming Platform Provider. <br>
                Sportsbet.io is owned and operated by Luckbox Ent. Ltd. The provision and regulation of the casino games and sports events offered on Sportsbet is through mBet Solutions NV, who are licensed and regulated by the Government of Curacao under the <b><a href="<?php echo e(url('about/gaming_licence')); ?>" target="_blank">1668/JAZ</a></b>.
            </div>
            <div id="contentFooter">
                <h3>About Sportsbet.io</h3>
                <div>
                    <p>
                        Sportsbet.io is an online bitcoin sportsbook for bitcoin players only! You can deposit, play and withdraw
                        money with Bitcoins - the only currency accepted at Sportsbet.io. You can find your favourite teams and
                        sports to bet on right here. Our support and VIP department are top notch and will make sure your stay here
                        is awesome! </p>
                    <p>
                        You can join our bitcoin sportsbook instantly by registering a free account in no more than 15 seconds! Once
                        you have made your first deposit, make sure to check for ranking promotions and leaderboards as we will
                        always have them running! In addition you can participate in some pretty cool bitcoin lotteries and walk
                        away with some cash. </p>
                    <p>
                        Our list of sports feature the big ones like Soccer, Basketball, Ice Hockey, NFL, NBA, MLB, Boxing and more. You can also bet on eSports which is getting bigger and bigger every year. </p>
                    <p>
                        We hope you will enjoy your stay here at Sportsbet.io! </p>
                </div>

            </div>
        </div>
    </footer>