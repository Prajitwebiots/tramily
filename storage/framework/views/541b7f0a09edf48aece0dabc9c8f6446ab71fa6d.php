
<?php $__env->startSection('title'); ?> Admin | Edit Country <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Edit Country
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Edit Country
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <?php if(count($errors)): ?>
        <div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                
                <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                Country
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <?php echo e(Form::model($country, array('route' => array('admin-country.update', $country->id), 'method' => 'PUT' , 'class' => 'm-form m-form--fit m-form--label-align-right' ))); ?>

                    <div class="m-portlet__body">
                        <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                        
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Country Name
                            </label>
                            <input type="text" class="form-control m-input" autocomplete="off" value="<?php echo e($country->country_name); ?>" id="exampleInputText" name="country_name"  placeholder="Enter Country Name">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Country Code (Numberic)
                            </label>
                            <input type="text" class="form-control m-input" autocomplete="off" value="<?php echo e($country->country_numcode); ?>" id="exampleInputText" name="country_numcode"  placeholder="Enter Country Code">
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Country Code (Alpha)
                            </label>
                            <input type="text" class="form-control m-input" autocomplete="off" value="<?php echo e($country->country_alphacode); ?>" id="exampleInputText" name="country_alphacode"  placeholder="Enter Country Code">
                        </div>
                        
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-primary">
                            Submit
                            </button>
                            <button type="reset" class="btn btn-secondary">
                            Cancel
                            </button>
                        </div>
                    </div>
                    <?php echo Form::close();; ?>

                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                
                
                
            </div>
            
            <!--end::Portlet-->
            
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>