<!-- head -->
<?php $__env->startSection('title'); ?>
    Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?php echo e(URL::asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(URL::asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
           <!--  <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="index.html">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Deposits</span>
                    </li>
                </ul>
            </div> -->
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title">
                
            </h1>
            <!-- END PAGE TITLE-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject bold uppercase"> Deposit Requests</span>
                            </div>
                        </div>
                        <?php if(Session::has('success')): ?> <div class="alert alert-success"> <?php echo e(Session::get('success')); ?> </div> <?php endif; ?>
                        <div class="portlet-body">
                            <?php $i=1;?>
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                <thead>
                                <tr>
                                    <th> Sr No </th>
                                    <th> User Name</th>
                                    <th> BTC Address</th>
                                    <th> BTC Amount</th>
                                    <th> Status</th>
                                    <th> Actions </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $deposit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="odd gradeX">
                                        <td> <?php echo e($i++); ?> </td>
                                        <td> <?php echo e($key->user->first_name); ?> </td>
                                        <td> <?php echo e($key->user->btc_address); ?> </td>
                                        <td> <?php echo e($key->btc); ?> </td>
                                        <td> <?php if($key->status == 0): ?> Pending <?php else: ?> Approve <?php endif; ?> </td>
                                        <td> <a href="<?php echo e(url('deposit-approve/'.$key->id)); ?>" class="btn btn-xs yellow" ><i class="fa fa-edit"></i> Approve </a> </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
    </div>

    <!-- END CONTENT -->

<?php $__env->stopSection(); ?>
<!-- content -->

<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script_bottom'); ?>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-managed.min.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<!-- script -->

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>