  
<?php $__env->startSection('head'); ?>
    <link href="<?php echo e(URL::asset('assets/css/about.css')); ?>" rel="stylesheet" id="style_components" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="center-content">

        <div class="row">
            <div class="col-12  col-md-4 col-lg-3 nopadding">
                  <?php echo $__env->make('frontend.about.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  <!--sidebar-->
            </div>
            <div class="col-12  col-md-8 col-lg-9 nopadding">
                <div class="container inner-page">
                   <!--content-->
                   <?php echo $about->content; ?>



                </div>
              <!--   <div class="link-boxes">
                    <?php echo $__env->make('frontend.about.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
                </div> -->
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>