<?php $__env->startSection('title'); ?>Admin | Booking history <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Booking history
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                               Booking history
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <?php if(session('error')): ?><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
        <?php if(session('success')): ?><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
            <div class="m-portlet m-portlet--mobile">

                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <h4>One Way bookings</h4>
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--begin: Datatable -->
                    <table class="m-datatable" id="html_table" width="100%">
                        <thead>
                        <tr>
                            <th>Sr .no</th>
                            <th>Booking Date</th>
                            <th>Name</th>
                            <th>Sector</th>
                             <th>Flight</th>
                            <th>Travel Date</th>
                            <th>Amount</th>
                            <th>No. Seat</th>

                            <th>Ticket</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $i = 1; ?>

                        <?php $__currentLoopData = $history_oneway; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($i++); ?></td>
                                <td><?php echo e(date_format($key->created_at,' D d-M-Y H:i')); ?></td>
                                <td><?php echo e($key->user->title_name.' '.$key->user->first_name.' '.$key->user->last_name.' '); ?></td>
                                <td><?php echo e(@$key->oneway->flightSource->flight_source.' - '.@$key->oneway->flightdestination->flight_destination); ?></td>
                                  <td><?php echo e($key->oneway->flight->flight_name.'-'.$key->oneway->flight->flight_code.'-'.$key->oneway->flight_number); ?></td>
                                <td><?php echo e(date("D d-M-Y", strtotime($key->oneway->flight_departure))); ?></td>
                                <td><?php echo e($key->amount); ?></td>
                                <td><?php echo e(count($key->booking_details)); ?></td>
                                <td><?php echo e(e('<a  class="btn btn-primary btn-sm"  href="'.asset('assets/ticket'.'/'.$key->pdf).'">Ticket</button>')); ?></td>
                               
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>
                    <!--end: Datatable -->

               

                </div>
            </div>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <h4>Round Trip</h4>
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search_r">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--begin: Datatable -->
                    <table class="m-datatable_r"  width="100%">
                        <thead>
                        <tr>
                            <th>Sr .no</th>
                            <th>Booking Date</th>
                            <th>Name</th>
                            <th>Sector</th>
                             <th>Flight</th>
                            <th>Travel Date</th>
                            <th>Amount</th>
                            <th>No. seat</th>

                            <th>Ticket</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $i = 1; ?>

                        <?php $__currentLoopData = $history_round_trip; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($i++); ?></td>
                                <td><?php echo e(date_format($key->created_at,' D d-M-Y H:i')); ?></td>
                                <td><?php echo e($key->user->title_name.' '.$key->user->first_name.' '.$key->user->last_name.' '); ?></td>
                                <td>
                                    <?php echo e(@$key->round_trip->flightSource->flight_source.' - '.@$key->round_trip->flightdestination->flight_destination); ?>

                                    <?php echo e('<span>-------------------------------------</span>'); ?>

                                    <?php echo e(@$key->round_trip->flightSourceReturn->flight_destination.' - '.@$key->round_trip->flightDestinationReturn->flight_source); ?>

                                </td>
                                 <td>
                                     <?php echo e($key->round_trip->flight->flight_name.'-'.$key->round_trip->flight->flight_code.'-'.$key->round_trip->flight_number); ?>

                                        <?php echo e(e('<span>------------------</span>')); ?>

                                    <?php echo e($key->round_trip->flightReturn->flight_name.'-'.$key->round_trip->flightReturn->flight_code.'-'.$key->round_trip->return_flight_number); ?>

                                    </td>
                                <td>
                                    <?php echo e(date("D d-M-Y", strtotime($key->round_trip->flight_departure_date))); ?>

                                    <?php echo e('<span>------------------</span>'); ?>

                                    <?php echo e(date("D d-M-Y", strtotime($key->round_trip->return_departure_date))); ?> 
                                </td>
                                <td><?php echo e($key->amount); ?></td>
                                <td><?php echo e(count($key->booking_details)); ?></td>
                                <td><?php echo e(e('<a  class="btn btn-primary btn-sm"  href="'.asset('assets/ticket'.'/'.$key->pdf).'">Ticket</button>')); ?></td>


                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>
                    <!--end: Datatable -->
                
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile">

                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <h4>Multi city Trip</h4>
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search_t">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--begin: Datatable -->
                    <table class="m-datatable_t"  width="100%">
                        <thead>
                        <tr>
                            <th>Sr .no</th>
                            <th>Booking Date</th>
                            <th>Name</th>
                            <th>Sector</th>
                            <th>Flight</th>
                            <th>Travel Date</th>
                            <th>Amount</th>
                            <th>No. Seat</th>

                            <th>Ticket</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $i = 1; ?>

                        <?php $__currentLoopData = $history_multicity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($i++); ?></td>
                                <td><?php echo e(date_format($key->created_at,' D d-M-Y H:i')); ?></td>
                                <td><?php echo e($key->user->title_name.' '.$key->user->first_name.' '.$key->user->last_name.' '); ?></td>
                            
                                <td>
                                    <?php echo e(@$key->multicity->flightSourceF->flight_source.' - '.@$key->multicity->flightDestinationF->flight_destination); ?>

                                    <?php echo e('<span>-------------------------------------</span>'); ?>

                                    <?php echo e(@$key->multicity->flightSourceS->flight_source.' - '.@$key->multicity->flightDestinationS->flight_destination); ?>

                                    <?php echo e('<span>-------------------------------------</span>'); ?>

                                    <?php echo e(@$key->multicity->flightSourceT->flight_source.' - '.@$key->multicity->flightDestinationT->flight_destination); ?>

                                </td>
                                <td>
                                     <?php echo e($key->multicity->flightF->flight_name.'-'.$key->multicity->flightF->flight_code.'-'.$key->multicity->flight_number_f); ?>

                                     <?php echo e('<span>-------------------------------------</span>'); ?>

                                     <?php echo e($key->multicity->flightS->flight_name.'-'.$key->multicity->flightS->flight_code.'-'.$key->multicity->flight_number_s); ?>

                                     <?php echo e('<span>-------------------------------------</span>'); ?>

                                     <?php echo e(@$key->multicity->flightT->flight_name.'-'.@$key->multicity->flightT->flight_code.'-'.@$key->multicity->flight_number_t); ?>

                                     
                                </td>
                                <td>
                                    <?php echo e(date("D d-M-Y", strtotime($key->multicity->flight_departure_date_f))); ?> 
                                    <?php echo e('<span>-----------------------</span>'); ?>

                                    <?php echo e(date("D d-M-Y", strtotime($key->multicity->flight_departure_date_s))); ?> 
                                    <?php echo e('<span>----------------------</span>'); ?>

                                    <?php echo e(date("D d-M-Y", strtotime($key->multicity->flight_departure_date_t))); ?> 

                                </td>
                                <td><?php echo e($key->amount); ?></td>
                                <td><?php echo e(count($key->booking_details)); ?></td>
                                <td><?php echo e(e('<a  class="btn btn-primary btn-sm"  href="'.asset('assets/ticket'.'/'.$key->pdf).'">Ticket</button>')); ?></td>

                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>
                    <!--end: Datatable -->
                <?php $__currentLoopData = $history_multicity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <!--begin::Modal-->
                        <div class="modal fade" id="m_modal_t<?php echo e($loop->iteration); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            Members information
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <?php $__currentLoopData = $key->booking_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="m-list-timeline__item">
                                                <span class="m-list-timeline__badge m-list-timeline__badge--brand"></span>
                                                <span class="m-list-timeline__text">
																<?php echo e($loop->iteration.') '.$info->title .' '.$info->firstname .' '.$info->lastname); ?>

															</span>
                                            </div>
                                            <br>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Modal-->
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                <!--end::Modal-->
                </div>

            </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>