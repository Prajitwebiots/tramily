 <div class="sidebar">
                     <?php if(Request::segment(1)=='live-betting'): ?>
                    <div class="aside-wrap">
                        <div class="live-toggle">
                            <div data-handle="live" class="active">Live</div>
                            <div data-handle="upcoming">Upcoming</div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="minigame-container logged-out">



                    </div>

                    <h3 class="featured">Featured Sports </h3>
                    <ul class="sidebar-main-menu">
                        <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li class="top-level no-children ">
                            <aside class="accordion">
                            <h1><a href="<?php echo e(url('events/'.$cat->slug)); ?>" ><?php echo e($cat->name); ?></a></h1>
                            <div>
                                <?php $__currentLoopData = $cat->subcat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <h2><a href="<?php echo e(url('events/'.$cat->slug.'/'.$subcat->slug)); ?>" ><?php echo e($subcat->name); ?></a></h2>
                                <div><ul class="subsub">
                                    <?php $__currentLoopData = $subcat->subsubcat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subsub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e(url('events/'.$cat->slug.'/'.$subcat->slug.'/'.$subsub->slug)); ?>" ><?php echo e($subsub->name); ?></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                                </aside>
                        </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </ul>

                    <h3 class="other">Other Sports</h3>
                    <ul class="other-sports">

                        <li class="top-level">American Football</li>


                        <li class="top-level">Badminton</li>


                        <li class="top-level">Basketball</li>


                        <li class="top-level">Baseball</li>


                        <li class="top-level">Boxing</li>


                        <li class="top-level">Cricket</li>


                        <li class="top-level">Cycling</li>


                        <li class="top-level no-children">Floorball</li>


                        <li class="top-level">Futsal</li>


                        <li class="top-level no-children">Darts</li>


                        <li class="top-level">Golf</li>


                        <li class="top-level">Handball</li>


                        <li class="top-level">Ice Hockey</li>


                        <li class="top-level no-children">Snooker</li>


                        <li class="top-level">Rugby Union</li>


                        <li class="top-level">Soccer</li>


                        <li class="top-level">Tennis Men</li>


                        <li class="top-level">Tennis Women</li>


                        <li class="top-level">Volleyball Men</li>


                        <li class="top-level">Volleyball Women</li>



                    </ul>
                </div>