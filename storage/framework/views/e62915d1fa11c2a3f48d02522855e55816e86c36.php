 
<!-- head -->
<?php $__env->startSection('title'); ?>
    Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
<meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
  <link href="<?php echo e(URL::asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>
	
<?php $__env->startSection('content'); ?>
	<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
	    <div class="page-content" style="min-height: 1603px;">
	        <!-- BEGIN PAGE HEAD-->
	        <div class="page-head">
	            <!-- BEGIN PAGE TITLE -->
	            <div class="page-title">
	                <h1>Edit Event

	                </h1>
	            </div>
	            <!-- END PAGE TITLE -->

	        </div>
	        <!-- END PAGE HEAD-->
	        <!-- BEGIN PAGE BREADCRUMB -->
	        <ul class="page-breadcrumb breadcrumb">
	            <li>
	                <a href="index.html">Home</a>
	                <i class="fa fa-circle"></i>
	            </li>
	            <li>
	                <span class="active">Edit Event</span>
	            </li>
	        </ul>
	        <!-- END PAGE BREADCRUMB -->
	        <!-- BEGIN PAGE BASE CONTENT -->
	        <div class="row">
	            <div class="col-md-10 ">
	                <!-- BEGIN SAMPLE FORM PORTLET-->
	                <div class="portlet light bordered">
	                    <div class="portlet-title">
	                        <div class="caption font-blue-sunglo">
	                            <i class="icon-settings font-blue-sunglo"></i>
	                            <span class="caption-subject bold uppercase"> Edit Event</span>
	                        </div>

	                    </div>

	                

	                    <?php if(session('success')): ?>
	                    <div class="alert alert-success">
	                        <?php echo e(session('success')); ?>

	                    </div>
	                    <?php endif; ?>

<div class="portlet-body form">
     <?php echo e(Form::model($event, array('route' => array('admin-event.update', $event->id), 'method' => 'PUT'))); ?>

       <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
         <div class="form-body">

        	 <!--Event Name-->
          	  <div class="row">
                 <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?> col-md-8">
                    <label>Event Name</label> 
                  	<select name="EventName" id="eventmaster"  class="form-control">
                   		<option value="">Select Event</option>	
                   		<?php $__currentLoopData = $eventmaster; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ev): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   		<option value="<?php echo e($ev->id); ?>" <?php if($event->event_master_id == $ev->id): ?> selected <?php endif; ?>><?php echo e($ev->event_name); ?></option>
                   		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   	</select>
                    <?php if($errors->has('name')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('name')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div> 
        
                 </div> 

                  <!--Category Name-->
            	  <div class="row">
                <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?> col-md-8">
                    <label>Category Name</label> 
                  	<select name="catid" id="category"  class="form-control cateid">
                   		<option value="">Select Category</option>	
                   		<?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   		<option value="<?php echo e($cat->id); ?>" <?php if($event->subcatEvent->subcat->id == $cat->id): ?> selected <?php endif; ?>><?php echo e($cat->name); ?></option>
                   		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   	</select>
                    <?php if($errors->has('name')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('catname')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div> 
               
                 </div>


                 <!--Sub Category Name-->
            	  <div class="row">
                <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?> col-md-8">
                    <label>Sub Category Name</label> 
                  	<select name="scatid" id="sub_category"  class="form-control subcatid">
                   		<option value="">Select SubCategory</option>	
                   		<?php $__currentLoopData = $subcategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $scat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($scat->id); ?>" <?php if($event->subcatEvent->id == $scat->id): ?> selected <?php endif; ?>><?php echo e($scat->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                   	</select>
                    <?php if($errors->has('name')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('scatname')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div> 
            
                 </div>


                 <!--Sub Sub Category Name-->
            	  <div class="row">
                <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?> col-md-8">
                    <label>Sub Sub Category Name</label> 
                  	<select name="sscatid" id="sub_sub_category"  class="form-control subsubcatid">
                   		<option value="">Select SubSubCategory</option>	
                   		<?php $__currentLoopData = $subsubcategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sscat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($sscat->id); ?>" <?php if($event->subsubcatEvent->id == $sscat->id): ?> selected <?php endif; ?>><?php echo e($sscat->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                   	</select>
                    <?php if($errors->has('name')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('scatname')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div> 
               
                 </div>



                <!--Channel Id-->
                <div class="row">
                <div class="form-group <?php echo e($errors->has('channel_id') ? ' has-error' : ''); ?> col-md-8">
                    <label>Channel Id</label>
                    <input type="text" class="form-control" placeholder="Enter Channel id" name="channel_id" value="<?php echo e($event->channel_id); ?>">
                    <?php if($errors->has('channel_id')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('channel_id')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div>
              
            </div>


                 <!--Team 1-->
                 <div class="row">
                <div class="form-group <?php echo e($errors->has('team_1') ? ' has-error' : ''); ?> col-md-8">
                    <label>Team 1</label>
                   	<select name="team_1" id="team_1"  class="form-control team1">
                   		<option value="">Select Team</option>
                   		<?php $__currentLoopData = $teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($team->id); ?>" <?php if($event->Team1Event->id == $team->id): ?> selected <?php endif; ?>><?php echo e($team->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 	
                   	</select>
               		<?php if($errors->has('team_1')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('team_1')); ?></strong>
                       </span>
                   	<?php endif; ?>
                </div>
                  
                </div> 


                <!--Odds 1-->
                 <div class="row">
                <div class="form-group <?php echo e($errors->has('odds_1') ? ' has-error' : ''); ?> col-md-8">
                    <label>Odds 1</label>
                    <input type="number" step="0.01" class="form-control" placeholder="Enter Odds 1" name="odds_1" value="<?php echo e($event->odds); ?>">
                    <?php if($errors->has('odds_1')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('odds_1')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div>
              
                </div> 

                 <!--Team 2-->
                 <div class="row">
                <div class="form-group <?php echo e($errors->has('team_2') ? ' has-error' : ''); ?> col-md-8">
                    <label>Team 2</label>
                   	<select name="team_2" id="team_2"  class="form-control team2">
                   		<option value="">Select Team</option>	
                   		<?php $__currentLoopData = $teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($team->id); ?>" <?php if($event->Team2Event->id == $team->id): ?> selected <?php endif; ?>><?php echo e($team->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                   	</select>
               		<?php if($errors->has('team_2')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('team_2')); ?></strong>
                       </span>
                   	<?php endif; ?>
                </div>
               
                </div> 

                 <!--Odds 2-->
                  <div class="row">
                <div class="form-group <?php echo e($errors->has('odds_2') ? ' has-error' : ''); ?> col-md-8">
                    <label>Odds 2</label>
                    <input type="number" step="0.01" class="form-control" placeholder="Enter Odds 2" name="odds_2" value="<?php echo e($event->odds2); ?>">
                    <?php if($errors->has('odds_2')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('odds_2')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div>
                </div>

                <!--Drafts-->
                <div class="row">
                <div class="form-group <?php echo e($errors->has('drafts') ? ' has-error' : ''); ?> col-md-8">
                    <label>Draw</label>
                    <input type="number" step="0.01" class="form-control" placeholder="Enter Draw" name="draw" value="<?php echo e($event->draw); ?>">
                    <?php if($errors->has('drafts')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('drafts')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div>
                </div>

                <!--Scroll-->
                 <div class="row">
                <div class="form-group <?php echo e($errors->has('scroll') ? ' has-error' : ''); ?> col-md-8">
                    <label>Scroll</label>
                    <input type="number" step="0.01" class="form-control" placeholder="Enter Scroll" name="scroll" value="<?php echo e($event->scroll); ?>">
                    <?php if($errors->has('scroll')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('scroll')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div>
                 </div>

             	<!--EVENT DATE -->
                   <div class="row"> 
              	   <div class="form-group col-md-6">
                   <label>Event Date</label>
                   <div class="input-group input-large date-picker input-daterange" data-date="12/11/2017" data-date-format="mm/dd/yyyy">
                          <input type="text" class="form-control  datetimepicker"  id="start_date" name="start_date" value="<?php echo e($event->start_date); ?>" placeholder="Start Date">
                          <span class="input-group-addon"> to </span>
                          <input type="text" class="form-control datetimepicker" id="end_date" value="<?php echo e($event->end_date); ?>"  name="end_date" placeholder="End Date">
                    </div>
                          <span class="help-block"> Select date range </span>
                   </div>
                   </div>

                    <!--- live event -->
             <input type="checkbox" class="make-switch" name="live"  data-on-color="success" data-off-color="warning" <?php if($event->live == 1): ?>checked <?php endif; ?> >
        <span class="help-block"> If Event Is Live Please Turn ON </span>

                   </div>

                   </div>

            <div class="form-actions">
                <button type="submit" class="btn blue">Submit</button>
                  <a class="btn default" href="<?php echo e(route('admin-event.index')); ?>">Cancel</a>
            </div>
      <?php echo Form::close();; ?>

    </div>
	                </div>

	            </div>

	        </div>

	        <!-- END PAGE BASE CONTENT -->
	    </div>
	    <!-- END CONTENT BODY -->
	</div>



	                  
	                </div>

	            </div>

	        </div>

	        <!-- END PAGE BASE CONTENT -->
	    </div>
	    <!-- END CONTENT BODY -->
	</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
  <script src="<?php echo e(asset('assets/global/plugins/moment.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script_bottom'); ?>
<script>
	$('#start_date').datetimepicker({
        	format: 'yyyy-mm-dd HH:ii:ss'
   });
   $('#end_date').datetimepicker({
        	format: 'yyyy-mm-dd HH:ii:ss'
   });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>