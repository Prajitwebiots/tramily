<!-- head -->
<?php $__env->startSection('title'); ?>
    Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
	    <div class="page-content" style="min-height: 1603px;">
	        <!-- BEGIN PAGE HEAD-->
	        <div class="page-head">
	            <!-- BEGIN PAGE TITLE -->
	            <div class="page-title">
	                <h1>Edit Profile

	                </h1>
	            </div>
	            <!-- END PAGE TITLE -->

	        </div>
	        <!-- END PAGE HEAD-->
	        <!-- BEGIN PAGE BREADCRUMB -->
	        <ul class="page-breadcrumb breadcrumb">
	            <li>
	                <a href="index.html">Home</a>
	                <i class="fa fa-circle"></i>
	            </li>
	            <li>
	                <span class="active">Update Profile</span>
	            </li>
	        </ul>
	        <!-- END PAGE BREADCRUMB -->
	        <!-- BEGIN PROFILE PAGE BASE CONTENT -->
	        <div class="row">
	            <div class="col-md-6 ">
	                <!-- BEGIN SAMPLE FORM PORTLET-->
	                <div class="portlet light bordered">
	                    <div class="portlet-title">
	                        <div class="caption font-blue-sunglo">
	                            <i class="icon-settings font-blue-sunglo"></i>
	                            <span class="caption-subject bold uppercase"> Update Profile</span>
	                        </div>

	                    </div>

	                    <?php if(session('success')): ?>
	                    <div class="alert alert-success">
	                        <?php echo e(session('success')); ?>

	                    </div>
	                    <?php endif; ?> 
	                    <?php if(session('error')): ?>
	                    <div class="alert alert-danger">
	                        <?php echo e(session('error')); ?>

	                    </div>
	                    <?php endif; ?>


	                    <div class="portlet-body form">
	                      <?php echo e(Form::model($user, array('route' => array('profile.update', $user->id), 'method' => 'POST','files'=>'true'))); ?>

	                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
	                            <div class="form-body">
	                            	 <!--First  Name-->
	                                <div class="form-group <?php echo e($errors->has('first_name') ? ' has-error' : ''); ?>">
	                                    <label>First Name</label>
	                                    <input type="text" class="form-control" placeholder="Enter first name" name="first_name" value="<?php echo e($user->first_name); ?>">
	                                    <?php if($errors->has('first_name')): ?>
    				                       <span class="help-block">
    				                           <strong><?php echo e($errors->first('first_name')); ?></strong>
    				                       </span>
    				                   <?php endif; ?>
	                                </div> 
	                                 <!--Last  Name-->
	                                <div class="form-group <?php echo e($errors->has('last_name') ? ' has-error' : ''); ?>">
	                                    <label>Last Name</label>
	                                    <input type="text" class="form-control" placeholder="Enter Last name" name="last_name" value="<?php echo e($user->last_name); ?>">
	                                    <?php if($errors->has('last_name')): ?>
    				                       <span class="help-block">
    				                           <strong><?php echo e($errors->first('last_name')); ?></strong>
    				                       </span>
    				                   <?php endif; ?>
	                                </div> 
	                                 <!--Email-->
	                                <div class="form-group <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
	                                    <label> Email Address</label>
	                                    <input type="email" class="form-control"  value="<?php echo e($user->email); ?>" readonly="">
	                                </div> 
	                                <!--Mobile Number-->
	                                <div class="form-group <?php echo e($errors->has('mobile_number') ? ' has-error' : ''); ?>">
	                                    <label>Mobile Number</label>
	                                    <input type="number" class="form-control" placeholder="Enter Mobile Number" name="mobile_number" value="<?php echo e($user->mobile); ?>">
	                                    <?php if($errors->has('mobile_number')): ?>
    				                       <span class="help-block">
    				                           <strong><?php echo e($errors->first('mobile_number')); ?></strong>
    				                       </span>
    				                   <?php endif; ?>
	                                </div>
	                                <div class="form-group">
	                                    <label>Profile Image</label>
	                                    <?php if($user->profile): ?>
		                                    <?php if(file_exists(public_path('assets/images/profile/'.$user->profile))): ?>
	                                            <img src="<?php echo e(URL::asset('assets/images/profile')); ?>/<?php echo e($user->profile); ?>" id="profile" width="80px" height="80px" /> 
	                                        <?php else: ?>
	                                            <img src="<?php echo e(URL::asset('assets/images/profile/default.jpg')); ?>" height="80px" id="profile" width="80px" class="img-thumbnail" />
	                                        <?php endif; ?> 
                                         <?php else: ?>
                                            <img src="<?php echo e(URL::asset('assets/images/profile/default.jpg')); ?>" height="80px" id="profile" width="80px" class="img-thumbnail" />
                                        <?php endif; ?>

                                        <input type="hidden" name="old_profile" value="<?php echo e($user->profile); ?>" />
	                                    <input type="file" name="profile" class="form-control" onchange="readURL(this);" />

	                                </div> 
	                            </div>
	                            <div class="form-actions">
	                                <button type="submit" class="btn blue">Update</button>
	                                <button type="submit" class="btn default" >Cancel</button>
	                            </div>
	                         <?php echo e(Form::close()); ?>

	                    </div>
	                </div>

	            </div>

	            <div class="col-md-6 ">
	                <!-- BEGIN SAMPLE FORM PORTLET-->
	                <div class="portlet light bordered">
	                    <div class="portlet-title">
	                        <div class="caption font-blue-sunglo">
	                            <i class="icon-settings font-blue-sunglo"></i>
	                            <span class="caption-subject bold uppercase"> Change Password</span>
	                        </div>

	                    </div>

						<?php if(session('successpass')): ?>
	                    <div class="alert alert-success">
	                        <?php echo e(session('successpass')); ?>

	                    </div>
	                    <?php endif; ?> 
	                    <?php if(session('errorpass')): ?>
	                    <div class="alert alert-danger">
	                        <?php echo e(session('errorpass')); ?>

	                    </div>
	                    <?php endif; ?>

	                    <div class="portlet-body form">
	                      <?php echo e(Form::model($user, array('route' => array('password.update', $user->id), 'method' => 'POST'))); ?>

	                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
	                            <div class="form-body">
	                            	 <!--Old Password-->
	                                <div class="form-group <?php echo e($errors->has('old_password') ? ' has-error' : ''); ?>">
	                                    <label>Old Password</label>
	                                    <input type="password" class="form-control" placeholder="Enter Old Password" name="old_password" >
	                                    <?php if($errors->has('old_password')): ?>
    				                       <span class="help-block">
    				                           <strong><?php echo e($errors->first('old_password')); ?></strong>
    				                       </span>
    				                   <?php endif; ?>
	                                </div>  
	                                <!--New Password-->
	                                <div class="form-group <?php echo e($errors->has('new_password') ? ' has-error' : ''); ?>">
	                                    <label>New Password</label>
	                                    <input type="password" class="form-control" placeholder="Enter New Password" name="new_password" >
	                                    <?php if($errors->has('new_password')): ?>
    				                       <span class="help-block">
    				                           <strong><?php echo e($errors->first('new_password')); ?></strong>
    				                       </span>
    				                   <?php endif; ?>
	                                </div>  
	                                <!--Confirm Password-->
	                                <div class="form-group <?php echo e($errors->has('confirm_password') ? ' has-error' : ''); ?>">
	                                    <label>Confirm Password</label>
	                                    <input type="password" class="form-control" placeholder="Enter Confirm Password" name="confirm_password" >
	                                    <?php if($errors->has('confirm_password')): ?>
    				                       <span class="help-block">
    				                           <strong><?php echo e($errors->first('confirm_password')); ?></strong>
    				                       </span>
    				                   <?php endif; ?>
	                                </div> 
	                               
	                            </div>
	                            <div class="form-actions">
	                                <button type="submit" class="btn blue">Update</button>
	                             
	                            </div>
	                         <?php echo e(Form::close()); ?>

	                    </div>
	                </div>

	            </div>
	        </div>

	        <!-- END PROFILE PAGE BASE CONTENT -->
	    </div>
	    <!-- END CONTENT BODY -->
	</div>

<?php $__env->stopSection(); ?>
<!-- content -->

<?php $__env->startSection('script'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script_bottom'); ?>
  <script type="text/javascript">
      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  $('#profile')
                      .attr('src', e.target.result);
              };

              reader.readAsDataURL(input.files[0]);
          }
      }
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>