<?php $__env->startSection('title'); ?> Tramily | A Travel Family <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<link href="<?php echo e(URL::asset('assets/pages/css/login-2.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<style>
.m-login.m-login--1 .m-login__wrapper {
overflow: hidden;
padding: 0% 2rem 2rem 2rem !important;
}
.m-login.m-login--1 .m-login__aside {
    width: 35%;
}

</style>
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
<<<<<<< HEAD
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--singin" id="m_login">
        <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
=======
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--singin" id="m_login" style="">
        <!-- <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside"> -->
        <div class="col-md-4"></div>
        <div class="col-md-4">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
            <div class="m-stack m-stack--hor m-stack--desktop">
                <div class="m-stack__item m-stack__item--fluid">
                    <div class="m-login__wrapper">
                        <div class="m-login__logo">
                            <a href="#">
                                <img src="<?php echo e(URL::asset('assets/app/media/img/logos/logo-2.png')); ?>"  style="width: 35%;">
                            </a>
                        </div>
                        <div class="m-login__signin">
                            <div class="m-login__head">
                                <h3 class="m-login__title ">
                                Sign In To Tramily
                                </h3>
                            </div>


                            <form class="m-login__form m-form" action="<?php echo e(route('login.store')); ?>" method="post">
                                  <?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div><br><br><?php endif; ?>
                            <?php if(session('success')): ?><br><div class="alert alert-success"><?php echo e(session('success')); ?></div><br><br><?php endif; ?>
                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off">
                                </div>
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
                                </div>
                                <div class="row m-login__form-sub">
                                    <div class="col m--align-left">
                                        <label class="m-checkbox m-checkbox--focus">
                                            <input type="checkbox" name="remember">
                                            Remember me
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="col m--align-right">
                                        <a href="javascript:;" id="m_login_forget_password" class="m-link">
                                            Forget Password ?
                                        </a>
                                    </div>
                                </div>
                                <div class="m-login__form-action">
                                    <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                    Sign In
                                    </button>
                                </div>
                            </form>

                            <div class="m-stack__item m-stack__item--center">
                    <div class="m-login__account">
                        <span class="m-login__account-msg">
                            Don't have an account yet ?
                        </span>
                        &nbsp;&nbsp;
                        <a href="<?php echo e(route('register.index')); ?>" id="m_login_signup1" class="m-link m-link--focus m-login__account-link">
                            Sign Up
                        </a>
                    </div>
                </div>
                        </div>
                        <div class="m-login__signup">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                Sign Up
                                </h3>
                                <div class="m-login__desc">
                                    Enter your details to create your account:
                                </div>
                            </div>
                            <?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
                            <?php if(session('success')): ?><br><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>

                            <form class="m-login__form m-form" action="<?php echo e(route('register.store')); ?>" method="post">
                               <!--  <div class="m-alert m-alert--outline alert alert-danger alert-dismissible errorsignup" role="alert">            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>           <span></span></div> -->
                               <!-- <div class="m-alert m-alert--outline alert alert-danger alert-dismissible errorsignuphide" style="display:none"><ul class="errorsignup" ></ul></div> -->
                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                <div class="row">
                                    <span><br><b>Personal Details</b><br><br></span>
                                    <div class="col-xl-12">


                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Agency Name" name="agency_name" autocomplete="off">
                                          <?php if($errors->has('agency_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('agency_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>


                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Concerned Person Name" name="full_name" autocomplete="off">
                                        </div>
                                         <div class="form-group m-form__group">
                                            <textarea class="form-control m-input m-input--solid" id="exampleTextarea" name="agency_address" autocomplete="off" placeholder="Agency Address" rows="3"></textarea>
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off">
                                        </div>
                                         <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="City" name="city" autocomplete="off">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="State" name="state" autocomplete="off">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Country" name="country" autocomplete="off">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Pin Code" name="pincode" autocomplete="off">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="number" placeholder="Mobile No" name="mobile" autocomplete="off">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="number" placeholder="Office No" name="office_no" autocomplete="off">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="password" placeholder="Password" name="password" autocomplete="off">
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="password" placeholder="Confirm Password" name="confirm_password" autocomplete="off">
                                        </div>

                                    </div>


                                    <span><br><b>Agency GST Details</b><br><br></span>


                                     <div class="col-xl-12">
                                  <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="Name" name="gst_full_name" autocomplete="off">
                                        </div>

                                     <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="State Name" name="gst_state" autocomplete="off">
                                        </div>
                                          <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" placeholder="GSTIN No" name="gst_no" autocomplete="off">
                                        </div>
                                     </div>

                                </div>
                                <div class="row form-group m-form__group m-login__form-sub">
                                    <div class="col m--align-left">
                                        <label class="m-checkbox m-checkbox--focus">
                                            <input type="checkbox" name="agree">
                                            I Agree the
                                            <a href="#" class="m-link m-link--focus">
                                                terms and conditions
                                            </a>
                                            .
                                            <span></span>
                                        </label>
                                        <span class="m-form__help"></span>
                                    </div>
                                </div>
                                <div class="m-login__form-action">
                                    <button id="m_login_signup_submitt" type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                    Sign Up
                                    </button>
                                    <button id="m_login_signup_cancel" class="btn btn-outline-focus  m-btn m-btn--pill m-btn--custom">
                                    Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="m-login__forget-password">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                Forgotten Password ?
                                </h3>
                                <div class="m-login__desc">
                                    Enter your email to reset your password:
                                </div>
                            </div>
                            <form class="m-login__form m-form" action="<?php echo e(route('forgot.store')); ?>" method="post">
                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
                                </div>
                                <div class="m-login__form-action">
                                    <button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                    Request
                                    </button>
                                    <button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
                                    Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
         <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1  m-login__content" style="background-image: url(<?php echo e(URL::asset('assets/app/media/img/bg/bg-4.jpg')); ?>)">
            <div class="m-grid__item m-grid__item--middle">
                <h3 class="m-login__welcome">
                WELCOME TO TRAMILY
                </h3>
                <p class="m-login__msg">
                    The world is a book and those who do not travel read only one page
                    <br>

                </p>
            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/snippets/pages/user/login.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/snippets/pages/user/select2.js')); ?>" type="text/javascript"></script>

<?php if(@$register): ?>
<script type="text/javascript">
$(document).ready(function(){
$("#m_login_signup").trigger('click');
});
</script>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>