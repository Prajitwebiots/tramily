<script src="<?php echo e(URL::asset('assets/js/jquery.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('assets/js/tether.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('assets/js/bootstrap.min.js')); ?>"></script>
<script>
 $(document).ready(function(){
  $("#accordianmenu p").click(function(){
   $("#accordianmenu ul li .betslip-open").slideToggle(600);
  });
 });
</script>

<!--sidebar-->
<script>
 var headers = ["H1","H2","H3","H4","H5","H6"];

 $(".accordion").click(function(e) {
  var target = e.target,
      name = target.nodeName.toUpperCase();

  if($.inArray(name,headers) > -1) {
   var subItem = $(target).next();

   //slideUp all elements (except target) at current depth or greater
   var depth = $(subItem).parents().length;
   var allAtDepth = $(".accordion p, .accordion div").filter(function() {
    if($(this).parents().length >= depth && this !== subItem.get(0)) {
     return true;
    }
   });
   $(allAtDepth).slideUp("fast");

   //slideToggle target content and adjust bottom border if necessary
   subItem.slideToggle("fast",function() {
    $(".accordion :visible:last").css("border-radius","0 0 0px 0px");
   });
   $(target).css({"border-bottom-right-radius":"0", "border-bottom-left-radius":"0"});
  }
 });
</script>
 <?php echo $__env->yieldContent('script'); ?>
 <?php echo $__env->yieldContent('script_bottom'); ?>
 