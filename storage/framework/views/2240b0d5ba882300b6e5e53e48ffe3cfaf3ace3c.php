<?php $__env->startSection('title'); ?> Admin Dashboard <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!-- BEGIN: Subheader -->
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title ">
				My Profile
				</h3>
			</div>
			<div>
				
			</div>
		</div>
	</div>
	<!-- END: Subheader -->
	<div class="m-content">
		<div class="row">
			<div class="col-xl-3 col-lg-4">
				<div class="m-portlet m-portlet--full-height  ">
					<div class="m-portlet__body">
						<div class="m-card-profile">
							<div class="m-card-profile__title m--hide">
								Your Profile
							</div>
							<div class="m-card-profile__pic">
								<div class="m-card-profile__pic-wrapper">
									<img src="<?php echo e(URL::asset('assets/images/user')); ?>/<?php echo e(Sentinel::getUser()->profile); ?>" alt=""/>
								</div>
							</div>
							<div class="m-card-profile__details">
								<span class="m-card-profile__name">
									<?php echo e(Sentinel::getUser()->first_name); ?>

								</span>
								<a href="" class="m-card-profile__email m-link">
									<?php echo e(Sentinel::getUser()->email); ?>

								</a>
							</div>
						</div>
						<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
							<li class="m-nav__separator m-nav__separator--fit"></li>
							<li class="m-nav__section m--hide">
								<span class="m-nav__section-text">
									Section
								</span>
							</li>
							<li class="m-nav__item">
								<a href="admin/profile" class="m-nav__link">
									<i class="m-nav__link-icon flaticon-profile-1"></i>
									<span class="m-nav__link-title">
										<span class="m-nav__link-wrap">
											<span class="m-nav__link-text">
												My Profile
											</span>
											<span class="m-nav__link-badge">
												
											</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="<?php echo e(url('admin/changePass')); ?>" class="m-nav__link">
									<i class="m-nav__link-icon flaticon-lock"></i>
									<span class="m-nav__link-text">
										Change Password
									</span>
								</a>
							</li>
							
						</ul>
						
					</div>
				</div>
			</div>
			<div class="col-xl-9 col-lg-8">
				<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
					<div class="m-portlet__head">
						<div class="m-portlet__head-tools">
							<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
								<li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
										<i class="flaticon-share m--hide"></i>
										Update Profile
									</a>
								</li>
								
							</ul>
						</div>
						
					</div>
					<div class="tab-content">
						<div class="tab-pane active" id="m_user_profile_tab_1">
							<form action=" <?php echo e(url('profile')); ?>/<?php echo e($userdata->id); ?>" method="post" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
								<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
								<input type="hidden" name="user_id" value="<?php echo e($userdata->id); ?>">
								<br>
								<?php if(session('error')): ?>
								
								<div class="alert alert-danger">
									<?php echo e(session('error')); ?>

								</div>
								<?php endif; ?>
								<?php if(session('success')): ?>
								
								<div class="alert alert-success">
									<?php echo e(session('success')); ?>

								</div>
								<?php endif; ?>
								
								<div class="m-portlet__body">
									<div class="form-group m-form__group m--margin-top-10 m--hide">
										<div class="alert m-alert m-alert--default" role="alert">
											The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
										</div>
									</div>
									<div class="form-group m-form__group row">
										<div class="col-10 ml-auto">
											<h3 class="m-form__section">
											1. Personal Details
											</h3>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Profile  Picture
										</label>
										<div class="col-7">
											<input class="form-control m-input" type="file" name="photo">
										</div>
									</div>
									<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												Title Name
											</label>
											<div class="col-7">
												<select class="form-control m-select  " id="" name="title_name">
                                                <option <?php if('' == $userdata->title_name): ?> selected <?php endif; ?> value="">TITLE *</option>
                                                <option  <?php if("Mr."== $userdata->title_name): ?> selected <?php endif; ?> value="Mr.">Mr.</option>
                                                <option <?php if( 'Ms.' == $userdata->title_name): ?> selected <?php endif; ?> value="Ms.">Ms.</option>
                                                <option <?php if('Mrs.' == $userdata->title_name): ?> selected <?php endif; ?> value="Mrs.">Mrs.</option>
                                            </select>
											</div>
											<?php if($errors->has('title_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('title_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
											</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											First Name
										</label>
										<div class="col-7">
											<input class="form-control m-input" name="first_name" type="text" value="<?php echo e($userdata->first_name); ?>">
											<?php if($errors->has('first_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('first_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input"  class="col-2 col-form-label" >
											Last Name
										</label>
										<div class="col-7">
											<input class="form-control m-input" name="last_name" type="text" value="<?php echo e($userdata->last_name); ?>">
											<?php if($errors->has('last_name')): ?>
                                                <span class="help-block text-danger">
                                                    <strong><?php echo e($errors->first('last_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Email
										</label>
										<div class="col-7">
											<input class="form-control m-input" name="email" type="email" value="<?php echo e($userdata->email); ?>">
										</div>
									</div>
									
									
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-2"></div>
											<div class="col-7">
												<button type="submit" class="btn btn-primary m-btn m-btn--air m-btn--custom">
												Save changes
												</button>
												&nbsp;&nbsp;
												<button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
												Cancel
												</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="tab-pane active" id="m_user_profile_tab_2"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>