<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
   <head>
      <meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
      <title></title>
      <meta name="generator" content="LibreOffice 5.2.5.1 (Windows)"/>
      <meta name="author" content="SAI WORLD"/>
      <meta name="created" content="2018-01-27T10:48:00"/>
      <meta name="changedby" content="SAI WORLD"/>
      <meta name="changed" content="2018-01-27T10:48:00"/>
      <meta name="AppVersion" content="12.0000"/>
      <meta name="Company" content="Deftones"/>
      <meta name="DocSecurity" content="0"/>
      <meta name="HyperlinksChanged" content="false"/>
      <meta name="LinksUpToDate" content="false"/>
      <meta name="ScaleCrop" content="false"/>
      <meta name="ShareDoc" content="false"/>
      <style type="text/css">
         @page  { margin: 0.33in }
         p { margin-left: 1.29in; margin-bottom: 0.14in; direction: ltr; line-height: 128%; text-align: left; orphans: 2; widows: 2 }
         p.western { font-family: "Cambria", serif; font-size: 10pt }
         p.cjk { font-family: "Cambria"; font-size: 10pt }
         p.ctl { font-family: "Arial"; font-size: 10pt }
      </style>
   </head>
   <body lang="en-US" dir="ltr">
      <center>
         <div class="container" style="width:750px;">
            <div title="header">
               <p style="margin-left: -0.5in; margin-bottom: 0in; line-height: 100%">
               </p>
            </div>
            <p class="western" style="margin-left: 0in; margin-bottom: 0in; line-height: 100% ; ">
               <img src="<?php echo e(public_path('assets/app/media/img/logos/logo-2.png')); ?>"   name="Picture 1" align="left" hspace="13" width="106" height="103" border="0"/>
               TRAMILY
               HOSPITAITY SOLUTIONS PVT LTD
            </p>
            <p class="western" style="margin-left: 0in; margin-bottom: 0in; line-height: 100%">
               <font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font face="Cambria, serif">113,
               Poddar Plaza, Opp Fire Station </font></font></font>
            </p>
            <p class="western" style="margin-left: 0in; margin-bottom: 0in; line-height: 100%">
               <font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font face="Cambria, serif">Turning
               point, Bhatar Road Surat </font></font></font>
            </p>
            <p class="western" style="margin-left: 0in; margin-bottom: 0in; line-height: 100%">
               <font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font face="Cambria, serif">0261
               &ndash; 4893555, 8511887163</font></font></font>
            </p>
            <p class="western" style="margin-left: 0in; margin-bottom: 0in">     
            </p>
            <p class="western" align="center" style="margin-left: 0in; margin-bottom: 0in ;    text-align: center;">
               <font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font face="Cambria, serif"><font size="6" style="font-size: 22pt"><u><b>E
               &ndash; TICKET</b></u></font></font></font></font>
            </p>
            <p class="western" align="center" style="margin-left: 0in;     text-align: center;"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Cambria, serif"><b>TICKET
               IS NON-REFUNDABLE/NON-CHANGEABLE/NON-CANCELLABLE</b></font></font></font></font>
            </p>
            <table width="728" cellpadding="7" cellspacing="0">
               <col width="167">
               <col width="168">
               <col width="168">
               <col width="167">
               <tr>
                  <td width="167" height="9" style="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                     <p class="western" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><b>AIRLINE
                        PNR:</b><font color="#1f497d"><font face="Cambria, serif"><b><span style="background: #ffffff">
                        </span></b></font></font><font color="#000000"><b><span style="background: #ffffff"><?php echo $onewaybook->flight_pnr_no; ?></span></b></font></font></font>
                     </p>
                  </td>
                  <td width="168" style="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                     <p class="western" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><b>BOOKED
                        ON:<?php echo $bookings->id ?></b></font></font>
                     </p>
                  </td>
                  <td width="168" style="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                     <p class="western" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><b>STATUS:
                        CONFIRMED1</b></font></font>
                     </p>
                  </td>
                  <td width="167" style="border: 1px solid #000001; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in">
                     <p class="western" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><b>BOOKING
                        ID:<?php echo $bookings->bookingid ?></b></font></font>
                     </p>
                  </td>
               </tr>
            </table>
            <p class="western" align="justify" style="margin-left: 0in; margin-bottom: 0in">
               <br/>
            </p>



            <p class="western" align="justify" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PASSENGER
                                 INFORMATION</b></font></font></font></font></font>
            </p>

             <?php
             $count = count($userlist)/3;

             for ($i = 1;$i <= $count;$i++)
             {
                 echo ' <p class="western" align="justify" style="margin-left: 0in; margin-bottom: 0in; line-height: 100%; background: #ffffff">
               <font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Cambria, serif">';
                 echo $i.' '.$userlist['title_'.$i]. ' ' .$userlist['txtNamefirst_'.$i].'  '.$userlist['txtNamelast_'.$i];
                 echo '</font></font></font></font>
            </p>';
             }
//             ?>



            <p class="western" style="margin-left: 0in"><br/>
               <br/>
            </p>
            <center>
               <table width="732" cellpadding="7" cellspacing="0" bolder="solid">
                  <col width="131">
                  <col width="132">
                  <col width="132">
                  <col width="132">
                  <col width="131">
                  <tr>
                     <td width="131" height="27" bgcolor="#c5d9f1" style="background: #c5d9f1" style="border: 1px solid #00000a; padding: 0in 0.08in">
                        <p class="western" align="center" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><b>TRAVEL
                           DATE</b></font></font></font></font></font>
                        </p>
                     </td>
                     <td width="132" bgcolor="#c5d9f1" style="background: #c5d9f1" style="border-top: 1px solid #00000a; border-bottom: 1px solid #00000a; border-left: none; border-right: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0.08in">
                        <p class="western" align="center" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><b>SECTOR</b></font></font></font></font></font></p>
                     </td>
                     <td width="132" bgcolor="#c5d9f1" style="background: #c5d9f1" style="border-top: 1px solid #00000a; border-bottom: 1px solid #00000a; border-left: none; border-right: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0.08in">
                        <p class="western" align="center" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><b>FLIGHT
                           NO</b></font></font></font></font></font>
                        </p>
                     </td>
                     <td width="132" bgcolor="#c5d9f1" style="background: #c5d9f1" style="border-top: 1px solid #00000a; border-bottom: 1px solid #00000a; border-left: none; border-right: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0.08in">
                        <p class="western" align="center" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><b>DEP.
                           TIME </b></font></font></font></font></font>
                        </p>
                     </td>
                     <td width="131" bgcolor="#c5d9f1" style="background: #c5d9f1" style="border-top: 1px solid #00000a; border-bottom: 1px solid #00000a; border-left: none; border-right: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0.08in">
                        <p class="western" align="center" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><b>ARR.
                           TIME</b></font></font></font></font></font>
                        </p>
                     </td>
                  </tr>
                  <tr>
                     <td width="131" height="28" bgcolor="#c5d9f1" style="background: #c5d9f1" style="border-top: none; border-bottom: 1px solid #00000a; border-left: 1px solid #00000a; border-right: 1px solid #00000a; padding: 0in 0.08in">
                        <p class="western" align="center" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><?php echo date("d-M-Y D", strtotime($onewaybook->flight_departure)) ?></font></font></font></font></font></p>
                     </td>
                     <td width="132" bgcolor="#c5d9f1" style="background: #c5d9f1" style="border-top: none; border-bottom: 1px solid #00000a; border-left: none; border-right: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0.08in">
                        <p class="western" align="center" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><?php echo $onewaybook->flightSource->flight_source.' '.$onewaybook->flightDestination->flight_destination; ?></font></font></font></font></font></p>
                     </td>
                     <td width="132" bgcolor="#c5d9f1" style="background: #c5d9f1" style="border-top: none; border-bottom: 1px solid #00000a; border-left: none; border-right: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0.08in">
                        <p class="western" align="center" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"> <?php echo $onewaybook->flight->flight_code.' - '.$onewaybook->flight_number; ?></font></font></font></font></font>
                        </p>
                     </td>
                     <td width="132" bgcolor="#c5d9f1" style="background: #c5d9f1" style="border-top: none; border-bottom: 1px solid #00000a; border-left: none; border-right: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0.08in">
                        <p class="western" align="center" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><?php echo e(date("h:i A", strtotime($onewaybook->flight_departure_time))); ?></font></font></font></font></font></p>
                     </td>
                     <td width="131" bgcolor="#c5d9f1" style="background: #c5d9f1" style="border-top: none; border-bottom: 1px solid #00000a; border-left: none; border-right: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0.08in">
                        <p class="western" align="center" style="margin-left: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#000000"><font face="Calibri, serif"><font size="3" style="font-size: 12pt"><?php echo e(date("h:i A", strtotime($onewaybook->flight_arrival))); ?></font></font></font></font></font></p>
                     </td>
                  </tr>


               </table>
            </center>
            <p class="western" style="margin-left: 0in">
               <span dir="ltr" style="float: left; width: 7.58in; height: 0.24in; border: 1px solid #000000; padding: 0.05in 0.1in; background: #ffffff">
            <p class="western" style="    margin-left: 14px; margin-bottom: 0in; position: absolute;">CONTACT
            INFORMATION: TRAMILY HOSPITALITY SOLUTIONS PVT LTD</p>
            </span><br/>
            <br/>
            </p>
            
              <div style="    border: 1px solid black;">
            <p style="margin-left: 0.15in; margin-bottom: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#3c3c3c"><font face="Cambria, serif"><font size="2" style="font-size: 9pt"><b>Terms
            &amp; Conditions</b></font></font></font></font></font></p>
            <p style="margin-left: 0in; margin-bottom: 0in; line-height: 0.01in">
            </p>
            <p style="margin-left: 0.17in; margin-bottom: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#3c3c3c"><font face="Cambria, serif"><font size="2" style="font-size: 9pt">1.</font></font></font><font face="Cambria, serif"><font size="2" style="font-size: 9pt">	</font></font><font color="#3c3c3c"><font face="Cambria, serif"><font size="2" style="font-size: 9pt">This
            ticket is 100% Non Refundable, Non Changeable &amp; Non Cancellable.</font></font></font></font></font></p>
            <p style="margin-left: 0in; margin-bottom: 0in; line-height: 0.01in">
            </p>
            <p style="margin-left: 0.17in; margin-bottom: 0in"><font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#3c3c3c"><font face="Cambria, serif"><font size="2" style="font-size: 9pt">2.</font></font></font><font face="Cambria, serif"><font size="2" style="font-size: 9pt">	</font></font><font color="#3c3c3c"><font face="Cambria, serif"><font size="2" style="font-size: 9pt">Charged
            fare is totally agreed between &quot;BUYER &amp; SELLER&quot;, any
            issues related to fares thereafter will not be entertained.</font></font></font></font></font></p>
            <p style="margin-left: 0in; margin-bottom: 0in; line-height: 0.01in">
            </p>
            <p style="margin-left: 0.29in; margin-right: 0.35in; text-indent: -0.12in; margin-bottom: 0in; line-height: 105%">
            <font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#3c3c3c"><font face="Cambria, serif"><font size="2" style="font-size: 9pt">3.
            Check flight &amp; passenger(s) details directly by logging /
            calling to the respective airlines, any dispute within 24 hours
            prior to departure will not be entertained.</font></font></font></font></font></p>
            <p style="margin-left: 0in; margin-bottom: 0in; line-height: 0.01in">
            </p>
            <p style="margin-left: 0.29in; margin-right: 0.14in; text-indent: -0.12in; margin-bottom: 0in; line-height: 105%">
            <font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#3c3c3c"><font face="Cambria, serif"><font size="2" style="font-size: 9pt">4.
            No updates will be shared from our end in respect to flight
            cancellation / changes in timings, &quot;BUYER&quot; had to check
            directly with the respective airlines before departure.</font></font></font></font></font></p>
            <p style="margin-left: 0in; margin-bottom: 0in; line-height: 0in">
            </p>

                 <p style="margin-left: 0.29in; margin-right: 0.14in; text-indent: -0.12in; margin-bottom: 0in; line-height: 105%">
                    <font face="Calibri, serif"><font size="2" style="font-size: 11pt"><font color="#3c3c3c"><font face="Cambria, serif"><font size="2" style="font-size: 9pt">5.
                                   Web check in not allowed in this ticket.</font></font></font></font></font></p>
                 <p style="margin-left: 0in; margin-bottom: 0in; line-height: 0in">
                 </p>


        </div>
        
            <br/>
            
            <p class="western" style="margin-left: 0in"><br/>
               <br/>
            </p>
         </div>
      </center>
   </body>
</html>