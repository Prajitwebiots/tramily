<!-- head -->
<?php $__env->startSection('title'); ?>
    Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
	
<?php $__env->stopSection(); ?>
	
<?php $__env->startSection('content'); ?>
	<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
	    <div class="page-content" style="min-height: 1603px;">
	        <!-- BEGIN PAGE HEAD-->
	        <div class="page-head">
	            <!-- BEGIN PAGE TITLE -->
	            <div class="page-title">
	                <h1>Insert Team

	                </h1>
	            </div>
	            <!-- END PAGE TITLE -->

	        </div>
	        <!-- END PAGE HEAD-->
	        <!-- BEGIN PAGE BREADCRUMB -->
	        <ul class="page-breadcrumb breadcrumb">
	            <li>
	                <a href="index.html">Home</a>
	                <i class="fa fa-circle"></i>
	            </li>
	            <li>
	                <span class="active">Insert Team</span>
	            </li>
	        </ul>
	        <!-- END PAGE BREADCRUMB -->
	        <!-- BEGIN PAGE BASE CONTENT -->
	        <div class="row">
	            <div class="col-md-10 ">
	                <!-- BEGIN SAMPLE FORM PORTLET-->
	                <div class="portlet light bordered">
	                    <div class="portlet-title">
	                        <div class="caption font-blue-sunglo">
	                            <i class="icon-settings font-blue-sunglo"></i>
	                            <span class="caption-subject bold uppercase"> Insert Team</span>
	                        </div>

	                    </div>

	                    <?php if($errors->any()): ?>
	                    <div class="alert alert-danger">
	                        <ul class="alert alert-danger">
	                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                        <li> <?php echo e($error); ?></li><br>
	                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                        </ul>
	                    </div>
	                    <?php endif; ?>

	                    <?php if(session('success')): ?>
	                    <div class="alert alert-success">
	                        <?php echo e(session('success')); ?>

	                    </div>
	                    <?php endif; ?>


	                    <div class="portlet-body form">
	                        <form role="form" method="post" action="<?php echo e(route('admin-team.store')); ?>" enctype="multipart/form-data">
	                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
	                            <div class="form-body">
	                            	 <!--Team Name-->
	                                <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
	                                    <label>Team Name</label>
	                                    <input type="text" class="form-control" placeholder="Enter Title" name="name" value="<?php echo e(old('name')); ?>">
	                                    <?php if($errors->has('name')): ?>
    				                       <span class="help-block">
    				                           <strong><?php echo e($errors->first('name')); ?></strong>
    				                       </span>
    				                   <?php endif; ?>
	                                </div>
	                                 <!--Category-->
	                                <div class="form-group <?php echo e($errors->has('category') ? ' has-error' : ''); ?>">
	                                    <label>Category</label>
	                                   	<select name="category" id="category"  class="form-control">
	                                   		<option value="">Select category</option>	
	                                   		<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                                   		<option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
	                                   		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                                   	</select>
	                               		<?php if($errors->has('category')): ?>
    				                       <span class="help-block">
    				                           <strong><?php echo e($errors->first('category')); ?></strong>
    				                       </span>
    				                   	<?php endif; ?>
	                                </div> 
	                                 <!--Sub  Category-->
	                                <div class="form-group <?php echo e($errors->has('sub_category') ? ' has-error' : ''); ?>">
	                                    <label>Select Sub Category</label>
	                                   	<select name="sub_category" id="sub_category"  class="form-control">
	                                   		<option value="">Select SubCategory</option>	
	                                   		
	                                   	</select>
	                               		<?php if($errors->has('sub_category')): ?>
    				                       <span class="help-block">
    				                           <strong><?php echo e($errors->first('sub_category')); ?></strong>
    				                       </span>
    				                   	<?php endif; ?>
	                                </div>
	                                <!--Sub Sub Category-->
	                                <div class="form-group <?php echo e($errors->has('sub_sub_category') ? ' has-error' : ''); ?>">
	                                    <label>Select Sub Sub Category</label>
	                                   	<select name="sub_sub_category" id="sub_sub_category"  class="form-control">
	                                   		<option value="">Select Sub Sub Category</option>	
	                                   	</select>
	                               		<?php if($errors->has('sub_sub_category')): ?>
    				                       <span class="help-block">
    				                           <strong><?php echo e($errors->first('sub_sub_category')); ?></strong>
    				                       </span>
    				                   	<?php endif; ?>
	                                </div>
	                              
	                              
	                            </div>
	                            <div class="form-actions">
	                                <button type="submit" class="btn blue">Submit</button>
	                                 <a  class="btn default" href="<?php echo e(route('admin-team.index')); ?>">Cancel</a>
	                            </div>
	                        </form>
	                    </div>
	                </div>

	            </div>

	        </div>

	        <!-- END PAGE BASE CONTENT -->
	    </div>
	    <!-- END CONTENT BODY -->
	</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>