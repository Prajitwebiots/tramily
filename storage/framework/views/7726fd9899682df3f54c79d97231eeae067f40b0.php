 
<!-- head -->
<?php $__env->startSection('title'); ?>
    Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
<meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
 <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
<link href="<?php echo e(URL::asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet" type="text/css" /><?php $__env->stopSection(); ?>
	<style>
  .adds {
  padding-top: 22px;
}
.bootstrap-switch {
    height: 34px;
    }
  </style>
<?php $__env->startSection('content'); ?>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
<div class="page-content" style="min-height: 1603px;">
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
<!-- BEGIN PAGE TITLE -->
<div class="page-title">
<h1>Insert Event</h1>
</div>
<!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
<li>
<a href="index.html">Home</a>
<i class="fa fa-circle"></i>
</li>
<li>
<span class="active">Insert Event</span>
</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
<div class="col-md-10 ">
<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-blue-sunglo">
            <i class="icon-settings font-blue-sunglo"></i>
            <span class="caption-subject bold uppercase"> Insert Event</span>
        </div>

    </div>

   

    <?php if(session('success')): ?>
    <div class="alert alert-success">
        <?php echo e(session('success')); ?>

    </div>
    <?php endif; ?>
    <?php if($errors->any()): ?>
      <div class="alert alert-danger">
          <ul>
              <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li><?php echo e($error); ?></li>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </ul>
      </div>
    <?php endif; ?>

<div class="portlet-body form">
    <form role="form" method="post" action="<?php echo e(route('admin-event.store')); ?>" enctype="multipart/form-data">
       <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
         <div class="form-body">

        	 <!--Event Name-->
          	  <div class="row">
                 <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?> col-md-8">
                    <label>Event Name</label> 
                  	<select name="eventname" id="eventmaster"  class="form-control">
                   		<option value="">Select Event</option>	
                   		<?php $__currentLoopData = $eventmaster; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   		<option value="<?php echo e($event->id); ?>" <?php echo e((collect(old('eventname'))->contains($event->id)) ? 'selected':''); ?>><?php echo e($event->event_name); ?></option>
                   		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   	</select>
                    <?php if($errors->has('name')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('name')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div> 
        <div class="col-md-4">
         <span class="set-left adds"> <a class="btn blue btn-outline sbold" data-toggle="modal" href="#evtmaster"> Add</a></span> 
              </div> 
                 </div> 

                  <!--Category Name-->
            	  <div class="row">
                <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?> col-md-8">
                    <label>Category Name</label> 
                  	<select name="catid" id="category"  class="form-control cateid">
                   		<option value="">Select Category</option>	
                   		<?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   		<option value="<?php echo e($cat->id); ?>" <?php echo e((collect(old('catid'))->contains($cat->id)) ? 'selected':''); ?>><?php echo e($cat->name); ?></option>
                   		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   	</select>
                    <?php if($errors->has('name')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('catname')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div> 
              <div class="col-md-4">
               <span class="set-left adds"> <a class="btn blue btn-outline sbold" data-toggle="modal" href="#eventcat"> Add</a></span> 
            </div> 
                 </div>


                 <!--Sub Category Name-->
            	  <div class="row">
                <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?> col-md-8">
                    <label>Sub Category Name</label> 
                  	<select name="scatid" id="sub_category"  class="form-control subcatid">
                   		<option value="0">Select SubCategory</option>	
                   		
                   	</select>
                    <?php if($errors->has('name')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('scatname')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div> 
              <div class="col-md-4">
               <span class="set-left adds"> <a class="btn blue btn-outline sbold" data-toggle="modal" href="#eventscat"> Add</a></span> 
            </div> 
                 </div>


                       <!--Sub Sub Category Name-->
                  	  <div class="row">
                      <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?> col-md-8">
                          <label>Sub Sub Category Name</label> 
                        	<select name="sscatid" id="sub_sub_category"  class="form-control subsubcatid">
                         		<option value="">Select SubSubCategory</option>	
                         		
                         	</select>
                          <?php if($errors->has('name')): ?>
                             <span class="help-block">
                                 <strong><?php echo e($errors->first('scatname')); ?></strong>
                             </span>
                          <?php endif; ?>
                      </div> 
                    <div class="col-md-4">
                     <span class="set-left adds"> <a class="btn blue btn-outline sbold" data-toggle="modal" href="#eventsscat"> Add</a></span> 
                  </div> 
                 </div>

                <!--Channel Id-->
                <div class="row">
                <div class="form-group <?php echo e($errors->has('channel_id') ? ' has-error' : ''); ?> col-md-8">
                    <label>Channel Id</label>
                    <input type="text" class="form-control" placeholder="Enter Channel id" name="channel_id" value="<?php echo e(old('channel_id')); ?>">
                    <?php if($errors->has('channel_id')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('channel_id')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div>
              
            </div>


                 <!--Team 1-->
                 <div class="row">
                <div class="form-group <?php echo e($errors->has('team_1') ? ' has-error' : ''); ?> col-md-8">
                    <label>Team 1</label>
                   	<select name="team_1" id="team_1"  class="form-control team1">
                   		<option value="">Select Team</option>	
                   	</select>
               		<?php if($errors->has('team_1')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('team_1')); ?></strong>
                       </span>
                   	<?php endif; ?>
                </div>
                <div class="col-md-4">
               <span class="set-left adds"> <a class="btn blue btn-outline sbold" data-toggle="modal" href="#eventteam"> Add </a></span> 
            </div>  
                </div> 


                <!--Odds 1-->
                 <div class="row">
                <div class="form-group <?php echo e($errors->has('odds_1') ? ' has-error' : ''); ?> col-md-8">
                    <label>Odds 1</label>
                    <input type="number" step="0.01" class="form-control" placeholder="Enter Odds 1" name="odds_1" value="<?php echo e(old('odds_1')); ?>">
                    <?php if($errors->has('odds_1')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('odds_1')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div>
              
                </div> 

                 <!--Team 2-->
                 <div class="row">
                <div class="form-group <?php echo e($errors->has('team_2') ? ' has-error' : ''); ?> col-md-8">
                    <label>Team 2</label>
                   	<select name="team_2" id="team_2"  class="form-control team2">
                   		<option value="">Select Team</option>	
                   	</select>
               		<?php if($errors->has('team_2')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('team_2')); ?></strong>
                       </span>
                   	<?php endif; ?>
                </div>
                <div class="col-md-4">
               <span class="set-left adds"> <a class="btn blue btn-outline sbold" data-toggle="modal" href="#eventteam"> Add  </a></span> 
            </div> 
                </div> 

                 <!--Odds 2-->
                  <div class="row">
                <div class="form-group <?php echo e($errors->has('odds_2') ? ' has-error' : ''); ?> col-md-8">
                    <label>Odds 2</label>
                    <input type="number" step="0.01" class="form-control" placeholder="Enter Odds 2" name="odds_2" value="<?php echo e(old('odds_2')); ?>">
                    <?php if($errors->has('odds_2')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('odds_2')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div>
                </div>

                <!--Drafts-->
                <div class="row">
                <div class="form-group <?php echo e($errors->has('drafts') ? ' has-error' : ''); ?> col-md-8">
                    <label>Draw</label>
                    <input type="number" step="0.01" class="form-control" placeholder="Enter Draw" name="draw" value="<?php echo e(old('draw')); ?>">
                    <?php if($errors->has('draw')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('draw')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div>
                </div>

                <!--Scroll-->
               <div class="row">
                <div class="form-group <?php echo e($errors->has('scroll') ? ' has-error' : ''); ?> col-md-8">
                    <label>Scroll</label>
                    <input type="number" step="0.01" class="form-control" placeholder="Enter Scroll" name="scroll" value="<?php echo e(old('scroll')); ?>">
                    <?php if($errors->has('scroll')): ?>
                       <span class="help-block">
                           <strong><?php echo e($errors->first('scroll')); ?></strong>
                       </span>
                   <?php endif; ?>
                </div>
               </div>
             	<!--EVENT DATE -->
                <div class="row"> 
              	  <div class="form-group col-md-6">
                   <label>Event Date</label>
                   <div class="input-group input-large date-picker input-daterange" data-date="12/11/2017" data-date-format="mm/dd/yyyy">
                          <input type="text" class="form-control datetimepicker" value="<?php echo e(old('start_date')); ?>" id="start_date" name="start_date" placeholder="Start Date">
                          <span class="input-group-addon"> to </span>
                          <input type="text" class="form-control datetimepicker" value="<?php echo e(old('end_date')); ?>" id="end_date" name="end_date" placeholder="End Date">
                    </div>
                        <span class="help-block"> Select date range </span>
                  </div>
                </div>

             <!--- live event -->
             <input type="checkbox" class="make-switch" name="live"  data-on-color="success" data-off-color="warning">
        <span class="help-block"> If Event Is Live Please Turn ON </span>


                   </div>

            <div class="form-actions">
                <button type="submit" class="btn blue">Submit</button>
                  <a class="btn default" href="<?php echo e(route('admin-event.index')); ?>">Cancel</a>
            </div>
        </form>
    </div>
	                </div>

	            </div>

	        </div>

	        <!-- END PAGE BASE CONTENT -->
	    </div>
	    <!-- END CONTENT BODY -->
	</div>



<!-- Category model -->
<div class="modal fade" id="eventcat" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Category</h4>
            </div>
            <div class="modal-body"> 
              <div class="alert alert-danger" style="display:none"><ul class="val_errors1" ></ul></div> 
         <form action="" method="post" id="cat">
    <div class="form-group">
	   <label>Category Name</label>
	       <input type="text" class="form-control " placeholder="Enter Category Name" name="cname">                       
	</div>
	<div class="form-group">
	     <label>Slug</label>
	     <input type="text" class="form-control" placeholder="Enter Slug Name" name="slug">
	</div>
	<div class="form-group">
	    <label>Icon</label>
	    <input type="text" class="form-control" placeholder="Enter Icon Name" name="icon">                                  
	</div>
	</form>
    </div>
       <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <button type="button" id="Submit-cat" class="btn green">Save</button>
        </div>
    </div>                                 
    </div>                                       
</div>

<!-- Sub Category model -->
<div class="modal fade" id="eventscat" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Sub Category</h4>
            </div>
            <div class="modal-body"> 
              <div class="alert alert-danger" style="display:none"><ul class="val_errors2" ></ul></div> 
         <form action="" method="post" id="subcat">
   <div class="form-group">
	    <label>Category Name</label> 
	    <select name="CategoryName" id="category"  class="form-control cateid">
	        <option value="">Select Category</option>	
	        <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	        <option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name); ?></option>
	        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	    </select>
    </div> 
    <div class="form-group">
	     <label>Sub Category Name</label>
	     <input type="text" class="form-control subcatid" placeholder="Enter Sub Category Name" name="SubCategoryName">
	</div>
	<div class="form-group">
	     <label>Slug</label>
	     <input type="text" class="form-control" placeholder="Enter Slug Name" name="slug">
	</div>
	
	</form>
    </div>
       <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <button type="button" id="Submit-subcat" class="btn green">Save</button>
        </div>
    </div>                                 
    </div>                                       
</div>


<!-- Sub Sub Category model -->
<div class="modal fade" id="eventsscat" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Sub Sub Category</h4>
            </div>
            <div class="modal-body"> 
              <div class="alert alert-danger" style="display:none"><ul class="val_errors3" ></ul></div> 
               <form action="" method="post" id="subsubcat">
               <div class="form-group">
            	    <label>Category Name</label> 
            	    <select name="CategoryName" id="categoryid"  class="form-control cateid">
            	        <option value="">Select Category</option>	
            	        <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            	        <option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name); ?></option>
            	        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            	    </select>
                </div> 
                <div class="form-group">
            	     <label>Sub Category Name</label> 
            	     <select name="SubCategoryName" id="sub_categoryid"  class="form-control subcatid">
            	        <option value="0">Select SubCategory</option>	     		
            	     </select>
            	</div> 
            	<div class="form-group">
            	     <label>Sub Sub Category</label>
            	     <input type="text" class="form-control" placeholder="Enter Sub Sub Category Name" name="SubSubCategoryName">
            	</div>
            	<div class="form-group">
            	     <label>Slug</label>
            	     <input type="text" class="form-control" placeholder="Enter Slug Name" name="slug">
            	</div>
            	</form>
          </div>
       <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <button type="button" id="Submit-subsubcat" class="btn green">Save</button>
        </div>
    </div>                                 
    </div>                                       
</div>


<!-- Team model -->
<div class="modal fade" id="eventteam" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Team</h4>
            </div>
            <div class="modal-body"> 
              <div class="alert alert-danger" style="display:none"><ul class="val_errors4" ></ul></div> 
         <form action="" method="post" id="evntteam">
         <div class="form-group">
      	     <label>Team Name</label>
      	     <input type="text" class="form-control" placeholder="Enter Taem Name" name="TeamName">
         </div>     	
         <div class="form-group">
      	    <label>Category Name</label> 
      	    <select name="CategoryName" id=""  class="form-control categoryteam">
      	        <option value="">Select Category</option>	
      	        <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      	        <option value="<?php echo e($cat->id); ?>"><?php echo e($cat->name); ?></option>
      	        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      	    </select>
          </div> 
          <div class="form-group">
      	     <label>Sub Category Name</label> 
      	     <select name="SubCategoryName" id=""  class="form-control sub_categoryteam">
      	        <option value="0">Select SubCategory</option>	     		
      	     </select>
      	</div> 
      	<div class="form-group">
      	     <label>Sub Sub Category Name</label> 
      	     <select name="SubSubCategoryName" id=""  class="form-control sub_sub_categoryteam">
      	        <option value="">Select SubSubCategory</option>	     		
      	     </select>
      	</div> 
	
	     </form>
    </div>
       <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <button type="button" id="Submit-evntteam" class="btn green">Save</button>
        </div>
    </div>                                 
    </div>                                       
</div>


<!-- Event Master model -->
<div class="modal fade" id="evtmaster" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Event Master</h4>
            </div>
            <div class="modal-body">
          
            <div class="alert alert-danger" style="display:none"><ul class="val_errors5" ></ul></div> 
         
         <form action="" method="post" id="evntmaster">
   <div class="form-group">
	     <label>Event Name</label>
	     <input type="text" class="form-control" placeholder="Enter Event Name" name="EventName">
   </div>     	
  
	</form>
    </div>
       <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <button type="button" id="Submit-evntmaster" class="btn green">Save</button>
        </div>
    </div>                                 
    </div>                                       
</div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
  <script src="<?php echo e(asset('assets/global/plugins/moment.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/components-bootstrap-switch.min.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script_bottom'); ?>
<script>
	$('#start_date').datetimepicker({
        	format: 'yyyy-mm-dd HH:ii:ss'
   });
   $('#end_date').datetimepicker({
        	format: 'yyyy-mm-dd HH:ii:ss'
   });
</script>
<script src="<?php echo e(asset('js/custome.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>