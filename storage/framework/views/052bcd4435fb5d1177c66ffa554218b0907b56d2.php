 
<!-- head -->
<?php $__env->startSection('title'); ?>
   Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
     <link href="<?php echo e(URL::asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" /> 
     <link href="<?php echo e(URL::asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <style>
        .event-modal-main-div{
            padding-right: 30em;
        }
    </style>
<?php $__env->stopSection(); ?>
    



<?php $__env->startSection('content'); ?>

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="min-height: 1489px;">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Event Pages

                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo e(url('admin/dashboard')); ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span class="active">Event Pages</span>
            </li>

            <span class="set-right"><a href="<?php echo e(route('admin-event.create')); ?>" class="btn btn-primary ">Add New</a></span>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <div class="row">

        </div>
        <br>
        <?php if(session('error')): ?><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
        <?php if(session('success')): ?><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
        <br>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Event Pages</span>
                        </div>
                        <div class="tools"></div>
                    </div>
                    <div class="portlet-body">
                        <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Name</th>
                                <th>Category Name</th>
                                <th>Created Date</th>
                                <th>Live</th>
                                <th>Status </th>
                                <th>Event Odds</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody><?php $i = 1; ?>
                            <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($i++); ?></td>
                                <td><?php echo e(@$event->Master->event_name); ?></td>
                                <td><?php echo e(@$event->Cat->name); ?></td>
                                <td> <?php echo date('j M, Y h:i:s A', strtotime($event->created_at)); ?></td>
                                <?php if($event->live == 1): ?>
                                <td><span class="label label-sm label-danger"> Live </span></td>
                                <?php else: ?>
                                <td><span class="label label-sm label-success"> Not Live  </span></td> 
                                <?php endif; ?>
                               
                                <?php if($event->status == 1): ?>
                                <td><span class="label label-sm label-danger"> Deactivated </span></td>
                                <?php else: ?>
                                <td><span class="label label-sm label-success"> Activated </span></td> 
                                <?php endif; ?>
                                <td >
                                    <a href="#evntodd<?php echo e($event->id); ?>" data-toggle="modal" class="btn btn-xs red" style="margin-left: 5px;"><i class="fa fa-eye"></i>  </a>
                                    <a href="<?php echo e(route('admin-odd.edit',$event->id)); ?>" data-toggle="modal" class="btn btn-xs blue" style="margin-left: 5px;"><i class="fa fa-pencil"></i>  </a>
                                </td>
                                
                                <td width="18%">
                                    
                                    <a href="#form_moda<?php echo e($event->id); ?>" data-toggle="modal" class="btn btn-xs red" style="margin-left: 5px;"><i class="fa fa-eye"></i>  </a>
                                    <a href="<?php echo e(route('admin-event.edit',$event->id)); ?>" data-toggle="modal" class="btn btn-xs blue" style="margin-left: 5px;"><i class="fa fa-pencil"></i>  </a>

                                    <?php if($event->status == 0): ?>
                                     <a href="<?php echo e(url('changeEventStatus/'.$event->id.'/1')); ?>" class="btn btn-danger" >Deactivate</a>
                                    <?php else: ?>
                                      <a href="<?php echo e(url('changeEventStatus/'.$event->id.'/0')); ?>" class="btn btn-success" >Active</a>
                                    <?php endif; ?>

                                    <?php echo e(Form::open(['method' => 'DELETE', 'class'=>'set-right' ,'route' => ['admin-event.destroy',$event->id]])); ?>

                                       <?php echo e(Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs'] )); ?>

                                    <?php echo e(Form::close()); ?>

                                    <div id="form_moda<?php echo e($event->id); ?>" class=" event-modal-main-div modal fade" role="dialog" aria-hidden="true" tabindex="-1" style="display: none;">
                                        <div class="modal-dialog" >
                                            <div class="modal-content" style="width: 180%;">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Event  Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                       <!--  <h3 class="text-center"> Event</h3> -->
                                                        <div class="col-md-12">
                                                            <table class="table table-hover table-bordered">
                                                                <tr>
                                                                    <th>Name:</th><td><?php echo e(@$event->Master->event_name); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Category:</th><td><?php echo e(@$event->Cat->name); ?></td>
                                                                </tr>
                                                                 <tr>
                                                                    <th>Sub Category:</th><td><?php echo e(@$event->SubCat->name); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Sub Sub Category:</th><td><?php echo e(@$event->SubSubCat->name); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Channel Id:</th><td><?php echo e($event->channel_id); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Team 1:</th><td><?php echo e(@$event->Team1->name); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Odds 1:</th><td><?php echo e(@$event->odds); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Team 2:</th><td><?php echo e(@$event->Team2->name); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Odds 2:</th><td><?php echo e($event->odds2); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Draw:</th><td><?php echo e($event->draw); ?></td>
                                                                </tr> 
                                                                <tr>
                                                                    <th>Start Date:</th><td><?php echo e($event->start_date); ?></td>
                                                                </tr>
                                                                 <tr>
                                                                    <th>End Date:</th><td><?php echo e($event->end_date); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Scroll:</th><td><?php echo e($event->scroll); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Created by:</th><td><?php echo e($event->user->first_name); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Created at:</th><td><?php echo date('j M, Y h:i:s A', strtotime($event->created_at)); ?></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
   <div id="evntodd<?php echo e($event->id); ?>" class=" event-modal-main-div modal fade" role="dialog" aria-hidden="true" tabindex="-1" style="display: none;">
                                        <div class="modal-dialog" >
                                            <div class="modal-content" style="width: 180%;">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Event Odd Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                       <!--  <h3 class="text-center"> Event</h3> -->
                                                        <div class="col-md-12">
                                                    
                                                       <?php $__currentLoopData = $event->odd; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $oddmaster): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                       <?php $__currentLoopData = $oddmaster->odds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $odds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <h4><b> ODD TITLE :</b><b>   <?php echo e($odds->odd_title); ?> </b> </h4>
                                                                <br>
                                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                                                                <table class="table table-hover table-bordered">
                                                                <tr>
                                                                    <th style="width: 320px;">Name</th>
                                                                    <th>Odd</th>
                                                                    
                                                                </tr>
                                                                <tr>
                                                                    <td><?php echo e($oddmaster->name); ?></td>
                                                                      <td><?php echo e($oddmaster->odd); ?></td>
                                                                </tr>
                                                                
                                                                </table>
                                                                
                                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>     
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('script'); ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-buttons.min.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
      
    </script>
    <?php $__env->stopSection(); ?>
   
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>