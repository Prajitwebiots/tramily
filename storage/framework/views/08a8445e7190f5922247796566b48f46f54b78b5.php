  <?php $__env->startSection('head'); ?>
  <meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
  <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="center-content">
        <div class="row">
            <div class="col-12  col-md-4 col-lg-3 nopadding">
                <div class="site-sidebar">
                <?php echo $__env->make('frontend.layouts.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
            <div class="col-12  col-md-8 col-lg-9 nopadding">
                <div class="inner-page">
                    <div class="content">

                        <div class="date-badges-container">
                       <?php $__currentLoopData = $day_arr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $day): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php 
                            $nameOfDay = date('D', strtotime($day));
                        ?>
                       <div class="date-badge col-md-" date="<?php echo e($day); ?>" onclick="getEventByDate(this,'<?php echo e($day); ?>')"><span><?php echo e($nameOfDay); ?></span><br> <?php echo e($day); ?></div>
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <div class="date-badge active" date="future" onclick="getEventByDate(this,'')"><span>Next</span><br>7 Days</div>
                        </div>
                        <div id="showTime"></div>
                        <?php $event_arr=array();$i=0;?>
                        <div id="data" >
                        <?php $__currentLoopData = $eventmaster; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $master): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="title-container">
                                <h1 class="competition"><?php echo e($master->event_name); ?></h1>
                            </div>

                            <div class="events-header">
                                <div class="header">
                                    <div class="start">Start Time</div>
                                    <div class="event">Event</div>
                                    <div class="one">1</div>
                                    <div class="x">X</div>
                                    <div class="two">2</div>
                                </div>

                                <?php $__currentLoopData = $master->events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php 
                                    $event_arr['id'.$i]=$event->id;
                                    $event_arr['day'.$i]=$event->start_date;
                                    $i++;
                                ?>
                                <div class="event-container">
                                    <div class="start">
                                    <?php $now = \Carbon\Carbon::parse();
                                    $date = \Carbon\Carbon::parse($event->start_date);
                                    $length = $date->diffForHumans($now); ?>
                                        <div><?php echo e($date->toDateString()); ?> <b><?php echo e($date->format('h:i')); ?></b></div>
                                        <div class="timer" id="timer_<?php echo e($event->id); ?>"><?php echo e($length); ?></div>
                                    </div>

                                    <div class="icon"><i class="hd-sport-icon <?php echo e($event->cat->icon); ?>"></i></div>
                                    <a href="<?php echo e(url('event-odds/'.$event->id)); ?>">
                                    <div class="event">
                                        <div class="competition">
                                            <?php echo e($event->subcat->name); ?>: <?php echo e($event->subsubcat->name); ?>, <?php echo e($master->event_name); ?>

                                        </div>
                                        <div class="competitors"><?php echo e($event->team1->name); ?> V <?php echo e($event->team2->name); ?></div>
                                    </div></a>
                                    <div class="markets">
                                        <div class="selection-container">
                                            <button class="selection enabled">
                                                <div class="odds" odd-select="1" odd-val="<?php echo e($event->odds); ?>" odd-id="<?php echo e($event->id); ?>" odd-name1="<?php echo e($event->team1->name); ?>" odd-name2="<?php echo e($event->team2->name); ?>"  ><?php echo e($event->odds); ?></div>
                                            </button>
                                            <?php if($event->draw): ?>
                                            <button class="selection enabled">
                                                <div class="odds" odd-select="2" odd-val="<?php echo e($event->draw); ?>" odd-id="<?php echo e($event->id); ?>" odd-name1="<?php echo e($event->team1->name); ?>" odd-name2="<?php echo e($event->team2->name); ?>"><?php echo e($event->draw); ?></div>
                                            </button>
                                            <?php else: ?>
                                            <button class="selection disabled">
                                                <div class="odds">-</div>
                                            </button>
                                            <?php endif; ?>
                                            <button class="selection enabled">
                                                <div class="odds" odd-select="3" odd-val="<?php echo e($event->odds2); ?>" odd-id="<?php echo e($event->id); ?>" odd-name1="<?php echo e($event->team1->name); ?>" odd-name2="<?php echo e($event->team2->name); ?>"><?php echo e($event->odds2); ?></div>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="extra-odds">+<?php echo e($event->odd->count()); ?></div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('script_bottom'); ?>
<script type="text/javascript">
    // odd click function to show data in above cart
    var balance = <?php echo e($balance); ?>;
    $('.odds').click(function(){
        var odd_select = $(this).attr('odd-select');
        var odd_val = $(this).attr('odd-val');
        var odd_id = $(this).attr('odd-id');
        var odd_name1 = $(this).attr('odd-name1');
        var odd_name2 = $(this).attr('odd-name2');

        var team = (odd_select == 1) ? $(this).attr('odd-name1') : $(this).attr('odd-name2');

        if(odd_val != ""){
            $("#all_bets").append('<div class="single-portion" id="bet_'+odd_id+'">\
                <button class="close cross">x</button>\
                <div class="title">Match Winner</div>\
                <div class="vs">'+odd_name1+' <b>vs</b> '+odd_name2+'</div>\
                <hr>\
                <div class="team">'+team+'\
                    <div id="odd_'+odd_select+'" class="odds">'+odd_val+'</div>\
                </div>\
            </div><div class="single-portion">\
                <div class="bet single">\
                        <button class="btn grey dec-stake-50" onclick="check_odd(this,'+odd_select+')" value="-50">- 50</button>\
                        <button class="btn grey dec-stake-50" onclick="check_odd(this,'+odd_select+')" value="-10">- 10</button>\
                        <input type="text" class="stake-input single" onkeypress="return isNotNumberKey(event);" onkeyup="check_odd(null,'+odd_select+',this.value)" id="input_'+odd_select+'" value="" placeholder="Stake">\
                        <button class="btn grey dec-stake-50" onclick="check_odd(this,'+odd_select+')" value="10">+ 10</button>\
                        <button class="btn grey dec-stake-50" onclick="check_odd(this,'+odd_select+')" value="50">+ 50</button>\
                </div>\
            </div>');
            $("#multi_bets").append('<div class="single-portion" id="bet_'+odd_id+'"><button class="close cross">x</button>\
                        <div class="title">Match Winner</div>\
                        <div class="vs">'+odd_name1+' <b>vs</b> '+odd_name2+'</div>\
                        <hr>\
                        <div class="team">'+team+'\
                            <div id="odd_'+odd_select+'" class="odds">'+odd_val+'</div>\
                        </div></div>');
        }
    });

    function check_odd(elem,id,input_val=null){
        var stake = (input_val==null) ? $(elem).val() : input_val;
        var input = $("#input_"+id);
        if(input.val() == "" && input_val == null && !elem ){
        }else{

            if(input_val==null && input.val()){ // check if inputh has value than add stake on old value
                stake = parseFloat(input.val())+parseFloat(stake);
                input.val(stake);
                // console.log(stake);
            }
            else if(input_val==null){
                stake = parseFloat(stake);
                input.val(stake);
                // console.log(stake);
            }
            else
                input.val(parseFloat(stake));
            
            var potential_win = parseFloat(stake)*parseFloat($("#odd_"+id).html()); // selected stake (-50 ... +10 +50 ) * selected odds val 
            
            $("#Spotential_win").html(potential_win); // total potential win
            
            (potential_win > 0) ? $(".placeBtn").attr('disabled',false) : $(".placeBtn").addClass('disabled',true); // to enable/diable place button if odds > 0
            
            (balance > parseFloat(input.val())+parseFloat(stake) ) ? $(".placeBtn").attr('disabled',true) : $(".placeBtn").addClass('disabled',false); // to enable/diable place button if num of odds is higher than balance
        }
        
    }
// return if character enter
function isNotNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
        return false;
    return true;
}

    function getEventByDate(elem,day)
    {
        $(".date-badge").removeClass('active');
        $(elem).addClass('active');
        var path=window.location.pathname;
        
        $.ajax({
                type:'post',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
            url:"<?php echo e(url('getdata-day-wise')); ?>",
                data: {'day': day,'path': path},
                success: function (responseData) {
                    if(responseData)
                    {
                        $('#data').html('');
                        var htmldata = '';
                        $.each( responseData, function( key, value ) {
                          
                        htmldata += '<div class="title-container"><h1 class="competition">'+value['event_name']+'</h1></div>\
                            <div class="events-header">\
                                <div class="header">\
                                    <div class="start">Start Time</div>\
                                    <div class="event">Event</div>\
                                    <div class="one">1</div>\
                                    <div class="x">X</div>\
                                    <div class="two">2</div>\
                                </div>';
                                $.each( value['events'], function( ekey, evalue ) {
                                    var elem = "#timer_ekey"+evalue['id'];
                                   // showDiff(elem, evalue['start_date']);
                                htmldata +='<div class="event-container">\
                                    <div class="start">\
                                        <div>'+evalue['start_date']+'</div>\
                                        <div class="timer" id="timer_ekey'+evalue['id']+'">'+evalue['start_date']+'</div>\
                                    </div>\
                                    <div class="icon"><i class="hd-sport-icon '+evalue['cat']['icon']+'"></i></div>\
                                    <a href="event-odds/'+evalue['id']+'">\
                                    <div class="event">\
                                        <div class="competition">'+evalue['subcat']['name']+':'+evalue['subsubcat']['name']+','+value['event_name']+'</div>\
                                        <div class="competitors">'+evalue['team1']['name']+'V'+evalue['team2']['name']+'</div>\
                                    </div></a>\
                                    <div class="markets">\
                                        <div class="selection-container">\
                                            <button class="selection enabled">\
                                                <div class="odds" odd-select="1" odd-val="'+evalue['odds']+'" odd-id="'+evalue['id']+'" odd-name1="'+evalue['team1']['name']+'" odd-name2="'+evalue['team2']['name']+'" >'+evalue['odds']+'</div>\
                                            </button>';
                                            if(evalue['draw']){
                                            htmldata +='<button class="selection enabled">\
                                                <div class="odds">'+evalue['draw']+'</div>\
                                            </button>';
                                            }else{
                                            htmldata +='<button class="selection disabled">\
                                                <div class="odds">-</div>\
                                            </button>';
                                            }
                                            htmldata +='<button class="selection enabled">\
                                                <div class="odds">'+evalue['odds2']+'</div>\
                                            </button>\
                                        </div>\
                                    </div>\
                                    <div class="extra-odds">+6</div>\
                                </div>\
                               ';


   
                                });
                             htmldata +='</div>';
                        });
                        $('#data').html(htmldata);
                        $(".timer").trigger("click");
                         
                    }
                    else
                    {

                    }
                }
            });
    }
   //    var pausecontent = new Array();
   //  <?php foreach($eventmaster as $key => $val){ ?>
   //      pausecontent.push('<?php echo $val; ?>');
   //  <?php } ?>
   // // console.log(pausecontent);
   //  pausecontent.forEach(function(item){
   //        item.forEach(function(event)
   //        {
   //         // console.log(event);
   //        });
   //      });
  $(document).on('click', '.timer', function(evt) {
    showDiff($(this).attr('id'),$(this).html());
}); 

function showDiff(elem,start_date){
                                // console.log($('#'+elem));
                                // console.log($('#cart-title'));
                                var date2 = new Date(start_date);
                                var date1 = new Date();   

                                var diff = (date2 - date1)/1000;
                                var diff = Math.abs(Math.floor(diff));

                                var days = Math.floor(diff/(24*60*60));
                                var leftSec = diff - days * 24*60*60;

                                var hrs = Math.floor(leftSec/(60*60));
                                var leftSec = leftSec - hrs * 60*60;

                                var min = Math.floor(leftSec/(60));
                                var leftSec = leftSec - min * 60;
                                console.log(elem);
                                // console.log("- Start in " + hrs + " hours " + min + " minutes");
                                $('#'+elem).html("- Start in " + hrs + " hours " + min + " minutes");
                                //document.getElementById("showTime").innerHTML = "You have " + days + " days " + hrs + " hours " + min + " minutes and " + leftSec + " seconds .";

                            }
</script>
 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>