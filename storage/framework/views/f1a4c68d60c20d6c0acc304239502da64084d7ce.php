<?php $__env->startSection('title'); ?>Admin | Transaction history <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Transaction history
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Transaction history
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <?php if(session('error')): ?><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
        <?php if(session('success')): ?><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__body">
                 <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input   type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                             </div>
                        </div>

                <!--begin: Datatable -->
                <table class="m-datatable" id="html_table" width="100%">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>User name </th>
                            <th>Admin name</th>
                            <th>Amount</th>
                            <th>Add/Delete</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php $__currentLoopData = $history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($loop->iteration); ?></td>
                            <td><?php echo e(@$list->userinfo->title_name.' '.@$list->userinfo->first_name.' '.@$list->userinfo->last_name); ?></td>
                            <td><?php echo e($list->admininfo->first_name); ?></td>
                            <td><?php echo e($list->amount); ?></td>
                            <td>
                                <?php if($list->status == 0): ?>
                                    <?php echo e('<span><span class="m-badge  m-badge--primary m-badge--wide">Add</span></span>'); ?>

                                <?php else: ?>
                                    <?php echo e('<span><span class="m-badge  m-badge--danger m-badge--wide">Delete</span></span>'); ?>

                                <?php endif; ?>
                            </td>
                            <td><?php echo e(date_format($list->created_at,'d-M-Y D h:i ')); ?></td>
                            <td> <?php if($list->status == 0 && $list->is_deleted == 0 ): ?>
                                    <?php echo e('<a onclick="return deletemsg('.$list->id.');"  href="'.url('delete_balance').'/'.$list->id.'" class="btn m-btn--pill    btn-danger" >delete</a>'); ?>

                                     <?php else: ?>
                                    <?php echo e('<a onclick="return returnmsg();"     class="btn m-btn--pill btn-info" >delete </a>'); ?>

                                  <?php endif; ?></td>
                        </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>


</div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script>
<script>
    function deletemsg(id) {
        swal({
            title: 'Are you sure?',
            text: "Are you sure you wish to give balance to supplier ",
            showCancelButton: true,
            confirmButtonColor: '#34bfa3',
            cancelButtonColor: '#f4516c',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if(result.value)
        {
            
                window.location= "<?php echo e(url('delete_balance')); ?>/"+id;
            return true;
        }
    else
        {return false;
        }
    }
    );
        return false;
    }

    function returnmsg(){
        swal({
                  type: 'warning',
                  title: 'Oops...',
                  text: 'Balance already given back to supplier ',

              })
        return false;
    }

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>