<?php $__env->startSection('title'); ?> Tramily | Forgot Password <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
    <link href="<?php echo e(URL::asset('assets/pages/css/login-5.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<style>
.m-login.m-login--1 .m-login__wrapper {
overflow: hidden;
padding: 0% 2rem 2rem 2rem !important;
}
.m-login.m-login--1 .m-login__aside {
/*width: 50%;*/
}
</style>
<!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
                
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--singin" id="m_login"  style="background: url('<?php echo e(asset('assets/images/bg-2.jpg1')); ?>');">
           <div class="col-md-4"></div>
       
                <div class="col-md-4">
                   <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
                        <div class="m-stack m-stack--hor m-stack--desktop">
                            <div class="m-stack__item m-stack__item--fluid">
                                <div class="m-login__wrapper">
                                    <div class="m-login__logo">
                                        <a href="#">
                                            <img src="<?php echo e(URL::asset('assets/app/media/img/logos/logo-2.png')); ?>" style="width: 30%;">
                                        </a>
                                    </div>
                                    
                                    <div class="m-login__forget-password" style="display: block;">
                                        <div class="m-login__head">
                                            <h3 class="m-login__title  ">
                                                Reset Password 
                                            </h3>
                                            <div class="m-login__desc">
                                                Enter your New password :
                                            </div>
                                        </div>
                                        
                                         <?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
                                <?php if(session('success')): ?><br><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
                                
                                 <?php if(count($errors)): ?><br><br>
            <div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    
                    <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
             <?php endif; ?>
                                        
                                    <?php echo Form::model($user, ['method' => 'PATCH','route' => ['forgot.update', $user->id], 'class'=> 'm-login__form m-form']); ?>

                                            <input class="form-control" id="email" type="hidden" name="email" value="<?php echo e($email); ?>">
                                            <input class="form-control" id="code" type="hidden" name="code" value="<?php echo e($code); ?>">
                                            <div class="form-group m-form__group">
                                                <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
                                            </div>
                                            <div class="form-group m-form__group">
                                                <input class="form-control m-input" type="password" placeholder="confirm Password" name="confirm_password" id="password" autocomplete="off">
                                            </div>
                                            <div class="m-login__form-action">
                                                <button id="m_login_reset_password_submit1" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                                    Request
                                                </button>
                                                <a id="forgot" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
                                                    Cancel
                                                </a>
                                            </div>
                                        </form>
                                    </div>
    
                                </div>
                            </div>
                            <div class="m-stack__item m-stack__item--center">
                                <div class="m-login__account">
                                    <span class="m-login__account-msg">
                                        Don't have an account yet ?
                                    </span>
                                    &nbsp;&nbsp;
                                    <a href="javascript:;" id="m_login_signup" class="m-link m-link--focus m-login__account-link">
                                        Sign Up
                                    </a>
                                </div>
                            </div>
                        </div>
                 </div>
                    <!--<div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1  m-login__content" style="background-image: url(<?php echo e(URL::asset('assets/app/media/img/bg/bg-4.jpg')); ?>)">-->
                    <!--    <div class="m-grid__item m-grid__item--middle">-->
                    <!--        <h3 class="m-login__welcome">-->
                    <!--            Join Our Community-->
                    <!--        </h3>-->
                    <!--        <p class="m-login__msg">-->
                    <!--            Lorem ipsum dolor sit amet, coectetuer adipiscing-->
                    <!--            <br>-->
                    <!--            elit sed diam nonummy et nibh euismod-->
                    <!--        </p>-->
                    <!--    </div>-->
                    <!--</div>-->
                </div>
            <div class="col-md-4"></div>
            </div>
            </div>
        <!-- end:: Page -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('bottom_script'); ?>        
    <script src="<?php echo e(asset('assets/snippets/pages/user/login.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/snippets/pages/user/select2.js')); ?>" type="text/javascript"></script>
    <?php if(@$register): ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#m_login_signup").trigger('click');
            });
        </script>
    <?php endif; ?>
<?php $__env->stopSection(); ?>    
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>