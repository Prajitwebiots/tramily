<?php $__env->startSection('title'); ?> Admin | Manage Supplier <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<meta name="csrf_token"  content="<?php echo e(csrf_token()); ?>" />
<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Manage Supplier
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Supplier
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">

        <?php if(session('error')): ?><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
        <?php if(session('success')): ?><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>

        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                         <form action="<?php echo e(url('supplier/export')); ?>" enctype="multipart/form-data">
                                     <button class="btn m-btn--pill    btn-primary" type="submit">DOWNLOAD AS EXCEL</button>
                                         </form>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                    </div>
                </div>
                
                <!--begin: Datatable -->
                <table class="m-datatable" id="html_table" width="100%">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>Agency Name</th>
                            <th>Status</th>
                            <th>Actions</th>
                            <th>Login</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($user->user): ?>
                        <?php if($user->user->is_delete == 1): ?>
                        <tr>
                            <td><?php echo e($i++); ?></td>
                            <td><?php echo e($user->user->first_name); ?></td>
                            <td><?php echo e($user->user->email); ?></td>
                            <td><?php echo e($user->user->mobile); ?></td>
                            <td><?php echo e($user->user->agency_name); ?></td>
                            <td>
                                <?php if($user->user->status == 0): ?>
                                <?php echo e('<span><span class="m-badge  m-badge--danger m-badge--wide">Suspended</span></span>'); ?>

                                <?php elseif($user->user->status == 1): ?>
                                <?php echo e('<span><span class="m-badge  m-badge--primary m-badge--wide">Active</span></span>'); ?>

                                <?php elseif($user->user->status == 2): ?>
                                <?php echo e('<span><span class="m-badge  m-badge--metal m-badge--wide">Pending</span></span>'); ?>

                                <?php endif; ?>
                            </td>
                            <td>
                                <?php echo e('<a data-toggle="modal" data-target="#m_modal_view'.$user->user->id.'"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View "><i class="la la-eye"></i></a>'); ?>

                                <?php echo e('<a href="'.url('showUserProfile').'/'.$user->user->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit "><i class="la la-edit"></i></a>'); ?>

                                <?php echo e('<a href="'.url('userIsDelete').'/'.$user->user->id.'/0" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete " ><i class="la la-trash-o"></i></a>'); ?>                                
                            </td>
                            <td> 
                                  <?php echo e('<a href="'.url('supplierLogin').'/'.$user->user->id.'"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Login "><i class="la la-sign-in"></i></a>'); ?>

                        </td>
                       
                        <td>
                            <?php if($user->user->status == 0): ?>
                            <?php echo e('<a href="'.url('changeUserStatus').'/'.$user->user->id.'/1" class="btn m-btn--pill    btn-primary" >Active</a>'); ?>

                            <?php elseif($user->user->status == 2): ?>
                            <?php echo e('<a href="'.url('changeUserStatus').'/'.$user->user->id.'/1" class="btn m-btn--pill    btn-primary" >Active</a>'); ?>

                            <?php elseif($user->user->status == 1): ?>
                            <?php echo e('<a href="'.url('changeUserStatus').'/'.$user->user->id.'/0" class="btn m-btn--pill    btn-danger" >Block</a>'); ?>

                            <?php endif; ?>
                            
                        </td>
                        
                    </tr>
                    
                    <div class="modal fade" id="m_modal_view<?php echo e($user->user->id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">
                                    View Supplier Details
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">
                                        &times;
                                    </span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <b>Agency Name</b> :- <?php echo e($user->user->agency_name); ?> <br><br>
                                            <b>Concerned Person Name</b> :- <?php echo e($user->user->first_name); ?> <br><br>
                                            <b>Agency Address</b> :- <?php echo e($user->user->agency_address); ?> <br><br>
                                            <b>Email</b> :- <?php echo e($user->user->email); ?><br><br>
                                            <b>Phone No</b> :- <?php echo e($user->user->mobile); ?> <br><br>
                                            
                                            
                                        </div>
                                        <div class="col-xl-6">
                                            <b>City</b> :- <?php echo e($user->user->city); ?><br><br>
                                            <b>Satate</b> :- <?php echo e($user->user->state); ?><br><br>
                                            <b>Country</b> :- <?php echo e($user->user->country); ?><br><br>
                                            <b>Pin Code</b> :- <?php echo e($user->user->pincode); ?><br><br>
                                            <b>Office No</b> :- <?php echo e($user->user->office_no); ?> <br><br>
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <span><br><b>Agency GST Details</b><br><br></span>
                                            <b>Name</b> :- <?php echo e($user->user->gst_name); ?><br><br>
                                            <b>State</b> :- <?php echo e($user->user->gst_state); ?><br><br>
                                            <b>GSTIN No</b> :- <?php echo e($user->user->gst_no); ?>

                                            
                                        </div>
                                    </div>
                                    
                                    
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button"  class="btn btn-secondary" data-dismiss="modal">
                                    Close
                                    </button>
                                    <button type="button" id="Submit-profile" class="btn btn-primary">
                                    Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlayouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>