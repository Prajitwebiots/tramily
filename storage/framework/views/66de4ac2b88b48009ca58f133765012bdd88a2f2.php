<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style type="text/css">
        * {font-family: verdana;font-size: 14px;padding: 0;vertical-align: middle;line-height: 150%;}
        a {text-decoration: none;}
        a:hover {text-decoration: underline;}
        tr {
            font-size: 14px !important;
        }
        h2{
            font-size: 18px;
            margin-bottom: 10px;
        }
        table{
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: left;
            border: 1px solid black;
            color: #000;
        }

    </style>
</head>
<body style="margin:0 auto;padding:0; background-color:#fff;">
<table style="width:730px;  margin: 0 auto;" cellspacing="3" cellpadding="0">
    <tr>
        <td>
            <div style="font-size:12px;color: #707070;padding: 15px 3%;width: 93.8%;background-color:#262734;float:left;">
                <div style='float:left;text-align:left;display: inline-table;'>
                    <img src="<?php echo asset('assets/app/media/img/logos/logo-2.png'); ?>" alt="Tour Tickets" style="height: 100px;width: 100px" />
                </div>
            </div>
            <div style="font-size:12px;color: #707070;border: 1px solid #E5E5E5;padding: 6%;width: 87.7%;background-color:#FFF;float:left;font-family: verdana;">

                <h2 style="color:#000000c7">Hello Admin<br></h2>
                <h2 style="color:#000000c7">Supplier:<?php echo $user->title_name.' '.$user->first_name.' '.$user->last_name; ?><br /></h2>
                <h2 style="color:#000000c7">Contact Details:  <br></h2>
                <table style="width: 100%; margin-left: 5%; margin-bottom: 5%">

                    <tr>
                        <td style="border: none;width: 20%">Email</td> <td style="border: none;width: 80%">: <?php echo $user->email; ?></td>
                    </tr>
                    <tr>
                        <td style="border: none;width: 20%">Mobile No </td> <td style="border: none;width: 80%">: <?php echo $user->mobile; ?></td>
                    </tr>
                    <tr>
                        <td style="border: none;width: 20%">Company Name </td> <td style="border: none;width: 80%" >: <?php echo $user->agency_name; ?></td>
                    </tr>
                </table>
                <p style="color:#000000c7">  Has update blocks as below   <br><br>
                    <b style="color:#000000c7">Client Details:</b>
                    <br>
                </p>
                <table style=" width: 100%;text-align: left;margin-bottom: 20px;">
                    <thead>
                    <tr>
                        <th width="25%">Sector</th>
                        <th width="15%">Date</th>
                        <th width="30%">Flight</th>
                        <th width="15%">PNR</th>
                        <th width="15%">OLD SEATS</th>
                        <th width="15%">NEW SEATS</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?php echo $onewaybook->flightSource->flight_source; ?> -  <?php echo $onewaybook->flightDestination->flight_destination; ?></td>
                        <td><?php echo e($onewaybook->flight_departure); ?></td>
                        <td> <?php echo $onewaybook->flight->flight_code.' '.$onewaybook->flight_number.' '.$onewaybook->flight_departure_time.' '.$onewaybook->flight_arrival;?></td>
                        <td><?php echo e($onewaybook->flight_pnr_no); ?></td>
                        <td><?php echo e($onewaybook->seat); ?></td>
                        <td><?php echo e($onewaybook->seat); ?></td>
                    </tr>
                    </tbody>
                </table>
                <span>Request you to check & approve the same.</span>
                <br>
                <b style="color:#000000c7">Thank You                </b><br>

                <p></p>
            </div>

        </td>
    </tr>
</table>
</body>
</html>