
<nav class="navbar navbar-light rounded navbar-toggleable-md">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#containerNavbar" aria-controls="containerNavbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
            <img style="width: 210px;" src="<?php echo e(url::asset('assets/images/logo.png')); ?>">
        </a>

        <div class="collapse navbar-collapse" id="containerNavbar">
            <ul class="navbar-nav mr-auto navbar-header">
                <li class="nav-item menu-item first active">
                    <a class="nav-link" href="<?php echo e(url('/')); ?>">Sports <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item menu-item">
                    <a class="nav-link" href="<?php echo e(url('live-betting')); ?>">Live Betting</a>
                </li>
                <li class="nav-item menu-item">
                    <a class="nav-link" href="#">Player Props</a>
                </li>
                <li class="nav-item menu-item">
                    <a class="nav-link" href="#">Casino</a>
                </li>
                <li class="nav-item menu-item">
                    <a class="nav-link" href="<?php echo e(url('about/affiliate')); ?>">About</a>
                </li>
                <li class="nav-item menu-item">
                    <a class="nav-link" href="<?php echo e(url('promotions')); ?>">Promotions</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-md-0">
                <div class="login-button open-login-modal"><img src="<?php echo e(url::asset('assets/images/svg/login-icon.svg')); ?>"><a href="<?php echo e(url('login')); ?>">Login</a></div>
                <div class="register-button open-register-modal"><a href="<?php echo e(url('register')); ?>">Register in 10 seconds</a></div>
            </form>
        </div>
    </nav>
    <div class="secondary-menu">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6">
                <div class="search">
                    <form class="search-form">
                        <input type="text" id="search" name="search" placeholder="What do you want to bet on?" autocomplete="off">
                    </form>
                </div>

                <div class="bets-button active"><img src="//cdn.coingaming.io/sportsbet/images/active-bets-icon.svg"><a href="/active-bets">Active Bets</a></div>
                <div class="bets-button history"><img src="//cdn.coingaming.io/sportsbet/images/active-bets-icon.svg"><a href="/betting-history">Betting History</a></div>

            </div>
            <div class="col-12 col-sm-6 col-md-6">
                <ul id="menu">
                    <li>
                        <input class="inpt-drpdwn" id="check01" type="checkbox" name="menu"/>
                        <label class="lbl-drpdwn" for="check01">Tasto menu 01</label>
                        <ul class="submenu">
                            <li><a href="#">Sotto menu 1</a></li>
                            <li><a href="#">Sotto menu 2</a></li>
                        </ul>
                    </li>
                </ul>
                <div id="accordianmenu" class="betslip">
                    <ul>
                        <li class="active">
                            <p id="cart-title">Betslip</p>
                            <i class="fa fa-angle-down" aria-hidden="true" style="float: right;position: relative;top: -21px;right: 14px;font-weight: bold;"></i>
                            <div class="betslip-open" style="display: none">
                                <div class="content">
                                    <div class="tab_container">
                                        <input id="tab1" type="radio" name="tabs" checked>
                                        <label class="tb-lbl" for="tab1"><span>Single</span></label>

                                        <input id="tab2" type="radio" name="tabs">
                                        <label class="tb-lbl" for="tab2"><span>Multibet</span></label>


                                        <section id="content1" class="tab-content">
                                            <div id="all_bets">
                                                                                                
                                            </div>
                                            <div class="separator"></div>
                                            <div class="single-portion">
                                                <div class="title">
                                                    <div class="accept-all-odds">
                                                        <input type="checkbox" class="chck-bx">Accept all odds changes
                                                    </div>
                                                    <div class="accept-all-odds">
                                                        <input type="checkbox" class="chck-bx">Quick bet
                                                    </div>

                                                    <div class="win">Potential Win <b> <span id="Spotential_win"> 0.00 </span> <small class="text-muted">mBtc</small></b></div>
                                                    <hr>
                                                    <div class="alert alert-danger alert1">
                                                        Your bet is exceeding your available balance
                                                    </div>
                                                    <div class="buttons">
                                                        <button class="btn green placeBtn disabled">
                                                            Place Bet
                                                        </button>
                                                        <a class="btn grey clearBtn enabled" href="#betslip" data-toggle="collapse" aria-controls="betslip" aria-expanded="true">
                                                            Clear
                                                        </a>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                </div>
                                            </div>
                                        </section>

                                        <section id="content2" class="tab-content">
                                            <div id="multi_bets">
                                                
                                            </div>
                                            <div class="separator"></div>
                                            <div class="single-portion">
                                                <div class="title">
                                                    <div class="accept-all-odds">
                                                        <input type="checkbox" class="chck-bx">Accept all odds changes
                                                    </div>
                                                    <div class="accept-all-odds">
                                                        <input type="checkbox" class="chck-bx">Quick bet
                                                    </div>

                                                    <div class="win">Potential Win <b>0.00 <small class="text-muted">mBtc</small></b></div>
                                                    <div class="alert alert-danger alert2">
                                                        Your bet is exceeding your available balance
                                                    </div>
                                                    <hr>
                                                    <div class="bet single">
                                                        <button class="btn grey dec-stake-50" value="-50">- 50</button>
                                                        <button class="btn grey dec-stake-50" value="-50">- 10</button>
                                                        <input type="text" class="stake-input single" placeholder="Stake">
                                                        <button class="btn grey dec-stake-50" value="-50">- 10</button>
                                                        <button class="btn grey dec-stake-50" value="-50">+ 50</button>
                                                    </div>
                                                    <hr>
                                                    <div class="buttons">
                                                        <button class="btn green placeBtn disabled">
                                                            Place Bet
                                                        </button>
                                                        <a class="btn grey clearBtn enabled" href="#betslip" data-toggle="collapse" aria-controls="betslip" aria-expanded="true">
                                                            Clear
                                                        </a>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>

                                    </div>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

