<?php $__env->startSection('title'); ?> User | Multi City Flight - Book <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
	.que2a,.que2b,.que3a,.que3b{
		display: none;
	}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
						Multi City - Book
					</h3>
				</div>


				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tabs m-portlet--brand m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Multi City Flight - Book
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="tab-content">
								<div class="tab-pane active" id="m_tabs_8_1" role="tabpanel">
									<div class="row m--padding-top-10"></div>
									<h5> First Flight information</h5>
									<hr>
									<div class="row">
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Flight Source : <?php echo e($multicity->flightSourceF->flight_source); ?>

											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Flight Destination : <?php echo e($multicity->flightDestinationF->flight_destination); ?>

											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Flight Name : <?php echo e($multicity->flightF->flight_name.' - '.$multicity->flightF->flight_code); ?>

											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Flight Number : <?php echo e($multicity->flight_number_f); ?>

											</span>
											</p>
										</div>

									</div>

									<div class="row m--padding-top-40">
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Departure Date : <?php echo e(date("d-M-Y D", strtotime($multicity->flight_departure_date_f))); ?>

											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Departure Time : <?php echo e(date("h:i A", strtotime($multicity->flight_departure_time_f))); ?>

											</span>
											</p>
										</div>
										<div class="col-md-4">
											<p>
											<span class="m--font-bolder">
												Arrival Time : <?php echo e(date("h:i A", strtotime($multicity->flight_arrival_time_f))); ?>

											</span>
											</p>
										</div>
									</div>

									<div class="row m--padding-top-40"></div>
									<h5>Second Flight information</h5>
									<hr>

									<div class="row">
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Flight Source : <?php echo e($multicity->flightSourceS->flight_source); ?>

											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Flight Destination : <?php echo e($multicity->flightDestinationS->flight_destination); ?>

											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Flight Name : <?php echo e($multicity->flightS->flight_name.'-'.$multicity->flightS->flight_code); ?>

											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Flight Number : <?php echo e($multicity->flight_number_s); ?>

											</span>
											</p>
										</div>

									</div>

										<div class="row m--padding-top-40">
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Departure Date : <?php echo e(date("d-M-Y D", strtotime($multicity->flight_departure_date_s))); ?>

												</span>
												</p>
											</div>
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Departure Time : <?php echo e(date("h:i A", strtotime($multicity->flight_departure_time_s))); ?>

												</span>
												</p>
											</div>
											<div class="col-md-4">
												<p>
												<span class="m--font-bolder">
													Arrival Time : <?php echo e(date("h:i A", strtotime($multicity->flight_arrival_time_s))); ?>

												</span>
												</p>
											</div>
										</div>
										<?php if($Third != 1): ?>
										<div class="row m--padding-top-40"></div>
										<h5> Third Flight information</h5>
										<hr>

										<div class="row">
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Flight Source : <?php echo e($multicity->flightSourceT->flight_source); ?>

												</span>
												</p>
											</div>
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Flight Destination : <?php echo e($multicity->flightDestinationT->flight_destination); ?>

												</span>
												</p>
											</div>
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Flight Name : <?php echo e($multicity->flightT->flight_name.'-'.$multicity->flightT->flight_code); ?>

												</span>
												</p>
											</div>
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Flight Number : <?php echo e($multicity->flight_number_t); ?>

												</span>
												</p>
											</div>

										</div>

										<div class="row m--padding-top-40">
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Departure Date : <?php echo e(date("d-M-Y D", strtotime($multicity->flight_departure_date_t))); ?>

												</span>
												</p>
											</div>
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Departure Time : <?php echo e(date("h:i A", strtotime($multicity->flight_departure_time_t))); ?>

												</span>
												</p>
											</div>
											<div class="col-md-4">
												<p>
												<span class="m--font-bolder">
													Arrival Time : <?php echo e(date("h:i A", strtotime($multicity->flight_arrival_time_t))); ?>

												</span>
												</p>
											</div>
										</div>
										<?php endif; ?>

										<hr>
										<div class="row m--padding-top-40">
											<?php if($multicity->flight_via): ?>
											<div class="col-md-2 ">
												<p>
												<span class="m--font-bolder">
													Via : <?php echo e($multicity->flight_via); ?>

												</span>

												</p>
											</div>
											<?php endif; ?>

											<div class="col-md-3 ">
												<p>
												<span class="m--font-bolder">
													Price : <?php echo e($multicity->flight_price * $adults); ?>

												</span>
													<br>
													<span class="m-form__help"> price <?php echo e($multicity->flight_price); ?>* per person  </span>
												</p>
											</div>
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Seat : <?php echo e($adults); ?>

												</span>
												</p>
											</div>
										</div>
										<hr>
									<form action="<?php echo e(URL('flights/payment/multicity')); ?>" method="post" class="m-form m-form--fit m-form--label-align-right" id="multi_city" >
										<div class="row">
											<?php echo e(csrf_field()); ?>

											<?php for($i=1; $i<=$adults;$i++): ?>
												<div class="col-md-6 m--padding-top-20">
													<p>
												<span class="m--font-bolder">
													Full Name:
												</span>
													</p>
													<div class="row">
														<div class="col-md-2 ">
															<select name="title_<?php echo e($i); ?>" required class=" custom-select textname ">

																<option value="Mr.">Mr.</option>
																<option value="Ms.">Ms.</option>
																<option value="Mrs.">Mrs.</option>
															</select>
															<span class="m-form__help"></span>

														</div>
														<div class="col-md-5 ">
															<input type="text" id="txtNamefirst_<?php echo e($i); ?>" required  minlength="1" maxlength="20" name="txtNamefirst_<?php echo e($i); ?>" class="textname txtNamefirst form-control" autocomplete="off" value="" placeholder="First Name">
															<span class="m-form__help"></span>
														</div>
														<div class="col-md-5 ">
															<input type="text" id="txtNamelast_<?php echo e($i); ?>" required  minlength="1" maxlength="20"  name="txtNamelast_<?php echo e($i); ?>" class="textname txtNamefirst form-control " autocomplete="off" value="" placeholder="Last Name">
															<span class="m-form__help"></span>
														</div>
													</div>

												</div>
											<?php endfor; ?>
										</div>
									<hr>
									<div class="row">
										<div class="col m--align-right">
											<a class="btn btn-outline-primary" id="bookNowFlightModalform" href="#" data-id="in_flight_id" data-toggle="modal" data-target="#bookNowFlightModal">Checkout</a>

											<button type="reset" class="btn btn-secondary">
												Cancel
											</button>
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!--end::Portlet-->
				</div>
			</div>
		</div>
	</div>
	<!-- end::Body -->

	<!--begin::Modal-->
	<div class="modal fade" id="bookNowFlightModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Flight Booking</h4>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">
											&times;
										</span>
					</button>
				</div>
				<div class="modal-body ">
					<form id="frmOneWayBookFlight" name="frmOneWayBookFlight" method="POST" action="" class="form-hotel">
						<div class="modal-body">
							<div class="row m--padding-top-10"></div>
							<h5> First Flight information</h5>
							<hr>
							<div class="row">
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Source : <?php echo e($multicity->flightSourceF->flight_source); ?>

											</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Destination : <?php echo e($multicity->flightDestinationF->flight_destination); ?>

											</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Name : <?php echo e($multicity->flightF->flight_name.'-'.$multicity->flightF->flight_code); ?>

											</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Number : <?php echo e($multicity->flight_number_f); ?>

											</span>
									</p>
								</div>

							</div>

							<div class="row m--padding-top-40">
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Departure Date : <?php echo e(date("d-M-Y D", strtotime($multicity->flight_departure_date_f))); ?>

											</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Departure Time : <?php echo e(date("h:i A", strtotime($multicity->flight_departure_time_f))); ?>

											</span>
									</p>
								</div>
								<div class="col-md-4">
									<p>
											<span class="m--font-bolder">
												Arrival Time : <?php echo e(date("h:i A", strtotime($multicity->flight_arrival_time_f))); ?>

											</span>
									</p>
								</div>
							</div>

							<div class="row m--padding-top-40"></div>
							<h5>Second Flight information</h5>
							<hr>

							<div class="row">
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Source : <?php echo e($multicity->flightSourceS->flight_source); ?>

											</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Destination : <?php echo e($multicity->flightDestinationS->flight_destination); ?>

											</span>
									</p>
								</div>

								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Name : <?php echo e($multicity->flightS->flight_name.'-'.$multicity->flightS->flight_code); ?>

											</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Number : <?php echo e($multicity->flight_number_s); ?>

											</span>
									</p>
								</div>

							</div>

							<div class="row m--padding-top-40">
								<div class="col-md-3">
									<p>
												<span class="m--font-bolder">
													Departure Date : <?php echo e(date("d-M-Y D", strtotime($multicity->flight_departure_date_s))); ?>

												</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
												<span class="m--font-bolder">
													Departure Time : <?php echo e(date("h:i A", strtotime($multicity->flight_departure_time_s))); ?>

												</span>
									</p>
								</div>
								<div class="col-md-4">
									<p>
												<span class="m--font-bolder">
													Arrival Time : <?php echo e(date("h:i A", strtotime($multicity->flight_arrival_time_s))); ?>

												</span>
									</p>
								</div>
							</div>
							<?php if($Third != 1): ?>
								<div class="row m--padding-top-40"></div>
								<h5> Third Flight information</h5>
								<hr>

								<div class="row">
									<div class="col-md-3">
										<p>
												<span class="m--font-bolder">
													Flight Source : <?php echo e($multicity->flightSourceT->flight_source); ?>

												</span>
										</p>
									</div>
									<div class="col-md-3">
										<p>
												<span class="m--font-bolder">
													Flight Destination : <?php echo e($multicity->flightDestinationT->flight_destination); ?>

												</span>
										</p>
									</div>
									<div class="col-md-3">
										<p>
												<span class="m--font-bolder">
													Flight Name : <?php echo e($multicity->flightT->flight_name.'-'.$multicity->flightT->flight_code); ?>

												</span>
										</p>
									</div>
									<div class="col-md-3">
										<p>
												<span class="m--font-bolder">
													Flight Number : <?php echo e($multicity->flight_number_t); ?>

												</span>
										</p>
									</div>

								</div>

								<div class="row m--padding-top-40">
									<div class="col-md-3">
										<p>
												<span class="m--font-bolder">
													Departure Date : <?php echo e(date("d-M-Y D", strtotime($multicity->flight_departure_date_t))); ?>

												</span>
										</p>
									</div>
									<div class="col-md-3">
										<p>
												<span class="m--font-bolder">
													Departure Time : <?php echo e(date("h:i A", strtotime($multicity->flight_departure_time_t))); ?>

												</span>
										</p>
									</div>
									<div class="col-md-4">
										<p>
												<span class="m--font-bolder">
													Arrival Time : <?php echo e(date("h:i A", strtotime($multicity->flight_arrival_time_t))); ?>

												</span>
										</p>
									</div>
								</div>
							<?php endif; ?>

							<hr>
							<div class="row m--padding-top-40">
								<?php if($multicity->flight_via): ?>
									<div class="col-md-2 ">
										<p>
											<span class="m--font-bolder">
												Via : <?php echo e($multicity->flight_via); ?>

											</span>

										</p>
									</div>
								<?php endif; ?>

								<div class="col-md-3 ">
									<p>
												<span class="m--font-bolder">
													Price : <?php echo e($multicity->flight_price * $adults); ?>

												</span>
										<br>
										<span class="m-form__help"> price <?php echo e($multicity->flight_price); ?>*per person  </span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
										<span class="m--font-bolder">
											Seat : <?php echo e($adults); ?>

										</span>
									</p>
								</div>
							</div>
							<hr>

							<h6>Member Information </h6>
							<hr>
							<div class="row" id="name_info"></div>
						</div>
					</form>

				</div>


				<div class="row m--padding-20 ">
					<div class="col-md-6 m--padding-right-50 m--pull-left m--align-left">
						<label class="m-checkbox m-checkbox--focus">
							<input form="multi_city" type="checkbox" name="agree" required>
							I Agree the
							<a href="#" class="m-link m-link--focus">
								terms and conditions
							</a>
							.
							<span></span>
						</label>
					</div>
					<div class="col-md-6 m--align-right m--padding-left-50">

						<button  type="button" id="close_model" class="btn btn-secondary" data-dismiss="modal">
							Close
						</button>
						<button  form="multi_city" type="submit" class=" btn btn-primary">
							Buy
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--end::Modal-->
	<?php $__env->stopSection(); ?>
	<?php $__env->startSection('bottom_script'); ?>

	<!--begin::Page Vendors -->
	<script src="<?php echo e(asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>" type="text/javascript"></script>
	
		<script>
			$('#bookNowFlightModalform').click(function(){

				var nameform= "";
				var ms="";
				var fn ="";
				$(".textname").each(function() {

					if($(this).val() != "") {
						if((($(this).val())=="Mr.") || (($(this).val())=="Mrs.") || (($(this).val())=="Ms."))
						{
							ms = $(this).val();
						}
						else if($(this).attr('class')=='textname txtNamefirst form-control')
						{
							fn = $(this).val();
						}
						else
						{
							nameform += '<div class="col-xs-12 col-sm-12 col-md-6 form-group "><p>Name : ' + ms +fn+' '+$(this).val() + '</p></div>';
							$('#name_info').empty().append(nameform);
							ms="";
						}
					}
					else
					{
						var id = $(this).attr("id");
						$("#"+id).css("border-color","red");
						setTimeout(function(){
							$('#bookNowFlightModal').modal('toggle');
						}, 1000);
					}
				});

			});
			$( " .textname" ).keypress(function() {
				if($(this).val() == "")
				{
					var id = $(this).attr("id");
					$("#"+id).css("border-color","rgba(0,0,0,.15)");
				}

			});
		</script>
	<!--end::Page Vendors -->
	<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>