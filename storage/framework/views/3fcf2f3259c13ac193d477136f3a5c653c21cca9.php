<?php $__env->startSection('title'); ?> Supplier | Round Trip Flight <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
	.que2a,.que2b,.que3a,.que3b{
		display: none;
	}
	.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					Round Trip Flight List
					</h3>
				</div>
			</div>
		</div>
		<div class="m-content">
			
			<?php if(session('error')): ?><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div>   <br><?php endif; ?>
			<?php if(session('success')): ?>   <br><div class="alert alert-success"><?php echo e(session('success')); ?></div>   <br><?php endif; ?>
			
			<div class="m-portlet m-portlet--mobile">
				
				<div class="m-portlet__body">
					<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-4">
										<div class="m-input-icon m-input-icon--left">
											<input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
											<span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 order-1 order-xl-2 m--align-right">
								<a href="<?php echo e(url('round_trip_import')); ?>" class="btn m-btn--pill    btn-primary" >Import</a>
								<a href="<?php echo e(route('supplier-FlightRoundTrip.create')); ?>" class="btn m-btn--pill    btn-primary" >Add New</a>
								</h3>
								<div class="m-separator m-separator--dashed d-xl-none"></div>
							</div>
						</div>
					</div>
					<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form">
						<div class="row align-items-center">
							<div class="col-xl-12">
								<div class="m-form__group m-form__group--inline">
									<div class="m-form__label m-form__label-no-wrap">
										<label class="m--font-bold m--font-danger-">
											Selected
											<span id="m_datatable_selected_number"></span>
											records:
										</label>
									</div>
									<div class="m-form__control">
										<div class="btn-toolbar">
											&nbsp;&nbsp;&nbsp;
											<?php echo e(Form::open(['method' => 'post','url' => ['supplier-flightRoundTrip-destroy'],'style'=>'display:inline' ,'id'=>'myform'])); ?>

											<button class="btn btn-sm m-btn--pill btn-danger" type="submit" id="m_datatable_check_all">
											Delete
											</button>
											<?php echo e(Form::close()); ?>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--begin: Datatable -->
					<table class="m-datatable" id="html_table" width="100%">
						<thead>
							<tr>
								<th>RecordID</th>
								<th>Sr.</th>
								<th>Flight Departure</th>
								<th></th>
								<th>Flight Arrival</th>
								<th>Flight Name</th>
								<th>Price</th>
								<th>Seat</th>
								<th>Sold</th>
								<th>Avail.</th>
								<th>PNR No</th>
								<th>Via</th>
								<th>Status</th>
								<th>Action</th>
								
							</tr>
						</thead>
						<tbody>
							
							<?php $i = 1; ?>
							
							<?php $__currentLoopData = $flight; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flights): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>

								<td><?php echo e('<label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" form="myform"  value='.$flights->id.' class="select_checkbox" name="check[]" type="checkbox"><span></span></label>'); ?> </td>
								<td><?php echo e($i++); ?></td>
								<td>

									<?php echo e($flights->flightSource->flight_source); ?>

									<?php echo e('<span class="m--padding-3"></span>'); ?>

									<?php echo e("<code>"); ?>   <?php echo date('d M y', strtotime($flights->flight_departure_date)); ?> -  <?php echo e($flights->flight_departure_time); ?> <?php echo e("</code>"); ?>

									<?php echo e('<span>----------------------</span>'); ?>

									<?php echo e($flights->flightSourceReturn->flight_destination); ?>

									<?php echo e('<span class="m--padding-3"></span>'); ?>

									<?php echo e("<code>"); ?>   <?php echo date('d M y', strtotime($flights->return_departure_date)); ?> -  <?php echo e($flights->return_departure_time); ?> <?php echo e("</code>"); ?>

								</td>
								<td><?php echo e('<i class="fa fa-fighter-jet"></i>'); ?></td>
								<td>

									<?php echo e($flights->flightDestination->flight_destination); ?>


									<?php echo e('<span class="m--padding-3"></span>'); ?>

									<?php echo e("<code>"); ?>  <?php echo e($flights->flight_arrival_time); ?> <?php echo e("</code>"); ?>

									<?php echo e('<span>---------------</span>'); ?>

									<?php echo e($flights->flightDestinationReturn->flight_source); ?>

									<?php echo e('<span class="m--padding-3"></span>'); ?>

									<?php echo e("<code>"); ?>  <?php echo e($flights->return_arrival_time); ?> <?php echo e("</code>"); ?>

								</td>


								<td>
									<?php echo e("<b>"); ?> <?php echo e($flights->flight->flight_name); ?> - <?php echo e($flights->flight_number); ?>  <?php echo e("</b>"); ?>

									<?php echo e('<span>----------</span>'); ?>

									<?php echo e("<b>"); ?> <?php echo e($flights->flightReturn->flight_name); ?> - <?php echo e($flights->return_flight_number); ?>  <?php echo e("</b>"); ?>

								</td>

								<td><?php echo e($flights->flight_price); ?></td>
								<td><?php echo e($flights->seat + $flights->sold); ?></td>
								<td><?php echo e($flights->sold); ?></td>
								<td><?php echo e($flights->seat); ?></td>

								<td>

									<?php echo e($flights->flight_pnr_no); ?>

									<?php echo e('<span>------</span>'); ?>

									<?php echo e($flights->flight_pnr_no2); ?>

								</td>
								<td>

									<?php echo e($flights->flight_via); ?>


								</td>
								<td>
									<?php if($flights->status == 0): ?>
									<?php echo e('<span><span class="m-badge  m-badge--danger m-badge--wide">Disapproved</span></span>'); ?>

									<?php elseif($flights->status == 1): ?>
									<?php echo e('<span><span class="m-badge  m-badge--primary m-badge--wide">Approved</span></span>'); ?>

									<?php elseif($flights->status == 2): ?>
									<?php echo e('<span><span class="m-badge  m-badge--metal m-badge--wide">Pending</span></span>'); ?>

									<?php endif; ?>
								</td>

								<td>
									<?php echo e('<a href="'.route('supplier-FlightRoundTrip.edit',$flights->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit "><i class="la la-edit"></i></a>'); ?>

									<?php echo e('<a onclick="return deletemsg('.$flights->id.');" href="'.url('roundflightIsDelete').'/'.$flights->id.'/0" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete " ><i class="la la-trash-o"></i></a>'); ?>

								</td>
							</tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							
							
						</tbody>
					</table>
					<!--end: Datatable -->
				</div>
			</div>
		</div>
		<!--end:: Widgets/Stats-->
		
	</div>
</div>
<!-- end::Body -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('bottom_script'); ?>
<script src="<?php echo e(asset('assets/demo/default/custom/components/datatables/base/html-table.js')); ?>" type="text/javascript"></script>
<!--begin::Page Vendors -->
<script src="<?php echo e(asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')); ?>" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="<?php echo e(asset('assets/app/js/dashboard.js')); ?>" type="text/javascript"></script>
<!--end::Page Snippets -->
<script>
$( document ).ready(function() {
$(".m-checkbox").click(function(){
$(".collapse").addClass("show");
});
});
</script>
<script>
    function deletemsg(id) {
        swal({
            title: 'Are you sure?',
            text: "Are you sure you wish to delete ",
            showCancelButton: true,
            confirmButtonColor: '#34bfa3',
            cancelButtonColor: '#f4516c',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if(result.value)
        {
			
                window.location= "<?php echo e(url('roundflightIsDelete')); ?>/"+id+'/0';
            return true;
        }
    else
        {return false;
        }
    }
    );
        return false;
    }

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashMaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>