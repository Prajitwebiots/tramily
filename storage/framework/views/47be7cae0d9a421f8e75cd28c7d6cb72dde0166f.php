<!-- head -->
<?php $__env->startSection('title'); ?>
    Betting
<?php $__env->stopSection(); ?>
<!-- title -->
<?php $__env->startSection('head'); ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?php echo e(URL::asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" /> 
    <link href="<?php echo e(URL::asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
     <style>
        .blog-modal-main-div{
            padding-right: 30em;
        }
    </style>
<?php $__env->stopSection(); ?>
    
<?php $__env->startSection('content'); ?>


<?php $__env->startSection('content'); ?>

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="min-height: 1489px;">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Blogs

                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="<?php echo e(url('admin/dashboard')); ?>">Home</a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <span class="active">Blogs</span>
            </li>

            <span class="set-right"><a href="<?php echo e(route('admin-blog.create')); ?>" class="btn btn-primary "  >Add New</a></span>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <div class="row">

        </div>
        <br>
        <?php if(session('error')): ?><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
        <?php if(session('success')): ?><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
        <br>
        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Blogs</span>
                        </div>

                        <div class="tools"></div>
                    </div>

                    <div class="portlet-body">
                        <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Created Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody><?php $i = 1; ?>


                            <?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($i++); ?></td>
                                <td>
                                 <?php if(file_exists(public_path('assets/images/blog/'.$blog->image))): ?>
                                    <img src="<?php echo e(asset('assets/images/blog')); ?>/<?php echo e($blog->image); ?>" width="80px" height="80px" /> 
                                <?php else: ?>
                                    <img src="<?php echo e(asset('assets/images/noimage.png')); ?>" height="80px" width="80px" />
                                <?php endif; ?>
                                   
                                </td>
                                <td><?php echo e($blog->title); ?></td>
                                
                                <td><?php echo e(str_limit($blog->description, 100)); ?></td>
                               
                                <td> <?php echo date('j M, Y h:i:s A', strtotime($blog->created_at)); ?></td>
                                <td >
                                    <a href="#form_moda<?php echo e($blog->id); ?>" data-toggle="modal" class="btn btn-xs blue" style="margin-left: 5px;"><i class="fa fa-eye"></i>  </a>
                                    <a href="<?php echo e(route('admin-blog.edit',$blog->id)); ?>" data-toggle="modal" class="btn btn-xs blue" style="margin-left: 5px;"><i class="fa fa-pencil"></i>  </a>
                                   
                                   
                                    <?php echo e(Form::open(['method' => 'DELETE', 'class'=>'set-right' ,'route' => ['admin-blog.destroy',$blog->id]])); ?>

                                       <?php echo e(Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs'] )); ?>

                                    <?php echo e(Form::close()); ?>

                                    
                                    <div id="form_moda<?php echo e($blog->id); ?>" class="blog-modal-main-div modal fade" role="dialog" aria-hidden="true" tabindex="-1" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content" style="    width: 195%;">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Blog  Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <h3 class="text-center"><?php echo e($blog->title); ?></h3>
                                                        <div class="col-md-12">
                                                            <table class="table table-hover table-bordered">
                                                                <tr>
                                                                    <th>Image:</th><td>
                                                                         <?php if(file_exists(public_path('assets/images/blog/'.$blog->image))): ?>
                                                                            <img src="<?php echo e(URL::asset('assets/images/blog')); ?>/<?php echo e($blog->image); ?>" width="80px" height="80px" /> 
                                                                        <?php else: ?>
                                                                            <img src="<?php echo e(URL::asset('images/noimage.png')); ?>" height="80px" width="80px" />
                                                                        <?php endif; ?>
                                                                       </td>
                                                                </tr>

                                                                <tr>
                                                                    <th>Title:</th><td><?php echo e($blog->title); ?></td>
                                                                </tr>

                                                                <tr>
                                                                    <th>Description:</th><td><?php echo e($blog->description); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Long Description:</th><td><?php echo $blog->long_description; ?></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </td>

                            </tr>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>

    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('script'); ?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-buttons.min.js')); ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
      
    </script>
    <?php $__env->stopSection(); ?>
   
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>