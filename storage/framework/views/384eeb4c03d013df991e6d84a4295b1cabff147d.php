<?php $__env->startSection('title'); ?>
    Betting
<?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="min-height: 1603px;">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Edit Sub Category

                </h1>
            </div>
            <!-- END PAGE TITLE -->

        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span class="active">Edit Sub Category</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-10 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-blue-sunglo">
                            <i class="icon-settings font-blue-sunglo"></i>
                            <span class="caption-subject bold uppercase"> Edit Sub Category</span>
                        </div>

                    </div>


                    <?php if(session('success')): ?>
                    <div class="alert alert-success">
                        <?php echo e(session('success')); ?>

                    </div>
                    <?php endif; ?>


                    <div class="portlet-body form">
                    
                               <?php echo e(Form::model($subcategory, array('route' => array('admin-subcategory.update', $subcategory->id), 'method' => 'PUT'))); ?>

                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <div class="form-body">
                               <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <label>Category Name</label>
                                    <select class="form-control " name="catid">
                                        <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($cat->id); ?>"  <?php if($cat->id == $subcategory->parent_id): ?> selected <?php endif; ?> ><?php echo e($cat->name); ?></option> 
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>               
                                    </select>
                                     <?php if($errors->has('catid')): ?>
                                 <span class="help-block">
                                     <strong><?php echo e($errors->first('catid')); ?></strong>
                                 </span>
                                 <?php endif; ?>
                                </div>

                                <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <label>Sub Category Name</label>
                                    <input type="text" class="form-control" value="<?php echo e($subcategory->name); ?>" placeholder="Enter Name" name="name">
                                     <?php if($errors->has('name')): ?>
                                 <span class="help-block">
                                     <strong><?php echo e($errors->first('name')); ?></strong>
                                 </span>
                                 <?php endif; ?>
                                </div>

                                  <div class="form-group <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <label>Slug Name</label>
                                    <input type="text" class="form-control" value="<?php echo e($subcategory->slug); ?>" placeholder="Enter Slug" name="slug">
                                     <?php if($errors->has('slug')): ?>
                                 <span class="help-block">
                                     <strong><?php echo e($errors->first('slug')); ?></strong>
                                 </span>
                                 <?php endif; ?>
                                </div>


                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                      <?php echo Form::close();; ?>

                    </div>
                </div>

            </div>

        </div>

        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>