
$( ".errorsignuphidesupp").hide();
$( ".successsignupsupp").hide();
$('#m_login_signup_submit_supp').click( function(e){
    
 var formdata = $("#supp").serialize();
        $.ajax({
            type:'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
            url:"/supplierRegister",
            data: formdata,
            success: function (responseData) {
          if(responseData)  { 
         $( ".successsignupsupp").show();
          $(".successsignupsupp").append('<div class="m-alert m-alert--outline alert alert-success alert-dismissible" role="alert">         <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>           <span>Thank you. To complete your registration please check your email.</span>      </div>');                   
            }
            },
            error: function (responseData) {
                var errors = $.parseJSON(responseData.responseText);
                var i=1;
                $.each( errors, function( key, value ) {
                    $(".errorsignupsupp").append('<li>'+ i++ +'.'+ value+'</li><br>' );
                    $( ".errorsignuphidesupp").show();
                });
                
            }
        });
    });