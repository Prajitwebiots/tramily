$( ".errorsignuphide").hide();
var SnippetLogin = function() {
    var e = $("#m_login"),
        i = function(e, i, a) {
            var t = $('<div class="alert alert-' + i + ' alert-dismissible fade show   m-alert m-alert--air" role="alert">\t\t\t<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\t\t\t<span></span>\t\t</div>');
            e.find(".alert").remove(), t.prependTo(e), t.animateClass("fadeIn animated"), t.find("span").html(a)
        },
        a = function() {
            e.removeClass("m-login--forget-password"), e.removeClass("m-login--signin"), e.addClass("m-login--signup"), e.find(".m-login__signup").animateClass("flipInX animated")
        },
        t = function() {
            e.removeClass("m-login--forget-password"), e.removeClass("m-login--signup"), e.addClass("m-login--signin"), e.find(".m-login__signin").animateClass("flipInX animated")
        },
        r = function() {
            e.removeClass("m-login--signin"), e.removeClass("m-login--signup"), e.addClass("m-login--forget-password"), e.find(".m-login__forget-password").animateClass("flipInX animated")
        },
        n = function() {
            $("#m_login_forget_password").click(function(e) {
                e.preventDefault(), r()
            }), $("#m_login_forget_password_cancel").click(function(e) {
                e.preventDefault(), t()
            }), $("#m_login_signup").click(function(e) {
                e.preventDefault(), a()
            }), $("#m_login_signup_cancel").click(function(e) {
                e.preventDefault(), t()
            })
        },
        l = function() {
            $("#m_login_signin_submit").click(function(e) {

                e.preventDefault();
                var a = $(this),
                    t = $(this).closest("form");
                t.validate({
                    rules: {
                        email: {
                            required: !0,
                            email: !0
                        },
                        password: {
                            required: !0
                        }
                    }
                }), t.valid() && (a.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), t.ajaxSubmit({
                    url: "",
                    success: function(e, r, n, l) {
                    	if(e==1)

                    		 window.location = 'user/dashboard';
                    	else{
	                        setTimeout(function() {
	                            a.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(t, "danger", e)
	                        }, 2e3)

                // var errors = $.parseJSON(response.responseText);
                // var i=1;
                // $.each( errors, function( key, value ) {
                //     $(".errorsignup").append('<li>'+ i++ +'.'+ value+'</li><br>' );
                //     $( ".errorsignup").show();
                // });
	                    }
                    },
                    error: function(response,status,xhr ) { 
                            setTimeout(function() {
                                var keys = "";
                                var val = "";
                                for (var key in response.responseJSON.errors){
                                    console.log(key, response.responseJSON.errors[key]);
                                    val = val+' '+ (key, response.responseJSON.errors[key]);
                                }
                                a.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(t, "danger", val)
                            }, 2e3)


                // var errors = $.parseJSON(response.responseText);
                // var i=1;
                // $.each( errors, function( key, value ) {
                //     $(".errorsignup").append('<li>'+ i++ +'.'+ value+'</li><br>' );
                //     $( ".errorsignup").show();
                // });

                    }
                }))
            })
        },
        s = function() {
            $("#m_login_signup_submit").click(function(a) {
                a.preventDefault();
                var r = $(this),
                    n = $(this).closest("form");
                n.validate({
                    // rules: {
                    //     first_name: {
                    //         required: !0
                    //     },
                    //     last_name: {
                    //         required: !0
                    //     },
                    //     email: {
                    //         required: !0,
                    //         email: !0
                    //     },
                    //     password: {
                    //         required: !0
                    //     },
                    //     mobile: {
                    //         required: !0,
                    //         phoneUS:!0,
                    //     },
                    //     country: {
                    //         required: !0
                    //     },
                    //     state: {
                    //         required: !0
                    //     },
                    //     city: {
                    //         required: !0
                    //     },
                    //     agency_name: {
                    //         required: !0
                    //     },
                    //     agency_address: {
                    //         required: !0
                    //     },
                    //     pincode: {
                    //         required: !0
                    //     },
                    //     agree: {
                    //         required: !0
                    //     }


                    // }
                }), n.valid() && (r.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), n.ajaxSubmit({
                    url: "register",
                    success: function (a, l, s, o) {
                        if(a==1){
                            setTimeout(function () {
                                r.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), n.clearForm(), n.validate().resetForm(), t();
                                var a = e.find(".m-login__signin form");
                                a.clearForm(), a.validate().resetForm(), i(a, "success", "Thank you. To complete your registration please check your email.")
                            }, 2e3)
                        }
                        else {

                            setTimeout(function() {
                                r.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(n, "danger", a)
                            }, 2e3)
                        }
                    },
                    error: function(response,status,xhr ) { 
                        //     setTimeout(function() {
                        //         var keys = "";
                        //         var val = "";
                        //         for (var key in response.responseJSON.errors){
                        //             console.log(key, response.responseJSON.errors[key]);
                        //             val = val+' '+ (key, response.responseJSON.errors[key]);
                        //         }
                        //         r.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(n, "danger", val)
                        //     }, 2e3)
                        // console.log(response.responseJSON.errors); 
                    
                var errors = $.parseJSON(response.responseText);
                var i=1;
                $.each( errors, function( key, value ) {
                    $(".errorsignup").append('<li>'+ i++ +'.'+ value+'</li><br>' );
                    $( ".errorsignuphide").show();
                });

                    }
                }))
            })
        },
        ss = function() {
            // $("#m_login_signup_submit_supp").click(function(a) {
            //     a.preventDefault();
            //     var r = $(this),
            //         n = $(this).closest("form");
            //     n.validate({
            //         rules: {
            //             first_name: {
            //                 required: !0
            //             },
            //             last_name: {
            //                 required: !0
            //             },
            //             email: {
            //                 required: !0,
            //                 email: !0
            //             },
            //             password: {
            //                 required: !0
            //             },
            //             mobile: {
            //                 required: !0,
            //                 phoneUS:!0,
            //             },
            //             country: {
            //                 required: !0
            //             },
            //             state: {
            //                 required: !0
            //             },
            //             city: {
            //                 required: !0
            //             },
            //             agency_name: {
            //                 required: !0
            //             },
            //             agency_address: {
            //                 required: !0
            //             },
            //             pincode: {
            //                 required: !0
            //             },
            //             agree: {
            //                 required: !0
            //             }


            //         }
            //     }), n.valid() && (r.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), n.ajaxSubmit({
            //         url: "supplierRegister",
            //         success: function (a, l, s, o) {
            //             if(a==1){
            //                 setTimeout(function () {
            //                     r.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), n.clearForm(), n.validate().resetForm(), t();
            //                     var a = e.find(".m-login__signin form");
            //                     a.clearForm(), a.validate().resetForm(), i(a, "success", "Thank you. To complete your registration please check your email.")
            //                 }, 2e3)
            //             }
            //             else {

            //                 setTimeout(function() {
            //                     r.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(n, "danger", a)
            //                 }, 2e3)
            //             }
            //         },
            //         error: function(response,status,xhr ) { 
            //             //     setTimeout(function() {
            //             //         var keys = "";
            //             //         var val = "";
            //             //         for (var key in response.responseJSON.errors){
            //             //             console.log(key, response.responseJSON.errors[key]);
            //             //             val = val+' '+ (key, response.responseJSON.errors[key]);
            //             //         }
            //             //         r.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(n, "danger", val)
            //             //     }, 2e3)
            //             // console.log(response.responseJSON.errors); 
                    
            //     var errors = $.parseJSON(response.responseText);
            //     var i=1;
            //     $.each( errors, function( key, value ) {
            //         $(".errorsignup").append('<li>'+ i++ +'.'+ value+'</li><br>' );
            //         $( ".errorsignuphide").show();
            //     });

            //         }
            //     }))
            // })
        },
        o = function() {
            $("#m_login_forget_password_submit").click(function(a) {
                a.preventDefault();
                var r = $(this),
                    n = $(this).closest("form");
                n.validate({
                    rules: {
                        email: {
                            required: !0,
                            email: !0
                        }
                    }
                }), n.valid() && (r.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), n.ajaxSubmit({
                    url: "forgot",
                    success: function(a, l, s, o) {
                        console.log(a);
                        if(a==1){
                            setTimeout(function() {
                                r.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), n.clearForm(), n.validate().resetForm(), t();
                                var a = e.find(".m-login__signin form");
                                a.clearForm(), a.validate().resetForm(), i(a, "success", "Cool! Password recovery instruction has been sent to your email.")
                            }, 2e3)
                        }
                        else {

                            setTimeout(function() {
                                r.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(n, "danger", a)
                            }, 2e3)
                        }
                    },
                    error: function(response,status,xhr ) { 
                            setTimeout(function() {
                                var keys = "";
                                var val = "";
                                for (var key in response.responseJSON.errors){
                                    console.log(key, response.responseJSON.errors[key]);
                                    val = val+' '+ (key, response.responseJSON.errors[key]);
                                }
                                r.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(n, "danger", val)
                            }, 2e3)
                        console.log(response.responseJSON.errors); 
                    }

                }))
            })
        },
        k = function() {
            $("#m_login_reset_password_submit").click(function(a) {
                a.preventDefault();
                var r = $(this),
                    n = $(this).closest("form");
                n.validate({
                    rules: {
                        password: {
                            password: !0,
                        }
                    }
                }), (r.addClass("m-loader m-loader--right m-loader--light").attr("disabled", !0), n.ajaxSubmit({
                    url: n.attr( 'action' ),
                    success: function(a, l, s, k) {
                        console.log(a);
                        if(a==1){
                            setTimeout(function() {
                                r.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), n.clearForm(), n.validate().resetForm(), t();
                                var a = e.find(".m-login__signin form");
                                a.clearForm(), a.validate().resetForm(), i(a, "success", "Cool! Password recovery instruction has been sent to your email.")
                            }, 2e3)
                           // window.location.href = "/login";
                        }
                        else {

                            setTimeout(function() {
                                r.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(n, "danger", a)
                            }, 2e3)
                        }
                    },
                    error: function(response,status,xhr ) { 
                            setTimeout(function() {
                                var keys = "";
                                var val = "";
                                for (var key in response.responseJSON.errors){
                                    console.log(key, response.responseJSON.errors[key]);
                                    val = val+' '+ (key, response.responseJSON.errors[key]);
                                }
                                r.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(n, "danger", val)
                            }, 2e3)
                        console.log(response.responseJSON.errors); 
                    }

                }))
            })
        };
    return {
        init: function() {
            n(), l(), s(), o(),k()
        }
    }
}();
jQuery(document).ready(function() {
    SnippetLogin.init()
});