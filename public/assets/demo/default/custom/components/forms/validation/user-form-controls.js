var FormControls = function () {
   r = function () {
        $("#m_form_2").validate({
            rules: {
                txtNamefirst: {required: !0, minlength: 1, maxlength: 20},
                txtNamelast: {required: !0, minlength: 1, maxlength: 20},
                title: {required: !0},

            }, invalidHandler: function (r) {
                var i = $("#m_form_2_msg");
                i.removeClass("m--hide").show(), mApp.scrollTo(i, -200)
            }, submitHandler: function (r) {
            }
        })
    };
    return {
        init: function () {
             r()
        }
    }
}();
jQuery(document).ready(function () {
    FormControls.init()
});