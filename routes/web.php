<?php
use App\Csvdata;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

<<<<<<< HEAD
Route::get('/', function () {

   return view('homepage');
=======
Route::get('/', function () { 
    return redirect('login'); // user
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
});
Route::get('admin', function () {
    return redirect('admin/login'); // admin
});
Route::get('supplier', function () {
    return redirect('supplier/login'); // supplier
});
Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);

Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);

<<<<<<< HEAD
//Route::get('/', 'LoginController@homepage');


Route::get('admin', function () {
    return redirect('admin/login'); // admin
});
Route::get('supplier', function () {
    return redirect('supplier/login'); // supplier
});
Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);

Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);

=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
	Route::get('store1','AdminController@store1'); // reset password page

// login and registration
Route::group(['middleware'=>'guest'], function(){
   Route::resource('login','LoginController'); // user login
<<<<<<< HEAD
   Route::post('user_Login','LoginController@user_Login'); // user login
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
	Route::get('/register','RegisterController@index'); // user register
   Route::get('/supplier/login','SupplierController@index');// supplier login view
   Route::get('/supplier/register','RegisterController@supplierReg'); // supplier register view
   Route::post('/supplierRegister','RegisterController@Supplierstore');  // supplier register
   Route::post('/supplierLogin','LoginController@supplierLogin');  // supplier login
   Route::get('/admin/login','AdminController@index'); // admin login view
   Route::post('admin/login-post','AdminController@loginPost'); // admin login
	Route::resource('register','RegisterController');  // register module with resource
   Route::get("activate/{email}/{activationCode}", 'RegisterController@activate'); // Register mail
   Route::resource('forgot','ForgotController'); // forgot Password
<<<<<<< HEAD
   Route::post('supplier/forgot','ForgotController@storesupplier'); // forgot Password

	Route::get('reset/{email}/{code}','ForgotController@show'); // reset password
    Route::get('terms_and_condition','LoginController@termsandcondition'); // terms and condition
    Route::get('back-to-admin','AdminController@backtoadmin'); // terms and condition

    Route::get('state/{id}','RegisterController@state');

    Route::get('city/{id}','RegisterController@city');

=======
	Route::get('reset/{email}/{code}','ForgotController@show'); // reset password 
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

});


/**
Admin
 **/
Route::group(['middleware' => 'admin'], function() { 
 
   /* Admin Manage profile */ 
   Route::get('admin/dashboard','AdminController@dashboard'); 
   Route::get('admin/users','AdminController@users'); 
   Route::get('admin/supplier','AdminController@supplier'); 
   Route::get('admin/profile','AdminController@profile'); 
 
   /* Admin Manage own profile */ 
<<<<<<< HEAD
   Route::post('profile/{id}', array('as' => 'admin.updateProfile', 'uses' => 'ProfileController@updateProfileadmin'));     
=======
   Route::post('profile/{id}', array('as' => 'admin.updateProfile', 'uses' => 'ProfileController@updateProfile'));     
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
   Route::get('admin/changePass','AdminController@changePass');
   Route::post('update-password/{id}',['as'=>'password.update','uses'=>'ProfileController@updatePassword']);
   Route::get('changeUserStatus/{user_id}/{status}', ['as'=>'user.status.change','uses'=>'AdminController@changeUserStatus']);

   /* Admin Manage update profile (user & supplier ) */ 
   Route::get('showUserProfile/{id}','AdminController@showUserProfile');  
   Route::post('updateUserProfile/{id}','AdminController@updateUserProfile'); 
  
   /* Admin Manage Login (user & supplier) */ 
   Route::get('userLogin/{id}','AdminController@userLogin');  
   Route::get('supplierLogin/{id}','AdminController@supplierLogin'); 

   /* Admin Export User & supp Data */
   Route::get('user/export', 'AdminController@userExport');
   Route::get('supplier/export', 'AdminController@supplierExport');
  
   /* Admin Manage User & supplier delete */
   Route::get('userIsDelete/{user_id}/{status}','AdminController@userIsDelete');
  
   /* Admin Manage Flight */
   Route::resource('admin-flight','FlightController');  
   Route::get('flightStatus/{flight_id}/{status}','FlightController@flightStatus');
   Route::get('flightsIsDelete/{flight_id}/{status}','FlightController@flightIsDelete');
  
   /* Admin Manage Source */
   Route::resource('admin-flightSource','FlightSourceController');
   Route::get('flightSourceStatus/{flight_id}/{status}','FlightSourceController@flightSourceStatus');
   Route::get('flightSourceIsDelete/{flight_id}/{status}','FlightSourceController@flightSourceIsDelete');
  
   /* Admin Manage Destination */
   Route::resource('admin-flightDestination','FlightDestinationController');
   Route::get('flightDestinationStatus/{flight_id}/{status}','FlightDestinationController@flightDestinationStatus');
   Route::get('flightDestinationIsDelete/{flight_id}/{status}','FlightDestinationController@flightDestinationIsDelete');
  
   /* Admin Manage Country */
   Route::resource('admin-country','CountryController');
   Route::get('countryStatus/{country_id}/{status}','CountryController@countryStatus');
   Route::get('countryIsDelete/{country_id}/{status}','CountryController@countryIsDelete');
  
   /* Admin Manage State */
   Route::resource('admin-state','StateController');
   Route::get('stateStatus/{state_id}/{status}','StateController@stateStatus');
   Route::get('stateIsDelete/{state_id}/{status}','StateController@stateIsDelete');
  
   /* Admin Manage City */
   Route::resource('admin-city','CityController');
   Route::get('cityStatus/{city_id}/{status}','CityController@cityStatus');
   Route::get('cityIsDelete/{state_id}/{status}','CityController@cityIsDelete');
  
   /* Admin Manage One Way Flight */
   Route::get('admin/oneWayFlights','FlightOneWayController@listOneWayFlight');
   Route::get('admin/createOneWayFlight','FlightOneWayController@createOneWayFlight');
   Route::post('storeFlightOneWay','FlightOneWayController@storeFlightOneWay');
   Route::get('admin/flightIsDelete/{flight_id}/{status}','FlightOneWayController@flightIsDelete');
   Route::get('changeFlightStatus/{flight_id}/{status}','FlightOneWayController@changeFlightStatus');
   Route::get('editOneWayFlight/{id}','FlightOneWayController@editOneWayFlight');
   Route::post('updateOneWayFlight/{id}','FlightOneWayController@updateOneWayFlight');
   Route::post('admin-flightOneWay-destroy','FlightOneWayController@destroy');
  
   /* Admin Manage Round Trip Flight */
   Route::get('admin/roundTripFlights','FlightRoundTripController@listRoundTripFlight');
   Route::get('admin/createRoundTripFlight','FlightRoundTripController@createRoundTripFlight');
   Route::post('storeRoundTripFlight','FlightRoundTripController@storeFlightRoundTrip');
   Route::get('admin/roundflightIsDelete/{flight_id}/{status}','FlightRoundTripController@flightIsDelete');
   Route::get('changeRoundFlightStatus/{flight_id}/{status}','FlightRoundTripController@changeFlightStatus');
   Route::get('editRoundTripFlight/{id}','FlightRoundTripController@editRoundTripFlight');
   Route::post('updateRoundTripFlight/{id}','FlightRoundTripController@updateRoundTripFlight');
   Route::post('admin-flightRoundTrip-destroy','FlightRoundTripController@destroy');

   /* Admin Manage Multi City Flight */
   Route::get('admin/multiCityFlights','FlightMultiCityController@listMultiCityFlight');
   Route::get('admin/createMultiCityFlight','FlightMultiCityController@createMultiCityFlight');
   Route::post('storeMultiCityFlight','FlightMultiCityController@storeFlightMultiCity');
   Route::get('admin/multiCityflightIsDelete/{flight_id}/{status}','FlightMultiCityController@flightIsDelete');
   Route::get('changeMultiCityFlightStatus/{flight_id}/{status}','FlightMultiCityController@changeFlightStatus');
   Route::get('editMultiCityFlight/{id}','FlightMultiCityController@editMultiCityFlight');
   Route::post('updateMultiCityFlight/{id}','FlightMultiCityController@updateMultiCityFlight');

   /* add balance for user */
   Route::post('add_balance','AdminController@add_balance');
   Route::get('delete_balance/{id}','AdminController@delete_balance');
   Route::get('admin/transaction-history','AdminTransactionhistoryController@balancehistory');
<<<<<<< HEAD

    Route::get('admin_history','AdminController@userbookinghistory');

    Route::get('admin/delete/OneWayFlights','AdminController@OneWayFlights');
    Route::get('admin/delete/RoundTripFlights','AdminController@RoundTripFlights');
    Route::get('admin/delete/MultiCityFlights','AdminController@MultiCityFlights');

=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
});

/** 
User
**/
Route::group(['middleware' => 'user'], function() {

   /* user Manage own profile */
<<<<<<< HEAD
       Route::get('user/dashboard','UserController@index');
       Route::get('user/profile','UserController@profile');
       Route::get('user/changePass','UserController@changePass');
       Route::post('user/profile/{id}', 'ProfileController@updateProfile');
       Route::post('user/update-password/{id}','ProfileController@updatePassword');
   
   /* user Manage One Way Flight */
       Route::post('flights/one-way-search','OnewayFlightsController@onewaysearch');
       Route::post('flights/one-way-book','OnewayFlightsController@bookoneway');
       Route::get('flights/checkout_one_way','OnewayFlightsController@checkoutoneway');
       Route::get('flights/emailoneway','MailsendController@emailoneway');
       Route::post('flights/payment','OnewayFlightsController@paymentoneway');//one way payment


   /* user Manage Round Trip Flight */
       Route::post('flights/round-trip-search','RoundTripFlightsController@roundTripsearch');
       Route::post('flights/round-trip-book','RoundTripFlightsController@bookroundtrip');
       Route::get('flights/emailroundtrip','MailsendController@emailroundtrip');
       Route::post('flights/payment/roundtrip','RoundTripFlightsController@paymentroundtrip');//one way payment


    /* user Manage multi city Flight */
        Route::post('flights/multi-city-search','MultiCityFlightsController@multicitysearch');
        Route::post('flights/multi-city-book','MultiCityFlightsController@bookmulticity');
        Route::get('flights/emailmulticity','MailsendController@emailmulticity');
        Route::post('flights/payment/multicity','MultiCityFlightsController@paymentmulticity');//one way payment

        Route::get('payu','UserController@payu');

        Route::get('history','UserController@userhistory');
        Route::post('sendemail','UserController@emailsend');
=======
   Route::get('user/dashboard','UserController@index');
   Route::get('user/profile','UserController@profile');
   Route::get('user/changePass','UserController@changePass');
   Route::post('user/profile/{id}', 'ProfileController@updateProfile');
   Route::post('user/update-password/{id}','ProfileController@updatePassword');
   
   /* user Manage One Way Flight */
   Route::post('flights/one-way-search','OnewayFlightsController@onewaysearch');
   Route::post('flights/one-way-book','OnewayFlightsController@bookoneway');
   Route::get('flights/checkout_one_way','OnewayFlightsController@checkoutoneway');
   Route::get('flights/emailoneway','MailsendController@emailoneway');


   /* user Manage Round Trip Flight */
   Route::post('flights/round-trip-search','RoundTripFlightsController@roundTripsearch');
   Route::post('flights/round-trip-book','RoundTripFlightsController@bookroundtrip');
    Route::get('flights/emailroundtrip','MailsendController@emailroundtrip');


    /* user Manage multi city Flight */
    Route::post('flights/multi-city-search','MultiCityFlightsController@multicitysearch');
    Route::post('flights/multi-city-book','MultiCityFlightsController@bookmulticity');
    Route::get('flights/emailmulticity','MailsendController@emailmulticity');


>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

});


/**
Supplier
 **/
Route::group(['middleware' => 'supplier'], function() {

   /* Supplier Manage own profile */
<<<<<<< HEAD
       Route::get('supplier/dashboard','SupplierController@dashboard');
       Route::get('supplier/profile','SupplierController@profile');
       Route::get('supplier/changePass','SupplierController@changePass');
       Route::post('supplier/profile/{id}', 'ProfileController@updateProfile');
       Route::post('supplier/update-password/{id}','ProfileController@updatePassword');
  
           /* Supplier Manage One Way Flight */
        Route::resource('supplier-FlightOneWay','FlightOneWayController');
        Route::post('supplier-flightOneWay-destroy','FlightOneWayController@destroy');
        Route::get('flightIsDelete/{flight_id}/{status}','FlightOneWayController@flightIsDelete');

        /* Supplier Manage Round Trip Flight */
        Route::resource('supplier-FlightRoundTrip','FlightRoundTripController');
        Route::post('supplier-flightRoundTrip-destroy','FlightRoundTripController@destroy');
        Route::get('roundflightIsDelete/{flight_id}/{status}','FlightRoundTripController@flightIsDelete');

        /* Supplier Manage Multi City Flight */
        Route::resource('supplier-FlightMultiCity','FlightMultiCityController');
        Route::post('supplier-flightMultiCity-destroy','FlightMultiCityController@destroy');
        Route::get('multiCityFlightIsDelete/{flight_id}/{status}','FlightMultiCityController@flightIsDelete');

        /* admin pages inport*/
        Route::get('one_way_import','CsvDataController@onewayimport');
        Route::post('one_way_import','CsvDataController@onewayimportpost');
        Route::post('onewaysave','CsvDataController@onewaysave');

        Route::get('round_trip_import','CsvDataController@round_trip_import');
        Route::post('round_trip_import','CsvDataController@round_trip_importpost');
        Route::post('round_trip_save','CsvDataController@round_tripsave');

        Route::get('multiCity_import','CsvDataController@multiCity_import');
        Route::post('multiCity_import','CsvDataController@multiCity_importpost');
        Route::post('multiCity_save','CsvDataController@multiCity_save');

        Route::get('supplier_history','SupplierController@bookinghistory')->name('hello');

=======
   Route::get('supplier/dashboard','SupplierController@dashboard');
   Route::get('supplier/profile','SupplierController@profile');
   Route::get('supplier/changePass','SupplierController@changePass');
   Route::post('supplier/profile/{id}', 'ProfileController@updateProfile');
   Route::post('supplier/update-password/{id}','ProfileController@updatePassword');
  
   /* Supplier Manage One Way Flight */
   Route::resource('supplier-FlightOneWay','FlightOneWayController');
   Route::post('supplier-flightOneWay-destroy','FlightOneWayController@destroy');
   Route::get('flightIsDelete/{flight_id}/{status}','FlightOneWayController@flightIsDelete');
  
   /* Supplier Manage Round Trip Flight */
   Route::resource('supplier-FlightRoundTrip','FlightRoundTripController');
   Route::post('supplier-flightRoundTrip-destroy','FlightRoundTripController@destroy');
   Route::get('roundflightIsDelete/{flight_id}/{status}','FlightRoundTripController@flightIsDelete');
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

   /* Supplier Manage Multi City Flight */
   Route::resource('supplier-FlightMultiCity','FlightMultiCityController');
   Route::post('supplier-flightMultiCity-destroy','FlightMultiCityController@destroy');
   Route::get('multiCityFlightIsDelete/{flight_id}/{status}','FlightMultiCityController@flightIsDelete');

});

/* Logout session */
Route::post('logout','LoginController@logout');
<<<<<<< HEAD
Route::get('hello','LoginController@hello');
Route::get('image','SupplierController@download')->name('file');

=======


//Route::get('read-excel',function(){
//
//    $fileD = fopen('expertphp-product.csv',"r");
//    $column=fgetcsv($fileD);
//    while(!feof($fileD)){
//        $rowData[]=fgetcsv($fileD);
//    }
//    foreach ($rowData as $key => $value) {
//
//        $inserted_data=array('name'=>$value[0],
//            'details'=>$value[1],
//        );
//
//        Product::create($inserted_data);
//    }
//    print_r($rowData);
//});
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
