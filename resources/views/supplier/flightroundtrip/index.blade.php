@extends('layouts.dashMaster')
@section('title') Supplier | Round Trip Flight @endsection
@section('style')
<style type="text/css">
	.que2a,.que2b,.que3a,.que3b{
		display: none;
	}
	.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
@endsection
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					Round Trip Flight List
					</h3>
				</div>
			</div>
		</div>
		<div class="m-content">
			
			@if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div>   <br>@endif
			@if(session('success'))   <br><div class="alert alert-success">{{ session('success') }}</div>   <br>@endif
			
			<div class="m-portlet m-portlet--mobile">
				
				<div class="m-portlet__body">
					<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-4">
										<div class="m-input-icon m-input-icon--left">
											<input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
											<span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 order-1 order-xl-2 m--align-right">
<<<<<<< HEAD
								<a href="{{url('round_trip_import')}}" class="btn m-btn--pill    btn-primary" >Import</a>
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
								<a href="{{route('supplier-FlightRoundTrip.create')}}" class="btn m-btn--pill    btn-primary" >Add New</a>
								</h3>
								<div class="m-separator m-separator--dashed d-xl-none"></div>
							</div>
						</div>
					</div>
					<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form">
						<div class="row align-items-center">
							<div class="col-xl-12">
								<div class="m-form__group m-form__group--inline">
									<div class="m-form__label m-form__label-no-wrap">
										<label class="m--font-bold m--font-danger-">
											Selected
											<span id="m_datatable_selected_number"></span>
											records:
										</label>
									</div>
									<div class="m-form__control">
										<div class="btn-toolbar">
											&nbsp;&nbsp;&nbsp;
											{{  Form::open(['method' => 'post','url' => ['supplier-flightRoundTrip-destroy'],'style'=>'display:inline' ,'id'=>'myform'])}}
											<button class="btn btn-sm m-btn--pill btn-danger" type="submit" id="m_datatable_check_all">
<<<<<<< HEAD
											Delete
=======
											Delete All
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</button>
											{{ Form::close() }}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--begin: Datatable -->
					<table class="m-datatable" id="html_table" width="100%">
						<thead>
							<tr>
								<th>RecordID</th>
								<th>Sr.</th>
								<th>Flight Departure</th>
<<<<<<< HEAD
								<th>Flight</th>
=======
								<th></th>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
								<th>Flight Arrival</th>
								<th>Flight Name</th>
								<th>Price</th>
								<th>Seat</th>
								<th>Sold</th>
								<th>Avail.</th>
								<th>PNR No</th>
<<<<<<< HEAD
								<th>Via</th>
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
								<th>Status</th>
								<th>Action</th>
								
							</tr>
						</thead>
						<tbody>
							
							<?php $i = 1; ?>
							
							@foreach($flight as $flights)
							<tr>
<<<<<<< HEAD

								<td>{{'<label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" form="myform"  value='.$flights->id.' class="select_checkbox" name="check[]" type="checkbox"><span></span></label>'}} </td>
								<td>{{$i++}}</td>
								<td>

=======
								
								<td>{{'<label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" form="myform"  value='.$flights->id.' class="select_checkbox" name="check[]" type="checkbox"><span></span></label>'}} </td>
								<td>{{$i++}}</td>
								<td>
									
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
									{{ $flights->flightSource->flight_source}}
									{{'<span class="m--padding-3"></span>'}}
									{{"<code>"}}   <?php echo date('d M y', strtotime($flights->flight_departure_date)); ?> -  {{ $flights->flight_departure_time }} {{"</code>"}}
									{{'<span>----------------------</span>'}}
									{{ $flights->flightSourceReturn->flight_destination}}
									{{'<span class="m--padding-3"></span>'}}
									{{"<code>"}}   <?php echo date('d M y', strtotime($flights->return_departure_date)); ?> -  {{ $flights->return_departure_time }} {{"</code>"}}
								</td>
								<td>{{'<i class="fa fa-fighter-jet"></i>'}}</td>
								<td>
<<<<<<< HEAD

									{{ $flights->flightDestination->flight_destination}}

=======
									
									{{ $flights->flightDestination->flight_destination}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
									{{'<span class="m--padding-3"></span>'}}
									{{"<code>"}}  {{ $flights->flight_arrival_time }} {{"</code>"}}
									{{'<span>---------------</span>'}}
									{{ $flights->flightDestinationReturn->flight_source}}
									{{'<span class="m--padding-3"></span>'}}
									{{"<code>"}}  {{ $flights->return_arrival_time }} {{"</code>"}}
								</td>
<<<<<<< HEAD


=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
								<td>
									{{"<b>"}} {{ $flights->flight->flight_name}} - {{ $flights->flight_number }}  {{"</b>"}}
									{{'<span>----------</span>'}}
									{{"<b>"}} {{ $flights->flightReturn->flight_name}} - {{ $flights->return_flight_number }}  {{"</b>"}}
								</td>
<<<<<<< HEAD

								<td>{{$flights->flight_price}}</td>
								<td>{{$flights->seat + $flights->sold }}</td>
								<td>{{$flights->sold}}</td>
								<td>{{$flights->seat}}</td>

								<td>

=======
								<td>{{$flights->flight_price}}</td>
								<td>{{$flights->seat}}</td>
								<td>{{$flights->sold}}</td>
								<td>0</td>
								
								<td>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
									{{$flights->flight_pnr_no}}
									{{'<span>------</span>'}}
									{{$flights->flight_pnr_no2}}
								</td>
								<td>
<<<<<<< HEAD

									{{$flights->flight_via}}

								</td>
								<td>
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
									@if($flights->status == 0)
									{{'<span><span class="m-badge  m-badge--danger m-badge--wide">Disapproved</span></span>'}}
									@elseif($flights->status == 1)
									{{'<span><span class="m-badge  m-badge--primary m-badge--wide">Approved</span></span>'}}
									@elseif($flights->status == 2)
									{{'<span><span class="m-badge  m-badge--metal m-badge--wide">Pending</span></span>'}}
									@endif
								</td>
<<<<<<< HEAD

								<td>
									{{'<a href="'.route('supplier-FlightRoundTrip.edit',$flights->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit "><i class="la la-edit"></i></a>'}}
									{{'<a onclick="return deletemsg('.$flights->id.');" href="'.url('roundflightIsDelete').'/'.$flights->id.'/0" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete " ><i class="la la-trash-o"></i></a>'}}
								</td>
=======
								
								<td>
									{{'<a href="'.route('supplier-FlightRoundTrip.edit',$flights->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit "><i class="la la-edit"></i></a>'}}
									{{'<a href="'.url('roundflightIsDelete').'/'.$flights->id.'/0" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete " ><i class="la la-trash-o"></i></a>'}}
								</td>
								
								
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
							</tr>
							@endforeach
							
							
						</tbody>
					</table>
					<!--end: Datatable -->
				</div>
			</div>
		</div>
		<!--end:: Widgets/Stats-->
		
	</div>
</div>
<!-- end::Body -->
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
<!--begin::Page Vendors -->
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
<script>
$( document ).ready(function() {
$(".m-checkbox").click(function(){
$(".collapse").addClass("show");
});
});
</script>
<<<<<<< HEAD
<script>
    function deletemsg(id) {
        swal({
            title: 'Are you sure?',
            text: "Are you sure you wish to delete ",
            showCancelButton: true,
            confirmButtonColor: '#34bfa3',
            cancelButtonColor: '#f4516c',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if(result.value)
        {
			{{--alert("{{url('cityIsDelete')}}/"+id);--}}
                window.location= "{{url('roundflightIsDelete')}}/"+id+'/0';
            return true;
        }
    else
        {return false;
        }
    }
    );
        return false;
    }

</script>
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
@endsection