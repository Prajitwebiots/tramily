@extends('layouts.dashMaster')
@section('title') supplier | Change Password @endsection
@section('style')
@endsection
@section('content')
<!-- begin::Body -->

<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<!-- BEGIN: Left Aside -->
	<button class="m-aside-left-close m-aside-left-close--skin-light" id="m_aside_left_close_btn">
	<i class="la la-close"></i>
	</button>
	
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					My Profile
					</h3>
				</div>
				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-lg-4">
					<div class="m-portlet m-portlet--full-height  ">
						<div class="m-portlet__body">
							<div class="m-card-profile">
								<div class="m-card-profile__title m--hide">
									Your Profile
								</div>
								<div class="m-card-profile__pic">
									<div class="m-card-profile__pic-wrapper">
										<img src="{{URL::asset('assets/images/user')}}/{{Sentinel::getUser()->profile }}" alt=""/>
									</div>
								</div>
								<div class="m-card-profile__details">
									<span class="m-card-profile__name">
										{{ Sentinel::getUser()->first_name }} {{ Sentinel::getUser()->last_name }}
									</span>
									<a href="" class="m-card-profile__email m-link">
										{{ Sentinel::getUser()->email }}
									</a>
								</div>
							</div>
							<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
								<li class="m-nav__separator m-nav__separator--fit"></li>
								<li class="m-nav__section m--hide">
									<span class="m-nav__section-text">
										Section
									</span>
								</li>
								<li class="m-nav__item">
									<a href="{{ url('supplier/profile') }}" class="m-nav__link">
										<i class="m-nav__link-icon flaticon-profile-1"></i>
										<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-nav__link-text">
													My Profile
												</span>
												
											</span>
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="{{ url('supplier/changePass') }}" class="m-nav__link">
										<i class="m-nav__link-icon flaticon-lock"></i>
										<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-nav__link-text">
													Change Password
												</span>
												
											</span>
										</span>
									</a>
								</li>
								<!-- <li class="m-nav__item">
										<a href="header/profile&amp;demo=default.html" class="m-nav__link">
												<i class="m-nav__link-icon flaticon-share"></i>
												<span class="m-nav__link-text">
														Activity
												</span>
										</a>
								</li> -->
								
							</ul>
							<div class="m-portlet__body-separator"></div>
						
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
						<div class="m-portlet__head">
							<div class="m-portlet__head-tools">
								<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
											<i class="flaticon-share m--hide"></i>
											Change Password
										</a>
									</li>
									
								</ul>
							</div>
							
						</div>
						<div class="tab-content">
							<div class="tab-pane active" id="m_user_profile_tab_1">
								<form  action=" {{url('supplier/update-password')}}/{{Sentinel::getUser()->id}}" method="post" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
									<input type="hidden" name="_token" value="{{csrf_token()}}">
									<input type="hidden" name="user_id" value="{{Sentinel::getUser()->id}}">
									<br>
									@if(session('error'))
									
									<div class="alert alert-danger">
										{{ session('error') }}
									</div>
									@endif
									@if(session('success'))
									
									<div class="alert alert-success">
										{{ session('success') }}
									</div>
									@endif
									<div class="m-portlet__body">
										<!-- <div class="form-group m-form__group m--margin-top-10 m--hide">
												<div class="alert m-alert m-alert--default" role="alert">
														The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
												</div>
										</div> -->
										<div class="form-group m-form__group row">
											<div class="col-10 ml-auto">
												<h3 class="m-form__section">
												1. Change Password
												</h3>
											</div>
										</div>
										<div class="form-group m-form__group row">
										<label for="example-text-input" class="col-2 col-form-label">
											Old Password
										</label>
										<div class="col-7">
											<input class="form-control m-input" name="old_password" type="password" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input"  class="col-2 col-form-label" >
											New Password
										</label>
										<div class="col-7">
											<input class="form-control m-input" name="new_password" type="password" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label for="example-text-input"  class="col-2 col-form-label" >
											Re-Type New Password
										</label>
										<div class="col-7">
											<input class="form-control m-input" name="confirm_password" type="password" >
										</div>
									</div>
									
										
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions">
											<div class="row">
												<div class="col-2"></div>
												<div class="col-7">
													<button type="submit" class="btn btn-primary m-btn m-btn--air m-btn--custom">
													Save changes
													</button>
													&nbsp;&nbsp;
													<button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
													Cancel
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane active" id="m_user_profile_tab_2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- end::Body -->
@endsection
@section('bottom_script')
<!--begin::Page Vendors -->
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
@endsection