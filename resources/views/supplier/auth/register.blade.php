@extends('layouts.master')
@section('title') Tramily | A Travel Family @endsection
@section('style')
<link href="{{ URL::asset('assets/pages/css/login-5.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<style>
.m-login.m-login--1 .m-login__wrapper {
overflow: hidden;
padding: 0% 2rem 2rem 2rem !important;
}
.m-login.m-login--1 .m-login__aside {
width: 50%;
}
select.form-control:not([size]):not([multiple]) {
    height: calc(2.55rem + 2px);
    border: 1px solid #80808054;
}
.form-control.m-input--solid {
    background-color: #f4f5f8a8;
}
</style>
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--singin" id="m_login">
        <!-- <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside"> -->
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="m-stack m-stack--hor m-stack--desktop">
                <div class="m-stack__item m-stack__item--fluid">
                    <div class="m-login__wrapper">
                        <div class="m--padding-15"></div>
                        <div class="m-login__logo">
                            <a href="#">
                                <img src="{{ URL::asset('assets/app/media/img/logos/logo-2.png')}}"  style="width: 30%;">
                            </a>
                        </div>
                        
                       
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                Sign Up
                                </h3>
                                <div class="m-login__desc">
                                    Enter your details to create your account:
                                </div>
                            </div>
                            @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div>@endif
                            @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div>@endif

<<<<<<< HEAD
                           <form class="m-login__form m-form" action="{{url('supplierRegister')}}" method="post" id="supp">
                            <!--  <div class="m-alert m-alert--outline alert alert-danger alert-dismissible errorsignup" role="alert">            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>           <span></span></div> -->
                            <!-- <div class="m-alert m-alert--outline alert alert-danger alert-dismissible errorsignuphide" style="display:none"><ul class="errorsignup" ></ul></div> -->
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="row">
                                <span class=""><br><b >Personal Details</b><br><br></span>
                                <div class="col-xl-12">

                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="text" value="{{ old('agency_name') }}" placeholder="Agency Name *" name="agency_name" autocomplete="off">
                                        @if ($errors->has('agency_name'))
                                            <span class="help-block text-danger">
=======
                            <form class="m-login__form m-form" action="{{url('/supplierRegister')}}" method="post" id="supp">
                               <!--  <div class="m-alert m-alert--outline alert alert-danger alert-dismissible errorsignup" role="alert">            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>           <span></span></div> -->
                               <!-- <div class="m-alert m-alert--outline alert alert-danger alert-dismissible errorsignuphide" style="display:none"><ul class="errorsignup" ></ul></div> -->
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="row">
                                    <span><br><b>Personal Details</b><br><br></span>
                                    <div class="col-xl-12">
                                        

                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input" type="text" value="{{ old('agency_name') }}" placeholder="Agency Name" name="agency_name" autocomplete="off">
                                          @if ($errors->has('agency_name'))
                                                <span class="help-block text-danger">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                                                    <strong>{{ $errors->first('agency_name') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <br>
                                    @if('Mrs.' == old('Mrs.')) selected @endif

                                    <div class="form-group " style="padding-top: 2px;">
                                        <select class="form-control m-select  " id="" name="title_name">
                                            <option @if('' == old('title_name')) selected @endif value="">TITLE <span class="m--font-danger">*</span></option>
                                            <option  @if("Mr."== old('title_name')) selected @endif value="Mr.">Mr.</option>
                                            <option @if( 'Ms.' == old('title_name')) selected @endif value="Ms.">Ms.</option>
                                            <option @if('Mrs.' == old('title_name')) selected @endif value="Mrs.">Mrs.</option>
                                        </select>
                                    </div>
                                    {{--<div class="form-group" style="padding-top: 2px;">--}}
                                    {{--<select class="form-control m-select " id="" name="industry"  >--}}
                                    {{--<option value=""> Select Industry </option>--}}

                                    {{--@foreach($industries as $industry)--}}
                                    {{--<option value="{{$industry->id}}">{{$industry->industry_name}}</option>--}}
                                    {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--</div>--}}



                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="text" value="{{ old('first_name') }}" placeholder="First name *" name="first_name" autocomplete="off">
                                        @if ($errors->has('first_name'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="text" value="{{ old('last_name') }}" placeholder="Last name *" name="last_name" autocomplete="off">
                                        @if ($errors->has('last_name'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                        @endif
                                    </div>



                                    <div class="form-group m-form__group">
                                        <textarea class="form-control m-input m-input--solid" value="" id="exampleTextarea" name="agency_address" autocomplete="off" placeholder="Agency Address *" rows="3">{{ old('agency_address') }}</textarea>
                                        @if ($errors->has('agency_address'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('agency_address') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="text"  value="{{ old('email') }}" placeholder="Email *" name="email" autocomplete="off">
                                        @if ($errors->has('email'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                        @endif
                                    </div>


                                    <div class="form-group " style="padding-top: 2px;">
                                        <select class="form-control m-select  " id="country" name="country">
                                            <option value="">Country <span class="m--font-danger">*</span></option>
                                            @foreach($countrylist as $country)
                                                <option @if(old('country') !="" && $country->id == old('country')) selected @endif value="{{$country->id}}">{{$country->country_name .' / '.$country->country_alphacode }}</option>
                                            @endforeach

                                        </select>
                                        @if ($errors->has('country'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('country') }}</strong>
                                                </span>
                                        @endif
                                    </div>


                                    <div class="form-group " style="padding-top: 2px;">
                                        <select class="form-control m-select  " id="state" name="state">
                                            <option value="">State <span class="m--font-danger">*</span></option>
                                            @foreach($statelist as $state)
                                                @if(old('state') !="" && $state->id == old('state')) <option selected  value="{{$state->id}}">{{$state->state_name }}</option> @endif
                                            @endforeach

                                        </select>
                                        @if ($errors->has('state'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('state') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <div class="form-group " style="padding-top: 2px;">
                                        <select class="form-control m-select  " id="city" name="city">
                                            <option value="">City <span class="m--font-danger">*</span></option>
                                            @foreach($citylist as $city)
                                                @if(old('state') !="" && $city->id == old('city')) <option selected  value="{{$city->id}}">{{$city->city_name }}</option> @endif
                                            @endforeach

                                        </select>
                                        @if ($errors->has('city'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="text" value="{{ old('pincode') }}"  placeholder="Pin Code *" name="pincode" autocomplete="off">
                                        @if ($errors->has('pincode'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('pincode') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="number" value="{{ old('mobile') }}" placeholder="Mobile No *" name="mobile" autocomplete="off">
                                        @if ($errors->has('mobile'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('mobile') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="number" value="{{ old('office_no') }}" placeholder="Office No *" name="office_no" autocomplete="off">
                                        @if ($errors->has('office_no'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('office_no') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="password" placeholder="Password *" name="password" autocomplete="off">
                                        @if ($errors->has('password'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="password" placeholder="Confirm Password *" name="confirm_password" autocomplete="off">
                                        @if ($errors->has('confirm_password'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('confirm_password') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="text" value="{{ old('pan_no') }}" placeholder="PAN No" name="pan_no" autocomplete="off">
                                        @if ($errors->has('pan_no'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('pan_no') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                </div>


                                <span class=""><br><b>Agency GST Details</b><br><br></span>


                                <div class="col-xl-12">
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="text" placeholder="Name *" value="{{ old('gst_full_name') }}" name="gst_full_name" autocomplete="off">
                                        @if ($errors->has('gst_full_name'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('gst_full_name') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                     <div class="form-group ">


                                        <select class="form-control m-select  " id="state" name="gst_state"  style="padding-top: 7px;">
                                            <option value="">State *</option>
                                            @foreach($statelist as $state)
                                                <option   @if(old('gst_state') !="" && $state->id == old('gst_state')) selected @endif    value="{{$state->id}}">{{$state->state_name }}</option>
                                            @endforeach

                                        </select>
                                        @if ($errors->has('gst_state'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('gst_state') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <textarea class="form-control m-input m-input--solid" value="" id="exampleTextarea" name="gst_agency_address" autocomplete="off" placeholder="GST Agency Address " rows="3">{{ old('gst_agency_address') }}</textarea>
                                        @if ($errors->has('gst_agency_address'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('gst_agency_address') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="email" value="{{ old('register_email') }}" placeholder="Registered Email id " name="register_email" autocomplete="off">
                                        @if ($errors->has('register_email'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('register_email') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="number"  name="gst_phone_number" value="{{ old('gst_phone_number') }}" placeholder="Phone Number " >
                                        <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('gst_phone_number') }}</strong>
                                                </span>

                                    </div>

                                    <div class="form-group m-form__group">
                                        <input class="form-control m-input" type="text" value="{{ old('gst_no') }}" placeholder="GSTIN No *" name="gst_no" autocomplete="off">
                                        @if ($errors->has('gst_no'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('gst_no') }}</strong>
                                                </span>
<<<<<<< HEAD
                                        @endif
=======
                                            @endif
                                        </div>
                                     </div>
                                   
                                </div>
                                <div class="row form-group m-form__group m-login__form-sub">
                                    <div class="col m--align-left">
                                        <label class="m-checkbox m-checkbox--focus">
                                            <input type="checkbox" name="agree">
                                            I Agree the
                                            <a href="#" class="m-link m-link--focus" required>
                                                terms and conditions
                                            </a>
                                            .
                                            <span></span>
                                        </label>
                                        <span class="m-form__help"></span>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                                    </div>



                                </div>

                            </div>
                            <div class="row form-group m-form__group m-login__form-sub">
                                <div class="col m--align-left">
                                    <label class="m-checkbox m-checkbox--focus">
                                        <input type="checkbox" name="agree" required>
                                        I Agree the
                                        <a href="#" class="m-link m-link--focus">
                                            terms and conditions
                                        </a>
                                        .
                                        <span></span>
                                    </label>
                                    <span class="m-form__help"></span>
                                </div>
                            </div>
                            <div class="m-login__form-action">
                                <button id="m_login_signup_submitt" type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                    Sign Up
<<<<<<< HEAD
                                </button>
                                <a href="/login" id="m_login_signup_cancel1" class="btn btn-outline-focus  m-btn m-btn--pill m-btn--custom">
                                    Cancel
                                </a>
                            </div>
                        </form>
=======
                                    </button>
                                    <a href="/supplier/login" id="m_login_signup_cancel1" class="btn btn-outline-focus  m-btn m-btn--pill m-btn--custom">
                                  Cancel                              
                              </a>
                                </div>
                            </form>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                        </div>
                      
                    </div>
                </div>
                
            </div>
            </div>
<<<<<<< HEAD
            <!--<div class="col-md-4"></div>-->
=======
    
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
     <!--    </div> -->
     <!--    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1  m-login__content" style="background-image: url({{ URL::asset('assets/app/media/img/bg/bg-4.jpg')}})">
            <div class="m-grid__item m-grid__item--middle">
                <h3 class="m-login__welcome">
                WELCOME TO TRAMILY
                </h3>
                <p class="m-login__msg">
                    The world is a book and those who do not travel read only one page
                    <br>
                    
                </p>
            </div>
        </div> -->

</div>
<!-- end:: Page -->
@endsection

@section('bottom_script')
<script src="{{ asset('assets/snippets/pages/user/login.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/snippets/pages/user/select2.js')}}" type="text/javascript"></script>

@if(@$register)
<script type="text/javascript">
$(document).ready(function(){
$("#m_login_signup").trigger('click');
});
</script>
@endif
        <script type="text/javascript">
            $(document).ready(function(){
                $('#country').on('change',function(){

                    var countryID = $(this).val();
                    if(countryID){
                        $.ajax({
                            type:'get',
                            url:'{{URL('state/')}}/'+countryID,
                            data:'country_id='+countryID,
                            success:function(html){
                                $('#state').html(html);
                                $('#city').html('<option value="">Select state first</option>');
                            }
                        });
                    }else{
                        $('#state').html('<option value="">Select country first</option>');
                        $('#city').html('<option value="">Select state first</option>');
                    }
                });

                $('#state').on('change',function(){
                    var stateID = $(this).val();
                    if(stateID){
                        $.ajax({
                            type:'get',
                            url:'{{URL('city/')}}/'+stateID,
                            data:'state_id='+stateID,
                            success:function(html){
                                $('#city').html(html);
                            }
                        });
                    }else{
                        $('#city').html('<option value="">Select state first</option>');
                    }
                });
            });
        </script>
@endsection