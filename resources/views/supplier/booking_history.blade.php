@extends('layouts.dashMaster')
@section('title') Supplier  |booking history   @endsection
@section('style')
<style type="text/css">
	.quMemberue2b,.que3a,.que3b{
		display: none;
	}
	.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
@endsection
@section('content')
<!-- begin::Body -->

<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
						Booking history
					</h3>
				</div>
			</div>
		</div>
		<div class="m-content">

			@if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div>   <br>@endif
			@if(session('success'))   <br><div class="alert alert-success">{{ session('success') }}</div>   <br>@endif

                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <h4>One Way Bookings</h4>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <table class="m-datatable" id="html_table" width="100%">
                            <thead>
                            <tr>
                                <th>Sr .no</th>
                                <th>Booking Date</th>
                                <th>Sector</th>
                                <th>Travel Date</th>
                                <th>Flight Details</th>
                                <th>No.  of Seats</th>
                                <th>PNR</th>
                                <th>Fare</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1; ?>

                            @foreach($history_oneway as $key)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{date_format($key->created_at,' D d-M-Y h:i A')}}</td>
                                     <td>{{$key->oneway->flightSource->flight_source.' - '.$key->oneway->flightDestination->flight_destination}}</td>
                                   
                                    <td>{{date_format(date_create($key->oneway->flight_departure),' D d-M-Y')}}</td>
                                    <td>{{$key->oneway->flight->flight_name.'-'.$key->oneway->flight->flight_code.'-'.$key->oneway->flight_number }}</td>
                                    <td>{{$key->number_of_seat}}</td>
                                    <td>{{$key->oneway->flight_pnr_no }}</td>
                                    <td>{{'<i class="la la-inr"></i>'.$key->amount }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <h4>Round Trip Bookings</h4>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search_r">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <table class="m-datatable_r" id="html_table" width="100%">
                            <thead>
                            <tr>
                                <th>Sr .no</th>
                                <th>Booking Date</th>
                                <th>Sector</th>
                                <th>Date</th>
                                <th>Flight Details</th>
                                <th>No.  of Seats</th>
                                <th>PNR</th>
                                <th>Fare</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1; ?>

                            @foreach($history_round_trip as $key)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{date_format($key->created_at,' D d-M-Y h:i A')}}</td>
                                    <td>{{$key->round_trip->flightSource->flight_source.' - '.$key->round_trip->flightDestination->flight_destination}}
                                    {{e('<span>-------------------------------------</span>')}}
                                     {{$key->round_trip->flightSourceReturn->flight_destination.' - '.$key->round_trip->flightDestinationReturn->flight_source}}
                                    </td>
                                    <td>{{date_format(date_create($key->round_trip->flight_departure),' D d-M-Y ')}}
                                        {{e('<span>------------------</span>')}}
                                        {{date_format(date_create($key->round_trip->return_departure_date),' D d-M-Y ')}}
                                    </td>
                                    <td>
                                        {{$key->round_trip->flight->flight_name.'-'.$key->round_trip->flight->flight_code.'-'.$key->round_trip->flight_number }}
                                        {{e('<span>------------------</span>')}}
                                    {{$key->round_trip->flightReturn->flight_name.'-'.$key->round_trip->flightReturn->flight_code.'-'.$key->round_trip->return_flight_number }}
                                    </td>
                                    <td>{{$key->number_of_seat }}</td>
                                    <td>{{$key->round_trip->flight_pnr_no.'-'.$key->round_trip->flight_pnr_no2 }}</td>
                                    <td>{{'<i class="la la-inr"></i>'.$key->amount }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <!--end: Datatable -->

                    </div>
                </div>
                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <h4>Multi city Trip Bookings</h4>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search_t">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <table class="m-datatable_t"  width="100%">
                            <thead>
                            <tr>
                                <th>Sr .no</th>
                                <th>Booking Date</th>
                                <th>Sector</th>
                                <th>Date</th>
                                <th>Flight Details</th>
                                <th>No.  of Seats</th>
                                <th>PNR</th>
                                <th>Fare</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1; ?>

                            @foreach($history_multicity as $key)

                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{date_format($key->created_at,' D d-M-Y h:i A')}}</td>
                                        <td>
                                        {{$key->multicity->flightSourceF->flight_source.' - '.$key->multicity->flightDestinationF->flight_destination}}
                                            {{e('<span>-------------------------------------</span>')}}
                                        {{$key->multicity->flightSourceS->flight_source.' - '.$key->multicity->flightDestinationS->flight_destination}}
                                        @if($key->multicity->flight_departure_date_t)
                                        {{e('<span>-------------------------------------</span>')}}
                                        {{@$key->multicity->flightSourceT->flight_source.' - '.@$key->multicity->flightDestinationT->flight_destination}}
                                        @endif
                                     
                                        {{--</td>--}}
                                        <td>
                                        {{date_format(date_create($key->multicity->flight_departure_date_f),' D d-M-Y ')}}
                                        {{e('<span>---------------------------</span>')}}
                                        {{date_format(date_create($key->multicity->flight_departure_date_s),' D d-M-Y ')}}
                                        @if($key->multicity->flight_departure_date_t)
                                        
                                        {{e('<span>---------------------------</span>')}}
                                        {{date_format(date_create($key->multicity->flight_departure_date_t),' D d-M-Y ')}}
                                       @endif
                                        </td>
                                        <td>{{$key->multicity->flightF->flight_name.'-'.$key->multicity->flightF->flight_code.'-'.$key->multicity->flight_number_f }}
                                            {{e('<span>------------------</span>')}}
                                            {{$key->multicity->flightS->flight_name.'-'.$key->multicity->flightS->flight_code.'-'.$key->multicity->flight_number_s }}
                                            @if($key->multicity->flight_departure_date_t)
                                            {{e('<span>------------------</span>')}}
                                            {{@$key->multicity->flightT->flight_name.'-'.@$key->multicity->flightT->flight_code.'-'.@$key->multicity->flight_number_t }}
                                            @endif
                                          
                                        </td>
                                        <td>{{$key->number_of_seat }}</td>
                                        <td>{{$key->multicity->flight_pnr_no.'-'.$key->multicity->flight_pnr_no2.'-'.$key->multicity->flight_pnr_no3 }}</td>
                                        <td>{{'<i class="la la-inr"></i>'.$key->amount }}</td>
                                    </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <!--end: Datatable -->
                    
                    
                            <!--end::Modal-->

                    </div>

                </div>
			</div>
		<!--end:: Widgets/Stats-->

	</div>
</div>
<!-- end::Body -->
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
{{--<script src="{{ asset('assets/demo/default/custom/components/datatables/datatables/child/data-local.js')}}" type="text/javascript"></script>--}}
<!--begin::Page Vendors -->
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
<script>
$( document ).ready(function() {
$(".m-checkbox").click(function(){
$(".collapse").addClass("show");
});
});
</script>
@endsection