@extends('layouts.dashMaster')
@section('title') Supplier | Dashboard @endsection
@section('style')
<style type="text/css">
	.que2a,.que2b,.que3a,.que3b{
		display: none;
	}
	.circle-cus{
		border-radius: 5em;
background: #5867dd;
color: #fff;
height: 100px;
width: 101px;
padding-top: 35px;
	}
	.ic-cus {
		font-size: 33px;
	}
</style>
@endsection
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					Supplier Dashboard
					</h3>
				</div>
				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<!--begin:: Widgets/Stats-->
			<div class="m-portlet ">
				<div class="m--padding-30"></div>
				<section class="wallet-security">
					<div class="container">
						<div class="row icons-container">
							<!-- Stage -->
							<div class="col-md-4 m--align-center">
								<div class="icon-box-2 with-line borderradius1 box-height">
									<a href="{{url('supplier-FlightOneWay')}}"><div class="icon_holder " style="margin: 15px 0 15px 0;">
										<span class="fa-stack fa-2x circle-cus" ><i class="fa fa-paper-plane fa-stack-1x ic-cus" ></i></span>
									</div>
								</a>
								<div class="m--padding-10"></div>
								<h4>One Way</h4>
							</div>
						</div>
						<!-- Stage -->
						<div class="col-md-4 m--align-center">
							<div class="icon-box-2 with-line borderradius1 box-height">
								<a href="{{url('supplier-FlightRoundTrip')}}"><div class="icon_holder " style="margin: 15px 0 15px 0;">
									<span class="fa-stack fa-2x circle-cus"><i class="fa fa-plane fa-stack-1x ic-cus" ></i></span>
								</div></a>
								<div class="m--padding-10"></div>
								<h4>Round Trip</h4>
							</div>
						</div>
						<!-- Stage -->
						<div class="col-md-4 m--align-center">
							<div class="icon-box-2 with-line borderradius1 box-height">
								<a href="{{url('supplier-FlightMultiCity')}}"><div class="icon_holder " style="margin: 15px 0 15px 0;">
									<span class="fa-stack fa-2x circle-cus" ><i class="fa fa-paper-plane-o fa-stack-1x ic-cus" ></i></span>
								</div></a>
								<div class="m--padding-10"></div>
								<h4>Multi City</h4>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
	<!--end:: Widgets/Stats-->
	
</div>
<<<<<<< HEAD
=======
</div>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
</div>
<!-- end::Body -->
@endsection
@section('bottom_script')
<!--begin::Page Vendors -->
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
@endsection