@extends('layouts.dashMaster')
@section('title') Supplier | One Way Flight @endsection
@section('style')
<style type="text/css">
	.que2a,.que2b,.que3a,.que3b{
		display: none;
	}
</style>
<link rel="stylesheet" type="text/css" href="{{asset('css\bootstrap-clockpicker.css')}}">

@endsection
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					One Way Flight
					</h3>
				</div>
			</div>
		</div>
		<div class="m-content">
			
			@if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div>   <br>@endif
			@if(session('success'))   <br><div class="alert alert-success">{{ session('success') }}</div>   <br>@endif
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tab">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<span class="m-portlet__head-icon m--hide">
										<i class="la la-gear"></i>
									</span>
									<h3 class="m-portlet__head-text">
									Add Flight
									</h3>
								</div>
							</div>
						</div>
						<!--begin::Form-->
						<form class="m-form m-form--fit m-form--label-align-right" action="{{route('supplier-FlightOneWay.store')}}" method="post" >
						 <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
							@if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div> <br>@endif
							@if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div> <br>@endif
							<div class="m-portlet__body">
								
								<div class="row">
									<div class="col-md-6">

										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Source <span class="m--font-danger"> *</span>
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="source" name="source">
												<option value="">Select Flight Source</option>
												@foreach($flightSource as $flightSources)
												<option @if($flightSources->id == old('source'))selected @endif value="{{ $flightSources->id }}">{{ $flightSources->flight_source}}</option>
												@endforeach
											</select>
											
											@if ($errors->has('source'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('source') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Name <span class="m--font-danger"> *</span>
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="flight" name="flight">
												<option value="">Select Flight</option>
												@foreach($flight as $flights)
												<option @if($flights->id == old('flight'))selected @endif  value="{{ $flights->id }}">{{ $flights->flight_name}}</option>
												@endforeach
											</select>
											
											@if ($errors->has('flight'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('flight') }}</strong>
											</span>
											@endif
										</div>
										
									</div>
									<div class="col-md-6">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Destination <span class="m--font-danger"> *</span>
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="destination" name="destination">
												<option value="">Select Flight Destination</option>
												@foreach($flightDestination as $flightDestinations)
												<option @if($flightDestinations->id == old('destination'))selected @endif value="{{ $flightDestinations->id }}">{{ $flightDestinations->flight_destination}}</option>
												@endforeach
											</select>
											
											@if ($errors->has('destination'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('destination') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Flight Number <span class="m--font-danger"> *</span>
											</label>
											
											<input type="text" class="form-control m-input" value="{{ old('flight_number') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Flight Number" name="flight_number"  >
											@if ($errors->has('flight_number'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('flight_number') }}</strong>
											</span>
											@endif
										</div>
										
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Departure Date <span class="m--font-danger"> *</span>
											</label>
											
											<input type="text" value="{{old('departure_date')}}" class="form-control m-input" id="m_datepicker_1" name="departure_date" readonly="" placeholder="Select date">
											@if ($errors->has('departure_date'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('departure_date') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Price <span class="m--font-danger"> *</span>
											</label>
											
											<input type="text"  class="form-control m-input" value="{{old('flight_price')}}" autocomplete="off" id="exampleInputText" placeholder="Enter Price" name="flight_price"  >
											@if ($errors->has('flight_price'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('flight_price') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Departure Time <span class="m--font-danger"> *</span>
											</label>
											<div class="input-group timepicker" >
												<input type="text" value="{{old('departure_time')}}" class="form-control m-input" id="m_datepicker_2" readonly="" name="departure_time" placeholder="Select time">
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											@if ($errors->has('departure_time'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('departure_time') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Seat <span class="m--font-danger"> *</span>
											</label>
											
											<input type="text" class="form-control m-input" value="{{ old('seat') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Seat" name="seat"  >
											@if ($errors->has('seat'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('seat') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Arrival Time <span class="m--font-danger"> *</span>
											</label>
											<div class="input-group timepicker" id="">
												<input type="text" class="form-control m-input" value="{{old('arrival_time')}}" id="m_datepicker_3" readonly="" name="arrival_time" placeholder="Select time">
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											@if ($errors->has('arrival_time'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('arrival_time') }}</strong>
											</span>
											@endif
										</div>
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Pnr No
											</label>
											
											<input type="text" class="form-control m-input" value="{{ old('pnr_no') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Pnr No" name="pnr_no"  >
											@if ($errors->has('pnr_no'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('pnr_no') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4 m--padding-top-50">

										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Via
											</label>

											<input type="text" class="form-control m-input" value="{{ old('via') }}" autocomplete="off" id="exampleInputText" placeholder="Enter via" name="via"  >
											@if ($errors->has('via'))
												<span class="help-block text-danger">
												<strong>{{ $errors->first('via') }}</strong>
											</span>
											@endif
										</div>
									</div>

								</div>
							</div>
							<div class="m-portlet__foot m-portlet__foot--fit">
								<div class="m-form__actions">
									<button type="submit" class="btn btn-primary">
									Submit
									</button>
									<a href="{{ url('supplier-FlightOneWay') }}" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
						</form>
						<!--end::Form-->
					</div>
					<!--end::Portlet-->
					
					
					
				</div>
				
				<!--end::Portlet-->
				
			</div>
		</div>
		<!--end:: Widgets/Stats-->
		
	</div>
</div>
<!-- end::Body -->
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/jquery-clockpicker.js')}}" type="text/javascript"></script>

<script>
$('#m_datepicker_1').datepicker({
format: 'dd-mm-yyyy',
		todayHighlight:	true,
	autoclose:true,
startDate:new Date(),
});

$('#m_datepicker_3').clockpicker({
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    'default': 'now'
});
$('#m_datepicker_2').clockpicker({
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    'default': 'now'
});
</script>

<!--begin::Page Vendors -->
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
@endsection