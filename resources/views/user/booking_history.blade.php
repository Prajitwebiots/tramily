@extends('layouts.dashMaster')
@section('title') User |booking history   @endsection
@section('style')
<style type="text/css">
	.quMemberue2b,.que3a,.que3b{
		display: none;
	}
	.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
@endsection
@section('content')
<!-- begin::Body -->

<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
						booking history
					</h3>
				</div>
			</div>
		</div>
		<div class="m-content">

			@if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div>   <br>@endif
			@if(session('success'))   <br><div class="alert alert-success">{{ session('success') }}</div>   <br>@endif

                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <h4>One Way bookings</h4>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <table class="m-datatable" id="html_table" width="100%">
                            <thead>
                            <tr>
                                <th>Sr .no</th>
                                <th>Payment type</th>
                                <th>Amount</th>
                                <th>Seat</th>
                                <th>Date/Time</th>
                                <th>e-ticket</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1; ?>

                            @foreach($history_oneway as $key)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$key->payment_type == 1 ? 'Wallet balance':''}}</td>
                                    <td>{{$key->amount}}</td>
                                    <td>{{$key->number_of_seat}}</td>
                                    <td>{{date_format($key->created_at,' D d-M-Y h:i')}}</td>
                                    {{--<td>{{e('<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#m_modal_ticket'.$loop->iteration.'">Ticket</button>')}}</td>--}}
                                    <td>{{e('<a  class="btn btn-primary btn-sm"  href="'.asset('assets/ticket'.'/'.$key->pdf).'">Ticket</button>')}}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <!--end: Datatable -->


                    </div>
                </div>
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <h4>Round Trip</h4>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search_r">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <table class="m-datatable_r"  width="100%">
                            <thead>
                            <tr>
                                <th>Sr .no</th>
                                <th>Payment type</th>
                                <th>Amount</th>
                                <th>Seat</th>
                                <th>Date/Time</th>
                                <th>Members</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1; ?>

                            @foreach($history_round_trip as $key)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$key->payment_type == 1 ? 'Wallet balance':''}}</td>
                                    <td>{{$key->amount}}</td>
                                    <td>{{$key->number_of_seat}}</td>
                                    <td>{{date_format($key->created_at,' D d-M-Y h:i')}}</td>
{{--                                    <td>{{e('<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#m_modal_r'.$loop->iteration.'">Member</button>')}}</td>--}}
                                    <td>{{e('<a  class="btn btn-primary btn-sm"  href="'.asset('assets/ticket'.'/'.$key->pdf).'">Ticket</button>')}}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <!--end: Datatable -->

                    </div>
                </div>
                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <h4>Multi city Trip</h4>
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search_t">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
												<span>
													<i class="la la-search"></i>
												</span>
											</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <table class="m-datatable_t"  width="100%">
                            <thead>
                            <tr>
                                <th>Sr .no</th>
                                <th>Payment type</th>
                                <th>Amount</th>
                                <th>Seat</th>
                                <th>Date/Time</th>
                                <th>Members</th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1; ?>

                            @foreach($history_multicity as $key)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$key->payment_type == 1 ? 'Wallet balance':''}}</td>
                                    <td>{{$key->amount}}</td>
                                    <td>{{$key->number_of_seat}}</td>
                                    <td>{{date_format($key->created_at,' D d-M-Y h:i')}}</td>
                                    <td>{{e('<a  class="btn btn-primary btn-sm"  href="'.asset('assets/ticket'.'/'.$key->pdf).'">Ticket</button>')}}</td>
                                    {{--<td>{{e('<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#m_modal_t'.$loop->iteration.'">Member</button>')}}</td>--}}
                                     </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <!--end: Datatable -->


                    </div>

                </div>
			</div>
		<!--end:: Widgets/Stats-->

	</div>
</div>
<!-- end::Body -->
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js')}}" type="text/javascript"></script>
{{--<script src="{{ asset('assets/demo/default/custom/components/datatables/datatables/child/data-local.js')}}" type="text/javascript"></script>--}}
<!--begin::Page Vendors -->
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
<script>
$( document ).ready(function() {
$(".m-checkbox").click(function(){
$(".collapse").addClass("show");
});
});
</script>
@endsection