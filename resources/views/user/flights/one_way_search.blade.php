@extends('layouts.dashMaster')
@section('title') User | Search One Way Flight @endsection
@section('style')
<style type="text/css">
	.que2a,.que2b,.que3a,.que3b{
		display: none;
	}
	ul.dropdown-menu.inner {
		max-height: 217px !important;
	}
</style>

@endsection
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					Flight Search - One Way
					</h3>
				</div>
				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-md-12">
					
					<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tabs m-portlet--primary m-portlet--head-solid-bg m-portlet--head-sm">
						<?php
							$request = Session::get('oneway', 'default');
						?>
						<div class="m-portlet__body">
							<div class="tab-content">
								{{--one way--}}
								<div class="tab-pane active  " id="m_tabs_7_1" role="tabpanel" aria-expanded="false">
<<<<<<< HEAD

									<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-primary alert-dismissible fade show" role="alert">
										<div class="m-alert__icon">
											<i class="la la-warning"></i>
										</div>
										<div class="m-alert__text">
											<strong>
											Search Result For,
											</strong>
											Flying Source: @foreach($source as $fligth) @if($fligth->id == $request['flying_source']) {{$fligth->flight_source}} @endif @endforeach
											| Flying Destination:  @foreach($destination as $fligth) @if($fligth->id == $request['flying_destination']) {{$fligth->flight_destination}} @endif @endforeach |
											Departure:  {{$request['departure']}} | Adults: {{$request['adults']}}
										</div>

										<div class="m-alert__close">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
										</div>
									</div>
									<!--begin::one way-->
									<!--begin::Form-->
								@if(($showalert == 1) )
=======
									<!--begin::one way-->
									<!--begin::Form-->
									@if(($showalert == 1) && count($oneway) > 0)
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
									{{--alert +3  dates--}}
									<!-- <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-primary alert-dismissible fade show" role="alert">
											<div class="m-alert__icon">
													<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
													Sorry, We couldn't find blocks as per your Requirement, So Near by dates are Offered for your Reference
											</div>
											<div class="m-alert__close">
													<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
											</div>
									</div> -->
<<<<<<< HEAD
										<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
											<strong>
												Sorry ! We couldn't find blocks as per your Requirement, So Near by dates are Offered for your Reference..
											</strong>

										</div>
										{{--alert +3  dates--}}
									@elseif(($showalert == 2) && count($oneway) >= 0 )
										<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
											<strong>
												Searched Result Not found.
											</strong>

										</div>
									@endif
									{{--alert--}}
=======
									<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
										<strong>
										Sorry ! We couldn't find blocks as per your Requirement, So Near by dates are Offered for your Reference..
										</strong>
										
									</div>
									{{--alert +3  dates--}}
									@endif
									{{--alert--}}
									<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-primary alert-dismissible fade show" role="alert">
										<div class="m-alert__icon">
											<i class="la la-warning"></i>
										</div>
										<div class="m-alert__text">
											<strong>
											Search Result For,
											</strong>
											Flying Source: @foreach($source as $fligth) @if($fligth->id == $request['flying_source']) {{$fligth->flight_source}} @endif @endforeach
											| Flying Destination:  @foreach($destination as $fligth) @if($fligth->id == $request['flying_destination']) {{$fligth->flight_destination}} @endif @endforeach |
											Departure:  {{$request['departure']}} | Adults: {{$request['adults']}}
										</div>
										<div class="m-alert__close">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
										</div>
									</div>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
									{{--alertend--}}
									<div class="m-content">
										
										<div class="row">
							
											<div class="col-xl-12">
												<form class="m-form m-form--state m-form--fit m-form--label-align-right" method="post" action="{{url('flights/one-way-search')}}" id="m_form_2" novalidate="novalidate">
													<div class="m-portlet__body">
														
														<div class="form-group m-form__group row">
															<div class="row">
																<div class="col-md-3">
																	{{csrf_field()}}
																	<select class="m--padding-top-10 form-control m-bootstrap-select m_selectpicker" name="flying_source" title="Flying Source" placeholder="Flying Source" data-live-search="true">
																		@foreach($source as $fligth)
																		<option @if($fligth->id == $request['flying_source']) selected @endif value="{{$fligth->id}}">{{$fligth->flight_source}}</option>
																		@endforeach
																	</select>
																	
																	<span class="m-form__help">
																		Select your flying source
																	</span>
																</div>
																<div class="col-md-3">
																	<select class="m--padding-top-10 form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination" data-live-search="true">
																		@foreach($destination as $fligth)
																		<option @if($fligth->id == $request['flying_destination']) selected @endif value="{{$fligth->id}}">{{$fligth->flight_destination}}</option>
																		@endforeach
																	</select>
																	<span class="m-form__help">
																		Select your flying destination
																	</span>
																</div>
																<div class="col-md-3" style="
																	margin-top: 9px;
																	">
																	<input type="text" class="m--padding-top-10 form-control" value="{{$request['departure']}}" id="m_datepicker_1" placeholder="Departure" name="departure" readonly="" >
																	<span class="m-form__help">
																		Please select Departure date
																	</span>
																</div>
																<div class="col-md-3">
																	<select  name="adults"  class="m--padding-top-10 form-control m-bootstrap-select m_selectpicker">
																		<option value="1" selected > Adults </option>
<<<<<<< HEAD
																		@for ($i=1;$i <=20;$i++)
=======
																		@for ($i=1;$i <=9;$i++)
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
																		<option @if($i == $request['adults']) selected @endif  value="{{$i}}">{{$i}}</option>
																		@endfor
																	</select>
																	<span class="m-form__help">
																		Please select number of Adults
																	</span>
																</div>
															</div>
														</div>
														<div class="form-group m-form__group m--align-right">
															<div class="">
																<button type="submit" class="btn btn-primary">
																Search
																</button>
															</div>
														</div>
													</div>
												</form>
											</div>
											<div class="col-xl-12">
												@if(count($oneway) > 0)
												{{--filter result--}}
												<div class="m--padding-10 m--padding-top-30 ">
													<!--begin: Datatable -->
													<table class="m-datatable" id="html_table" width="100%">
														<thead class="">
															<tr >

<<<<<<< HEAD
																<th>SR .No</th>
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
																<th>Date</th>
																<th>Flight Departure</th>
																<th>Flight Arrival</th>
																<th>Flight Number</th>
																<th>Price </th>
																<th>Action </th>
															</tr>
														</thead>
														<tbody>
<<<<<<< HEAD
																@foreach($oneway as $filte)
															<tr>
																	<td>
																		{{$loop->iteration}}
																	</td>
=======
															<tr>
																@foreach($oneway as $filte)

>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
																<td>
																	{{date_format(date_create($filte->flight_departure),"D d - M - Y ")}}
																</td>
																<td>
																	{{$filte->flightSource->flight_source }}
																	{{'<br>'}}
																	
																	{{'<code>'}}{{date_format(date_create($filte->flight_departure_time)," h:i A")}}{{'</code>'}}
																</td>
																
																<td>
																	{{$filte->flightDestination->flight_destination}}
																	{{'<br>'}}
																	{{'<code>'}}{{date_format(date_create($filte->flight_arrival)," h:i A")}}{{'</code>'}}
																</td>
<<<<<<< HEAD
																<td>{{'<code>'.$filte->flight->flight_name.'-'.$filte->flight->flight_code.'-'.$filte->flight_number.'</code>'}}</td>
																<td>
																	{{'<i class="fa fa-rupee"> '.$filte->flight_price.'</i>'.' / Per Person'}}{{'<br>'}}
=======
																<td>{{'<code>'.$filte->flight->flight_name.'-'.$filte->flight->flight_code.'</code>'}}</td>
																<td>
																	{{'<i class="fa fa-rupee"> '.$filte->flight_price.'</i>'}}{{'<br>'}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
																	{{'[Avail. Seats: '.$filte->seat.']'}}
																</td>
																<td>

																	{{e( Form::open(['method' => 'post','url' => ['flights/one-way-book'],'style'=>'display:inline']))}}
																	{{e('<input type="hidden" value="'.$request['adults'].'" name="adults">')}}
																	{{e('<input type="hidden" value="'.$filte->id.'" name="id">')}}
																	{{'<button type="submit"   class="btn btn-outline-brand m-btn m-btn--icon" title ="Book Now">
																		<span>
																			<i class="la la-envelope-o"></i>
																			<span>Book Now</span>
																		</span>
																	</button>'}}
																	{{ e( Form::close() )}}
																</td>

<<<<<<< HEAD
															</tr>
																@endforeach

=======
																@endforeach

															</tr>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
														</tbody>
													</table>
													<!--end: Datatable -->
												</div>


												{{--end filter result--}}
												@endif
<<<<<<< HEAD
												@if($showalert == 2  || $showalert == 1)
=======
												@if($showalert == 1)
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												{{--alert mail--}}
												<div class="m--padding-10 m--padding-top-30 ">
													<div class="m-alert m-alert--icon alert alert-danger" role="alert">
														<div class="m-alert__icon">
															<i class="flaticon-danger"></i>
														</div>
														<div class="m-alert__text">
														<strong>If The Desired Results Doesn't Meet Your Requirement, Please Click on Send mail, Our Team shall help you to fulfill your Requirement</strong> </div>
														<div class="m-alert__actions" style="width: 220px;">
															<a href="{{url('flights/emailoneway')}}" class="btn btn-outline-light btn-sm m-btn m-btn--hover-brand" >
															<i class="fa fa-envelope"></i> Send Search Mail
															</a>
														</div>
													</div>
												</div>
												{{--end alert mail--}}
												@endif
											</div>
										</div>
									</div>
									<!--end::Form-->
								</div>
								<!--end::one way-->
								{{--end one way--}}
							</div>
						</div>
						<!--end::Portlet-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end::Body -->
	@endsection
	@section('bottom_script')
	<!--begin::Page Vendors -->
	<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
	<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js')}}" type="text/javascript"></script>
	{{--<script src="{{ asset('assets/demo/default/custom/components/forms/validation/form-controls.js')}}" type="text/javascript"></script>--}}
	<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>
	<!--end::Page Vendors -->
	<!--begin::Page Snippets -->
	<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
	<!--end::Page Snippets -->
	<script>
	$('#m_datepicker_1').datepicker({
<<<<<<< HEAD
	format: 'dd-mm-yyyy',
	todayHighlight:	true,
	autoclose:true,
	startDate:new Date()
=======
	format: 'yyyy-mm-dd',
			todayHighlight:	true,
	startDate:new Date(),
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
	});
	</script>
	@endsection