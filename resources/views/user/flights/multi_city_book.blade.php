@extends('layouts.dashMaster')
@section('title') User | Multi City Flight - Book @endsection
@section('style')
<style type="text/css">
	.que2a,.que2b,.que3a,.que3b{
		display: none;
	}
</style>
@endsection
@section('content')

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
						Multi City - Book
					</h3>
				</div>


				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tabs m-portlet--brand m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Multi City Flight - Book
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="tab-content">
								<div class="tab-pane active" id="m_tabs_8_1" role="tabpanel">
									<div class="row m--padding-top-10"></div>
									<h5> First Flight information</h5>
									<hr>
									<div class="row">
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
<<<<<<< HEAD
												Flight Source : {{$multicity->flightSourceF->flight_source}}
=======
												Flying Source : {{$multicity->flightSourceF->flight_source}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
<<<<<<< HEAD
												Flight Destination : {{$multicity->flightDestinationF->flight_destination}}
=======
												Flying Destination : {{$multicity->flightDestinationF->flight_destination}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
<<<<<<< HEAD
												Flight Name : {{$multicity->flightF->flight_name.' - '.$multicity->flightF->flight_code}}
=======
												Flight Name : {{$multicity->flightF->flight_name}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
<<<<<<< HEAD
												Flight Number : {{$multicity->flight_number_f}}
=======
												Flight Number : {{$multicity->flightF->flight_code}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</span>
											</p>
										</div>

									</div>

									<div class="row m--padding-top-40">
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
<<<<<<< HEAD
												Departure Date : {{date("d-M-Y D", strtotime($multicity->flight_departure_date_f))}}
=======
												Departure Date : {{date("Y-M-d D", strtotime($multicity->flight_departure_date_f))}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
<<<<<<< HEAD
												Departure Time : {{date("h:i A", strtotime($multicity->flight_departure_time_f))}}
=======
												Departure Time : {{date("H:i A", strtotime($multicity->flight_departure_time_f))}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</span>
											</p>
										</div>
										<div class="col-md-4">
											<p>
											<span class="m--font-bolder">
<<<<<<< HEAD
												Arrival Time : {{date("h:i A", strtotime($multicity->flight_arrival_time_f))}}
=======
												Arrival Time : {{date("H:i A", strtotime($multicity->flight_arrival_time_f))}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</span>
											</p>
										</div>
									</div>

									<div class="row m--padding-top-40"></div>
									<h5>Second Flight information</h5>
									<hr>

									<div class="row">
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
<<<<<<< HEAD
												Flight Source : {{$multicity->flightSourceS->flight_source}}
=======
												Flying Source : {{$multicity->flightSourceS->flight_source}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
<<<<<<< HEAD
												Flight Destination : {{$multicity->flightDestinationS->flight_destination}}
=======
												Flying Destination : {{$multicity->flightDestinationS->flight_destination}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
<<<<<<< HEAD
												Flight Name : {{$multicity->flightS->flight_name.'-'.$multicity->flightS->flight_code}}
=======
												Flight Name : {{$multicity->flightS->flight_name}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
<<<<<<< HEAD
												Flight Number : {{$multicity->flight_number_s}}
=======
												Flight Number : {{$multicity->flightS->flight_code}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</span>
											</p>
										</div>

									</div>

<<<<<<< HEAD
										<div class="row m--padding-top-40">
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Departure Date : {{date("d-M-Y D", strtotime($multicity->flight_departure_date_s))}}
												</span>
												</p>
											</div>
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Departure Time : {{date("h:i A", strtotime($multicity->flight_departure_time_s))}}
												</span>
												</p>
											</div>
											<div class="col-md-4">
												<p>
												<span class="m--font-bolder">
													Arrival Time : {{date("h:i A", strtotime($multicity->flight_arrival_time_s))}}
												</span>
												</p>
											</div>
										</div>
										@if($Third != 1)
										<div class="row m--padding-top-40"></div>
										<h5> Third Flight information</h5>
										<hr>

										<div class="row">
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Flight Source : {{$multicity->flightSourceT->flight_source}}
												</span>
												</p>
											</div>
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Flight Destination : {{$multicity->flightDestinationT->flight_destination}}
												</span>
												</p>
											</div>
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Flight Name : {{$multicity->flightT->flight_name.'-'.$multicity->flightT->flight_code}}
												</span>
												</p>
											</div>
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Flight Number : {{$multicity->flight_number_t}}
												</span>
												</p>
											</div>

										</div>

										<div class="row m--padding-top-40">
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Departure Date : {{date("d-M-Y D", strtotime($multicity->flight_departure_date_t))}}
												</span>
												</p>
											</div>
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Departure Time : {{date("h:i A", strtotime($multicity->flight_departure_time_t))}}
												</span>
												</p>
											</div>
											<div class="col-md-4">
												<p>
												<span class="m--font-bolder">
													Arrival Time : {{date("h:i A", strtotime($multicity->flight_arrival_time_t))}}
												</span>
												</p>
											</div>
										</div>
										@endif

										<hr>
										<div class="row m--padding-top-40">
											@if($multicity->flight_via)
											<div class="col-md-2 ">
												<p>
												<span class="m--font-bolder">
													Via : {{$multicity->flight_via }}
=======
									<div class="row m--padding-top-40">
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Departure Date : {{date("Y-M-d D", strtotime($multicity->flight_departure_date_s))}}
											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Departure Time : {{date("H:i A", strtotime($multicity->flight_departure_time_s))}}
											</span>
											</p>
										</div>
										<div class="col-md-4">
											<p>
											<span class="m--font-bolder">
												Arrival Time : {{date("H:i A", strtotime($multicity->flight_arrival_time_s))}}
											</span>
											</p>
										</div>
									</div>
							@if($Third != 1)
									<div class="row m--padding-top-40"></div>
									<h5> Third Flight information</h5>
									<hr>

									<div class="row">
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Flying Source : {{$multicity->flightSourceT->flight_source}}
											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Flying Destination : {{$multicity->flightDestinationT->flight_destination}}
											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Flight Name : {{$multicity->flightT->flight_name}}
											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Flight Number : {{$multicity->flightT->flight_code}}
											</span>
											</p>
										</div>

									</div>

									<div class="row m--padding-top-40">
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Departure Date : {{date("Y-M-d D", strtotime($multicity->flight_departure_date_t))}}
											</span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Departure Time : {{date("H:i A", strtotime($multicity->flight_departure_time_t))}}
											</span>
											</p>
										</div>
										<div class="col-md-4">
											<p>
											<span class="m--font-bolder">
												Arrival Time : {{date("H:i A", strtotime($multicity->flight_arrival_time_t))}}
											</span>
											</p>
										</div>
									</div>
									@endif

									<hr>
									<div class="row m--padding-top-40">
										@if($multicity->flight_pnr_no)
										<div class="col-md-2 ">
											<p>
											<span class="m--font-bolder">
												PNR No : {{$multicity->flight_pnr_no }}
											</span>

											</p>
										</div>
										@endif
											@if($multicity->flight_pnr_no2)
												<div class="col-md-2 ">
												<p>
												<span class="m--font-bolder">
													PNR No 2 : {{$multicity->flight_pnr_no2 }}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												</span>

												</p>
											</div>
											@endif
<<<<<<< HEAD

											<div class="col-md-3 ">
												<p>
												<span class="m--font-bolder">
													Price : {{$multicity->flight_price * $adults }}
												</span>
													<br>
													<span class="m-form__help"> price {{$multicity->flight_price}}* per person  </span>
												</p>
											</div>
											<div class="col-md-3">
												<p>
												<span class="m--font-bolder">
													Seat : {{$adults}}
												</span>
												</p>
											</div>
										</div>
										<hr>
									<form action="{{URL('flights/payment/multicity')}}" method="post" class="m-form m-form--fit m-form--label-align-right" id="multi_city" >
										<div class="row">
											{{csrf_field()}}
=======
											@if($multicity->flight_pnr_no3)
												<div class="col-md-2 ">
													<p>
												<span class="m--font-bolder">
													PNR No 3 : {{$multicity->flight_pnr_no3 }}
												</span>

													</p>
												</div>
											@endif
										<div class="col-md-3 ">
											<p>
											<span class="m--font-bolder">
												Price : {{$multicity->flight_price * $adults }}
											</span>
												<br>
												<span class="m-form__help"> price {{$multicity->flight_price}}*Seat  </span>
											</p>
										</div>
										<div class="col-md-3">
											<p>
											<span class="m--font-bolder">
												Seat : {{$adults}}
											</span>
											</p>
										</div>
									</div>
									<hr>
									<form action="pay" method="get" class="m-form m-form--fit m-form--label-align-right" id="m_form_2" >
										<div class="row">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											@for($i=1; $i<=$adults;$i++)
												<div class="col-md-6 m--padding-top-20">
													<p>
												<span class="m--font-bolder">
													Full Name:
												</span>
													</p>
													<div class="row">
														<div class="col-md-2 ">
<<<<<<< HEAD
															<select name="title_{{$i}}" required class=" custom-select textname ">
=======
															<select name="title_{{$i}}" required class=" custom-select ">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

																<option value="Mr.">Mr.</option>
																<option value="Ms.">Ms.</option>
																<option value="Mrs.">Mrs.</option>
															</select>
															<span class="m-form__help"></span>

														</div>
														<div class="col-md-5 ">
<<<<<<< HEAD
															<input type="text" id="txtNamefirst_{{$i}}" required  minlength="1" maxlength="20" name="txtNamefirst_{{$i}}" class="textname txtNamefirst form-control" autocomplete="off" value="" placeholder="First Name">
															<span class="m-form__help"></span>
														</div>
														<div class="col-md-5 ">
															<input type="text" id="txtNamelast_{{$i}}" required  minlength="1" maxlength="20"  name="txtNamelast_{{$i}}" class="textname txtNamefirst form-control " autocomplete="off" value="" placeholder="Last Name">
=======
															<input type="text" id="txtNamefirst_{{$i}}" required  minlength="1" maxlength="20" name="txtNamefirst_{{$i}}" class="form-control" autocomplete="off" value="" placeholder="First Name">
															<span class="m-form__help"></span>
														</div>
														<div class="col-md-5 ">
															<input type="text" id="txtNamelast_{{$i}}" required  minlength="1" maxlength="20"  name="txtNamelast_{{$i}}" class="form-control " autocomplete="off" value="" placeholder="Last Name">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
															<span class="m-form__help"></span>
														</div>
													</div>

												</div>
											@endfor
										</div>
									<hr>
									<div class="row">
										<div class="col m--align-right">
<<<<<<< HEAD
											<a class="btn btn-outline-primary" id="bookNowFlightModalform" href="#" data-id="in_flight_id" data-toggle="modal" data-target="#bookNowFlightModal">Checkout</a>

=======
											<button type="submit" class="btn btn-brand">
												Submit
											</button>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											<button type="reset" class="btn btn-secondary">
												Cancel
											</button>
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!--end::Portlet-->
				</div>
			</div>
		</div>
	</div>
	<!-- end::Body -->
<<<<<<< HEAD

	<!--begin::Modal-->
	<div class="modal fade" id="bookNowFlightModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Flight Booking</h4>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">
											&times;
										</span>
					</button>
				</div>
				<div class="modal-body ">
					<form id="frmOneWayBookFlight" name="frmOneWayBookFlight" method="POST" action="" class="form-hotel">
						<div class="modal-body">
							<div class="row m--padding-top-10"></div>
							<h5> First Flight information</h5>
							<hr>
							<div class="row">
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Source : {{$multicity->flightSourceF->flight_source}}
											</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Destination : {{$multicity->flightDestinationF->flight_destination}}
											</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Name : {{$multicity->flightF->flight_name.'-'.$multicity->flightF->flight_code}}
											</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Number : {{$multicity->flight_number_f}}
											</span>
									</p>
								</div>

							</div>

							<div class="row m--padding-top-40">
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Departure Date : {{date("d-M-Y D", strtotime($multicity->flight_departure_date_f))}}
											</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Departure Time : {{date("h:i A", strtotime($multicity->flight_departure_time_f))}}
											</span>
									</p>
								</div>
								<div class="col-md-4">
									<p>
											<span class="m--font-bolder">
												Arrival Time : {{date("h:i A", strtotime($multicity->flight_arrival_time_f))}}
											</span>
									</p>
								</div>
							</div>

							<div class="row m--padding-top-40"></div>
							<h5>Second Flight information</h5>
							<hr>

							<div class="row">
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Source : {{$multicity->flightSourceS->flight_source}}
											</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Destination : {{$multicity->flightDestinationS->flight_destination}}
											</span>
									</p>
								</div>

								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Name : {{$multicity->flightS->flight_name.'-'.$multicity->flightS->flight_code}}
											</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
											<span class="m--font-bolder">
												Flight Number : {{$multicity->flight_number_s}}
											</span>
									</p>
								</div>

							</div>

							<div class="row m--padding-top-40">
								<div class="col-md-3">
									<p>
												<span class="m--font-bolder">
													Departure Date : {{date("d-M-Y D", strtotime($multicity->flight_departure_date_s))}}
												</span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
												<span class="m--font-bolder">
													Departure Time : {{date("h:i A", strtotime($multicity->flight_departure_time_s))}}
												</span>
									</p>
								</div>
								<div class="col-md-4">
									<p>
												<span class="m--font-bolder">
													Arrival Time : {{date("h:i A", strtotime($multicity->flight_arrival_time_s))}}
												</span>
									</p>
								</div>
							</div>
							@if($Third != 1)
								<div class="row m--padding-top-40"></div>
								<h5> Third Flight information</h5>
								<hr>

								<div class="row">
									<div class="col-md-3">
										<p>
												<span class="m--font-bolder">
													Flight Source : {{$multicity->flightSourceT->flight_source}}
												</span>
										</p>
									</div>
									<div class="col-md-3">
										<p>
												<span class="m--font-bolder">
													Flight Destination : {{$multicity->flightDestinationT->flight_destination}}
												</span>
										</p>
									</div>
									<div class="col-md-3">
										<p>
												<span class="m--font-bolder">
													Flight Name : {{$multicity->flightT->flight_name.'-'.$multicity->flightT->flight_code}}
												</span>
										</p>
									</div>
									<div class="col-md-3">
										<p>
												<span class="m--font-bolder">
													Flight Number : {{$multicity->flight_number_t}}
												</span>
										</p>
									</div>

								</div>

								<div class="row m--padding-top-40">
									<div class="col-md-3">
										<p>
												<span class="m--font-bolder">
													Departure Date : {{date("d-M-Y D", strtotime($multicity->flight_departure_date_t))}}
												</span>
										</p>
									</div>
									<div class="col-md-3">
										<p>
												<span class="m--font-bolder">
													Departure Time : {{date("h:i A", strtotime($multicity->flight_departure_time_t))}}
												</span>
										</p>
									</div>
									<div class="col-md-4">
										<p>
												<span class="m--font-bolder">
													Arrival Time : {{date("h:i A", strtotime($multicity->flight_arrival_time_t))}}
												</span>
										</p>
									</div>
								</div>
							@endif

							<hr>
							<div class="row m--padding-top-40">
								@if($multicity->flight_via)
									<div class="col-md-2 ">
										<p>
											<span class="m--font-bolder">
												Via : {{$multicity->flight_via }}
											</span>

										</p>
									</div>
								@endif

								<div class="col-md-3 ">
									<p>
												<span class="m--font-bolder">
													Price : {{$multicity->flight_price * $adults }}
												</span>
										<br>
										<span class="m-form__help"> price {{$multicity->flight_price}}*per person  </span>
									</p>
								</div>
								<div class="col-md-3">
									<p>
										<span class="m--font-bolder">
											Seat : {{$adults}}
										</span>
									</p>
								</div>
							</div>
							<hr>

							<h6>Member Information </h6>
							<hr>
							<div class="row" id="name_info"></div>
						</div>
					</form>

				</div>


				<div class="row m--padding-20 ">
					<div class="col-md-6 m--padding-right-50 m--pull-left m--align-left">
						<label class="m-checkbox m-checkbox--focus">
							<input form="multi_city" type="checkbox" name="agree" required>
							I Agree the
							<a href="#" class="m-link m-link--focus">
								terms and conditions
							</a>
							.
							<span></span>
						</label>
					</div>
					<div class="col-md-6 m--align-right m--padding-left-50">

						<button  type="button" id="close_model" class="btn btn-secondary" data-dismiss="modal">
							Close
						</button>
						<button  form="multi_city" type="submit" class=" btn btn-primary">
							Buy
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--end::Modal-->
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
	@endsection
	@section('bottom_script')

	<!--begin::Page Vendors -->
	<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
	{{--<script src="{{ asset('assets/demo/default/custom/components/forms/validation/user-form-controls.js')}}" type="text/javascript"></script>--}}
<<<<<<< HEAD
		<script>
			$('#bookNowFlightModalform').click(function(){

				var nameform= "";
				var ms="";
				var fn ="";
				$(".textname").each(function() {

					if($(this).val() != "") {
						if((($(this).val())=="Mr.") || (($(this).val())=="Mrs.") || (($(this).val())=="Ms."))
						{
							ms = $(this).val();
						}
						else if($(this).attr('class')=='textname txtNamefirst form-control')
						{
							fn = $(this).val();
						}
						else
						{
							nameform += '<div class="col-xs-12 col-sm-12 col-md-6 form-group "><p>Name : ' + ms +fn+' '+$(this).val() + '</p></div>';
							$('#name_info').empty().append(nameform);
							ms="";
						}
					}
					else
					{
						var id = $(this).attr("id");
						$("#"+id).css("border-color","red");
						setTimeout(function(){
							$('#bookNowFlightModal').modal('toggle');
						}, 1000);
					}
				});

			});
			$( " .textname" ).keypress(function() {
				if($(this).val() == "")
				{
					var id = $(this).attr("id");
					$("#"+id).css("border-color","rgba(0,0,0,.15)");
				}

			});
		</script>
=======

>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
	<!--end::Page Vendors -->
	@endsection