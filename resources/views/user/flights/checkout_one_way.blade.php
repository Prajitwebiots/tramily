@extends('layouts.dashMaster')
@section('title') User Dashboard @endsection
@section('style')
<style type="text/css">
	.que2a,.que2b,.que3a,.que3b{
		display: none;
	}
</style>
@endsection
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
						Flight List
					</h3>
				</div>
				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-md-12">
					<h4 class="m-portlet__head-text m--padding-bottom-20 ">Flight List - One Way  </h4>
					<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tabs m-portlet--primary m-portlet--head-solid-bg m-portlet--head-sm">
						<div class="m-portlet__head">
							<div class="m-portlet__head-tools">
								<ul class="nav nav-tabs m-tabs m-tabs-line" role="tablist">
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_7_1" role="tab" aria-expanded="false">
											Flight List
										</a>
									</li>
								</ul>
							</div>
						</div>

					<!--end::Portlet-->
				</div>
			</div>
		</div>
	</div>
</div>

<!-- end::Body -->
@endsection
@section('bottom_script')

<!--begin::Page Vendors -->
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js')}}" type="text/javascript"></script>
{{--<script src="{{ asset('assets/demo/default/custom/components/forms/validation/form-controls.js')}}" type="text/javascript"></script>--}}
	<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>

<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
<script>
    $('#m_datepicker_1').datepicker({
<<<<<<< HEAD
        format: 'dd-mm-yyyy',
=======
        format: 'yyyy-mm-dd',
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        todayHighlight:	true,
        startDate:new Date(),
    });

</script>
@endsection