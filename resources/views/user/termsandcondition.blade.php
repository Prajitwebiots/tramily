@extends('layouts.master')
@section('title') Tramily | A Travel Family @endsection
@section('style')
	<link href="{{ URL::asset('assets/pages/css/login-5.min.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('assets/login/style.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<!-- begin::Body -->

<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="row">

		<div class="col-md-2"></div>

		<div class="col-md-8">
		<div class="m-stack m-stack--hor m-stack--desktop">
			<div class="m-stack__item m-stack__item--fluid">
				<div class="m-login__wrapper">
					<div class="m--padding-15"></div>
					<div class="m-login__logo">
						<a href="#">
							<img src="{{ URL::asset('assets/app/media/img/logos/logo-2.png')}}"  style="width: 10%;">
						</a>
					</div>&nbsp;&nbsp;

							</div>
						</div>
					</div>
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-2"></div>

		<div class="col-md-8">
			<div class="m-portlet m-portlet--tabs m-portlet--primary m-portlet--head-solid-bg m-portlet--head-sm">
				<div class="m-portlet__head">

					<div class="m-portlet__head-tools">
						<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand  m-tabs-line--left m-tabs-line-danger" role="tablist">

							<li class="nav-item dropdown m-tabs__item">
								<a class="nav-link m-tabs__link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="la la-map-marker"></i>
									Settings
								</a>
								<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(150px, 57px, 0px); top: 0px; left: 0px; will-change: transform;">
									<a class="dropdown-item" data-toggle="tab" href="#m_tabs_9_2">
										Action
									</a>
									<a class="dropdown-item active" data-toggle="tab" href="#m_tabs_9_3" aria-expanded="true">
										Another action
									</a>
									<a class="dropdown-item" data-toggle="tab" href="#m_tabs_9_2">
										Something else here
									</a>

								</div>
							</li>
						</ul>
					</div>
					<div class="m-portlet__head-caption m-tabs-line--right ">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								terms @ conditions
								<small>
									with lineawesome icons
								</small>
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="tab-content">
						<div class="tab-pane" id="m_tabs_9_1" role="tabpanel" aria-expanded="false">
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
						</div>
						<div class="tab-pane " id="m_tabs_9_2" role="tabpanel">
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled..
						</div>
						<div class="tab-pane active" id="m_tabs_9_3" role="tabpanel" aria-expanded="true">
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-2"></div>




	</div>
	</div>
</div>

<!-- end::Body -->
@endsection
@section('bottom_script')
<!--begin::Page Vendors -->
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
@endsection