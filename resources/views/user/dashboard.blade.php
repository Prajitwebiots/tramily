@extends('layouts.dashMaster')
@section('title') User Dashboard @endsection
@section('style')
<<<<<<< HEAD
	<link href="{{asset('assets/demo/default/base/dashboard.css')}}" rel="stylesheet" type="text/css" />
	<style>
		ul.dropdown-menu.inner {
			max-height: 217px !important;
		}
	</style>
=======
<style type="text/css">
	.que2a,.que2b,.que3a,.que3b{
		display: none;
	}
	.hide-block{
		visibility: hidden;
		height: 0;
		width: 0;
		transition: all 0.3s ease;
		padding: 0 !important;
		opacity: 0;
	}
	.show-block{
		visibility: visible;
		height: inherit;
		transition: all 0.3s ease;
		opacity: 1;
		width: inherit;
	}
</style>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
@endsection
@section('content')
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
						Book Flight Tickets . . .
					</h3>
				</div>

			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-lg-12">
					<!-- <h4 class="m-portlet__head-text m--padding-bottom-20 ">What would you like to search Today! </h4> -->
					<!--begin::Portlet-->
<<<<<<< HEAD
					@if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div>   <br>@endif
					@if(session('success'))   <br><div class="alert alert-success">{{ session('success') }}</div>   <br>@endif
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
					<div class="m-portlet m-portlet--tabs m-portlet--brand m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
						<div class="m-portlet__head">

							<div class="m-portlet__head-tools">
								<ul class="nav nav-tabs m-tabs m-tabs-line" role="tablist">

									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_7_1" role="tab" aria-expanded="false">
											One way
										</a>
									</li>
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_7_2" role="tab" aria-expanded="false">
											Round trip
										</a>
									</li>
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link " data-toggle="tab" href="#m_tabs_7_3" role="tab" aria-expanded="true">
											Multi city
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="tab-content">
								{{--one way--}}
								<div class="tab-pane active " id="m_tabs_7_1" role="tabpanel" aria-expanded="false">
									<!--begin::one way-->
										<!-- <h4 class="m-portlet__head-text">Search Flight</h4><hr> -->
										<!--begin::Form-->
									<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"  METHOD="post" action="{{url('flights/one-way-search')}}" id="m_form_2" novalidate="novalidate">
										{{csrf_field()}}
										<div class="m-portlet__body">
												<div class="form-group m-form__group row">
													<div class="col-md-3">
														<select required class="form-control m-bootstrap-select m_selectpicker" name="flying_source"  title="Flying Source" placeholder="Flying Source" data-live-search="true">

															@foreach($source as $fligth)
																<option value="{{$fligth->id}}">{{$fligth->flight_source}}</option>
															@endforeach

														</select>
														<span class="m-form__help">
																Select your flying source
														</span>
													</div>
													<div class="col-md-3">
														<select required class="form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination" data-live-search="true">
															@foreach($destination as $fligth)
																<option value="{{$fligth->id}}">{{$fligth->flight_destination}}</option>
															@endforeach
														</select>
														<span class="m-form__help">
																Select your flying destination
														</span>
													</div>
													<div class="col-md-3">
														<input type="text" required class="form-control" id="m_datepicker_1" placeholder="Departure" name="departure" readonly="" >
														<span class="m-form__help">
														Please select Departure date
													</span>
													</div>
													<div class="col-md-3">
														<select  name="adults" required size="5" class="size_5 form-control m-bootstrap-select m_selectpicker">
															<option value="1" selected > Adults </option>
															<?php
															for($i=1;$i<=20;$i++)
																echo '<option value="'.$i.'">'. $i.' </option>';
															?>

														</select>
														<span class="m-form__help">
														Please select number of Adults
													</span>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-md-11 m--align-right">
														<button type="submit" class="btn btn-primary">
															Search
														</button>
														<button type="reset" class="btn btn-secondary">
															Cancel
														</button>
													</div>
													<div class="col-md-1"></div>
												</div>
											</div>
										</form>
										<!--end::Form-->
									</div>
									<!--end::one way-->


								{{--end one way--}}
								{{--ROUND   way--}}
								<div class="tab-pane" id="m_tabs_7_2" role="tabpanel" aria-expanded="false">
									<!--begin::one way-->
									<!-- <h4 class="m-portlet__head-text">Flight</h4><hr> -->
									<!--begin::Form-->
									<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{url('flights/round-trip-search')}}" id="m_form_2" method="post" novalidate="novalidate">
                                       {{csrf_field()}}
										<div class="m-portlet__body">
											<div class="form-group m-form__group row">
												<div class="col-md-6 m--padding-10">

													<select class="form-control m-bootstrap-select m_selectpicker" name="flying_source" aria-describedby="option-error" aria-invalid="true"  title="Flying Source" placeholder="Flying Source" data-live-search="true">
															@foreach($source as $fligth)
																<option value="{{$fligth->id}}">{{$fligth->flight_source}}</option>
															@endforeach
													</select>
													<span class="m-form__help">
																Select your flying source
														</span>
												</div>
												<div class="col-md-6 m--padding-10">
													<select class="form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination" data-live-search="true">
														@foreach($destination as $fligth)
																<option value="{{$fligth->id}}">{{$fligth->flight_destination}}</option>
															@endforeach
													</select>
													<span class="m-form__help">
																Select your flying destination
														</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<input type="text" class="form-control" id="m_datepicker_2" placeholder="Departure Date" name="departure" readonly="" >
													<span class="m-form__help">
														Please select Departure date
													</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<input type="text" class="form-control" id="m_datepicker_3" placeholder="Return Date" name="returnDate" readonly="" >
													<span class="m-form__help">
														Please select Return date
													</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<select  name="adults"  class="form-control m-bootstrap-select m_selectpicker">
														<option value="1" selected > Adults </option>
                                                        <?php
                                                        for($i=1;$i <=20;$i++)
                                                            echo '<option value="'.$i.'">'. $i.' </option>';
                                                        ?>

													</select>
													<span class="m-form__help">
														Please select number of Adults
													</span>
												</div>
											</div>
											<div class="form-group m-form__group row">
												<div class="col-md-11 m--align-right">
													<button type="submit" class="btn btn-primary">
														Search
													</button>
													<button type="reset" class="btn btn-secondary">
														Cancel
													</button>
												</div>
												<div class="col-md-1"></div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
									<!--end::one way-->


								{{--end ROUND --}}
								{{--MULT ICITY way--}}
								<div class="tab-pane" id="m_tabs_7_3" role="tabpanel" aria-expanded="false">
									<!--begin::one way-->
									<!-- <h4 class="m-portlet__head-text">Flight</h4><hr> -->
									<!--begin::Form-->
									<form method="post" action="{{url('flights/multi-city-search')}}" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">

										{{csrf_field()}}
										<div class="m-portlet__body">
											<div class="form-group m-form__group row">
												<div class="col-md-4 m--padding-10">
													<select class="form-control m-bootstrap-select m_selectpicker" name="flying_source"  title="Flying Source" placeholder="Flying Source" data-live-search="true">
														@foreach($source as $fligth)
															<option value="{{$fligth->id}}">{{$fligth->flight_source}}</option>
														@endforeach
													</select>
													<span class="m-form__help">
																Select your flying source
														</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<select class="form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination" data-live-search="true">
														@foreach($destination as $fligth)
															<option value="{{$fligth->id}}">{{$fligth->flight_destination}}</option>
														@endforeach
													</select>
													<span class="m-form__help">
																Select your flying destination
														</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<input type="text" class="form-control" id="m_datepicker_4" placeholder="Departure" name="departure" readonly="" >
													<span class="m-form__help">
														Please select Departure date
													</span>
												</div>

												{{----}}
												<div class="col-md-4 m--padding-10">
													<select class="form-control m-bootstrap-select m_selectpicker" name="flying_source2"  title="Flying Source" placeholder="Flying Source" data-live-search="true">
														@foreach($source as $fligth)
															<option value="{{$fligth->id}}">{{$fligth->flight_source}}</option>
														@endforeach
													</select>
													<span class="m-form__help">
																Select your flying source
														</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<select class="form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination2" data-live-search="true">
														@foreach($destination as $fligth)
															<option value="{{$fligth->id}}">{{$fligth->flight_destination}}</option>
														@endforeach
													</select>
													<span class="m-form__help">
																Select your flying destination
														</span>
												</div>
												<div class="col-md-4 m--padding-10">
													<input type="text" class="form-control" id="m_datepicker_5" placeholder="Departure" name="departure2" readonly="" >
													<span class="m-form__help">
														Please select Departure date
													</span>
												</div>

												{{----}}

												{{--3--}}
												<div class="col-md-4 m--padding-10 hide  hide-block" >
													<select class=" form-control m-bootstrap-select m_selectpicker" name="flying_source3"  title="Flying Source" placeholder="Flying Source" data-live-search="true">
														@foreach($source as $fligth)
															<option value="{{$fligth->id}}">{{$fligth->flight_source}}</option>
														@endforeach
													</select>
													<span class="m-form__help">
																Select your flying source
														</span>
												</div>
												<div class="col-md-4 m--padding-10 hide  hide-block">
													<select class=" form-control m-bootstrap-select m_selectpicker"  title="Flying Destination" name="flying_destination3" data-live-search="true">
														@foreach($destination as $fligth)
															<option value="{{$fligth->id}}">{{$fligth->flight_destination}}</option>
														@endforeach
													</select>
													<span class="m-form__help">
																Select your flying destination
														</span>
												</div>
												<div class="col-md-4 m--padding-10 hide hide-block">
													<input type="text" class=" form-control" id="m_datepicker_6" placeholder="Departure" name="departure3" readonly="" >
													<span class="m-form__help">
														Please select Departure date
													</span>
												</div>

												{{--3--}}
												<div class="col-md-4 m--padding-10">
													<button type="button" class=" bth-remove m--pull-left bth-sm hide-block btn btn-secondary remove_city" ><i class="fa fa fa-minus"></i> Remove City</button>
													<button type="button" class=" bth-add btn m--pull-left show-block bth-sm btn-secondary add_city" ><i class="fa fa-plus"></i> Add City</button>
												</div>

												<div class="col-md-4 m--padding-10">
													<select  name="adults"  class="form-control m-bootstrap-select m_selectpicker">
														<option value="1" selected > Adults </option>
                                                        <?php
                                                        for($i=1;$i <=20;$i++)
                                                            echo '<option value="'.$i.'">'. $i.' </option>';
                                                        ?>

													</select>
													<span class="m-form__help">
														Please select number of Adults
													</span>
												</div>
											</div>
											<div class="m--padding-10 row">
												<div class="col-md-11 m--align-right">
													<button type="submit" class="btn btn-primary">
														Search
													</button>
													<button type="reset" class="btn btn-secondary">
														Cancel
													</button>
												</div>
												<div class="col-md-1"></div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
									<!--end::one way-->


								{{--end MULTICITY --}}


							</div>
						</div>
					</div>
					<!--end::Portlet-->

				</div>
			</div>
		</div>
	</div>
</div>
<!-- end::Body -->
@endsection
@section('bottom_script')

<!--begin::Page Vendors -->
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js')}}" type="text/javascript"></script>
{{--<script src="{{ asset('assets/demo/default/custom/components/forms/validation/form-controls.js')}}" type="text/javascript"></script>--}}
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
<script>



    $('#m_datepicker_1').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
        orientation: "bottom left",
<<<<<<< HEAD
		autoclose:true,
=======

>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    });

    $('#m_datepicker_2').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		orientation: "bottom left",
<<<<<<< HEAD
		autoclose:true,
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    });

    $('#m_datepicker_3').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
<<<<<<< HEAD
        startDate: '04-05-2018',
		orientation: "bottom left",
		autoclose:true,
=======
        startDate:new Date(),
		orientation: "bottom left",
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    });

    $('#m_datepicker_4').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		orientation: "bottom left",
<<<<<<< HEAD
		autoclose:true,
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    });

    $('#m_datepicker_5').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		orientation: "bottom left",
<<<<<<< HEAD
		autoclose:true,
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    });

    $('#m_datepicker_6').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:	true,
        startDate:new Date(),
		orientation: "bottom left",
<<<<<<< HEAD
		autoclose:true,
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    });

    $(".add_city").click(function(){
        $(".hide").removeClass('hide-block');
        $(".hide").addClass('show-block');
        $(".bth-remove").removeClass('hide-block');
        $(".bth-remove").addClass('show-block');
        $(".bth-add").removeClass('show-block');
        $(".bth-add").addClass('hide-block');
    });
    $(".bth-remove").click(function(){
        $(".hide").removeClass('show-block');
        $(".hide").addClass('hide-block');
        $(".bth-remove").removeClass('show-block');
        $(".bth-remove").addClass('hide-block');
        $(".bth-add").removeClass('hide-block');
        $(".bth-add").addClass('show-block');
<<<<<<< HEAD
    });
	$("#header_menu .m-menu__link").click(function(){
		$("#header_menu>li").removeClass("m-menu__item--active");
		$(this).parent('li').addClass('m-menu__item--active');
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    });
</script>
	<script>// for round trip
        var start = new Date();
        var end = new Date(new Date().setYear(start.getFullYear()+1));


        $('#m_datepicker_2').datepicker({
            startDate : start,
            endDate   : end
        }).on('changeDate', function(){

            
//           $('#m_datepicker_3').datepicker('setStartDate',  $(this).val());
           $('#m_datepicker_3').datepicker('setStartDate', $(this).val());
        });

        $('#m_datepicker_3').datepicker({
            startDate : start,
            endDate   : end
        }).on('changeDate', function(){

            $('#m_datepicker_2').datepicker('setEndDate',  $(this).val());
        });
	</script>
	<script> // for multicity
		var start = new Date();
		var end = new Date(new Date().setYear(start.getFullYear()+1));

		$('#m_datepicker_4').datepicker({
			startDate : start,
			endDate   : end
		}).on('changeDate', function(){
			$('#m_datepicker_5').datepicker('setStartDate',  $(this).val());
			$('#m_datepicker_6').datepicker('setStartDate',  $(this).val());

		});

		$('#m_datepicker_5').datepicker({
			startDate : start,
			endDate   : end
		}).on('changeDate', function(){
			$('#m_datepicker_4').datepicker('setEndDate',  $(this).val());
			$('#m_datepicker_5').datepicker('setStartDate',  $(this).val());
			$('#m_datepicker_6').datepicker('setStartDate',  $(this).val());

		});
		$('#m_datepicker_6').datepicker({
			startDate : start,
			endDate   : end
		}).on('changeDate', function(){
			$('#m_datepicker_5').datepicker('setStartDate',  $(this).val());
			$('#m_datepicker_5').datepicker('setEndDate',  $(this).val());
			$('#m_datepicker_4').datepicker('setEndDate',  $(this).val());
		});

	</script>


@endsection