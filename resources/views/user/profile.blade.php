@extends('layouts.dashMaster')
@section('title') User Profile @endsection
@section('style')
@endsection
@section('content')
<!-- begin::Body -->

<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
	<!-- BEGIN: Left Aside -->
	<button class="m-aside-left-close m-aside-left-close--skin-light" id="m_aside_left_close_btn">
	<i class="la la-close"></i>
	</button>
	
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title ">
					My Profile
					</h3>
				</div>
				
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-lg-3">
					<div class="m-portlet m-portlet--full-height  ">
						<div class="m-portlet__body">
							<div class="m-card-profile">
								<div class="m-card-profile__title m--hide">
									Your Profile
								</div>
								<div class="m-card-profile__pic">
									<div class="m-card-profile__pic-wrapper">
										<img class="m--img-centered" src="{{URL::asset('assets/images/user')}}/{{Sentinel::getUser()->profile }}" alt="" style="height: 150px;width: 150px"/>
									</div>
								</div>
								<div class="m-card-profile__details">
									<span class="m-card-profile__name">
									{{ Sentinel::getUser()->title_name }}
										{{ Sentinel::getUser()->first_name }} {{ Sentinel::getUser()->last_name }}
									</span>
									<a href="" class="m-card-profile__email m-link">
										{{ Sentinel::getUser()->email }}
									</a>
								</div>
							</div>
							<ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
								<li class="m-nav__separator m-nav__separator--fit"></li>
								<li class="m-nav__section m--hide">
									<span class="m-nav__section-text">
										Section
									</span>
								</li>
								<li class="m-nav__item">
									<a href="{{ url('user/profile') }}" class="m-nav__link">
										<i class="m-nav__link-icon flaticon-profile-1"></i>
										<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-nav__link-text">
													My Profile
												</span>
												
											</span>
										</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="{{ url('user/changePass') }}" class="m-nav__link">
										<i class="m-nav__link-icon flaticon-lock"></i>
										<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-nav__link-text">
													Change Password
												</span>
												
											</span>
										</span>
									</a>
								</li>
								<!-- <li class="m-nav__item">
										<a href="header/profile&amp;demo=default.html" class="m-nav__link">
												<i class="m-nav__link-icon flaticon-share"></i>
												<span class="m-nav__link-text">
														Activity
												</span>
										</a>
								</li> -->
								
							</ul>
							
							
						</div>
					</div>
				</div>
				<div class="col-lg-9">
					<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
						<div class="m-portlet__head">
							<div class="m-portlet__head-tools">
								<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
									<li class="nav-item m-tabs__item">
										<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
											<i class="flaticon-share m--hide"></i>
											Update Profile
										</a>
									</li>
									
								</ul>
							</div>
							
						</div>
						<div class="tab-content">
							<div class="tab-pane active" id="m_user_profile_tab_1">
								<form  action=" {{url('user/profile')}}/{{$userdata->id}}" method="post" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
									<input type="hidden" name="_token" value="{{csrf_token()}}">
									<input type="hidden" name="user_id" value="{{$userdata->id}}">
									<br>
									@if(session('error'))
									
									<div class="alert alert-danger">
										{{ session('error') }}
									</div>
									@endif
									@if(session('success'))
									
									<div class="alert alert-success">
										{{ session('success') }}
									</div>
									@endif
									<div class="m-portlet__body">
										<!-- <div class="form-group m-form__group m--margin-top-10 m--hide">
												<div class="alert m-alert m-alert--default" role="alert">
														The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
												</div>
										</div> -->
										
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												Profile  Picture
											</label>
											<div class="col-8">
												<input class="form-control m-input" type="file" name="photo">
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												Agency Name
											</label>
											<div class="col-8">
												<input class="form-control m-input" autocomplete="off" name="agency_name" type="text" value="{{$userdata->agency_name}}">
											</div>
											 @if ($errors->has('agency_name'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('agency_name') }}</strong>
                                                </span>
                                            @endif
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												Agency Address
											</label>
											<div class="col-8">
												 <textarea class="form-control m-input m-input--solid" autocomplete="off" value="{{ old('agency_address') }}" id="exampleTextarea" name="agency_address" autocomplete="off" placeholder="Agency Address" rows="3">{{$userdata->agency_address}}</textarea>
											</div>
											 @if ($errors->has('agency_address'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('agency_address') }}</strong>
                                                </span>
                                            @endif
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												Concerned Person Name
											</label>
											<div class="col-2">
												<select class="form-control m-select  " id="" name="title_name">
                                                <option @if('' == $userdata->title_name) selected @endif value="">TITLE *</option>
                                                <option  @if("Mr."== $userdata->title_name) selected @endif value="Mr.">Mr.</option>
                                                <option @if( 'Ms.' == $userdata->title_name) selected @endif value="Ms.">Ms.</option>
                                                <option @if('Mrs.' == $userdata->title_name) selected @endif value="Mrs.">Mrs.</option>
                                            </select>
											</div>
											<div class="col-3">
												<input class="form-control m-input" type="text" value="{{ $userdata->first_name }}" placeholder="First name *" name="first_name" autocomplete="off">
											</div>
											<div class="col-3">
											<input class="form-control m-input" type="text" value="{{ $userdata->last_name }}" placeholder="Last name *" name="last_name" autocomplete="off">
											</div>
											@if ($errors->has('title_name'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('title_name') }}</strong>
                                                </span>
                                            @endif
                                             @if ($errors->has('first_name'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                </span>
                                            @endif
                                             @if ($errors->has('last_name'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                            @endif
										</div>
										
                                       
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												Email
											</label>
											<div class="col-8">
												<input disabled="" class="form-control m-input" name="email" autocomplete="off" type="email" value="{{$userdata->email}}">
											</div>
											@if ($errors->has('email'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
										</div>
										
										
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Phone No.
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="mobile" autocomplete="off" type="text" value="{{$userdata->mobile}}">
											</div>
											@if ($errors->has('mobile'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('mobile') }}</strong>
                                                </span>
                                            @endif
										</div>

										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Office No.
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="office_no" autocomplete="off" type="text" value="{{$userdata->office_no}}">
											</div>
											@if ($errors->has('office_no'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('office_no') }}</strong>
                                                </span>
                                            @endif
										</div>
										
                                        <div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Country
											</label>
											<div class="col-8">
												<select class="form-control m-select  " id="country" name="country">
                                                
                                                @foreach($countrylist as $country)
                                                    <option @if($userdata->country == $country->id ) selected @endif value="{{$country->id}}">{{$country->country_name .' / '.$country->country_alphacode }}</option>
                                                @endforeach

                                            </select>
											</div>
											@if ($errors->has('country'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('country') }}</strong>
                                                </span>
                                            @endif
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												State
											</label>
											<div class="col-8">
												<select class="form-control m-select  " id="state" name="state">
                                                
                                                @foreach($statelist as $state)
                                                    <option @if($userdata->state == $state->id ) selected  @endif  value="{{$state->id}}">{{$state->state_name }}</option>
                                                @endforeach

                                            </select>
											</div>
											@if ($errors->has('state'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('state') }}</strong>
                                                </span>
                                            @endif
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												City
											</label>
											<div class="col-8">
												<select class="form-control m-select  " id="city" name="city">
                                               
                                                @foreach($citylist as $city)
                                                   <option  @if($city->id == $userdata->city)  selected  @endif  value="{{$city->id}}">{{$city->city_name }}</option>
                                                @endforeach

                                            </select>
											</div>
											@if ($errors->has('city'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                            @endif
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Pincode
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="pincode" autocomplete="off" type="text" value="{{$userdata->pincode}}">
											</div>
											@if ($errors->has('pincode'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('pincode') }}</strong>
                                                </span>
                                            @endif
										</div>

										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												PAN no
											</label>
											<div class="col-8">
												 <input class="form-control m-input" type="text" value="{{ $userdata->pan_no }}" placeholder="PAN No" name="pan_no" autocomplete="off">
											</div>
											@if ($errors->has('pan_no'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('pan_no') }}</strong>
                                                </span>
                                            @endif
										</div>

										

										<div class="m--padding-15"></div><strong style="font-weight: 600;margin-left: 28px;">Agency GST Details</strong><div class="m--padding-15"></div>
									<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Name
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="gst_full_name" autocomplete="off" type="text" value="{{$userdata->gst_name}}">
											</div>
											@if ($errors->has('gst_name'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('gst_name') }}</strong>
                                                </span>
                                            @endif
										</div><div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												State
											</label>
											<div class="col-8">
											<select class="form-control m-select  " id="state" name="gst_state">
                                                <option value="">State *</option>
                                                @foreach($statelist as $state)
                                                  <option  @if( $state->id == $userdata->gst_state)  selected @endif value="{{$state->id}}">{{$state->state_name }}</option> 
                                                @endforeach

                                            </select>
												
											</div>
											@if ($errors->has('gst_state'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('gst_state') }}</strong>
                                                </span>
                                            @endif
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label">
												GST Address
											</label>
											<div class="col-8">
												 <textarea class="form-control m-input m-input--solid" autocomplete="off"  id="exampleTextarea" name="gst_address" autocomplete="off" placeholder="GST Address" rows="3">{{$userdata->gst_address}}</textarea>
											</div>
											 @if ($errors->has('agency_address'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('agency_address') }}</strong>
                                                </span>
                                            @endif
										</div>
										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Phone Number
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="gst_phone" autocomplete="off" type="text" value="{{$userdata->gst_phone}}">
											</div>
											@if ($errors->has('gst_phone'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('gst_phone') }}</strong>
                                                </span>
                                            @endif
										</div>

										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												Registeres email id
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="regi_gmail" autocomplete="off" type="text" value="{{$userdata->regi_gmail}}">
											</div>
											@if ($errors->has('regi_gmail'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('regi_gmail') }}</strong>
                                                </span>
                                            @endif
										</div>

										<div class="form-group m-form__group row">
											<label for="example-text-input" class="col-2 col-form-label" >
												GSTIN No
											</label>
											<div class="col-8">
												<input class="form-control m-input" name="gst_no" autocomplete="off" type="text" value="{{$userdata->gst_no}}">
											</div>
											@if ($errors->has('gst_no'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('gst_no') }}</strong>
                                                </span>
                                            @endif
										</div>
										
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions">
											<div class="row">
												<div class="col-2"></div>
												<div class="col-8">
													<button type="submit" class="btn btn-primary m-btn m-btn--air m-btn--custom">
													Save changes
													</button>
													&nbsp;&nbsp;
													<button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
													Cancel
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="tab-pane active" id="m_user_profile_tab_2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- end::Body -->
@endsection
@section('bottom_script')
<!--begin::Page Vendors -->
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#country').on('change',function(){

            var countryID = $(this).val();
            if(countryID){
                $.ajax({
                    type:'get',
                    url:'{{URL('state/')}}/'+countryID,
                    data:'country_id='+countryID,
                    success:function(html){
                        $('#state').html(html);
                        $('#city').html('<option value="">Select state first</option>');
                    }
                });
            }else{
                $('#state').html('<option value="">Select country first</option>');
                $('#city').html('<option value="">Select state first</option>');
            }
        });

        $('#state').on('change',function(){
            var stateID = $(this).val();
            if(stateID){
                $.ajax({
                    type:'get',
                    url:'{{URL('city/')}}/'+stateID,
                    data:'state_id='+stateID,
                    success:function(html){
                        $('#city').html(html);
                    }
                });
            }else{
                $('#city').html('<option value="">Select state first</option>');
            }
        });
    });
</script>
@endsection