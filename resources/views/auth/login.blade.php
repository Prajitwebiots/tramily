@extends('layouts.master')
@section('title') Tramily | A Travel Family @endsection
@section('style')
<link href="{{ URL::asset('assets/pages/css/login-5.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<style>
.m-login.m-login--1 .m-login__wrapper {
overflow: hidden;
padding: 0% 2rem 2rem 2rem !important;
}
.m-login.m-login--1 .m-login__aside {
width: 50%;
}

</style>
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--singin" id="m_login" style="">
        <!-- <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside"> -->
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="m-stack m-stack--hor m-stack--desktop">
                <div class="m-stack__item m-stack__item--fluid">
                    <div class="m-login__wrapper">
                        <div class="m--padding-15"></div>
                        <div class="m-login__logo">
                            <a href="#">
                                <img src="{{ URL::asset('assets/app/media/img/logos/logo-2.png')}}"  style="width: 30%;">
                            </a>
                        </div>
                        <div class="m-login__signin">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                Sign In To Tramily
                                </h3>
                            </div>


                            <form class="m-login__form m-form" action="{{route('login.store')}}" method="post">
                                  @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br><br>@endif
                            @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br><br>@endif
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off">
                                </div>
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
                                </div>
                                <div class="row m-login__form-sub">
                                    <div class="col m--align-left">
                                        <label class="m-checkbox m-checkbox--focus">
                                            <input type="checkbox" name="remember">
                                            Remember me
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="col m--align-right">
                                        <a href="javascript:;" id="m_login_forget_password" class="m-link">
                                            Forget Password ?
                                        </a>
                                    </div>
                                </div>
                                <div class="m-login__form-action">
                                    <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                    Sign In
                                    </button>
                                </div>
                            </form>

                            <div class="m-stack__item m-stack__item--center">
                    <div class="m-login__account">
                        <span class="m-login__account-msg">
                            Don't have an account yet ?
                        </span>
                        &nbsp;&nbsp;
                        <a href="{{route('register.index')}}" id="m_login_signup1" class="m-link m-link--focus m-login__account-link">
                            Sign Up
                        </a>
                    </div>
                </div>
                        </div>
                      
                        <div class="m-login__forget-password">
                            <div class="m-login__head">
                                <h3 class="m-login__title">
                                Forgotten Password ?
                                </h3>
                                <div class="m-login__desc">
                                    Enter your email to reset your password:
                                </div>
                            </div>
                            <form class="m-login__form m-form" action="{{route('forgot.store')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
                                </div>
                                <div class="m-login__form-action">
                                    <button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                    Request
                                    </button>
                                    <button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
                                    Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1  m-login__content" style="background-image: url({{ URL::asset('assets/app/media/img/bg/bg-4.jpg')}})">-->
        <!--    <div class="m-grid__item m-grid__item--middle">-->
        <!--        <h3 class="m-login__welcome">-->
        <!--        WELCOME TO TRAMILY-->
        <!--        </h3>-->
        <!--        <p class="m-login__msg">-->
        <!--            The world is a book and those who do not travel read only one page-->
        <!--            <br>-->

        <!--        </p>-->
        <!--    </div>-->
        <!--</div>-->
    </div>
</div>
<!-- end:: Page -->
@endsection
@section('bottom_script')
<script src="{{ asset('assets/snippets/pages/user/login.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/snippets/pages/user/select2.js')}}" type="text/javascript"></script>

@if(@$register)
<script type="text/javascript">
$(document).ready(function(){
$("#m_login_signup").trigger('click');
});
</script>
@endif
@endsection