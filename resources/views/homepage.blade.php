<!DOCTYPE html>
<html class="boxed">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Tramily - A Travel Family</title>

    <meta name="keywords" content="" />
    <meta name="description" content="">



    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

{{--<!-- Vendor CSS -->--}}
{{--<link rel="stylesheet" href="http://tramily.in/assets//vendor/bootstrap/css/bootstrap.min.css">--}}
{{--<link rel="stylesheet" href="http://tramily.in/assets//vendor/font-awesome/css/font-awesome.min.css">--}}
{{--<link rel="stylesheet" href="http://tramily.in/assets//vendor/animate/animate.min.css">--}}
{{--<link rel="stylesheet" href="http://tramily.in/assets//vendor/simple-line-icons/css/simple-line-icons.min.css">--}}
{{--<link rel="stylesheet" href="http://tramily.in/assets//vendor/owl.carousel/assets/owl.carousel.min.css">--}}
{{--<link rel="stylesheet" href="http://tramily.in/assets//vendor/owl.carousel/assets/owl.theme.default.min.css">--}}
{{--<link rel="stylesheet" href="http://tramily.in/assets//vendor/magnific-popup/magnific-popup.min.css">--}}
{{--<link rel="stylesheet" href="http://tramily.in/assets//vendor/bootstrap-datepicker/bootstrap-datepicker3.css">--}}

<!-- Theme CSS -->
    <link rel="stylesheet" href="http://tramily.in/assets//css/theme.css">
    <link rel="stylesheet" href="http://tramily.in/assets//css/theme-elements.css">
    <link rel="stylesheet" href="http://tramily.in/assets//css/theme-blog.css">
    <link rel="stylesheet" href="http://tramily.in/assets//css/theme-shop.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="http://tramily.in/assets//vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="http://tramily.in/assets//vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="http://tramily.in/assets//vendor/rs-plugin/css/navigation.css">

    <!-- Skin CSS -->
    <link rel="stylesheet" href="http://tramily.in/assets//css/skins/skin-hotel.css">

    {{--<!-- Demo CSS -->--}}
    <link rel="stylesheet" href="http://tramily.in/assets//css/demos/demo-hotel.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <style type="text/css">
        html.boxed .body {
            margin:0;
        }
        .topbar{

            padding: 0;
            background: #094797;
        }

        .topbar .container .row {
            margin:-7px;
            padding:0;
        }

        .topbar .container .row .col-md-12 {
            padding:0;
        }

        .topbar p{
            margin:0;
            display:inline-block;
            font-size: 13px;
            color: #f1f6ff;
        }

        .topbar p > i{
            margin-right:5px;
        }
        .topbar p:last-child{
            text-align:right;
        }

        header .navbar {
            margin-bottom: 0;
        }

        .topbar li.topbar {
            display: inline;
            padding-right: 18px;
            line-height: 52px;
            transition: all .3s linear;
        }

        .topbar li.topbar:hover {
            color: #1bbde8;
        }

        .topbar ul.info i {
            color: #131313;
            font-style: normal;
            margin-right: 8px;
            display: inline-block;
            position: relative;
            top: 4px;
        }

        .topbar ul.info li {
            float: right;
            padding-left: 30px;
            color: #ffffff;
            font-size: 13px;
            line-height: 44px;
        }

        .topbar ul.info i span {
            color: #aaa;
            font-size: 13px;
            font-weight: 400;
            line-height: 50px;
            padding-left: 18px;
        }

        ul.social-network {
            border:none;
            margin:0;
            padding:0;
        }

        ul.social-network li {
            border:none;
            margin:0;
        }

        ul.social-network li i {
            margin:0;
        }

        ul.social-network li {
            display:inline;
            margin: 0 5px;
            border: 0px solid #2D2D2D;
            padding: 5px 0 0;
            width: 32px;
            display: inline-block;
            text-align: center;
            height: 32px;
            vertical-align: baseline;
            color: #000;
        }
        ul.social-network1 li {
            display:inline;
            margin: 0 5px;
            border: 0px solid #2D2D2D;
            padding: 5px 0 0;

            display: inline-block;
            text-align: center;
            height: 32px;
            vertical-align: baseline;
            color: #000;
        }
        ul.social-network {
            list-style: none;
            margin: 8px 0 10px -25px;
            float: right;
        }
        ul.social-network1 {
            list-style: none;
            margin: 8px 0 10px -25px;
            float: left;
        }
        .header-section {
            padding: 15px 0;
            background-color: white;

        }
        .search-form .btn {
            padding: 7.5px;
            border-radius: 0;
            height: 44px;
            background: #094797;
            border: none;
            /* background: linear-gradient(to right,#00abc1,#006ab6);*/
            color: white;
            /* font-weight: bold; */
            font-size: 18px;
            border-radius: 4px;
        }
        .navbar-brand {
            float: left;
            padding: 0;
        }
        .navbar-brand img {
            float: left;
            transition: all .5s ease-out;
            margin: 0;
        }
        .header input {
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12), 0 1px 2px 0 rgba(0, 0, 0, .24);
        }
        .form-control, select {
            display: block;
            width: 100%;
            height: 44px;
            border: 1px solid #094797;
            border-radius: 4px;
            background-color: #fff;
            background-image: none;
            box-shadow: none;
            color: #000;
            font-size: 14px;
            line-height: 1.42857143;
        }
        .img-logo{
            width: 119px;
            margin-top: -2px !important;
        }
        .waves-effect{
            color: white;
        }
        .waves-effect:hover{
            color: white;
            text-decoration: none;
        }
        @media screen and (max-width: 767px) {
            .smallwidth{
                width: 100%;
            }
            .margin76{
                margin-bottom: 76px;
            }
        }
    </style>
    </head>
    <body>



<div class="body">
    <header class="topbar">

        <div class="container">
            <!-- social icon-->
            <div class="col-sm-12">
                <ul class="social-network">
                    <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-pinterest"></i></a></li>
                    <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-google-plus"></i></a></li>
                </ul>
                <ul class="social-network1 pull-left">
                    <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-phone" aria-hidden="true"></i> 0261-4012162-169</a></li>
                    <li><a class="waves-effect waves-dark" href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> support@tramily.dsss.in</a></li>

                </ul>
            </div>

        </div>

    </header>
    <div class="header-section">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-3 col-sm-12 margin76">
                    <a href="#" class="navbar-brand logo">
                        <img src="{{asset('assets/app/media/img/logos/logo-2.png')}}"  class="img-responsive img-logo">
                    </a>
                </div>
                <div class="col-sm-12 col-lg-9 col-md-9 pull-right smallwidth">
                    @if (Sentinel::check())
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">&nbsp;</label>
                                <a href="{{url('user/dashboard')}}" class="btn btn-raised ripple-effect btn-default btn-block" >
                                    My Dashboard
                                </a>
                            </div>
                        </div>

                   @else
                        <form class="" action="{{url('user_Login')}}" method="post">
                            @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br><br>@endif
                            @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br><br>@endif
                            <div class="search-form ">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="col-md-2">
                                </div>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address</label>
                                                <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- /.col 4 -->

                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Password</label>
                                                <input type="password" name="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col 3 -->
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">&nbsp;</label>
                                        <button type="submit" class="btn btn-raised ripple-effect btn-default btn-block" href="results.html">
                                            Login
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </form>
                @endif


                        <!-- /.col 1 -->

                    </div>
                </div>
            </div>
        </div>
    </div>


<!-- <section class="parallax section section-parallax section-center m-none section-overlay-opacity section-overlay-opacity-scale-4" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="img/demos/hotel/parallax-hotel.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 center">
                    <h3 class="mb-xs mt-none text-light text-uppercase">Client Login</h3>
                    <a class="btn btn-primary btn-lg font-size-sm text-uppercase mt-sm" href="<?php echo 'login'?>">Click Here To Login</a>
                </div>
            </div>
        </div>
    </section>-->


    <div role="main" class="main">
        <div class="slider-container rev_slider_wrapper" style="height: 530px;">
            <div id="revolutionSlider" class="slider rev_slider manual">
                <ul>
                    <li data-transition="boxfade">

                        <img src="{{asset('assets/home/slide-hotel-1.jpg')}}"
                             alt=""
                             data-bgposition="center bottom"
                             data-bgfit="cover"
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10"
                             class="rev-slidebg"
                             data-no-retina>
                    </li>
                    <li data-transition="boxfade">

                        <img src="{{asset('assets/home/slide-hotel-2.jpg')}}"
                             alt=""
                             data-bgposition="center bottom"
                             data-bgfit="cover"
                             data-bgrepeat="no-repeat"
                             data-bgparallax="10"
                             class="rev-slidebg"
                             data-no-retina>
                    </li>

                </ul>
            </div>
        </div>

        <section class="section section-no-background section-no-border m-none">
            <div class="container">
                <div class="row mb-xl">
                    <div class="col-md-4">

                        <div class="">
                            <div>
                                <img alt="" class="img-responsive" src="{{asset('assets/app/media/img/logos/logo-2.png')}}">
                            </div>

                        </div>

                    </div>
                    <div class="col-md-8">

                        <h3 class="mt-xl mb-none pb-none text-uppercase">Welcome To Tramily </h3>
                        <div class="divider divider-primary divider-small mb-xl mt-none">
                            <hr class="mt-sm">
                        </div>

                        <p class="mt-lg">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultrices malesuada ante quis pharetra. Nullam non bibendum dolor. Ut vel turpis accumsan, efficitur dolor fermentum, tincidunt metus ut vel turpis accumsan, efficitur dolor fermentum, tincidunt metus. Etiam ut. </p>

                        <p class="mt-lg">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultrices malesuada ante quis pharetra. Nullam non bibendum dolor. Ut vel turpis accumsan, efficitur dolor fermentum, tincidunt metus ut vel turpis accumsan, efficitur dolor fermentum, tincidunt metus. Etiam ut. </p>

                    </div>
                </div>
            </div>
        </section>


        <footer id="footer" class="color color-primary mt-none pt-xlg pb-xlg">
            <div class="container">
                <div class="row pt-md pb-md">

                    <div class="col-md-6 mb-none">
                        <div class="footer-info">
                            <i class="icon-location-pin icons"></i>
                            <label>Address</label>
                            <strong>412, Poddar Plaza, Opp. Majura Fire Station, Turning Point, Bhatar Road, Surat.</strong>
                        </div>
                    </div>
                    <div class="col-md-3 mb-none">
                        <div class="footer-info">
                            <i class="icon-phone icons"></i>
                            <label>Call Us</label>
                            <strong>0261-4012162-169</strong>
                        </div>
                    </div>
                    <div class="col-md-3 mb-none">
                        <div class="footer-info">
                            <i class="icon-share icons"></i>
                            <label>follow us</label>
                            <ul class="social-icons">

                                <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i> Facebook</a></li>
                                <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i> Twitter</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div>

</div>

<div class="footer-copyright">
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <p>© Copyright 2017. Tramily. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</div>
<!-- Vendor -->
<script src="http://tramily.in/assets//vendor/jquery/jquery.min.js"></script>
<script src="http://tramily.in/assets//vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="http://tramily.in/assets//vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="http://tramily.in/assets//vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="http://tramily.in/assets//vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="http://tramily.in/assets//vendor/common/common.min.js"></script>
<script src="http://tramily.in/assets//vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="http://tramily.in/assets//vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="http://tramily.in/assets//vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="http://tramily.in/assets//vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="http://tramily.in/assets//vendor/isotope/jquery.isotope.min.js"></script>
<script src="http://tramily.in/assets//vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="http://tramily.in/assets//vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="http://tramily.in/assets//vendor/vide/vide.min.js"></script>
<script src="http://tramily.in/assets//vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="http://tramily.in/assets//js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="http://tramily.in/assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="http://tramily.in/assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Demo -->
<script src="http://tramily.in/assets//js/demos/demo-hotel.js"></script>

<!-- Theme Custom -->
<script src="http://tramily.in/assets//js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="http://tramily.in/assets//js/theme.init.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY"></script>
<script>

    // Map Markers
    var mapMarkers = [{
        address: "New York, NY 10017",
        icon: {
            image: "img/pin.png",
            iconsize: [26, 46],
            iconanchor: [12, 46]
        }
    }];

    // Map Initial Location
    var initLatitude = 40.75198;
    var initLongitude = -73.96978;

    $('#googleMapsMacro').gMap({
        controls: {
            draggable: true,
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: true,
            streetViewControl: true,
            overviewMapControl: true
        },
        scrollwheel: false,
        markers: mapMarkers,
        latitude: initLatitude,
        longitude: initLongitude,
        zoom: 13
    });

    $('#googleMapsMicro').gMap({
        controls: {
            draggable: false,
            panControl: false,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false
        },
        scrollwheel: false,
        markers: mapMarkers,
        latitude: initLatitude,
        longitude: initLongitude,
        zoom: 13
    });

    var mapRef = $('#googleMapsMacro, #googleMapsMicro').data('gMap.reference');

    // Styles from https://snazzymaps.com/
    var styles = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}];

    var styledMap = new google.maps.StyledMapType(styles, {
        name: 'Styled Map'
    });

    mapRef.mapTypes.set('map_style', styledMap);
    mapRef.setMapTypeId('map_style');

</script>


</body>
</html>