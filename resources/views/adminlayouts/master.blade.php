<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
    @include('layouts.head')
<!-- end::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	@yield('content')

	@include('layouts.footer') 
	
</body>

</html>