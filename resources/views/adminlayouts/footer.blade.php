        <!--begin::Base Scripts -->
        <script src="{{ asset('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
        <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/jquery-clockpicker.js')}}" type="text/javascript"></script>

        <!--end::Base Scripts -->
        <!--begin::Page Snippets -->
        @yield('bottom_script')
        <!--end::Page Snippets -->        