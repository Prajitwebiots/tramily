<!DOCTYPE html><!-- 
<html lang="en" >
	<!-- begin::Head -->
    @include('adminlayouts.dashHead')
	<!-- end::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			@yield('content')
		</div>
	
		@yield('bottom_script')
	</body>

</html>