@extends('adminlayouts.dashMaster')
@section('title')Admin | Flight Source @endsection
@section('style')
<style type="text/css">
.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
@endsection
@section('content')
<meta name="csrf_token"  content="{{ csrf_token() }}" />
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Flight Source
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Flight Source
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        @if(session('error'))<div class="alert alert-danger">{{ session('error') }}</div>@endif
        @if(session('success'))<div class="alert alert-success">{{ session('success') }}</div>@endif
        <div class="m-portlet m-portlet--mobile">
           
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                     <a href="{{route('admin-flightSource.create')}}" class="btn m-btn--pill    btn-primary" >Add New</a>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                             </div>
                        </div>
                
                <!--begin: Datatable -->
                <table class="m-datatable" id="html_table" width="100%">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            
                            <th>Flight Source</th>
                            <th>Status</th>
                            <th>Actions</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($flight as $flights)
                        
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{$flights->flight_source}}</td>
                            <td>
                                @if($flights->status == 0)
                                {{'<span><span class="m-badge  m-badge--danger m-badge--wide">Deactivated</span></span>'}}
                                @else
                                {{'<span><span class="m-badge  m-badge--primary m-badge--wide">Activated</span></span>'}}
                                @endif
                            </td>
                            <td>
                                 {{'<a href="'.route('admin-flightSource.edit',$flights->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit "><i class="la la-edit"></i></a>'}}
<<<<<<< HEAD
                                 {{'<a href="'.url('flightSourceIsDelete').'/'.$flights->id.'/0" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete "  onclick="return deletemsg('.$flights->id.');"><i class="la la-trash-o"></i></a>'}}
=======
                                 {{'<a href="'.url('flightSourceIsDelete').'/'.$flights->id.'/0" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete " ><i class="la la-trash-o"></i></a>'}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                            </td>
                            <td>
                               
                                @if($flights->status == 0)
                                {{'<a href="'.url('flightSourceStatus').'/'.$flights->id.'/1" class="btn m-btn--pill    btn-primary" >Active</a>'}}
                                @else
                                {{'<a href="'.url('flightSourceStatus').'/'.$flights->id.'/0" class="btn m-btn--pill    btn-danger" >Block</a>'}}
                                @endif
                            </td>
                            
                        </tr>
                        
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>

<script>
    function deletemsg(id) {
        swal({
            title: 'Are you sure?',
            text: "Are you sure you wish to delete ",
            showCancelButton: true,
            confirmButtonColor: '#34bfa3',
            cancelButtonColor: '#f4516c',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if(result.value)
        {
            {{--alert("{{url('cityIsDelete')}}/"+id);--}}
                window.location= "{{url('flightSourceIsDelete')}}/"+id+'/0';
            return true;
        }
    else
        {return false;
        }
    }
    );
        return false;
    }

</script>
@endsection