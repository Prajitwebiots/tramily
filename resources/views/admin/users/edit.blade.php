@extends('adminlayouts.dashMaster')
@section('title') Admin |  Edit Users @endsection
@section('style')
    <style>
        '<span class="m--font-danger"> *</span>'{
            color: red;
        }
    </style>
@endsection
@section('content')
<meta name="csrf_token"  content="{{ csrf_token() }}" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Edit User
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Edit User
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        
        <div class="row">
            <div class="col-md-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                  
                    <form class="m-form m-form--fit m-form--label-align-right" action="{{url('updateUserProfile',$user->id)}}" method="post" >
                        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                        @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div> <br>@endif
                        @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div> <br>@endif
                        <div class="m-portlet__body">
                            <div class="row">
                                <div class="col-md-6">
                                    
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Agency Name <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="{{$user->agency_name}}" name="agency_name"  >
                                        @if ($errors->has('agency_name'))
                                        <span class="help-block text-danger">
                                            <strong>{{ $errors->first('agency_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>

                                    <div class="form-group m-form__group ">
                                        <label for="example-text-input" class="col-form-label">
                                            Concerned Person Name <span class="m--font-danger"> *</span>
                                        </label>
                                        <div class="row">
                                        <div class="col-3">
                                            <select class="form-control m-select  " id="" name="title_name">
                                                <option @if('' == $user->title_name) selected @endif value="">TITLE <span class="m--font-danger"><span class="m--font-danger"> *</span></span></option>
                                                <option  @if("Mr."== $user->title_name) selected @endif value="Mr.">Mr.</option>
                                                <option @if( 'Ms.' == $user->title_name) selected @endif value="Ms.">Ms.</option>
                                                <option @if('Mrs.' == $user->title_name) selected @endif value="Mrs.">Mrs.</option>
                                            </select>
                                        </div>
                                        <div class="col-3">
                                            <input class="form-control m-input" type="text" value="{{ $user->first_name }}" placeholder="First name " name="first_name" autocomplete="off">
                                        </div>
                                        <div class="col-3">
                                            <input class="form-control m-input" type="text" value="{{ $user->last_name }}" placeholder="Last name " name="last_name" autocomplete="off">
                                        </div>
                                        @if ($errors->has('title_name'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('title_name') }}</strong>
                                                </span>
                                        @endif
                                        @if ($errors->has('first_name'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                </span>
                                        @endif
                                        @if ($errors->has('last_name'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                        </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Agency Address <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <textarea class="form-control"  id="m_autosize_1" name="agency_address"  placeholder="Agency Address" >{{$user->agency_address}}</textarea>
                                        @if ($errors->has('agency_address'))
                                        <span class="help-block text-danger">
                                            <strong>{{ $errors->first('agency_address') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Mobile No <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="{{$user->mobile}}" name="mobile"  >
                                        @if ($errors->has('mobile'))
                                        <span class="help-block text-danger">
                                            <strong>{{ $errors->first('mobile') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group   m-form__group">
                                        <label for="example-text-input" >
                                            PAN no
                                        </label>

                                        <input class="form-control m-input" type="text" value="{{ $user->pan_no }}" placeholder="PAN No" name="pan_no" autocomplete="off">

                                        @if ($errors->has('pan_no'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('pan_no') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    
                                    
                                    
                                    
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group m-form__group ">
                                        <label for="example-text-input"  >
                                            Country <span class="m--font-danger"> *</span>
                                        </label>
                                        <div class="col-8">
                                            <select class="form-control m-select  " id="country" name="country">

                                                @foreach($countrylist as $country)
                                                    <option @if($user->country == $country->id ) selected @endif value="{{$country->id}}">{{$country->country_name .' / '.$country->country_alphacode }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        @if ($errors->has('country'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('country') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group ">
                                        <label for="example-text-input"  >
                                            State <span class="m--font-danger"> *</span>
                                        </label>
                                        <div class="col-8">
                                            <select class="form-control m-select  " id="state" name="state">

                                                @foreach($statelist as $state)
                                                    <option @if($user->state == $state->id ) selected  @endif  value="{{$state->id}}">{{$state->state_name }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        @if ($errors->has('state'))
                                            <span class="help-block text-danger">
<<<<<<< HEAD
                                                    <strong>{{ $errors->first('state') }}</strong>
                                                </span>
=======
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group ">
                                        <label for="example-text-input"  >
                                            City <span class="m--font-danger"> *</span>
                                        </label>
<<<<<<< HEAD
                                        <div class="col-8">
                                            <select class="form-control m-select  " id="city" name="city">

                                                @foreach($citylist as $city)
                                                    <option  @if($city->id == $user->city)  selected  @endif  value="{{$city->id}}">{{$city->city_name }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        @if ($errors->has('city'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
=======
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="{{$user->country}}" name="country"  >
                                        @if ($errors->has('country'))
                                            <span class="help-block text-danger">
                                                <strong>{{ $errors->first('country') }}</strong>
                                            </span>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Pincode <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="{{$user->pincode}}" name="pincode"  >
                                        @if ($errors->has('pincode'))
                                            <span class="help-block text-danger">
                                                <strong>{{ $errors->first('pincode') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Office No <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="{{$user->office_no}}" name="office_no"  >
                                        @if ($errors->has('office_no'))
                                        <span class="help-block text-danger">
                                            <strong>{{ $errors->first('office_no') }}</strong>
                                        </span>
                                        @endif
                                    </div>


                                </div>
                                
                            </div>
                            <div class="m--padding-15"></div><strong style="font-weight: 600;margin-left: 28px;">Agency GST Details</strong><div class="m--padding-15"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            Name <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="{{$user->gst_name}}" name="gst_full_name"  >
                                        @if ($errors->has('gst_full_name'))
                                        <span class="help-block text-danger">
                                            <strong>{{ $errors->first('gst_full_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            State <span class="m--font-danger"> *</span>
                                        </label>

                                        <select class="form-control m-select  " id="state" name="gst_state">
                                            <option value="">State <span class="m--font-danger"><span class="m--font-danger"> *</span></span></option>
                                            @foreach($statelist as $state)
                                                <option  @if( $state->id == $user->gst_state)  selected @endif value="{{$state->id}}">{{$state->state_name }}</option>
                                            @endforeach

                                        </select>
                                        @if ($errors->has('gst_state'))
                                        <span class="help-block text-danger">
                                            <strong>{{ $errors->first('gst_state') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputEmail1">
                                            GSTIN No <span class="m--font-danger"> *</span>
                                        </label>
                                        
                                        <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="{{$user->gst_no}}" name="gst_no"  >
                                        @if ($errors->has('gst_no'))
                                        <span class="help-block text-danger">
                                            <strong>{{ $errors->first('gst_no') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            GST Address
                                        </label>
                                        <div class="col-8">
                                            <textarea class="form-control m-input m-input--solid" autocomplete="off"  id="exampleTextarea" name="gst_address" autocomplete="off" placeholder="GST Address" rows="3">{{$user->gst_address}}</textarea>
                                        </div>
                                        @if ($errors->has('agency_address'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('agency_address') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label" >
                                            Phone Number
                                        </label>
                                        <div class="col-8">
                                            <input class="form-control m-input" name="gst_phone" autocomplete="off" type="text" value="{{$user->gst_phone}}">
                                        </div>
                                        @if ($errors->has('gst_phone'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('gst_phone') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label" >
                                            Registeres email id
                                        </label>
                                        <div class="col-8">
                                            <input class="form-control m-input" name="regi_gmail" autocomplete="off" type="text" value="{{$user->regi_gmail}}">
                                        </div>
                                        @if ($errors->has('regi_gmail'))
                                            <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('regi_gmail') }}</strong>
                                                </span>
                                        @endif
                                    </div></div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button type="submit" class="btn btn-primary">
                                Submit
                                </button>
                                <a href="{{ url('admin/users') }}" class="btn btn-secondary">
                            Cancel
                            </a>
                                
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                
                
                
            </div>
            
            <!--end::Portlet-->
            
        </div>
    </div>
</div>
@endsection
@section('bottom_script')
    <script src="{{asset('assets/demo/default/custom/components/forms/widgets/autosize.js')}}" type="text/javascript"></script>

@endsection