@extends('adminlayouts.dashMaster')
@section('title') Admin Dashboard @endsection
@section('style')

@endsection
@section('content')
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">
			Dashboard
			</h3>
		</div>
	</div>
</div>
<!-- END: Subheader -->
<div class="m-content">
	<!--Begin::Main Portlet-->
	
	<!--End::Main Portlet-->
	<!--Begin::Main Portlet-->
	
	<!--End::Main Portlet-->
</div>
@endsection
@section('bottom_script')
<!--begin::Page Vendors -->
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
@endsection