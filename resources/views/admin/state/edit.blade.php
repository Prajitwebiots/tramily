@extends('adminlayouts.dashMaster')
@section('title') Admin | Edit State @endsection
@section('style')
@endsection
@section('content')
<meta name="csrf_token"  content="{{ csrf_token() }}" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Edit State<span class="m--font-danger"> *</span>

                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Edit  State<span class="m--font-danger"> *</span>

                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        @if (count($errors))
        <div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            <ul>
                @foreach($errors->all() as $error)
                
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                State
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    {{ Form::model($state, array('route' => array('admin-state.update', $state->id), 'method' => 'PUT' , 'class' => 'm-form m-form--fit m-form--label-align-right' )) }}
                    
                    <div class="m-portlet__body">
                        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Country Name
                            </label>
                            <select class="form-control m-input" id="country" name="country_id">
                                <option value="">Select Country</option>
                                @foreach($country as $countries)
                                <option value="{{ $countries->id }}" @if($countries->id == $state->country_id) selected @endif>{{ $countries->country_name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('country_id'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('country_id') }}</strong>
                                                </span>
                                            @endif 
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                State Name
                            </label>
                            <input type="text" class="form-control m-input" id="exampleInputText" name="state_name"  value="{{$state->state_name}} " placeholder="Enter State Name">
                            @if ($errors->has('state_name'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('state_name') }}</strong>
                                                </span>
                                            @endif 
                        </div>
                        
                        
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-primary">
                            Submit
                            </button>
                            <button type="reset" class="btn btn-secondary">
                            Cancel
                            </button>
                        </div>
                    </div>
                    {!! Form::close(); !!}
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                
                
                
            </div>
            
            <!--end::Portlet-->
            
        </div>
    </div>
</div>
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
@endsection