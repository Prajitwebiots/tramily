@extends('adminlayouts.dashMaster')
@section('title')Admin | State @endsection
@section('style')
@endsection
@section('content')
<meta name="csrf_token"  content="{{ csrf_token() }}" />
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                State
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                State
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <br>
        @if(session('error'))<div class="alert alert-danger">{{ session('error') }}</div>@endif
        @if(session('success'))<div class="alert alert-success">{{ session('success') }}</div>@endif
        <br>
        <div class="m-portlet m-portlet--mobile">
            
            <div class="m-portlet__body">
                 <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                     <a href="{{route('admin-state.create')}}" class="btn m-btn--pill    btn-primary" >Add New</a>
                        </h3>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                             </div>
                        </div>
                
                <!--begin: Datatable -->
                <table class="m-datatable" id="html_table" width="100%">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            
                            <th>Country Name</th>
                            <th>State Name</th>
                            <th>Status</th>
                            <th>Actions</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($state as $states)          
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $states->country->country_name}}</td>
                            <td>{{$states->state_name}}</td>
                            <td>
                                @if($states->status == 0)
                                {{'<span><span class="m-badge  m-badge--danger m-badge--wide">Deactivated</span></span>'}}
                                @else
                                {{'<span><span class="m-badge  m-badge--primary m-badge--wide">Activated</span></span>'}}
                                @endif
                            </td>
                            {{--<td>--}}
                                {{--{{'<a href="'.route('admin-state.edit',$states->id).'" class="btn m-btn--pill    btn-primary" >Edit</a>'}}--}}
                                {{--@if($states->status == 0)--}}
                                {{--{{'<a href="'.url('stateStatus').'/'.$states->id.'/1" class="btn m-btn--pill    btn-primary" >Active</a>'}}--}}
                                {{--@else--}}
                                {{--{{'<a href="'.url('stateStatus').'/'.$states->id.'/0" class="btn m-btn--pill    btn-danger" >Block</a>'}}--}}
                                {{--@endif--}}
                            {{--</td>--}}
                            


                        <td>
                            {{'<a href="'.route('admin-state.edit',$states->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit "><i class="la la-edit"></i></a>'}}
                            {{'<a href="'.url('stateIsDelete').'/'.$states->id.'/0" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete " onclick="return deletemsg('.$states->id.');" ><i class="la la-trash-o"></i></a>'}}
                        </td>
                        <td>
                            @if($states->status == 0)
                                {{'<a href="'.url('stateStatus').'/'.$states->id.'/1" class="btn m-btn--pill    btn-primary" >Active</a>'}}
                            @else
                                {{'<a href="'.url('stateStatus').'/'.$states->id.'/0" class="btn m-btn--pill    btn-danger" >Block</a>'}}
                            @endif
                        </td>
                        </tr>

                        
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
<script>
    function deletemsg(id) {
        swal({
            title: 'Are you sure?',
            text: "Are you sure you wish to delete ",
            showCancelButton: true,
            confirmButtonColor: '#34bfa3',
            cancelButtonColor: '#f4516c',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if(result.value)
        {
            {{--alert("{{url('cityIsDelete')}}/"+id);--}}
                window.location= "{{url('stateIsDelete')}}/"+id+'/0';
            return true;
        }
    else
        {return false;
        }
    }
    );
        return false;
    }

</script>
@endsection