@extends('layouts.dashMaster')
@section('title') Supplier |  Round trip Flight @endsection
@section('style')
    <style type="text/css">
        .btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show > .btn.m-btn--hover-accent.dropdown-toggle {
            border-color: #5767de !important;
            color: #fff !important;
            background-color: #5767de !important;
        }
    </style>
@endsection
@section('content')
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver	m-container m-container--responsive m-container--xxl m-page__container">
    <meta name="csrf_token" content="{{ csrf_token() }}"/>
    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Round trip import
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Round trip import
                            </span>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            @if(session('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>@endif
            @if(session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>@endif
            <div class="m-portlet m-portlet--mobile">
                <!--begin::Form-->
                <form action="{{url('round_trip_import')}}" method="post"
                      class="m-form m-form--fit m-form--label-align-right row" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group m-form__group col-md-12 m--align-center ">
                        <a  href="{{url('assets/file/flightRoundTrips.csv')}}">
                            	Click here to download sample csv file.
                        </a>
                        <br>
                        <br>
                        <h5 for="exampleInputEmail1">
                            File
                        </h5>
                        <div></div>
                        <input type="file" name="file">

                        @if ($errors->has('file'))
                            <span class="help-block text-danger">
									<br>
									<strong>{{ $errors->first('file') }}</strong>
								</span>
                        @endif
                    </div>
                    <div class="form-group m-form__group col-md-12 m--align-center m--padding-top-40">

                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </form>
                <!--end::Form-->
                <div class="m--padding-20">

                @if(count($data) >= 1)
                    <!--begin: Datatable -->
                        <table class="m-datatable " id="html_table" width="100%">
                            <thead>
                            <tr>
                                <th>sr.no</th>
                                <th>flight_name</th>
                                <th>flight_number</th>
                                <th>flight_source</th>
                                <th>flight_destination</th>
                                <th>flight_departure_date</th>
                                <th>flight_departure_time</th>
                                <th>flight_arrival_time</th>
                                <th>return_flight_source</th>
                                <th>return_flight_destination</th>
                                <th>return_flight_name</th>
                                <th>return_flight_number</th>
                                <th>return_departure_date</th>
                                <th>return_departure_time</th>
                                <th>return_arrival_time</th>
                                <th>flight_price</th>
                                <th>flight_pnr_no</th>
                                <th>flight_pnr_no2</th>
                                <th>flight_via</th>
                                <th>sold</th>
                                <th>seat</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1;$city[] = ""; ?>
                            @foreach($data as $list)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{$list['flight_name'] }}</td>
                                    <td>{{$list['flight_number'] }}</td>
                                    <td>{{$list['flight_source'] }}</td>
                                    <td>{{$list['flight_destination'] }}</td>
                                    <td>{{$list['flight_departure_date'] }}</td>
                                    <td>{{$list['flight_departure_time'] }}</td>
                                    <td>{{$list['flight_arrival_time'] }}</td>
                                    <td>{{$list['return_flight_source'] }}</td>
                                    <td>{{$list['return_flight_destination'] }}</td>
                                    <td>{{$list['return_flight_name'] }}</td>
                                    <td>{{$list['return_flight_number'] }}</td>
                                    <td>{{$list['return_departure_date'] }}</td>
                                    <td>{{$list['return_departure_time'] }}</td>
                                    <td>{{$list['return_arrival_time'] }}</td>
                                    <td>{{$list['flight_price'] }}</td>
                                    <td>{{$list['flight_pnr_no'] }}</td>
                                    <td>{{$list['flight_pnr_no2'] }}</td>
                                    <td>{{$list['flight_via'] }}</td>
                                    <td>{{$list['sold'] }}</td>
                                    <td>{{$list['seat'] }}</td>
                                </tr>
                             @endforeach
                            </tbody>
                        </table>
                        <!--end: Datatable -->

                        <form action="{{url('round_trip_save')}}" method="post"
                              class="m-form m-form--fit m-form--label-align-right row" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group m-form__group col-md-12 m--align-center m--padding-bottom-50">
                                <input type="hidden" name="onewayfile" value="{{$name}}">
                                <button type="submit" class="btn btn-primary">
                                    Save data
                                </button>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
        </div>
    </div>

@endsection
@section('bottom_script')
    <script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
@endsection