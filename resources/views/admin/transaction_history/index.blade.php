@extends('adminlayouts.dashMaster')
@section('title')Admin | Transaction history @endsection
@section('style')
<style type="text/css">
.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
@endsection
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Transaction history
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Transaction history
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        @if(session('error'))<div class="alert alert-danger">{{ session('error') }}</div>@endif
        @if(session('success'))<div class="alert alert-success">{{ session('success') }}</div>@endif
        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__body">
                 <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
<<<<<<< HEAD
                                                <input   type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
=======
                                                <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                             </div>
                        </div>

                <!--begin: Datatable -->
                <table class="m-datatable" id="html_table" width="100%">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>User name </th>
                            <th>Admin name</th>
                            <th>Amount</th>
                            <th>Add/Delete</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($history as $list)
                        <tr>
                            <td>{{ $loop->iteration}}</td>
<<<<<<< HEAD
                            <td>{{@$list->userinfo->title_name.' '.@$list->userinfo->first_name.' '.@$list->userinfo->last_name }}</td>
=======
                            <td>{{$list->userinfo->first_name}}</td>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                            <td>{{$list->admininfo->first_name}}</td>
                            <td>{{$list->amount}}</td>
                            <td>
                                @if($list->status == 0)
                                    {{'<span><span class="m-badge  m-badge--primary m-badge--wide">Add</span></span>'}}
                                @else
                                    {{'<span><span class="m-badge  m-badge--danger m-badge--wide">Delete</span></span>'}}
                                @endif
                            </td>
                            <td>{{date_format($list->created_at,'d-M-Y D h:i ')}}</td>
<<<<<<< HEAD
                            <td> @if($list->status == 0 && $list->is_deleted == 0 )
                                    {{'<a onclick="return deletemsg('.$list->id.');"  href="'.url('delete_balance').'/'.$list->id.'" class="btn m-btn--pill    btn-danger" >delete</a>'}}
                                     @else
                                    {{'<a onclick="return returnmsg();"     class="btn m-btn--pill btn-info" >delete </a>'}}
                                  @endif</td>
=======
                            <td> @if($list->status == 0 && $list->is_deleted == 0 ){{'<a href="'.url('delete_balance').'/'.$list->id.'" class="btn m-btn--pill    btn-danger" >delete</a>'}}@endif</td>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                        </tr>

                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>


</div>


@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
<<<<<<< HEAD
<script>
    function deletemsg(id) {
        swal({
            title: 'Are you sure?',
            text: "Are you sure you wish to give balance to supplier ",
            showCancelButton: true,
            confirmButtonColor: '#34bfa3',
            cancelButtonColor: '#f4516c',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if(result.value)
        {
            {{--alert("{{url('cityIsDelete')}}/"+id);--}}
                window.location= "{{url('delete_balance')}}/"+id;
            return true;
        }
    else
        {return false;
        }
    }
    );
        return false;
    }

    function returnmsg(){
        swal({
                  type: 'warning',
                  title: 'Oops...',
                  text: 'Balance already given back to supplier ',

              })
        return false;
    }

</script>
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

@endsection