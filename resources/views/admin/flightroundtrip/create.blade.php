@extends('adminlayouts.dashMaster')
@section('title') Admin | Add Round Trip Flight @endsection
@section('style')
@endsection
@section('content')
<meta name="csrf_token"  content="{{ csrf_token() }}" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
             Add Round Trip Flight
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                               Round Trip Flight
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
		<div class="m-content">
			
			@if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div>   <br>@endif
			@if(session('success'))   <br><div class="alert alert-success">{{ session('success') }}</div>   <br>@endif
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tab">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<span class="m-portlet__head-icon m--hide">
										<i class="la la-gear"></i>
									</span>
									<h3 class="m-portlet__head-text">
									Add Flight
									</h3>
								</div>
							</div>
						</div>
						<!--begin::Form-->
						<form class="m-form m-form--fit m-form--label-align-right" action="{{url('storeRoundTripFlight')}}" method="post" >
							<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
							@if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div> <br>@endif
							@if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div> <br>@endif
							<div class="m-portlet__body">

								<strong style="font-weight: 600;margin-left: 28px;">Flight Details </strong>
								<div class="m--padding-15"></div>
								<div class="row">
									<div class="col-md-6">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Flight Source <span class="m--font-danger"> *</span>
=======
												Flight Source
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="source" name="source">
												<option value="">Select Flight Source</option>
												@foreach($flightSource as $flightSources)
<<<<<<< HEAD
												<option @if($flightSources->id == old('source'))  selected  @endif value="{{ $flightSources->id }}">{{ $flightSources->flight_source}}</option>
=======
												<option value="{{ $flightSources->id }}">{{ $flightSources->flight_source}}</option>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												@endforeach
											</select>
											
											@if ($errors->has('source'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('source') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Flight Destination <span class="m--font-danger"> *</span>
=======
												Flight Destination
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="destination" name="destination">
												<option value="">Select Flight Destination</option>
												@foreach($flightDestination as $flightDestinations)
<<<<<<< HEAD
												<option @if($flightDestinations->id == old('destination'))  selected  @endif value="{{ $flightDestinations->id }}">{{ $flightDestinations->flight_destination}}</option>
=======
												<option value="{{ $flightDestinations->id }}">{{ $flightDestinations->flight_destination}}</option>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												@endforeach
											</select>
											
											@if ($errors->has('destination'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('destination') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Flight Name <span class="m--font-danger"> *</span>
=======
												Flight Name
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="flight" name="flight_name">
												<option value="">Select Flight</option>
												@foreach($flight as $flights)
<<<<<<< HEAD
												<option  @if($flights->id == old('flight_name'))  selected  @endif value="{{ $flights->id }}">{{ $flights->flight_name}}</option>
=======
												<option value="{{ $flights->id }}">{{ $flights->flight_name}}</option>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												@endforeach
											</select>
											
											@if ($errors->has('flight_name'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('flight_name') }}</strong>
											</span>
											@endif
										</div>
										
										
									</div>
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Flight Number <span class="m--font-danger"> *</span>
=======
												Flight Number
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											
											<input type="text" class="form-control m-input" value="{{ old('flight_number') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Flight Number" name="flight_number"  >
											@if ($errors->has('flight_number'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('flight_number') }}</strong>
											</span>
											@endif
										</div>
										
										
									</div>
								<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Pnr No
											</label>
											
											<input type="text" class="form-control m-input" value="{{ old('pnr_no') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Pnr No" name="pnr_no"  >
											@if ($errors->has('pnr_no'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('pnr_no') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Departure Date <span class="m--font-danger"> *</span>
											</label>
											
											<input type="text" class="form-control m-input" value="{{ old('departure_date') }}" id="m_datepicker_1" name="departure_date" readonly="" placeholder="Select date">
=======
												Departure Date
											</label>
											
											<input type="text" class="form-control m-input" id="m_datepicker_1" name="departure_date" readonly="" placeholder="Select date">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											@if ($errors->has('departure_date'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('departure_date') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Departure Time <span class="m--font-danger"> *</span>
											</label>
											<div class="input-group timepicker" >
												<input type="text" class="form-control m-input" value="{{ old('departure_time') }}" id="m_time_1" readonly="" name="departure_time" placeholder="Select time">
=======
												Departure Time
											</label>
											<div class="input-group timepicker" >
												<input type="text" class="form-control m-input" id="m_timepicker_2" readonly="" name="departure_time" placeholder="Select time">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											@if ($errors->has('departure_time'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('departure_time') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Arrival Time <span class="m--font-danger"> *</span>
											</label>
											<div class="input-group timepicker" id="">
												<input type="text" class="form-control m-input"  value="{{ old('arrival_time') }}" id="m_time_2" readonly="" name="arrival_time" placeholder="Select time">
=======
												Arrival Time
											</label>
											<div class="input-group timepicker" id="">
												<input type="text" class="form-control m-input"  id="m_timepicker_2" readonly="" name="arrival_time" placeholder="Select time">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											@if ($errors->has('arrival_time'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('arrival_time') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div>
																<div class="m--padding-20"></div>
								<strong style="font-weight: 600;margin-left: 28px;">Flight Details </strong>
								<div class="m--padding-15"></div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Return Flight Name <span class="m--font-danger"> *</span>
=======
												Return Flight Name
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="flight" name="return_flight_name">
												<option value="">Select Flight</option>
												@foreach($flight as $flights)
<<<<<<< HEAD
												<option @if($flights->id == old('return_flight_name'))  selected  @endif value="{{ $flights->id }}">{{ $flights->flight_name}}</option>
=======
												<option value="{{ $flights->id }}">{{ $flights->flight_name}}</option>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												@endforeach
											</select>
											
											@if ($errors->has('return_flight_name'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('return_flight_name') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Return Flight Number <span class="m--font-danger"> *</span>
=======
												Return Flight Number
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											
											<input type="text" class="form-control m-input" value="{{ old('return_flight_number') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Flight Number" name="return_flight_number"  >
											@if ($errors->has('return_flight_number'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('return_flight_number') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Pnr No
											</label>
											
											<input type="text" class="form-control m-input" value="{{ old('pnr_no2') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Pnr No" name="pnr_no2"  >
											@if ($errors->has('pnr_no2'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('pnr_no2') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Return Date <span class="m--font-danger"> *</span>
											</label>
											
											<input type="text" class="form-control m-input" value="{{ old('return_departure_date') }}" id="m_datepicker_2" name="return_departure_date" readonly="" placeholder="Select date">
=======
												Return Date
											</label>
											
											<input type="text" class="form-control m-input" id="m_datepicker_2" name="return_departure_date" readonly="" placeholder="Select date">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											@if ($errors->has('return_departure_date'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('return_departure_date') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Departure Time <span class="m--font-danger"> *</span>
											</label>
											<div class="input-group timepicker" >
												<input type="text" class="form-control m-input" value="{{ old('return_departure_time') }}" id="m_time_3" readonly="" name="return_departure_time" placeholder="Select time">
=======
												Departure Time
											</label>
											<div class="input-group timepicker" >
												<input type="text" class="form-control m-input" id="m_timepicker_2" readonly="" name="return_departure_time" placeholder="Select time">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											@if ($errors->has('return_departure_time'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('return_departure_time') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Arrival Time <span class="m--font-danger"> *</span>
											</label>
											<div class="input-group timepicker" id="">
												<input type="text" class="form-control m-input" value="{{ old('return_arrival_time') }}"  id="m_time_4" readonly="" name="return_arrival_time" placeholder="Select time">
=======
												Arrival Time
											</label>
											<div class="input-group timepicker" id="">
												<input type="text" class="form-control m-input"  id="m_timepicker_2" readonly="" name="return_arrival_time" placeholder="Select time">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											@if ($errors->has('return_arrival_time'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('return_arrival_time') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
<<<<<<< HEAD
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Price <span class="m--font-danger"> *</span>
											</label>
											
											<input type="text" class="form-control m-input" value="{{ old('flight_price') }}"  autocomplete="off" id="exampleInputText" placeholder="Enter Price" name="flight_price"  >
=======
									<div class="col-md-6">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Price
											</label>
											
											<input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" placeholder="Enter Price" name="flight_price"  >
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											@if ($errors->has('flight_price'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('flight_price') }}</strong>
											</span>
											@endif
										</div>
									</div>
<<<<<<< HEAD
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Seat <span class="m--font-danger"> *</span>
=======
									<div class="col-md-6">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Seat
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											
											<input type="text" class="form-control m-input" value="{{ old('seat') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Seat" name="seat"  >
											@if ($errors->has('seat'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('seat') }}</strong>
											</span>
											@endif
										</div>
									</div>
<<<<<<< HEAD
									<div class="col-md-4">

										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Via
											</label>

											<input type="text" class="form-control m-input" value="{{ old('via') }}" autocomplete="off" id="exampleInputText" placeholder="Enter via" name="via"  >
											@if ($errors->has('via'))
												<span class="help-block text-danger">
												<strong>{{ $errors->first('via') }}</strong>
											</span>
											@endif
										</div>
									</div>
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
									
								</div>
							</div>
							<div class="m-portlet__foot m-portlet__foot--fit">
								<div class="m-form__actions">
									<button type="submit" class="btn btn-primary">
									Submit
									</button>
									<a href="{{ url('admin/roundTripFlights') }}" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
						</form>
						<!--end::Form-->
					</div>
					<!--end::Portlet-->
					
					
					
				</div>
				
				<!--end::Portlet-->
				
			</div>
		</div>
		<!--end:: Widgets/Stats-->
		
	</div>
<!-- end::Body -->
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js')}}" type="text/javascript"></script>
<script>
$('#m_datepicker_1').datepicker({
<<<<<<< HEAD
format: 'dd-mm-yyyy',
		todayHighlight:	true,
    autoclose:true,
startDate:new Date(),
});
$('#m_datepicker_2').datepicker({
format: 'dd-mm-yyyy',
		todayHighlight:	true,
    autoclose:true,
startDate:new Date(),
});
$('#m_time_1').clockpicker({
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    'default': 'now'
});
$('#m_time_2').clockpicker({
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    'default': 'now'
});
$('#m_time_3').clockpicker({
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    'default': 'now'
});
$('#m_time_4').clockpicker({
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    'default': 'now'
});
</script>
<script>// for round trip
    var start = new Date();
    var end = new Date(new Date().setYear(start.getFullYear()+1));


    $('#m_datepicker_1').datepicker({
        startDate : start,
        endDate   : end
    }).on('changeDate', function(){


//           $('#m_datepicker_3').datepicker('setStartDate',  $(this).val());
        $('#m_datepicker_2').datepicker('setStartDate', $(this).val());
    });

    $('#m_datepicker_2').datepicker({
        startDate : start,
        endDate   : end
    }).on('changeDate', function(){

        $('#m_datepicker_1').datepicker('setEndDate',  $(this).val());
    });


=======
format: 'yyyy-mm-dd',
		todayHighlight:	true,
startDate:new Date(),
});
$('#m_datepicker_2').datepicker({
format: 'yyyy-mm-dd',
		todayHighlight:	true,
startDate:new Date(),
});
</script>
<script type="text/javascript">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

</script>
<!--begin::Page Vendors -->
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->
@endsection