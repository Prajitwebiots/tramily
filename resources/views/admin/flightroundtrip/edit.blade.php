@extends('adminlayouts.dashMaster')
@section('title') Admin |  Edit Round Trip Flight @endsection
@section('style')
@endsection
@section('content')
<meta name="csrf_token"  content="{{ csrf_token() }}" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Edit Round Trip Flight
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Edit Flight
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
		<div class="m-content">
			
			@if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div>   <br>@endif
			@if(session('success'))   <br><div class="alert alert-success">{{ session('success') }}</div>   <br>@endif
			<div class="row">
				<div class="col-md-12">
					<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tab">
					
						    <form class="m-form m-form--fit m-form--label-align-right" action="{{url('updateRoundTripFlight',$flightroundtrip->id)}}" method="post" >
						<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
							@if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div> <br>@endif
							@if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div> <br>@endif
						<div class="m-portlet__body">
									<strong style="font-weight: 600;margin-left: 28px;">Flight Details </strong>
								<div class="m--padding-15"></div>
								<div class="row">
									<div class="col-md-6">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Flight Source <span class="m--font-danger"> *</span>
=======
												Flight Source
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="source" name="source">
												<option value="">Select Flight Source</option>
												@foreach($flightSource as $flightSources)
												<option value="{{ $flightSources->id }}" @if($flightSources->id == $flightroundtrip->flight_source) selected @endif>{{ $flightSources->flight_source}}</option>
												@endforeach
											</select>
											
											@if ($errors->has('source'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('source') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Flight Destination <span class="m--font-danger"> *</span>
=======
												Flight Destination
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="destination" name="destination">
												<option value="">Select Flight Destination</option>
												@foreach($flightDestination as $flightDestinations)
												<option value="{{ $flightDestinations->id }}" @if($flightDestinations->id == $flightroundtrip->flight_destination) selected @endif>{{ $flightDestinations->flight_destination}}</option>
												@endforeach
											</select>
											
											@if ($errors->has('destination'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('destination') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Flight Name <span class="m--font-danger"> *</span>
=======
												Flight Name
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="flight" name="flight_name">
												<option value="">Select Flight</option>
												@foreach($flight as $flights)
												<option value="{{ $flights->id }}" @if($flights->id == $flightroundtrip->flight_name) selected @endif>{{ $flights->flight_name}}</option>
												@endforeach
											</select>
											
											@if ($errors->has('flight_name'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('flight_name') }}</strong>
											</span>
											@endif
										</div>
										
										
									</div>
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Flight Number <span class="m--font-danger"> *</span>
=======
												Flight Number
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											
											<input type="text" class="form-control m-input" value="{{ $flightroundtrip->flight_number}}" value="{{ old('flight_number') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Flight Number" name="flight_number"  >
											@if ($errors->has('flight_number'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('flight_number') }}</strong>
											</span>
											@endif
										</div>
										
										
									</div>
								<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Pnr No
											</label>
											
											<input type="text" class="form-control m-input" value="{{ $flightroundtrip->flight_pnr_no}}" value="{{ old('pnr_no') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Pnr No" name="pnr_no"  >
											@if ($errors->has('pnr_no'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('pnr_no') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Departure Date <span class="m--font-danger"> *</span>
=======
												Departure Date
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											
											<input type="text" class="form-control m-input" value="{{ $flightroundtrip->flight_departure_date}}" id="m_datepicker_1" name="departure_date" readonly="" placeholder="Select date">
											@if ($errors->has('departure_date'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('departure_date') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Departure Time <span class="m--font-danger"> *</span>
											</label>
											<div class="input-group timepicker" >
												<input type="text" class="form-control m-input" value="{{ $flightroundtrip->flight_departure_time}}" id="m_time_1" readonly="" name="departure_time" placeholder="Select time">
=======
												Departure Time
											</label>
											<div class="input-group timepicker" >
												<input type="text" class="form-control m-input" value="{{ $flightroundtrip->flight_departure_time}}" id="m_timepicker_2" readonly="" name="departure_time" placeholder="Select time">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											@if ($errors->has('departure_time'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('departure_time') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Arrival Time <span class="m--font-danger"> *</span>
											</label>
											<div class="input-group timepicker" id="">
												<input type="text" class="form-control m-input" value="{{ $flightroundtrip->flight_arrival_time}}"  id="m_time_2" readonly="" name="arrival_time" placeholder="Select time">
=======
												Arrival Time
											</label>
											<div class="input-group timepicker" id="">
												<input type="text" class="form-control m-input" value="{{ $flightroundtrip->flight_arrival_time}}"  id="m_timepicker_2" readonly="" name="arrival_time" placeholder="Select time">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											@if ($errors->has('arrival_time'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('arrival_time') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div>
								<div class="m--padding-20"></div>
								<strong style="font-weight: 600;margin-left: 28px;">Return Flight Details </strong>
								<div class="m--padding-15"></div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Return Flight Name <span class="m--font-danger"> *</span>
=======
												Return Flight Name
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											<select class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="flight" name="return_flight_name">
												<option value="">Select Flight</option>
												@foreach($flight as $flights)
												<option value="{{ $flights->id }}" @if($flights->id == $flightroundtrip->return_flight_name) selected @endif>{{ $flights->flight_name}}</option>
												@endforeach
											</select>
											
											@if ($errors->has('return_flight_name'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('return_flight_name') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Return Flight Number <span class="m--font-danger"> *</span>
=======
												Return Flight Number
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											
											<input type="text" class="form-control m-input" value="{{ $flightroundtrip->return_flight_number}}" value="{{ old('return_flight_number') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Flight Number" name="return_flight_number"  >
											@if ($errors->has('return_flight_number'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('return_flight_number') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Pnr No
											</label>
											
											<input type="text" class="form-control m-input" value="{{ $flightroundtrip->flight_pnr_no2}}" value="{{ old('pnr_no2') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Pnr No" name="pnr_no2"  >
											@if ($errors->has('pnr_no2'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('pnr_no2') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Return Date <span class="m--font-danger"> *</span>
=======
												Return Date
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
											</label>
											
											<input type="text" class="form-control m-input" value="{{ $flightroundtrip->return_departure_date}}" id="m_datepicker_2" name="return_departure_date" readonly="" placeholder="Select date">
											@if ($errors->has('return_departure_date'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('return_departure_date') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Departure Time <span class="m--font-danger"> *</span>
											</label>
											<div class="input-group timepicker" >
												<input type="text" class="form-control m-input" value="{{ $flightroundtrip->return_departure_time}}" id="m_time_3" readonly="" name="return_departure_time" placeholder="Select time">
=======
												Departure Time
											</label>
											<div class="input-group timepicker" >
												<input type="text" class="form-control m-input" value="{{ $flightroundtrip->return_departure_time}}" id="m_timepicker_2" readonly="" name="return_departure_time" placeholder="Select time">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											@if ($errors->has('return_departure_time'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('return_departure_time') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
<<<<<<< HEAD
												Arrival Time <span class="m--font-danger"> *</span>
											</label>
											<div class="input-group timepicker" id="">
												<input type="text" class="form-control m-input"  value="{{ $flightroundtrip->return_arrival_time}}" id="m_time_4" readonly="" name="return_arrival_time" placeholder="Select time">
=======
												Arrival Time
											</label>
											<div class="input-group timepicker" id="">
												<input type="text" class="form-control m-input"  value="{{ $flightroundtrip->return_arrival_time}}" id="m_timepicker_2" readonly="" name="return_arrival_time" placeholder="Select time">
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
												<span class="input-group-addon">
													<i class="la la-clock-o"></i>
												</span>
											</div>
											@if ($errors->has('return_arrival_time'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('return_arrival_time') }}</strong>
											</span>
											@endif
										</div>
									</div>
								</div>
								<br>
								<br>
								<div class="row">
<<<<<<< HEAD
									<div class="col-md-4">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Supplier price/ Admin Price <span class="m--font-danger"> *</span>
											</label>

											<div class="row m--padding-left-10">
												<input type="text" class="form-control m-input  col-md-5 " disabled="" value="{{ $flightroundtrip->supplier_price}}" autocomplete="off" id="exampleInputText" placeholder="Enter Price" name=""  >
												<input type="text" class="form-control m-input col-md-5  " value="{{ $flightroundtrip->flight_price}}" autocomplete="off" id="exampleInputText" placeholder="Enter Price" name="flight_price"  >
												@if ($errors->has('flight_price'))
													<span class="help-block text-danger">
												<strong>{{ $errors->first('flight_price') }}</strong>
											</span>
												@endif
											</div>
										</div>
									</div>
									<div class="col-md-4">

										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Seat <span class="m--font-danger"> <span class="m--font-danger"> *</span></span> <code  id="addseat" class="m-link m-link--state m-link--warning m--margin-right-10 ">Add </code><code  id="changeminus" class="m-link m-link--state m-link--warning m--margin-right-10 ">Minus </code>
											</label>
											<br><code>Totle Seat / Sold seat /Available Seat  </code>

											<div class="row m--margin-top-10">
												<input type="text" class="form-control m-input col-md-2 m--margin-right-10" disabled="" value="{{ $flightroundtrip->seat+$flightroundtrip->sold}}" autocomplete="off" id="exampleInputText" >
												<input type="text" class="form-control m-input col-md-2 m--margin-right-10"  disabled="" value="{{$flightroundtrip->sold}}" autocomplete="off" id="newseat" placeholder="add new  Seat" name="seat">
												<input type="text" class="form-control m-input col-md-2 m--margin-right-10"  disabled="" value="{{$flightroundtrip->seat}}" autocomplete="off" id="newseat" placeholder="add new  Seat" >
												<i  id="sign" class="fa m--margin-right-5"></i>
												<input type="hidden" value="" id="seatcount" name="seatcount">
												<input type="text" class="form-control m-input col-md-2 m--margin-right-10" disabled=""  value="" autocomplete="off" name="available" id="totoleseat" >
											</div>

											@if ($errors->has('seat'))
												<span class="help-block text-danger">
												<strong>{{ $errors->first('seat') }}</strong>
											</span>
											@endif
											@if ($errors->has('available'))
												<span class="help-block text-danger">
												<strong>{{ $errors->first('available') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-4">

										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Via
											</label>

											<input type="text" class="form-control m-input" value="{{ $flightroundtrip->flight_via }}" autocomplete="off" id="exampleInputText" placeholder="Enter via" name="via"  >
											@if ($errors->has('via'))
												<span class="help-block text-danger">
												<strong>{{ $errors->first('via') }}</strong>
											</span>
											@endif
=======
									<div class="col-md-6">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Price
											</label>
											
											<input type="text" class="form-control m-input" value="{{ $flightroundtrip->flight_price}}" autocomplete="off" id="exampleInputText" placeholder="Enter Price" name="flight_price"  >
											@if ($errors->has('flight_price'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('flight_price') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="col-md-6">
										
										<div class="form-group m-form__group">
											<label for="exampleInputEmail1">
												Seat
											</label>
											
											<input type="text" class="form-control m-input"  value="{{ $flightroundtrip->seat}}" value="{{ old('seat') }}" autocomplete="off" id="exampleInputText" placeholder="Enter Seat" name="seat"  >
											@if ($errors->has('seat'))
											<span class="help-block text-danger">
												<strong>{{ $errors->first('seat') }}</strong>
											</span>
											@endif
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
										</div>
									</div>
									
								</div>
							</div>
							<div class="m-portlet__foot m-portlet__foot--fit">
								<div class="m-form__actions">
									<button type="submit" class="btn btn-primary">
									Update
									</button>
									<a href="{{ url('admin/roundTripFlights') }}" class="btn btn-secondary">Cancel</a>
								</div>
							</div>
					</form>
						<!--end::Form-->
					</div>
					<!--end::Portlet-->
					
					
					
				</div>
				
				<!--end::Portlet-->
				
			</div>
		</div>
		<!--end:: Widgets/Stats-->
		
	</div>
<!-- end::Body -->
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js')}}" type="text/javascript"></script>
<script>
$('#m_datepicker_1').datepicker({
<<<<<<< HEAD
format: 'dd-mm-yyyy',
=======
format: 'yyyy-mm-dd',
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
		todayHighlight:	true,
startDate:new Date(),
});
$('#m_datepicker_2').datepicker({
<<<<<<< HEAD
format: 'dd-mm-yyyy',
		todayHighlight:	true,
startDate:new Date(),
});
$('#m_time_1').clockpicker({
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    'default': 'now'
});
$('#m_time_2').clockpicker({
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    'default': 'now'
});
$('#m_time_3').clockpicker({
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    'default': 'now'
});
$('#m_time_4').clockpicker({
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    'default': 'now'
});
</script>
<script>// for round trip
    var start = new Date();
    var end = new Date(new Date().setYear(start.getFullYear()+1));


    $('#m_datepicker_1').datepicker({
        startDate : start,
        endDate   : end
    }).on('changeDate', function(){
//           $('#m_datepicker_3').datepicker('setStartDate',  $(this).val());
        $('#m_datepicker_2').datepicker('setStartDate', $(this).val());
    });

    $('#m_datepicker_2').datepicker({
        startDate : start,
        endDate   : end
    }).on('changeDate', function(){

        $('#m_datepicker_1').datepicker('setEndDate',  $(this).val());
    });



</script>
<script>// for round trip
    var start = new Date();
    var end = new Date(new Date().setYear(start.getFullYear()+1));


    $('#m_datepicker_1').datepicker({
        startDate : start,
        endDate   : end
    }).on('changeDate', function(){


//           $('#m_datepicker_3').datepicker('setStartDate',  $(this).val());
        $('#m_datepicker_2').datepicker('setStartDate', $(this).val());
    });

    $('#m_datepicker_2').datepicker({
        startDate : start,
        endDate   : end
    }).on('changeDate', function(){

        $('#m_datepicker_1').datepicker('setEndDate',  $(this).val());
    });
    //        available seat  change
    $('#addseat').on('click', function() {
//            $('#totoleseat').prop('disabled', true);
        $('#totoleseat').prop('disabled', function(i, v) { return !v; });
        $('#seatcount').val("add");
        $('#sign').removeClass("fa-minus");
        $('#sign').addClass("fa-plus");
    });
    $('#changeminus').on('click', function() {
//            $('#totoleseat').prop('disabled', true);
        $('#totoleseat').prop('disabled', function(i, v) { return !v; });
        $('#seatcount').val("minus");
        $('#sign').removeClass("fa-plus");
        $('#sign').addClass("fa-minus");
    });
    //        price change
    $('#pricechange').on('click', function() {
//            $('#totoleseat').prop('disabled', true);
        $('#supplier_price').prop('disabled', function(i, v) { return !v; });
    });
    $('#m_datepicker_time_1').on('click', function() {
        $('#m_datepicker_time_1').val('');

    });


=======
format: 'yyyy-mm-dd',
		todayHighlight:	true,
startDate:new Date(),
});
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
</script>
<!--begin::Page Vendors -->
<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->

@endsection