@extends('adminlayouts.loginMaster')
@section('title') Tramily | Admin Login @endsection
@section('style')

@endsection
@section('content')
	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url(../assets//images/bg-2.jpg);">

	<div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
		<div class="m-login__container">
			<div class="m-login__logo">
				<a href="#">
					<img src="{{asset('assets/app/media/img/logos/logo-2.png')}}" style="width: 50%;">
				</a>
			</div>
			<div class="m-login__signin">
				<div class="m-login__head">
					<h3 class="m-login__title">
					Sign In To Admin
					</h3>
				</div>
				@if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div>@endif
				@if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div>@endif
				<form class="m-login__form m-form" action="{{ url('admin/login-post') }}" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group m-form__group">
						<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off">
					</div>
					<div class="form-group m-form__group">
						<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
					</div>
					<div class="row m-login__form-sub">
					</div>
					<div class="m-login__form-action">
						<button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">
						  Sign In
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('bottom_script')
<script src="{{ asset('assets/snippets/pages/user/login.js')}}" type="text/javascript"></script>
@endsection