@extends('adminlayouts.dashMaster')
@section('title') Admin | Edit Flights @endsection
@section('style')
@endsection
@section('content')
<meta name="csrf_token"  content="{{ csrf_token() }}" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Edit Flights
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Edit Flights
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
                    {{ Form::model($flight, array('route' => array('admin-flight.update', $flight->id), 'method' => 'PUT' , 'class' => 'm-form m-form--fit m-form--label-align-right' )) }}
                    <div class="m-portlet__body">
                        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Flight Name<span class="m--font-danger"> *</span>

                            </label>
                            
                            <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="{{$flight->flight_name}}" name="flight_name"  placeholder="Enter Flight Name">
                            @if ($errors->has('flight_name'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('flight_name') }}</strong>
                                                </span>
                                            @endif 
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Flight Code<span class="m--font-danger"> *</span>

                            </label>
                            
                            <input type="text" class="form-control m-input" autocomplete="off" id="exampleInputText" value="{{$flight->flight_code}}" name="flight_code"  placeholder="Enter Flight Code">
                            @if ($errors->has('flight_code'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('flight_code') }}</strong>
                                                </span>
                                            @endif 
                        </div>
                        
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-primary">
                            Submit
                            </button>
                            <a href="{{ url('admin-flight') }}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                    {!! Form::close(); !!}
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                
                
                
            </div>
            
            <!--end::Portlet-->
            
        </div>
    </div>
</div>
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
@endsection