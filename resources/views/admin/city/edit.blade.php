@extends('adminlayouts.dashMaster')
@section('title') Admin | Edit City @endsection
@section('style')
@endsection
@section('content')
<meta name="csrf_token"  content="{{ csrf_token() }}" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Edit City
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Edit  City
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
 
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                   
                    <!--begin::Form-->
                    {{ Form::model($city, array('route' => array('admin-city.update', $city->id), 'method' => 'PUT' , 'class' => 'm-form m-form--fit m-form--label-align-right' )) }}
                    <div class="m-portlet__body">
                        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                Country Name
                            </label>
                            <select class="form-control m-input" id="country" name="country_id">
                                <option value="">Select Country</option>
                                @foreach($country as $countries)
                                <option value="{{ $countries->id }}" @if($city->country_id == $countries->id) selected @endif>{{ $countries->country_name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('country_id'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('country_id') }}</strong>
                                                </span>
                                            @endif 
                        </div>
                         <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                State Name
                            </label>
                            <select class="form-control m-input" id="state" name="state_id">
                                <option value="">Select State</option>
                                @foreach($state as $states)
                                <option value="{{ $states->id }}" @if($city->state_id == $states->id) selected @endif>{{ $states->state_name}}</option>
                                @endforeach
                            </select>
                            
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">
                                City Name
                            </label>
                            <input type="text"  autocomplete="off" class="form-control m-input" id="exampleInputText" name="city_name" value="{{$city->city_name}}"  placeholder="Enter State Name">
                            @if ($errors->has('city_name'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('city_name') }}</strong>
                                                </span>
                                            @endif 
                        </div>
                        
                        
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-primary">
                            Submit
                            </button>
                           <a href="{{ url('admin-city') }}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                    {!! Form::close(); !!}
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
                
                
                
            </div>
            
            <!--end::Portlet-->
            
        </div>
    </div>
</div>
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
@endsection