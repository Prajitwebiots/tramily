@extends('adminlayouts.dashMaster')
@section('title') Admin |  Deleted Multi City Flight tickets  @endsection
@section('style')
<style type="text/css">
.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
@endsection
@section('content')
<meta name="csrf_token"  content="{{ csrf_token() }}" />
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Deleted Multi City Flight tickets
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Deleted Multi City Flight tickets
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        
        @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div>@endif
        @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div>@endif
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form">
                    <div class="row align-items-center">
                        <div class="col-xl-12">
                            <div class="m-form__group m-form__group--inline">
                                <div class="m-form__label m-form__label-no-wrap">
                                    <label class="m--font-bold m--font-danger-">
                                        Selected
                                        <span id="m_datatable_selected_number"></span>
                                        records:
                                    </label>
                                </div>
                                <div class="m-form__control">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--begin: Datatable -->
                <table class="m-datatable" id="html_table" width="100%">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Supplier Name</th>
                            <th>Flight Departure</th>
                            <th>Flight</th>
                            <th>Flight Arrival</th>
                            <th>Flight Name</th>
                            <th>Price</th>
                            <th>Seat</th>
                            <th>Sold</th>
                            <th>Avail.</th>
                            <th>PNR No</th>
                            <th>Via</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php $i = 1; ?>
                        
                        @foreach($flight as $flights)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $flights->supplier->first_name}}</td>
                            <td>
                                
                                {{ $flights->flightSourceF->flight_source}}
                                {{'<span class="m--padding-3"></span>'}}
                                {{"<code>"}}   <?php echo date('d M y', strtotime($flights->flight_departure_date_f)); ?> -  {{ $flights->flight_departure_time_f }} {{"</code>"}}
                                {{'<span>----------------------</span>'}}
                                {{ $flights->flightSourceS->flight_source}}
                                {{'<span class="m--padding-3"></span>'}}
                                {{"<code>"}}   <?php echo date('d M y', strtotime($flights->flight_departure_date_s)); ?> -  {{ $flights->flight_departure_time_s }} {{"</code>"}}
                                @if(count($flights->flightSourceT) > 0)
                                {{'<span>----------------------</span>'}}
                                {{ $flights->flightSourceT->flight_source}}
                                {{'<span class="m--padding-3"></span>'}}
                                {{"<code>"}}   <?php echo date('d M y', strtotime($flights->flight_departure_date_t)); ?> -  {{ $flights->flight_departure_time_t }} {{"</code>"}}
                                @endif
                            </td>
                            <td>{{'<i class="fa fa-fighter-jet"></i>'}}</td>
                            <td>
                                
                                {{ $flights->flightDestinationF->flight_destination}}
                                {{'<span class="m--padding-3"></span>'}}
                                {{"<code>"}}  {{ $flights->flight_arrival_time_f }} {{"</code>"}}
                                {{'<span>---------------</span>'}}
                                {{ $flights->flightDestinationS->flight_destination}}
                                {{'<span class="m--padding-3"></span>'}}
                                {{"<code>"}}  {{ $flights->flight_arrival_time_s }} {{"</code>"}}
                                @if(count($flights->flightDestinationT) > 0)
                                    {{'<span>---------------</span>'}}
                                    {{ $flights->flightDestinationT->flight_destination}}
                                    {{'<span class="m--padding-3"></span>'}}
                                    {{"<code>"}}  {{ $flights->flight_arrival_time_t }} {{"</code>"}}
                                @endif
                            </td>
                            <td>
                                {{"<b>"}} {{ $flights->flightF->flight_name}} - {{ $flights->flight_number_f }}  {{"</b>"}}
                                {{'<span>----------</span>'}}
                                {{"<b>"}} {{ $flights->flightS->flight_name}} - {{ $flights->flight_number_s }}  {{"</b>"}}
                                @if(count($flights->flightT) > 0)
                                    {{'<span>----------</span>'}}
                                    {{"<b>"}} {{ $flights->flightT->flight_name}} - {{ $flights->flight_number_t }}  {{"</b>"}}
                                @endif
                            </td>
                            <td>{{$flights->flight_price}}</td>
                            <td>{{$flights->seat}}</td>
                            <td>{{$flights->sold}}</td>
                            <td>0</td>
                            <td>
                                    {{$flights->flight_pnr_no or 'NOT SET'}}
                                    {{'<span>------</span>'}}
                                    {{$flights->flight_pnr_no2 or 'NOT SET' }}
                                    {{'<span>------</span>'}}
                                    {{$flights->flight_pnr_no3 or 'NOT SET'}}
                            </td>
                            <td>
                                {{$flights->flight_via  }}

                            </td>
                            <td>
                                @if($flights->status == 0)
                                {{'<span><span class="m-badge  m-badge--danger m-badge--wide">Block</span></span>'}}
                                @elseif($flights->status == 1)
                                {{'<span><span class="m-badge  m-badge--primary m-badge--wide">Active</span></span>'}}
                                @elseif($flights->status == 2)
                                {{'<span><span class="m-badge  m-badge--metal m-badge--wide">Pending</span></span>'}}
                                @endif
                            </td>
                            

                            
                        </tr>
                        @endforeach
                        
                        
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
<script>
$( document ).ready(function() {
$(".m-checkbox").click(function(){
$(".collapse").addClass("show");
});
});
</script>
@endsection