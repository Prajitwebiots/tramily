@extends('adminlayouts.dashMaster')
@section('title') Admin | Manage Multi City Flight @endsection
@section('style')
<style type="text/css">
.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
@endsection
@section('content')
<meta name="csrf_token"  content="{{ csrf_token() }}" />
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Multi City Flight
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Multi City Flights
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        
        @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div>@endif
        @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div>@endif
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                            <a href="{{url('admin/createMultiCityFlight')}}" class="btn m-btn--pill    btn-primary" >Add New</a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form">
                    <div class="row align-items-center">
                        <div class="col-xl-12">
                            <div class="m-form__group m-form__group--inline">
                                <div class="m-form__label m-form__label-no-wrap">
                                    <label class="m--font-bold m--font-danger-">
                                        Selected
                                        <span id="m_datatable_selected_number"></span>
                                        records:
                                    </label>
                                </div>
                                <div class="m-form__control">
                                    <div class="btn-toolbar">
                                        &nbsp;&nbsp;&nbsp;
                                        {{  Form::open(['method' => 'post','url' => ['admin-flightMultiCity-destroy'],'style'=>'display:inline' ,'id'=>'myform'])}}
                                        <button class="btn btn-sm m-btn--pill btn-danger" type="submit" id="m_datatable_check_all">
<<<<<<< HEAD
                                        Delete
=======
                                        Delete All
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                                        </button>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--begin: Datatable -->
                <table class="m-datatable" id="html_table" width="100%">
                    <thead>
                        <tr>
                            <th>RecordID</th>
                            <th>Sr No</th>
                            <th>Supplier Name</th>
                            <th>Flight Departure</th>
<<<<<<< HEAD
                            <th>Flight</th>
                            <th>Flight Arrival</th>
                            <th>Flight Name</th>
                            <th>Supplier Price</th>
                            <th>Admin Price</th>
=======
                            <th></th>
                            <th>Flight Arrival</th>
                            <th>Flight Name</th>
                            <th>Price</th>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                            <th>Seat</th>
                            <th>Sold</th>
                            <th>Avail.</th>
                            <th>PNR No</th>
<<<<<<< HEAD
                            <th>Via</th>
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                            <th>Status</th>
                            <th>Actions</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php $i = 1; ?>
                        
                        @foreach($flight as $flights)
                        <tr>
                            <td>{{'<label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" form="myform"  value='.$flights->id.' class="select_checkbox" name="check[]" type="checkbox"><span></span></label>'}} </td>
                            <td>{{ $i++ }}</td>
                            <td>{{ $flights->supplier->first_name}}</td>
                            <td>
                                
                                {{ $flights->flightSourceF->flight_source}}
                                {{'<span class="m--padding-3"></span>'}}
                                {{"<code>"}}   <?php echo date('d M y', strtotime($flights->flight_departure_date_f)); ?> -  {{ $flights->flight_departure_time_f }} {{"</code>"}}
                                {{'<span>----------------------</span>'}}
                                {{ $flights->flightSourceS->flight_source}}
                                {{'<span class="m--padding-3"></span>'}}
                                {{"<code>"}}   <?php echo date('d M y', strtotime($flights->flight_departure_date_s)); ?> -  {{ $flights->flight_departure_time_s }} {{"</code>"}}
<<<<<<< HEAD
                                @if(count($flights->flightSourceT) > 0)
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                                {{'<span>----------------------</span>'}}
                                {{ $flights->flightSourceT->flight_source}}
                                {{'<span class="m--padding-3"></span>'}}
                                {{"<code>"}}   <?php echo date('d M y', strtotime($flights->flight_departure_date_t)); ?> -  {{ $flights->flight_departure_time_t }} {{"</code>"}}
<<<<<<< HEAD
                                @endif
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                            </td>
                            <td>{{'<i class="fa fa-fighter-jet"></i>'}}</td>
                            <td>
                                
                                {{ $flights->flightDestinationF->flight_destination}}
                                {{'<span class="m--padding-3"></span>'}}
                                {{"<code>"}}  {{ $flights->flight_arrival_time_f }} {{"</code>"}}
                                {{'<span>---------------</span>'}}
                                {{ $flights->flightDestinationS->flight_destination}}
                                {{'<span class="m--padding-3"></span>'}}
                                {{"<code>"}}  {{ $flights->flight_arrival_time_s }} {{"</code>"}}
<<<<<<< HEAD
                                @if(count($flights->flightDestinationT) > 0)
                                    {{'<span>---------------</span>'}}
                                    {{ $flights->flightDestinationT->flight_destination}}
                                    {{'<span class="m--padding-3"></span>'}}
                                    {{"<code>"}}  {{ $flights->flight_arrival_time_t }} {{"</code>"}}
                                @endif
=======
                                {{'<span>---------------</span>'}}
                                {{ $flights->flightDestinationT->flight_destination}}
                                {{'<span class="m--padding-3"></span>'}}
                                {{"<code>"}}  {{ $flights->flight_arrival_time_t }} {{"</code>"}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                            </td>
                            <td>
                                {{"<b>"}} {{ $flights->flightF->flight_name}} - {{ $flights->flight_number_f }}  {{"</b>"}}
                                {{'<span>----------</span>'}}
                                {{"<b>"}} {{ $flights->flightS->flight_name}} - {{ $flights->flight_number_s }}  {{"</b>"}}
<<<<<<< HEAD
                                @if(count($flights->flightT) > 0)
                                    {{'<span>----------</span>'}}
                                    {{"<b>"}} {{ $flights->flightT->flight_name}} - {{ $flights->flight_number_t }}  {{"</b>"}}
                                @endif
                            </td>
                            <td>{{$flights->supplier_price}}</td>
                            <td>{{$flights->flight_price}}</td>
                            <td>{{$flights->seat+$flights->sold}}</td>
                            <td>{{$flights->sold}}</td>
                            <td>{{$flights->seat}}</td>
                            <td>
                                    {{$flights->flight_pnr_no or 'NOT SET'}}
                                    {{'<span>------</span>'}}
                                    {{$flights->flight_pnr_no2 or 'NOT SET' }}
                                    {{'<span>------</span>'}}
                                    {{$flights->flight_pnr_no3 or 'NOT SET'}}
                            </td>
                            <td>
                                {{$flights->flight_via  }}

=======
                                {{'<span>----------</span>'}}
                                {{"<b>"}} {{ $flights->flightT->flight_name}} - {{ $flights->flight_number_t }}  {{"</b>"}}
                            </td>
                            <td>{{$flights->flight_price}}</td>
                            <td>{{$flights->seat}}</td>
                            <td>{{$flights->sold}}</td>
                            <td>0</td>
                            <td>
                                    {{$flights->flight_pnr_no}}
                                    {{'<span>------</span>'}}
                                    {{$flights->flight_pnr_no2}}
                                    {{'<span>------</span>'}}
                                    {{$flights->flight_pnr_no3}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                            </td>
                            <td>
                                @if($flights->status == 0)
                                {{'<span><span class="m-badge  m-badge--danger m-badge--wide">Block</span></span>'}}
                                @elseif($flights->status == 1)
                                {{'<span><span class="m-badge  m-badge--primary m-badge--wide">Active</span></span>'}}
                                @elseif($flights->status == 2)
                                {{'<span><span class="m-badge  m-badge--metal m-badge--wide">Pending</span></span>'}}
                                @endif
                            </td>
                            
                            <td>
                                {{'<a href="'.url('editMultiCityFlight').'/'.$flights->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit "><i class="la la-edit"></i></a>'}}
<<<<<<< HEAD
                                {{'<a onclick="return deletemsg('.$flights->id.');" href="'.url('admin/multiCityflightIsDelete').'/'.$flights->id.'/0" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete " ><i class="la la-trash-o"></i></a>'}}
=======
                                {{'<a href="'.url('admin/multiCityflightIsDelete').'/'.$flights->id.'/0" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete " ><i class="la la-trash-o"></i></a>'}}
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                            </td>
                            <td>
                                @if($flights->status == 0 )
                                {{'<a href="'.url('changeMultiCityFlightStatus').'/'.$flights->id.'/1" class="btn m-btn--pill    btn-primary" >Active</a>'}}
                                @elseif($flights->status == 2)
                                {{'<a href="'.url('changeMultiCityFlightStatus').'/'.$flights->id.'/1" class="btn m-btn--pill    btn-primary" >Active</a>'}}
                                @elseif($flights->status == 1)
                                {{'<a href="'.url('changeMultiCityFlightStatus').'/'.$flights->id.'/0" class="btn m-btn--pill    btn-danger" >Block</a>'}}
                                @endif
                            </td>
                            
                            
                        </tr>
                        @endforeach
                        
                        
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
<script>
$( document ).ready(function() {
$(".m-checkbox").click(function(){
$(".collapse").addClass("show");
});
});
</script>
<<<<<<< HEAD

<script>
    function deletemsg(id) {
        swal({
            title: 'Are you sure?',
            text: "Are you sure you wish to delete ",
            showCancelButton: true,
            confirmButtonColor: '#34bfa3',
            cancelButtonColor: '#f4516c',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if(result.value)
        {
            {{--alert("{{url('cityIsDelete')}}/"+id);--}}
                window.location= "{{url('admin/multiCityflightIsDelete')}}/"+id+'/0';
            return true;
        }
    else
        {return false;
        }
    }
    );
        return false;
    }

</script>
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
@endsection