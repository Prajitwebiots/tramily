@extends('adminlayouts.dashMaster')
@section('title') Admin | Manage Supplier @endsection
@section('style')
<style type="text/css">
.btn.m-btn--hover-accent.active, .btn.m-btn--hover-accent:active, .btn.m-btn--hover-accent:focus, .btn.m-btn--hover-accent:hover, .show>.btn.m-btn--hover-accent.dropdown-toggle {
border-color: #5767de!important;
color: #fff!important;
background-color: #5767de!important;
}
</style>
@endsection
@section('content')
<meta name="csrf_token"  content="{{ csrf_token() }}" />
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                Manage Supplier
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Supplier
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">

        @if(session('error'))<div class="alert alert-danger">{{ session('error') }}</div>@endif
        @if(session('success'))<div class="alert alert-success">{{ session('success') }}</div>@endif

        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="m_form_search">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                         <form action="{{url('supplier/export')}}" enctype="multipart/form-data">
                                     <button class="btn m-btn--pill    btn-primary" type="submit">DOWNLOAD AS EXCEL</button>
                                         </form>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                    </div>
                </div>
                
                <!--begin: Datatable -->
                <table class="m-datatable" id="html_table" width="100%">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>Agency Name</th>
                            <th>Status</th>
                            <th>Actions</th>
                            <th>Login</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($users as $user)
                        @if($user->user)
                        @if($user->user->is_delete == 1)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{  $user->user->title_name.''.$user->user->first_name.' '.$user->user->last_name  }}</td>
                            <td>{{ $user->user->email }}</td>
                            <td>{{ $user->user->mobile }}</td>
                            <td>{{ $user->user->agency_name }}</td>
                            <td>
                                @if($user->user->status == 0)
                                {{'<span><span class="m-badge  m-badge--danger m-badge--wide">Suspended</span></span>'}}
                                @elseif($user->user->status == 1)
                                {{'<span><span class="m-badge  m-badge--primary m-badge--wide">Active</span></span>'}}
                                @elseif($user->user->status == 2)
                                {{'<span><span class="m-badge  m-badge--metal m-badge--wide">Pending</span></span>'}}
                                @endif
                            </td>
                            <td>
                                {{'<a data-toggle="modal" data-target="#m_modal_view'.$user->user->id.'"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View "><i class="la la-eye"></i></a>'}}
                                {{'<a href="'.url('showUserProfile').'/'.$user->user->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit "><i class="la la-edit"></i></a>'}}
<<<<<<< HEAD
                                {{'<a onclick="return deletemsg('.$user->user->id.');"  href="'.url('userIsDelete').'/'.$user->user->id.'/0" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete " ><i class="la la-trash-o"></i></a>'}}
=======
                                {{'<a href="'.url('userIsDelete').'/'.$user->user->id.'/0" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete " ><i class="la la-trash-o"></i></a>'}}                                
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                            </td>
                            <td> 
                                  {{'<a href="'.url('supplierLogin').'/'.$user->user->id.'"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Login "><i class="la la-sign-in"></i></a>'}}
                        </td>
                       
                        <td>
                            @if($user->user->status == 0)
                            {{'<a href="'.url('changeUserStatus').'/'.$user->user->id.'/1" class="btn m-btn--pill    btn-primary" >Active</a>'}}
                            @elseif($user->user->status == 2)
                            {{'<a href="'.url('changeUserStatus').'/'.$user->user->id.'/1" class="btn m-btn--pill    btn-primary" >Active</a>'}}
                            @elseif($user->user->status == 1)
                            {{'<a href="'.url('changeUserStatus').'/'.$user->user->id.'/0" class="btn m-btn--pill    btn-danger" >Block</a>'}}
                            @endif
                            
                        </td>
                        
                    </tr>
                    
                    <div class="modal fade" id="m_modal_view{{$user->user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">
                                    View Supplier Details
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">
                                        &times;
                                    </span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <b>Agency Name</b> :- {{ $user->user->agency_name }} <br><br>
                                            <b>Concerned Person Name</b> :- {{  $user->user->title_name.''.$user->user->first_name.' '.$user->user->last_name  }} <br><br>
                                            <b>Agency Address</b> :- {{ $user->user->agency_address }} <br><br>
                                            <b>Email</b> :- {{ $user->user->email }}<br><br>
                                            <b>Phone No</b> :- {{ $user->user->mobile }} <br><br>
                                            
                                            
                                        </div>
                                         <div class="col-xl-6">
                                                <b>City</b> :- {{ $user->user->City->city_name }}<br><br>
                                                <b>Satate</b> :- {{ $user->user->State->state_name }}<br><br>
                                                <b>Country</b> :- {{ $user->user->Country->country_name }}<br><br>
                                                <b>Pin Code</b> :- {{ $user->user->pincode }}<br><br>
                                                <b>Office No</b> :- {{ $user->user->office_no }} <br><br>
                                                  <b>PAN No.</b> :- {{ $user->user->pan_no }} <br><br>
                                                
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-xl-6">
                                                <span><br><b>Agency GST Details</b><br><br></span>
                                                <b>Name</b> :- {{ $user->user->gst_name }}<br><br>
                                                <b>State</b> :- {{ $user->user->Gststate->state_name}}<br><br>
                                                <b>GSTIN No</b> :- {{ $user->user->gst_no }}
                                                
                                            </div>
                                            <div class="col-xl-6">
                                                <span><br><b>Agency GST Details</b><br><br></span>
                                                <b>Address</b> :- {{ $user->user->gst_address }}<br><br>
                                                <b>Email</b> :- {{ $user->user->regi_gmail }}<br><br>
                                                <b>PHONE </b> :- {{ $user->user->gst_phone }}<br><br>
                                                

                                                
                                            </div>
                                        </div>
                                    
                                    
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button"  class="btn btn-secondary" data-dismiss="modal">
                                    Close
                                    </button>
                                    <button type="button" id="Submit-profile" class="btn btn-primary">
                                    Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endif
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>
</div>
@endsection
@section('bottom_script')
<script src="{{ asset('assets/demo/default/custom/components/datatables/base/html-table.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
<script>
    function deletemsg(id) {
        swal({
            title: 'Are you sure?',
            text: "Are you sure you wish to Delete Supplier ",
            showCancelButton: true,
            confirmButtonColor: '#34bfa3',
            cancelButtonColor: '#f4516c',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if(result.value)
        {
            {{--alert("{{url('cityIsDelete')}}/"+id);--}}
                window.location= "{{url('userIsDelete')}}/"+id+'/0';
            return true;
        }
    else
        {return false;
        }
    }
    );
        return false;
    }
</script>
@endsection