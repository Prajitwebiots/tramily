<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
<i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
	<!-- BEGIN: Aside Menu -->
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
		@if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '1'){{-- admin --}}
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			<li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
				<a  href="index.html" class="m-menu__link ">
					<i class="m-menu__link-icon flaticon-line-graph"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">
								Dashboard
							</span>
							<span class="m-menu__link-badge">
								<span class="m-badge m-badge--danger">
									2
								</span>
							</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__section">
				<h4 class="m-menu__section-text">
				Components
				</h4>
				<i class="m-menu__section-icon flaticon-more-v3"></i>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{route('coders.index')}}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-layers"></i>
					<span class="m-menu__link-text">
						Coders
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{URL('user/portfolio')}}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-squares"></i>
					<span class="m-menu__link-text">
						Portfolio
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{route('domain-questions.index')}}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-layers"></i>
					<span class="m-menu__link-text">
						Domains
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{route('industries.index')}}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-layers"></i>
					<span class="m-menu__link-text">
						Industries
					</span>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="#" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-share"></i>
					<span class="m-menu__link-text">
						Icons
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item " aria-haspopup="true" >
							<a  href="components/icons/flaticon.html" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
								<span></span>
								</i>
								<span class="m-menu__link-text">
									Flaticon
								</span>
							</a>
						</li>
						<li class="m-menu__item " aria-haspopup="true" >
							<a  href="components/icons/fontawesome.html" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
								<span></span>
								</i>
								<span class="m-menu__link-text">
									Fontawesome
								</span>
							</a>
						</li>
						<li class="m-menu__item " aria-haspopup="true" >
							<a  href="components/icons/lineawesome.html" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
								<span></span>
								</i>
								<span class="m-menu__link-text">
									Lineawesome
								</span>
							</a>
						</li>
						<li class="m-menu__item " aria-haspopup="true" >
							<a  href="components/icons/socicons.html" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
								<span></span>
								</i>
								<span class="m-menu__link-text">
									Socicons
								</span>
							</a>
						</li>
					</ul>
				</div>
			</li>
		</ul>
		@elseif(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '2') {{--//user--}}
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			<li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
				<a  href="index.html" class="m-menu__link ">
					<i class="m-menu__link-icon flaticon-line-graph"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">
								Dashboard
							</span>
							<span class="m-menu__link-badge">
								<span class="m-badge m-badge--danger">
									2
								</span>
							</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__section">
				<h4 class="m-menu__section-text">
				Components
				</h4>
				<i class="m-menu__section-icon flaticon-more-v3"></i>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{URL('user/portfolio')}}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-squares"></i>
					<span class="m-menu__link-text">
						Portfolio
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{route('domain.index')}}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-layers"></i>
					<span class="m-menu__link-text">
						Domains
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{URL('')}}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-layers"></i>
					<span class="m-menu__link-text">
						Domains
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
			</li>
		</ul>
		@endif
	</div>
	<!-- END: Aside Menu -->