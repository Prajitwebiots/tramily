<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	@include('layouts.dashHead')
	<!-- end::Body -->
	<body class="m-page--wide m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			
			@include('layouts.dashHeader')
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				@yield('content')
			</div>
			@include('layouts.dashFooter')
			
			<!-- end:: Page -->
			@include('layouts.dashFooterContent')
			
		</div>
		@yield('bottom_script')
	</body>
</html>