<!-- begin::Header -->
<header class="m-grid__item		m-header "  data-minimize="minimize" data-minimize-offset="200" data-minimize-mobile-offset="200" >
	<div class="m-header__top">
		<div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
			<div class="m-stack m-stack--ver m-stack--desktop">
				<!-- begin::Brand -->
				<div class="m-stack__item m-brand">
					<div class="m-stack m-stack--ver m-stack--general m-stack--inline">
						<div class="m-stack__item m-stack__item--middle m-brand__logo">
							@if(Sentinel::getUser()->roles()->first()->slug == '2')
							<a href="{{ url('user/dashboard') }}" class="m-brand__logo-wrapper">
								<img alt="" src="{{ URL::asset('assets/app/media/img/logos/logo-2.png')}}" style="width: 50%;"/>
							</a>
							@else
							<a href="{{ url('supplier/dashboard') }}" class="m-brand__logo-wrapper">
								<img alt="" src="{{ URL::asset('assets/app/media/img/logos/logo-2.png')}}" style="width: 50%;"/>
							</a>
							@endif

						</div>
						<div class="m-stack__item m-stack__item--middle m-brand__tools">

							<!-- begin::Responsive Header Menu Toggler-->
							<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
								<span></span>
							</a>
							<!-- end::Responsive Header Menu Toggler-->
							<!-- begin::Topbar Toggler-->
							<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
								<i class="flaticon-more"></i>
							</a>
							<!--end::Topbar Toggler-->
						</div>
					</div>
				</div>
				<!-- end::Brand -->
				<!-- begin::Topbar -->

				<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
					<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
						<div class="m-stack__item m-topbar__nav-wrapper">
							<ul class="m-topbar__nav m-nav m-nav--inline">
								<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
									<a href="#" class="m-nav__link m-dropdown__toggle">
										<span class="m-topbar__userpic m--hide">
											<img src="{{URL::asset('assets/images/user')}}/{{Sentinel::getUser()->profile }}" class="m--img-rounded m--marginless m--img-centered" alt=""/>
										</span>
										<span class="m-topbar__welcome">
											Hello,&nbsp;
										</span>
										<span class="m-topbar__username">
											{{ Sentinel::getUser()->first_name }}

										</span>
										<?php  $sesion = Session::get('adminlogin', 'default')?>
										@if( $sesion == 1)
											<div class="fixed" style=" position: fixed;top: 40px;left: 100px;z-index: 3;">
												<a href="{{url('back-to-admin')}}"  class="btn btn-danger">
													Back to admin
												</a>
											</div>
										@endif
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__header m--align-center" style="background: url({{ URL::asset('assets/app/media/img/misc/user_profile_bg.jpg')}}); background-size: cover;">
												<div class="m-card-user m-card-user--skin-dark">
													<div class="m-card-user__pic">
														<img src="{{URL::asset('assets/images/user')}}/{{Sentinel::getUser()->profile }}" class="m--img-rounded m--marginless" style="width: 60px;height:60px; " alt=""/>
													</div>
													<div class="m-card-user__details">
														<span class="m-card-user__name m--font-weight-500">
															{{ Sentinel::getUser()->first_name }}
														</span>
														<a href="" class="m-card-user__email m--font-weight-300 m-link">
															{{ Sentinel::getUser()->email }}
														</a>

													</div>
												</div>
											</div>
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav m-nav--skin-light">
														<li class="m-nav__section m--hide">
															<span class="m-nav__section-text">
																Section
															</span>
														</li>
														@if(Sentinel::getUser()->roles()->first()->slug == '2')
														<li class="m-nav__item">
															<a href="{{ url('user/profile') }}" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-profile-1"></i>
																<span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			My Profile
																		</span>

																	</span>
																</span>
															</a>
															</li>
															<li class="m-nav__item">

															<a  class="m-nav__link">
																<i class="m-nav__link-icon fa fa-inr"></i>
																<span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			Wallet balance : {{Sentinel::getUser()->wallet_balance}}
																		</span>

																	</span>
																</span>
															</a>

<<<<<<< HEAD
=======
															<a  class="m-nav__link">
																<i class="m-nav__link-icon fa fa-inr"></i>
																<span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			Wallet balance : {{Sentinel::getUser()->wallet_balance}}
																		</span>

																	</span>
																</span>
															</a>

>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
														</li>
															<li class="m-nav__item">

																<a href="{{ url('history') }}" class="m-nav__link">
																	<i class="m-nav__link-icon flaticon-signs-1"></i>
																	<span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			Booking history
																		</span>

																	</span>
																</span>
																</a>

															</li>
														@else
														<li class="m-nav__item">
															<a href="{{ url('supplier/profile') }}" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-profile-1"></i>
																<span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			My Profile
																		</span>
																		
																	</span>
																</span>
															</a>
														</li>
															<li class="m-nav__item">
																<a href="{{ route('hello') }}" class="m-nav__link">
																	<i class="m-nav__link-icon flaticon-signs-1"></i>
																	<span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			Booking history
																		</span>

																	</span>
																</span>
																</a>
															</li>
														@endif
														
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<form id="form-submit" method="post" action="<?php echo url('/');?>/logout">
																{{ csrf_field() }}
																<a href="#" onclick="document.getElementById('form-submit').submit()" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
																	Logout
																</a>
															</form>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
				<!-- end::Topbar -->
			</div>
		</div>
	</div>
	<div class="m-header__bottom">
		<div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
			<div class="m-stack m-stack--ver m-stack--desktop">
				<!-- begin::Horizontal Menu -->
				<div class="m-stack__item m-stack__item--middle m-stack__item--fluid">

					<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

					<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">
						<ul id="header_menu" class=" nav nav-tabs m-tabs m-tabs-line m-menu__nav  m-menu__nav--submenu-arrow " role="tablist">


							@if(Sentinel::getUser()->roles()->first()->slug == '2')


								@if(Request::path() == 'user/dashboard')
									<li class="m-menu__item  m-menu__item--active"  aria-haspopup="true">
										<a class="m-menu__link " data-toggle="tab" href="#m_tabs_7_0" role="tab" aria-expanded="false">
											<span class="m-menu__item-here"></span>
											<span class="m-menu__link-text">
										Dashboard
									</span>
										</a>
									</li>
									{{--<li class="m-menu__item "  aria-haspopup="true">--}}
										{{--<a class="m-menu__link " data-toggle="tab" href="#m_tabs_7_1" role="tab" aria-expanded="false">--}}
											{{--<span class="m-menu__item-here"></span>--}}
											{{--<span class="m-menu__link-text">--}}
										{{--One way--}}
									{{--</span>--}}
										{{--</a>--}}
									{{--</li>--}}


									{{--<li class="m-menu__item  m-menu__item"  aria-haspopup="true">--}}
										{{--<a class="m-menu__link" data-toggle="tab" href="#m_tabs_7_2" role="tab" aria-expanded="false">--}}
											{{--<span class="m-menu__item-here"></span>--}}
											{{--<span class="m-menu__link-text">--}}
										{{--Round trip--}}
										{{--</span>--}}
										{{--</a>--}}
									{{--</li>--}}
									{{--<li class="m-menu__item  m-menu__item"  aria-haspopup="true">--}}
										{{--<a class="m-menu__link" data-toggle="tab" href="#m_tabs_7_3" role="tab" aria-expanded="true">--}}
											{{--<span class="m-menu__item-here"></span>--}}
											{{--<span class="m-menu__link-text">--}}
										{{--Multi city--}}
											{{--</span>--}}
										{{--</a>--}}
									{{--</li>--}}

								@else
									<li class="m-menu__item  m-menu__item--active"  aria-haspopup="true">
										<a  href="{{ url('user/dashboard')}}" class="m-menu__link ">
											<span class="m-menu__item-here"></span>
											<span class="m-menu__link-text">
										Dashboard
									</span>
										</a>
									</li>


								@endif
							@else
								<li class="m-menu__item  <?php echo Request::path() == 'supplier/dashboard'? 'm-menu__item--active':'' ?>"  aria-haspopup="true">
									<a  href="{{ url('supplier/dashboard')}}" class="m-menu__link ">
										<span class="m-menu__item-here"></span>
										<span class="m-menu__link-text">
										Dashboard
									</span>
<<<<<<< HEAD
									</a>
								</li>
								<li class="m-menu__item  <?php echo Request::path() == 'supplier-FlightOneWay'? 'm-menu__item--active':'' ?>"  aria-haspopup="true">
									<a class="m-menu__link "  href="{{ url('supplier-FlightOneWay')}}" >
										<span class="m-menu__item-here"></span>
										<span class="m-menu__link-text">
										One way
									</span>
									</a>
								</li>


								<li class="m-menu__item   <?php echo Request::path() == 'supplier-FlightRoundTrip'? 'm-menu__item--active':'' ?>"  aria-haspopup="true">
									<a class="m-menu__link"  href="{{ url('supplier-FlightRoundTrip')}}" >
										<span class="m-menu__item-here"></span>
										<span class="m-menu__link-text">
										Round trip
										</span>
									</a>
								</li>
								<li class="m-menu__item   <?php echo Request::path() == 'supplier-FlightMultiCity'? 'm-menu__item--active':'' ?>"  aria-haspopup="true">
									<a class="m-menu__link"  href="{{ url('supplier-FlightMultiCity')}}" >
										<span class="m-menu__item-here"></span>
										<span class="m-menu__link-text">
										Multi city
											</span>
									</a>
								</li>
=======
								</a>
							</li>
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
							@endif
						</ul>
					</div>
				</div>

				<!-- end::Horizontal Menu -->
				<!--begin::Search-->
				<div class="m-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-light m-header-search m-header-search--expandable m-header-search--skin-" id="m_quicksearch" data-search-type="default">
					<!--begin::Search Form -->
<<<<<<< HEAD
					<div >

						@if(Sentinel::getUser()->roles()->first()->slug == '2')
							<label class="m--font-light m--block-inline">

										Wallet balance : <i class="la la-inr m--block-inline"></i>  {{Sentinel::getUser()->wallet_balance }}
							</label>

						@endif
					</div>
=======
					<h4 class="m--font-light">

						@if(Sentinel::getUser()->roles()->first()->slug == '2')

										Wallet balance : <i class="la la-inr"></i> {{Sentinel::getUser()->wallet_balance}}
							@else
							<form class="m-header-search__form">
							<div class="m-header-search__wrapper">
							<span class="m-header-search__icon-search" id="m_quicksearch_search">
							<i class="la la-search"></i>
							</span>
							<span class="m-header-search__input-wrapper">
							<input autocomplete="off" type="text" name="q" class="m-header-search__input" value="" placeholder="Search..." id="m_quicksearch_input">
							</span>

							<span class="m-header-search__icon-close" id="m_quicksearch_close">
							<i class="la la-remove"></i>
							</span>
							<span class="m-header-search__icon-cancel" id="m_quicksearch_cancel">
							<i class="la la-times"></i>
							</span>
							</div>
							</form>
						@endif
					</h4>




>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
					<!--end::Search Form -->
					<!--begin::Search Results -->
					<div class="m-dropdown__wrapper">
						<div class="m-dropdown__arrow m-dropdown__arrow--center"></div>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-max-height="300" data-mobile-max-height="200">
									<div class="m-dropdown__content m-list-search m-list-search--skin-light"></div>
								</div>
							</div>
						</div>
					</div>
					<!--end::Search Results -->
				</div>

				<!--end::Search-->
			</div>
		</div>

	</div>

</header>


<!-- end::Header -->