<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Validator;
use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

        public function boot()
    {
        Validator::extend('user_validation', function($attribute, $value, $parameters, $validator) {
            $user = User::where('email',$value)->where('user_type',1)->get();

            if(count($user) == 0){
                return true;
            }
            return false;
        });
        Validator::extend('supplier_validation', function($attribute, $value, $parameters, $validator) {

            $user = User::where('email',$value)->where('user_type',2)->get();

            if(count($user) == 0){
                return true;
            }
            return false;
        });


    }



    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
