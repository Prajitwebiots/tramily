<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Sentinel;
use Activation;
use App\User;
use App\Models\Industries;
use Session;
use App\Models\FlightOneWay;
use App\Models\FlightRoundTrip;
use App\Models\FlightMultiCity;
use PDF;
use Dompdf\Dompdf;
use Spatie\Browsershot\Browsershot;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        try {
            if (Sentinel::check())
                return redirect('admin/dashboard');
            return view('auth.login');
            // Sentinel::logout();
        } catch (\Exception $e) {
            return view('auth.login')->with(['error' => 'Your Account Is not Activated !!']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
<<<<<<< HEAD

        try {
            $user = User::where('user_type', 1)->where('email', $request->email)->first(['id', 'username']);
            if ($user) {
                $act = Activation::where('user_id', $user->id)->first(['completed', 'suspend_by']);
                if ($act) {
                    if ($act->completed == 1 && $act->suspend_by != 0) {
                        return "You account is Suspended !";
                    } else if ($act->completed == 0)
                        return "Your account is not activated !!!";
                } else
                    return 'Incorrect email. Please try again.';

            } else {

                return 'Incorrect email. Please try again';
            }
            if (Sentinel::authenticate(['username' => $user->username, 'password' => $request->password])) {

                if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '2') {
                    return 1;
                } else {
                    return "Incorrect username or password. Please try again.";
                }
            } else
                return "Incorrect username or password. Please try again.";
        }
        catch (\Exception $e) {
            $delay = $e->getDelay();
            $s=60;
            if ($delay)
                return  "You are Banned with ".round($delay/$s)." minutes";
            else {
                return 'Incorrect username or password. Please try again.';
=======
         $user = User::where('user_type',1)->where('email', $request->email)->first(['id']);
        if($user){
                     $act = Activation::where('user_id', $user->id)->first(['completed', 'suspend_by']);
                    if($act){
                        if($act->completed == 1 && $act->suspend_by != 0){
                            return "You account is Suspended !";
                        }else if($act->completed == 0)
                            return "Your account is not activated !!!";
                    }
                    else
                       return 'Incorrect email. Please try again.';

        }else{

            return 'Incorrect email. Please try again';
        }
        if(Sentinel::authenticate($request->all())){
             if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '2') {
            return 1;
            }
            else {
                return "Incorrect username or password. Please try again.";
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
            }
        }

    }





    public function supplierLogin(Request $request)
    {
<<<<<<< HEAD
        try {

            $user = User::where('user_type', 2)->where('email', $request->email)->first(['id', 'username']);
            if ($user) {
                $act = Activation::where('user_id', $user->id)->first(['completed', 'suspend_by']);
                if ($act) {
                    if ($act->completed == 1 && $act->suspend_by != 0) {
                        return redirect()->back()->with(['error' => 'You account is Suspended !']);
                    } else if ($act->completed == 0)
                        return redirect()->back()->with(['error' => 'Your Account Is not Activated !!']);
                } else
                    return redirect()->back()->with(['error' => 'Incorrect email. Please try again.']);

            } else {
                return redirect()->back()->with(['error' => 'Incorrect email. Please try again']);
            }
=======
         $user = User::where('user_type',2)->where('email', $request->email)->first(['id']);
        if($user){
                    $act = Activation::where('user_id', $user->id)->first(['completed', 'suspend_by']);
                    if($act){
                        if($act->completed == 1 && $act->suspend_by != 0){
                           return redirect()->back()->with(['error'=>'You account is Suspended !']); 
                        }else if($act->completed == 0)
                            return redirect()->back()->with(['error'=>'Your Account Is not Activated !!']);
                    }
                    else
                        return redirect()->back()->with(['error'=>'Incorrect email. Please try again.']);   

        }else{
            return redirect()->back()->with(['error'=>'Incorrect email. Please try again']);
        }
            if(Sentinel::authenticate($request->all())){
                
                if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '3') {
                    return redirect('supplier/dashboard');
                }                    
             } else {
                return redirect()->back()->with(['error'=>'Wrong email and password.']);   
               }
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

            if (Sentinel::authenticate(['username' => $user->username, 'password' => $request->password])) {

                if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '3') {
                    return redirect('supplier/dashboard');
                }
            } else {
                return redirect()->back()->with(['error' => 'Wrong email and password.']);
            }

        } catch (\Exception $e) {
            $delay = $e->getDelay();
            $s=60;
            if ($delay)
                return redirect()->back()->with(['error' => "You are Banned with ".round($delay/$s)." minutes"]);
            else {
                return redirect()->back()->with(['error' => 'Incorrect username or password. Please try again.']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logout()
    {
<<<<<<< HEAD
        if (Sentinel::check()) {

            if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '1') {
                Session::forget('userlive');
                Session::forget('adminlogin');
                Session::flush();
                Sentinel::logout();
                return redirect('admin/login');
            } else if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '2') {
                Sentinel::logout();
                return redirect('/login');
            } else if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '3') {
                Sentinel::logout();
                return redirect('supplier/login');
            } else {
                Sentinel::logout();
                return redirect('/login');
            }
        } else
=======
        if(Sentinel::check()) {

            if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '1'){
                Sentinel::logout();
                return redirect('admin/login');
            }
            else if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '2'){
                Sentinel::logout();
                return redirect('/login');
            }
            else if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '3')
            {
                Sentinel::logout();
                return redirect('supplier/login');
            }
            else{
                Sentinel::logout();
                return redirect('/login');
            }
        }
        else
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
            return redirect('/login');
    }

    public function termsandcondition()
    {
        return view('user/termsandcondition');
    }

    public function homepage()
    {
        return view('homepage');
    }

    public function user_Login(Request $request)
    {
        try {
            $user = User::where('user_type', 1)->where('email', $request->email)->first(['id', 'username']);
            if ($user) {
                $act = Activation::where('user_id', $user->id)->first(['completed', 'suspend_by']);
                if ($act) {
                    if ($act->completed == 1 && $act->suspend_by != 0) {
                        return redirect()->to('login')->with(['error' => 'You account is Suspended']);

                    } else if ($act->completed == 0)
                        return redirect()->to('login')->with(['error' => 'Your account is not activated !!!']);
                } else
                    return redirect()->to('login')->with(['error' => 'Incorrect email. Please try again.']);

            } else {

                return redirect()->to('login')->with(['error' => 'Incorrect username or password. Please try again.']);
            }
            if (Sentinel::authenticate(['username' => $user->username, 'password' => $request->password])) {

                if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '2') {
                    return redirect()->to('user/dashboard');
                } else {
                    return redirect()->to('login')->with(['error' => 'Incorrect username or password. Please try again.']);
                }
            } else
                return redirect()->to('login')->with(['error' => 'Incorrect username or password. Please try again.']);

        } catch (\Exception $e) {
            $delay = $e->getDelay();
            $s=60;
            if($delay)
            return redirect()->to('login')->with(['error' => "You are Banned with ".round($delay/$s)." minutes "]);
            else{
                return redirect()->to('login')->with(['error' => 'Incorrect username or password. Please try again.']);
            }
        }

    }
    public function hello(){
        $asset= asset('');
        header("Content-type: image/png");
        $string = "fdsfs";
        $im     = imagecreatefrompng($asset."/assets/images/button1.png");
        $orange = imagecolorallocate($im, 220, 210, 60);
        $px     = (imagesx($im) - 70.5 * strlen($string)) / 2;
        imagestring($im, 3, $px, 9, $string, $orange);
        imagepng($im);
        imagedestroy($im);

    }
}
