<?php

namespace App\Http\Controllers;

use App\Models\Flight;
use Illuminate\Http\Request;

class FlightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
<<<<<<< HEAD
        $flight=Flight::where('is_delete','1')->orderBy('id', 'desc')->get();
=======
        $flight=Flight::where('is_delete','1')->get();
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        return view('admin.flight.index',compact('flight'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.flight.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'flight_name' =>'required|unique:flights',
            'flight_code' =>'required|unique:flights'
        ]);

        $flight = new  Flight;
        $flight->flight_name=$request->flight_name;
        $flight->flight_code=$request->flight_code;
        $flight->save();
        return redirect()->route('admin-flight.index')->with(['success'=>'Flight added successfully']);
    }

    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function show(Flight $flight)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function edit(Flight $flight,$id)
    {
        $flight=Flight::where('id',$id)->first();
        return view('admin.flight.edit',compact('flight'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Flight $flight,$id)
    {
         $this->validate($request,[
            'flight_name' =>'required|unique:flights,flight_name,'.$flight->id,
            'flight_code' =>'required|unique:flights,flight_code,'.$flight->id,
          
            
        ]);
            
        $flight = Flight::find($id);
        $flight->flight_name=$request->flight_name;
        $flight->flight_code=$request->flight_code;
        $flight->save();
        return redirect()->route('admin-flight.index')->with(['success'=>'Flight Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Flight  $flight
     * @return \Illuminate\Http\Response
     */
    public function destroy(Flight $flight)
    {
        //
    }


    public function flightStatus($flight_id,$status)
    {
         Flight::where('id',$flight_id)->update(['status'=>$status]);
         return redirect()->back()->with(['success' => 'Flight status updated successfully.']);
    }


    public function flightIsDelete($flight_id,$status)
    {
         Flight::where('id',$flight_id)->update(['is_delete'=>$status]);
         return redirect()->back()->with(['success' => 'Deleted successfully.']);
    }
}
