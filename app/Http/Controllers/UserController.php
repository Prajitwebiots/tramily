<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\Models\Domainans;
use App\Models\FlightDestination;
use App\Models\FlightSource;
<<<<<<< HEAD
use App\Models\Bookings;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
use Carbon\Carbon;
use Softon\Indipay\Facades\Indipay;
use App\User;
use Mail;
use View;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $destination=FlightDestination::where('is_delete','1')->where('status',1)->get();
        $source=FlightSource::where('is_delete','1')->where('status',1)->get();
        return view('user.dashboard',compact('destination','source'));
    }

    public function profile()
    {
         $countrylist = Country::where('is_delete',1)->where('status',1)->get();
         $statelist =State::where('is_delete',1)->where('status',1)->get();
         $citylist =City::where('is_delete',1)->where('status',1)->get();

        $userdata = User::where('id', Sentinel::getUser()->id)->first();
       return view('user.profile',compact('userdata','countrylist','statelist','citylist'));
        
    }
    
    public function changePass()
    {
        return view('user.changePass');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

<<<<<<< HEAD
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function payu()
    {
//        $parameters = [
//
//            'tid' => '1233221223322',
//
//            'order_id' => '1232212',
//
//            'amount' => '1200.00',
//
//        ];
//
//        // gateway = CCAvenue / PayUMoney / EBS / Citrus / InstaMojo / ZapakPay / Mocker
//
//        $order = Indipay::gateway('NameOfGateway')->prepare($parameters);
//        return Indipay::process($order);


        /////////////////////////////////////////

        // Merchant key here as provided by Payu
        $MERCHANT_KEY = "JBZaLc";

// Merchant Salt as provided by Payu
        $SALT = "GQs7yium";

// End point - change to https://secure.payu.in for LIVE mode
        $PAYU_BASE_URL = "https://test.payu.in";

        $action = '';

        $posted = array();
        if(!empty($_POST)) {
            //print_r($_POST);
            foreach($_POST as $key => $value) {
                $posted[$key] = $value;

            }
        }

        $formError = 0;

        if(empty($posted['txnid'])) {
            // Generate random transaction id
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
            $txnid = $posted['txnid'];
        }
        $hash = '';
// Hash Sequence
        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if(empty($posted['hash']) && sizeof($posted) > 0) {
            if(
                empty($posted['key'])
                || empty($posted['txnid'])
                || empty($posted['amount'])
                || empty($posted['firstname'])
                || empty($posted['email'])
                || empty($posted['phone'])
                || empty($posted['productinfo'])
                || empty($posted['surl'])
                || empty($posted['furl'])
                || empty($posted['service_provider'])
            ) {
                $formError = 1;
            } else {
                //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
                $hashVarsSeq = explode('|', $hashSequence);
                $hash_string = '';
                foreach($hashVarsSeq as $hash_var) {
                    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                    $hash_string .= '|';
                }

                $hash_string .= $SALT;


                $hash = strtolower(hash('sha512', $hash_string));
                $action = $PAYU_BASE_URL . '/_payment';
            }
        } elseif(!empty($posted['hash'])) {
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';
        }

        return view('user.payU',compact('action','hash','formError','MERCHANT_KEY','txnid'));
    }

    public function userhistory()
    {
        $id =  Sentinel::getuser()->id;
           $history_oneway = Bookings::with('booking_details','oneway','oneway.flight','oneway.flightSource','oneway.flightDestination')->where('ticket_type',1)->where('user_id',$id)->orderBy('id', 'desc')->get();
          $history_round_trip = Bookings::with('booking_details','round_trip','round_trip.flightDestination','round_trip.flightSourceReturn','round_trip.flightSource','round_trip.flightReturn','round_trip.flightSource','round_trip.flightDestinationReturn')->where('ticket_type',2)->where('user_id',$id)->orderBy('id', 'desc')->get();
          $history_multicity = Bookings::with('booking_details','multicity','multicity.flightF','multicity.flightS','multicity.flightT','multicity.flightSourceF','multicity.flightSourceS','multicity.flightSourceT','multicity.flightDestinationF','multicity.flightDestinationS','multicity.flightDestinationT')->where('ticket_type',3)->where('user_id',$id)->orderBy('id', 'desc')->get();
        return view('user.booking_history',compact('history_oneway','history_round_trip','history_multicity'));
    }
    public function emailsend(Request $request)
    {
        $pdf = $request->pdf;
        $email = $request->email;
        $user =  Sentinel::getuser();
        $type ="one way";
        Mail::send('emails.send_ticket',[
                'user' => $user,
                'type' => 'one way',
            ],function ($message) use ($user,$type,$pdf,$email) {
                $message->to($email);
                $message->subject("E - TICKET");
               $message->attach(url('assets/ticket/',$pdf));
            });

        return redirect()->to('user/dashboard')->with(['success' => 'Successfully buy tickets']);


    }

=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
}
