<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\State;
use App\Models\Country;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $city=City::with('country','state')->where('is_delete','1')->get();
        return view('admin.city.index',compact('city'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country=Country::where('status','1')->where('is_delete','1')->get();
        $state=State::where('status','1')->where('is_delete','1')->get();
        return view('admin.city.create',compact('country','state'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'country_id' =>'required',
            'state_id' =>'required',
            'city_name' =>'required|max:50'
        ]);

        $city = new  City;
        $city->country_id=$request->country_id;
        $city->state_id=$request->state_id;
        $city->city_name=$request->city_name;
        $city->save();
        return redirect()->route('admin-city.index')->with(['success'=>'City added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city,$id)
    {
        $country=Country::where('status','1')->where('is_delete','1')->get();
        $state=State::where('status','1')->where('is_delete','1')->get();
        $city=City::where('id',$id)->with('country','state')->first();
        return view('admin.city.edit',compact('country','state','city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city,$id)
    {
        $this->validate($request,[
            'country_id' =>'required',
            'state_id' =>'required',
            'city_name' =>'required|max:50'
        ]);

        $city = City::find($id);;
        $city->country_id=$request->country_id;
        $city->state_id=$request->state_id;
        $city->city_name=$request->city_name;
        $city->save();
        return redirect()->route('admin-city.index')->with(['success'=>'City Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        //
    }

    public function cityStatus($city_id,$status)
    {
         City::where('id',$city_id)->update(['status'=>$status]);
         return redirect()->back()->with(['success' => 'City status updated successfully.']);
    }


    public function cityIsDelete($city_id,$status)
    {
         City::where('id',$city_id)->update(['is_delete'=>$status]);
         return redirect()->back()->with(['success' => 'Deleted successfully.']);
    }
}
