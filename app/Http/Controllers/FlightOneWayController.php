<?php

namespace App\Http\Controllers;

use App\Models\FlightOneWay;
use App\Models\FlightDestination;
use App\Models\Flight;
use App\Models\FlightSource;
use Illuminate\Http\Request;
use Sentinel;
use Mail;
use View;

class FlightOneWayController extends Controller
{


<<<<<<< HEAD
     /**  For Supplier Function **/
=======
     /**  For Supplier Function **/ 


>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Sentinel::getUser()->id;
<<<<<<< HEAD
        $flight=FlightOneWay::with('flight','flightSource','flightDestination')->where('is_delete','1')->where('supplier_id',$id)->orderBy('id', 'DESC')->get();
        return view('supplier.flightoneway.index',compact('flight'));
=======
        $flight=FlightOneWay::with('flight','flightSource','flightDestination')->where('is_delete','1')->where('supplier_id',$id)->orderBy('created_at', 'DESC')->get();
        return view('supplier.flightOneWay.index',compact('flight'));
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $flightSource=FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination=FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight=Flight::where('is_delete','1')->where('status','1')->get();
<<<<<<< HEAD
        return view('supplier.flightoneway.create',compact('flightSource','flightDestination','flight'));
=======
        return view('supplier.flightOneWay.create',compact('flightSource','flightDestination','flight'));
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'source' =>'required',
            'destination' =>'required',
            'flight' =>'required',
<<<<<<< HEAD
            'flight_number' =>'required|max:50',
=======
            'flight_number' =>'required',
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
            'departure_date' =>'required',
            'departure_time' =>'date_format:"H:i"|required',
            'arrival_time' =>'date_format:"H:i"|required',
            'flight_price' =>'required | numeric|min:1|max:1000000',
            'seat' =>'required | integer|min:1|max:200',
            'pnr_no' =>'nullable|numeric|digits_between:1,10',
            'via' =>'nullable|max:200',

        ]);

        $flight = new  FlightOneWay;
        $flight->supplier_id=Sentinel::getUser()->id;
        $flight->flight_source=$request->source;
        $flight->flight_destination=$request->destination;
        $flight->flight_name=$request->flight;
        $flight->flight_number=$request->flight_number;
<<<<<<< HEAD
        $flight->flight_departure= $request->departure_date;
=======
        $flight->flight_departure=$request->departure_date;
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        $flight->flight_departure_time=$request->departure_time;
        $flight->flight_arrival=$request->arrival_time;
        $flight->seat=$request->seat;
        $flight->flight_price=$request->flight_price;
        $flight->supplier_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_via=$request->via;
        $flight->save();

        $onewaybook=FlightOneWay::with('flight','flightSource','flightDestination')->find($flight->id);

        $user = Sentinel::getUser();
        $text = 'Has add blocks as below ';
        $this->supplieradupdate($user,$text,$onewaybook); // this is email sent to admin for new supplier




        return redirect()->route('supplier-FlightOneWay.index')->with(['success'=>'Flight added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FlightOneWay  $flightOneWay
     * @return \Illuminate\Http\Response
     */
    public function show(FlightOneWay $flightOneWay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FlightOneWay  $flightOneWay
     * @return \Illuminate\Http\Response
     */
    public function edit(FlightOneWay $flightOneWay,$id)
    {
        $flightSource=FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination=FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight=Flight::where('is_delete','1')->where('status','1')->get();
        $flightoneway = FlightOneWay::where('id',$id)->first();
        return view('supplier.flightoneway.edit',compact('flightSource','flightDestination','flight','flightoneway'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FlightOneWay  $flightOneWay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FlightOneWay $flightOneWay,$id)
    {
        $this->validate($request,[
            'source' =>'required',
            'destination' =>'required',
            'flight' =>'required',
            'flight_price' =>'required',
            'flight_number' =>'required|max:50',
            'departure_date' =>'required',
            'departure_time' =>'date_format:"H:i"|required',
            'arrival_time'   =>'date_format:"H:i"|required',
            'flight_price' =>'nullable|numeric|min:1|max:1000000',
            'seat' =>'nullable|integer|min:1|max:200',
            'available' =>'nullable|integer|min:0|max:200',
            'pnr_no' =>'nullable|numeric|digits_between:1,10',
            'via' =>'nullable|max:200',
        ]);


        $flightold =FlightOneWay::find($id);
        $flight = FlightOneWay::find($id);
        $flight->supplier_id=Sentinel::getUser()->id;
        $flight->flight_source=$request->source;
        $flight->flight_destination=$request->destination;
        $flight->flight_name=$request->flight;
        $flight->flight_number=$request->flight_number;
        $flight->supplier_price=$request->flight_price;
        $flight->flight_departure=$request->departure_date;
        $flight->flight_departure_time=$request->departure_time;
        $flight->flight_arrival=$request->arrival_time;
        if($request->seatcount =='add'&& $request->available > 0) {
            $flight->increment('seat', $request->available);
        }
        elseif($request->seatcount =='minus' && $request->available > 0){
            $flight->decrement('seat', $request->available);
        }
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_via=$request->via;
        $flight->save();

        $onewaybook=FlightOneWay::with('flight','flightSource','flightDestination')->find($flight->id);

        $user = Sentinel::getUser();
        $text = 'Has update blocks as below ';
//         return view('emails.updateblockoneway',compact('user','text','onewaybook'));

         if($flightold->supplier_price !=  $flight->supplier_price){
             $old = $flightold->supplier_price;
             return view('emails.priceupdateblockoneway',compact('user','text','onewaybook','old'));
         }
         if($flightold->seat !=  $flight->seat){
             $old = $flightold->seat;
             return view('emails.seatupdateblockoneway',compact('user','text','onewaybook','old'));
         }
        if($flightold->supplier_price ==  $flight->supplier_price && $flightold->seat ==  $flight->seat){
         $this->supplieradupdate($user,$text,$onewaybook); // this is email sent to admin for new supplier
        }



        return redirect()->route('supplier-FlightOneWay.index')->with(['success'=>'Flight  Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FlightOneWay  $flightOneWay
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
       // return $request->all();

        FlightOneWay::wherein('id', $request->check)->update(['is_delete' => 0]);
        return redirect()->back()->with('success', 'Flight Deleted successfully ');
    }


     public function flightIsDelete($flight_id,$status)
    {
         FlightOneWay::where('id',$flight_id)->update(['is_delete'=>$status]);
         return redirect()->back()->with(['success' => 'Flight Deleted successfully.']);
    }



 /**  For Admin Function **/ 

    public function listOneWayFlight()
    {
<<<<<<< HEAD
          $flight=FlightOneWay::with('flight','flightSource','flightDestination','supplier')->orderBy('created_at', 'DESC')->where('is_delete','1')->get();
        return view('admin.flightoneway.index',compact('flight'));
=======
        $flight=FlightOneWay::with('flight','flightSource','flightDestination','supplier')->orderBy('created_at', 'DESC')->where('is_delete','1')->get();
        return view('admin.flightOneWay.index',compact('flight'));
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    }

    public function createOneWayFlight()
    {
        $flightSource=FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination=FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight=Flight::where('is_delete','1')->where('status','1')->get();
<<<<<<< HEAD
        return view('admin.flightoneway.create',compact('flightSource','flightDestination','flight'));
=======
        return view('admin.flightOneWay.create',compact('flightSource','flightDestination','flight'));
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    }

    public function storeFlightOneWay(Request $request)
    {
        //return $request->all();
        $this->validate($request,[
            'source' =>'required',
            'destination' =>'required',
            'flight' =>'required',
<<<<<<< HEAD
            'flight_number' =>'required|max:50',
            'departure_time' =>'date_format:"H:i"|required',
            'arrival_time'   =>'date_format:"H:i"|required',
            'arrival_time' =>'required ',
            'flight_price' =>'required|numeric|min:1|max:1000000',
            'seat' =>'required|numeric|min:1|max:200',
            'pnr_no' =>'nullable|numeric|digits_between:1,10',
            'via' =>'nullable|max:200',
=======
            'flight_number' =>'required',
            'departure_date' =>'required',
            'departure_time' =>'required',
            'arrival_time' =>'required ',
            'seat' =>'required | digits_between:1,10',
            'flight_price' =>'required | digits_between:1,10'
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        ]);

        $flight = new  FlightOneWay;
        $flight->supplier_id=Sentinel::getUser()->id;
        $flight->flight_source=$request->source;
        $flight->flight_destination=$request->destination;
        $flight->flight_name=$request->flight;
        $flight->flight_number=$request->flight_number;
        $flight->flight_departure=$request->departure_date;
        $flight->flight_departure_time=$request->departure_time;
        $flight->flight_arrival=$request->arrival_time;
        $flight->seat=$request->seat;
        $flight->flight_price=$request->flight_price;
<<<<<<< HEAD
        $flight->supplier_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_via=$request->via;
=======
        $flight->flight_pnr_no=$request->pnr_no;
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        $flight->save();
        return redirect('admin/oneWayFlights')->with(['success'=>'Flight added successfully']);
    }


     public function editOneWayFlight(FlightOneWay $flightOneWay,$id)
    {
        $flightSource=FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination=FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight=Flight::where('is_delete','1')->where('status','1')->get();
<<<<<<< HEAD
         $flightoneway = FlightOneWay::where('id',$id)->first();
        return view('admin.flightoneway.edit',compact('flightSource','flightDestination','flight','flightoneway'));
=======
        $flightoneway = FlightOneWay::where('id',$id)->first();
        return view('admin.flightOneWay.edit',compact('flightSource','flightDestination','flight','flightoneway'));
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    }

    public function updateOneWayFlight(Request $request, FlightOneWay $flightOneWay,$id)
    {
        $this->validate($request,[
            'source' =>'required',
            'destination' =>'required',
            'flight' =>'required',
            'flight_price' =>'required',
<<<<<<< HEAD
            'flight_number' =>'required|max:50',
            'departure_date' =>'required',
            'departure_time' =>'date_format:"H:i"|required',
            'arrival_time'   =>'date_format:"H:i"|required',
            'flight_price' =>'required | numeric|min:1|max:1000000',
            'seat' =>'nullable|numeric|min:1|max:200',
            'pnr_no' =>'nullable|numeric|digits_between:1,10',
            'via' =>'nullable|max:200',
=======
            'flight_number' =>'required',
            'departure_date' =>'required',
            'departure_time' =>'required',
            'arrival_time' =>'required ',
            'seat' =>'required | digits_between:1,10',
            'flight_price' =>'required | digits_between:1,10'
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        ]);

        $flight = FlightOneWay::find($id);
        $flight->flight_source=$request->source;
        $flight->flight_destination=$request->destination;
        $flight->flight_name=$request->flight;
        $flight->flight_number=$request->flight_number;
<<<<<<< HEAD

        $flight->flight_departure=$request->departure_date;
        $flight->flight_departure_time=$request->departure_time;
        $flight->flight_arrival=$request->arrival_time;
        if($request->seatcount =='add'&& $request->available > 0) {
            $flight->increment('seat', $request->available);
        }
        elseif($request->seatcount =='minus' && $request->available > 0){
            $flight->decrement('seat', $request->available);
        }
        $flight->flight_price=$request->flight_price;

        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_via=$request->via;
        $flight->save();
        return redirect('admin/oneWayFlights')->with(['success'=>'Flight  Updated successfully']);
    }
=======
        $flight->flight_price=$request->flight_price;
        $flight->flight_departure=$request->departure_date;
        $flight->flight_departure_time=$request->departure_time;
        $flight->flight_arrival=$request->arrival_time;
        $flight->seat=$request->seat;
        $flight->flight_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->save();
        return redirect('admin/oneWayFlights')->with(['success'=>'Flight  Updated successfully']);
    }
    


>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
     public function changeFlightStatus($flight_id,$status)
    {
         FlightOneWay::where('id',$flight_id)->update(['status'=>$status]);
         return redirect()->back()->with(['success' => 'Flight status updated successfully.']);
    }

<<<<<<< HEAD
    private function supplieradupdate($user,$text,$onewaybook){
        $admin_email = Sentinel::findById(2)->email;
        Mail::send('emails.updateblockoneway',[
            'user' => $user,
            'text' => $text,
            'onewaybook' => $onewaybook,
        ],function($message) use ($user, $text,$admin_email,$onewaybook) {
            $message->to($admin_email);
            $message->subject("$text");
        });
    }

=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

}
