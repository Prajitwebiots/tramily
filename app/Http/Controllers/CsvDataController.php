<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Psy\Exception\ErrorException;
use Sentinel;
use Hash;
use App\User;
use App\Models\FlightOneWay;
use App\Models\FlightRoundTrip;
use App\Models\FlightMultiCity;
use App\Models\Flight;
use App\Models\FlightSource;
use App\Models\FlightDestination;
use App\Models\Activation;
use App\Models\Addbalancehistory;
use Excel;
use Mail;
use DateTime;
use DB;


class CsvDataController extends Controller
{

    public function onewayimport(){
        $data = [];
       return  view('admin.csvimport.csvoneway',compact('data'));

    }
    public function onewayimportpost(Request $request){

        $data= [];
        $delimiter=',';
        $header = null;

        $this->validate($request, [
            'file' => 'required|mimes:csv,txt'
        ]);

            try {
                if ($request->file) {
                    $file = $request->file;
                    $subfile = '';
                    $destinationPath = 'assets/csv/';
                    $filename = studly_case(str_random(10) . $file->getClientOriginalName());
                    $file->move(public_path($destinationPath), $filename);
                    $name = $destinationPath . $filename;

                    if (($handle = fopen($destinationPath . $filename, 'r')) !== false) {
                        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                            if (!$header){

                                if (!$header){
                                    $header = $row;
                                    $array1 = $header;
                                    $array2 =array('id','flight_name', 'flight_number', 'flight_source', 'flight_destination', 'flight_departure', 'flight_departure_time', 'flight_arrival', 'flight_price', 'flight_pnr_no', 'flight_via', 'sold', 'seat');
                                    $result = array_diff($array1, $array2);
                                    if(count($result)!= 0)
                                        return redirect('one_way_import')->with(['error'=>" Something went wrong. Please try again."]);
                                }
                            }
                            else
                                $data[] = array_combine($header, $row);
                        }
                        fclose($handle);
                    }

                }
            }
            catch(\Exception $e){
                return redirect('one_way_import')->with(['error'=>" Something went wrong. Please try again."]);
                }

        return  view('admin.csvimport.csvoneway',compact('data','name'));
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function onewaysave(Request $request)
    {

        try{
            function validateDate($date)
            {
                $d = DateTime::createFromFormat('d-m-Y', $date);
                return  $d && $d->format('d-m-Y') == $date;
            }
            $header = "";
            $data = [];
            $statusdata=1;
            $save=array();
            if (($handle = fopen($request->onewayfile, 'r')) !== FALSE) {
                
               
                while (($row = fgetcsv($handle, 10000, ',')) !== FALSE) {
                    if (!$header){
                        $header = $row;
                        $array1 = $header;
                        $array2 =array('id', 'flight_name', 'flight_number', 'flight_source', 'flight_destination', 'flight_departure', 'flight_departure_time', 'flight_arrival', 'flight_price', 'flight_pnr_no', 'flight_via', 'sold', 'seat', 'status', 'is_delete', 'created_at', 'updated_at');
                        $result = array_diff($array1, $array2);
                        if(count($result)!= 0)
                            return redirect('one_way_import')->with(['error'=>" Something went wrong. Please try again."]);
                    }
                    else {
                      
                         $data = array_combine($header, $row);
                         

                        // get flight_name
                        $flight_name =  Flight::select('id')->where('flight_name',$data['flight_name'])->where('is_delete',1)->first();
                        if($flight_name != '') {$data['flight_name'] = $flight_name->id; } else { $statusdata=2;continue ;}

                        // get flight_source
                        echo  $flight_source =  FlightSource::select('id')->where('flight_source',$data['flight_source'])->where('is_delete',1)->first();
                        if($flight_source != '') {$data['flight_source'] = $flight_source->id; } else { $statusdata=3;continue ;}

                        // get flight_destination
                        $flight_destination =  FlightDestination ::select('id')->where('flight_destination',$data['flight_destination'])->where('is_delete',1)->first();
                        if($flight_destination != '') {$data['flight_destination'] = $flight_destination->id; } else { $statusdata=4;continue ;}


                        // get flight_destination
                        if(!validateDate($data['flight_departure']))
                        { $statusdata=5;continue ;}
                     
                        $save[]=$data;

                        // $oneway = new FlightOneWay ();
                        // $oneway->supplier_id = Sentinel::getuser()->id;
                        // $oneway->flight_name = $data['flight_name'];//1
                        // $oneway->flight_number = $data['flight_number'];//0
                        // $oneway->flight_source = $data['flight_source'];//1
                        // $oneway->flight_destination = $data['flight_destination'];//1
                        // $oneway->flight_departure = $data['flight_departure'];//0
                        // $oneway->flight_departure_time = $data['flight_departure_time'];
                        // $oneway->flight_arrival = $data['flight_arrival'];//0
                        // $oneway->flight_price = $data['flight_price'];//0
                        // $oneway->supplier_price = $data['flight_price'];//0
                        // $oneway->flight_pnr_no = $data['flight_pnr_no'];//0
                        // $oneway->flight_via = $data['flight_via'];//0
                        // $oneway->sold = 0;//0
                        // $oneway->seat = $data['seat'];//0
                        // $oneway->status = 2;//0
                        // $oneway->is_delete = 1;//0
                        // $oneway->created_at =  date("Y-m-d H:i:s");//0
                        // $oneway->updated_at =  date("Y-m-d H:i:s");//0
                        // $oneway->save();
                    }
                }
              
                        if($statusdata==1)
                        {
                           
                                foreach($save as $data)
                                {
                                   
                                      $oneway = new FlightOneWay ();
                                    $oneway->supplier_id = Sentinel::getuser()->id;
                                    $oneway->flight_name = $data['flight_name'];//1
                                    $oneway->flight_number = $data['flight_number'];//0
                                    $oneway->flight_source = $data['flight_source'];//1
                                    $oneway->flight_destination = $data['flight_destination'];//1
                                    $oneway->flight_departure = $data['flight_departure'];//0
                                    $oneway->flight_departure_time = $data['flight_departure_time'];
                                    $oneway->flight_arrival = $data['flight_arrival'];//0
                                    $oneway->flight_price = $data['flight_price'];//0
                                    $oneway->supplier_price = $data['flight_price'];//0
                                    $oneway->flight_pnr_no = $data['flight_pnr_no'];//0
                                    $oneway->flight_via = $data['flight_via'];//0
                                    $oneway->sold = 0;//0
                                    $oneway->seat = $data['seat'];//0
                                    $oneway->status = 2;//0
                                    $oneway->is_delete = 1;//0
                                    $oneway->created_at =  date("Y-m-d H:i:s");//0
                                    $oneway->updated_at =  date("Y-m-d H:i:s");//0
                                    $oneway->save();
                                   
                                    
                                }
                        }
                        else{
                            
                           return redirect('one_way_import')->with(['error'=>" Something went wrong. Please try again."]);
                        }
                fclose($handle);
                if(count($oneway) ==0)
                {
                    return redirect('one_way_import')->with(['error'=>" Something went wrong. Please try again."]);
                }
            }
            $data = [];
            return redirect('one_way_import')->with(['success'=>" Successfully add data "]);
        }
        catch(\Exception $e){
            return redirect('one_way_import')->with(['error'=>" Something went wrong. Please try again."]);
        }
    }

    /////// ********** round trip *************/////////////
    public function round_trip_import(){
        $data = [];
        return  view('admin.csvimport.cvs_round_trip',compact('data'));

    }
    public function round_trip_importpost(Request $request){

        $data= [];
        $delimiter=',';
        $header = null;


        $this->validate($request, [
            'file' => 'required|mimes:csv,txt'
        ]);

        try {
            if ($request->file) {
                $file = $request->file;
                $subfile = '';
                $destinationPath = 'assets/csv/';
                $filename = studly_case(str_random(10) . $file->getClientOriginalName());
                $file->move(public_path($destinationPath), $filename);
                $name = $destinationPath . $filename;

                if (($handle = fopen($destinationPath . $filename, 'r')) !== false) {
                    while (($row = fgetcsv($handle, 10000, $delimiter)) !== false) {
                        if (!$header){
                            $header = $row;
                             $array1 = $header;

                            $array2 =array('id','supplier_id','flight_name','flight_number','flight_source','flight_destination','flight_departure_date','flight_departure_time','flight_arrival_time','return_flight_source','return_flight_destination','return_flight_name','return_flight_number','return_departure_date','return_departure_time','return_arrival_time','flight_price','flight_pnr_no','flight_pnr_no2','flight_via','sold','seat','status','is_delete','created_at','updated_at');
                              $result = array_diff($array1, $array2);

                            if(count($result)!=0)

                                return redirect('round_trip_import')->with(['error'=>" Something went wrong. Please try again1."]);
                            }

                        else
                              $data[] = array_combine($header, $row);
                    }
                    fclose($handle);
                }
               }
        }
        catch(\Exception $e){
            return redirect('round_trip_import')->with(['error'=>" Something went wrong. Please try again."]);
        }


        return  view('admin.csvimport.cvs_round_trip',compact('data','name'));
    }

    public function round_tripsave(Request $request)
    {

        function validateDate($date)
        {
            $d = DateTime::createFromFormat('d-m-Y', $date);
            return  $d && $d->format('d-m-Y') == $date;
        }
        try {
            $data = [];
            $header="";
        $round_trip="";
         $statusdata=1;
            $save=array();
            if (($handle = fopen($request->onewayfile, 'r')) !== FALSE) {
                while (($row = fgetcsv($handle, 1000, ',')) !== FALSE) {
                    if (!$header)
                        $header = $row;
                    else {
                        
                        $array1 = $header;
                        $array2 = array('id',  'flight_name', 'flight_number', 'flight_source', 'flight_destination', 'flight_departure_date', 'flight_departure_time', 'flight_arrival_time', 'return_flight_source', 'return_flight_destination', 'return_flight_name', 'return_flight_number', 'return_departure_date', 'return_departure_time', 'return_arrival_time', 'flight_price', 'flight_pnr_no', 'flight_pnr_no2', 'flight_via', 'sold', 'seat');
                         $result = array_diff($array1, $array2);
                        if (count($result) != 0)
                            return redirect('round_trip_import')->with(['error' => " Something went wrong. Please try again."]);
                        else {
                            $data = array_combine($header, $row);


                            // get flight_name
                            $flight_name =  Flight::select('id')->where('flight_name',$data['flight_name'])->where('is_delete',1)->first();
                            if($flight_name != '') {  $data['flight_name'] = $flight_name->id; } else { $statusdata=2;continue ;}

                            // get flight_source
                            $flight_source =  FlightSource::select('id')->where('flight_source',$data['flight_source'])->where('is_delete',1)->first();
                            if($flight_source != '') { $data['flight_source'] = $flight_source->id; } else {$statusdata=3;continue ;}

                            // get flight_destination
                            $flight_destination =  FlightDestination ::select('id')->where('flight_destination',$data['flight_destination'])->where('is_delete',1)->first();
                            if($flight_destination != '') {$data['flight_destination'] = $flight_destination->id; } else {$statusdata=4;continue ;}

                            // get return_flight_name
                             $return_flight_name =  Flight::select('id')->where('flight_name',$data['return_flight_name'])->where('is_delete',1)->first();
                            if($return_flight_name != '') { $data['return_flight_name'] = $return_flight_name->id; } else {$statusdata=5;continue ;}

                            // get return_flight_source // save

                              $return_flight_name =  FlightSource::select('id')->where('flight_source',$data['return_flight_source'])->where('is_delete',1)->first();
                            if($return_flight_name != '') {  $data['return_flight_source'] = $return_flight_name->id; } else {$statusdata=6;continue ;}


                            // get return_flight_destination
                               $return_flight_destination =  FlightDestination ::select('id')->where('flight_destination',$data['return_flight_destination'])->where('is_delete',1)->first();
                             if($return_flight_destination != '') { $data['return_flight_destination'] = $return_flight_destination->id; } else {$statusdata=7;continue ;}

                             if(!validateDate($data['flight_departure_date']))
                             {
                                 $statusdata=8;continue ;
                             }
                            if(!validateDate($data['return_departure_date']))
                            {

                                $statusdata=9;continue ;
                            }
                             $save[]=$data;


                                // $round_trip = new FlightRoundTrip ();
                                // $round_trip->supplier_id = Sentinel::getuser()->id;
                                // $round_trip->flight_name = $data['flight_name']; //1
                                // $round_trip->flight_number = $data['flight_number']; //0
                                // $round_trip->flight_source = $data['flight_source']; //1
                                // $round_trip->flight_destination = $data['flight_destination']; //1
                                // $round_trip->flight_departure_date = $data['flight_departure_date'];//0
                                // $round_trip->flight_departure_time = $data['flight_departure_time'];//0
                                // $round_trip->flight_arrival_time = $data['flight_arrival_time'];//0
                                // $round_trip->return_flight_source =  $data['flight_destination'];//1
                                // $round_trip->return_flight_destination = $data['flight_source'];//1
                                // $round_trip->return_flight_name = $data['return_flight_name'];//1
                                // $round_trip->return_flight_number = $data['return_flight_number'];//0
                                // $round_trip->return_departure_date = $data['return_departure_date'];//0
                                // $round_trip->return_departure_time = $data['return_departure_time'];//0
                                // $round_trip->return_arrival_time = $data['return_arrival_time'];//0
                                // $round_trip->flight_price = $data['flight_price'];//0
                                // $round_trip->supplier_price = $data['flight_price'];//0
                                // $round_trip->flight_pnr_no = $data['flight_pnr_no'];//0
                                // $round_trip->flight_pnr_no2 = $data['flight_pnr_no2'];//0
                                // $round_trip->flight_via = $data['flight_via'];//0
                                // $round_trip->sold = 0;//0
                                // $round_trip->seat = $data['seat'];//0
                                // $round_trip->status = 2;//0
                                // $round_trip->is_delete = 1;//0
                                // $round_trip->created_at =  date("Y-m-d H:i:s");//0
                                // $round_trip->updated_at =  date("Y-m-d H:i:s");//0
                                // $round_trip->save();

                        }
                    }
                }
                if($statusdata==1)
                        {
                            // return $save;
                            
                           
                                foreach($save as $data)
                                {
                                   
                                    $round_trip = new FlightRoundTrip ();
                                $round_trip->supplier_id = Sentinel::getuser()->id;
                                $round_trip->flight_name = $data['flight_name']; //1
                                $round_trip->flight_number = $data['flight_number']; //0
                                $round_trip->flight_source = $data['flight_source']; //1
                                $round_trip->flight_destination = $data['flight_destination']; //1
                                $round_trip->flight_departure_date = $data['flight_departure_date'];//0
                                $round_trip->flight_departure_time = $data['flight_departure_time'];//0
                                $round_trip->flight_arrival_time = $data['flight_arrival_time'];//0
                                $round_trip->return_flight_source =  $data['flight_destination'];//1
                                $round_trip->return_flight_destination = $data['flight_source'];//1
                                $round_trip->return_flight_name = $data['return_flight_name'];//1
                                $round_trip->return_flight_number = $data['return_flight_number'];//0
                                $round_trip->return_departure_date = $data['return_departure_date'];//0
                                $round_trip->return_departure_time = $data['return_departure_time'];//0
                                $round_trip->return_arrival_time = $data['return_arrival_time'];//0
                                $round_trip->flight_price = $data['flight_price'];//0
                                $round_trip->supplier_price = $data['flight_price'];//0
                                $round_trip->flight_pnr_no = $data['flight_pnr_no'];//0
                                $round_trip->flight_pnr_no2 = $data['flight_pnr_no2'];//0
                                $round_trip->flight_via = $data['flight_via'];//0
                                $round_trip->sold = 0;//0
                                $round_trip->seat = $data['seat'];//0
                                $round_trip->status = 2;//0
                                $round_trip->is_delete = 1;//0
                                $round_trip->created_at =  date("Y-m-d H:i:s");//0
                                $round_trip->updated_at =  date("Y-m-d H:i:s");//0
                                $round_trip->save();
                                   
                                    
                                }
                        }
                        else{
                             
                           return redirect('round_trip_import')->with(['error'=>" Something went wrong. Please try again."]);
                        }
                if(count($round_trip) ==0)
                {
                    return redirect('round_trip_import')->with(['error'=>" Something went wrong. Please try again."]);
                }
                fclose($handle);
            }
            $data = [];
            return redirect('round_trip_import')->with(['success'=>" Successfully add data "]);
        }
        catch(\Exception $e){
            return redirect('round_trip_import')->with(['error'=>" Something went wrong. Please try again."]);
        }
    }

    /////// ***********************/////////////
    public function multiCity_import(){
        $data = [];
        return  view('admin.csvimport.csvmulti_City',compact('data'));

    }
    public function multiCity_importpost(Request $request){

        $data= [];
        $delimiter=',';
        $header = null;

        $this->validate($request, [
            'file' => 'required|mimes:csv,txt'
        ]);

        try {
            if ($request->file) {
               $file = $request->file;
                $subfile = '';
                $destinationPath = 'assets/csv/';
                $filename = studly_case(str_random(10) . $file->getClientOriginalName());
                $file->move(public_path($destinationPath), $filename);
                $name = $destinationPath . $filename;

                if (($handle = fopen($destinationPath . $filename, 'r')) !== false) {
                    while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                        if (!$header){
                            $header = $row;
                            $array1 = $header;
                            $array2 =array('id',  'flight_name_f', 'flight_number_f', 'flight_source_f', 'flight_destination_f', 'flight_departure_date_f', 'flight_departure_time_f', 'flight_arrival_time_f', 'flight_name_s', 'flight_number_s', 'flight_source_s', 'flight_destination_s', 'flight_departure_date_s', 'flight_departure_time_s', 'flight_arrival_time_s', 'flight_name_t', 'flight_number_t', 'flight_source_t', 'flight_destination_t', 'flight_departure_date_t', 'flight_departure_time_t', 'flight_arrival_time_t', 'flight_price', 'flight_pnr_no', 'flight_pnr_no2', 'flight_pnr_no3', 'flight_via', 'sold', 'seat');
                            $result = array_diff($array1, $array2);
                            if(count($result)!= 0)
                                return redirect('multiCity_import')->with(['error'=>" Something went wrong. Please try again1."]);
                        }
                        else
                            $data[] = array_combine($header, $row);
                    }
                    fclose($handle);
                }
            }
        }
        catch(\Exception $e){
            return redirect('multiCity_import')->with(['error'=>" Something went wrong. Please try again."]);
        }

        return  view('admin.csvimport.csvmulti_City',compact('data','name'));
    }

    public function multiCity_save(Request $request)
    {
        try {
            function validateDate($date)
            {
                $d = DateTime::createFromFormat('d-m-Y', $date);
                return  $d && $d->format('d-m-Y') == $date;
            }
            $header = "";
            $data = [];
              $statusdata=1;
            $multiCity=[];
            $save=array();
            if (($handle = fopen($request->multicity, 'r')) !== FALSE) {
                while (($row = fgetcsv($handle, 10000, ',')) !== FALSE) {
                    if (!$header){
                        $header = $row;
                        $array1 = $header;
                        $array2 =array('id', 'flight_name_f', 'flight_number_f', 'flight_source_f', 'flight_destination_f', 'flight_departure_date_f', 'flight_departure_time_f', 'flight_arrival_time_f', 'flight_name_s', 'flight_number_s', 'flight_source_s', 'flight_destination_s', 'flight_departure_date_s', 'flight_departure_time_s', 'flight_arrival_time_s', 'flight_name_t', 'flight_number_t', 'flight_source_t', 'flight_destination_t', 'flight_departure_date_t', 'flight_departure_time_t', 'flight_arrival_time_t', 'flight_price', 'flight_pnr_no', 'flight_pnr_no2', 'flight_pnr_no3', 'flight_via', 'sold', 'seat');
                        $result = array_diff($array1, $array2);
                        if(count($result)!= 0)
                            return redirect('multiCity_import')->with(['error'=>" Something went wrong. Please try again."]);
                    }
                    else {
                       
                          $data = array_combine($header, $row);
                        // get flight_name
                          $flight_name_f =  Flight::select('id')->where('flight_name',$data['flight_name_f'])->where('is_delete',1)->first();
                        if($flight_name_f != '') {   $data['flight_name_f'] = $flight_name_f->id; } else {  $statusdata=2;continue ;}
                        

                        // get flight_source
                        $flight_source_f =  FlightSource::select('id')->where('flight_source',$data['flight_source_f'])->where('is_delete',1)->first();
                        if($flight_source_f != '') { $data['flight_source_f'] = $flight_source_f->id; } else {  $statusdata=3;continue ;}

                        // get flight_destination
                         $flight_destination_f =  FlightDestination ::select('id')->where('flight_destination',$data['flight_destination_f'])->where('is_delete',1)->first();
                        if($flight_destination_f != '') {$data['flight_destination_f'] = $flight_destination_f->id; } else {  $statusdata=4;continue ;}
 
                        // ///
                        // get flight_name
                         $flight_name_s =  Flight::select('id')->where('flight_name',$data['flight_name_s'])->where('is_delete',1)->first();
                        if($flight_name_s != '') {   $data['flight_name_s'] = $flight_name_s->id; } else {  $statusdata=5;continue ;}

                        // get flight_source
                        $flight_source_s =  FlightSource::select('id')->where('flight_source',$data['flight_source_s'])->where('is_delete',1)->first();
                        if($flight_source_s != '') { $data['flight_source_s'] = $flight_source_s->id; } else {  $statusdata=6;continue ;}
                       

                        // get flight_destination
                         $flight_destination_s =  FlightDestination ::select('id')->where('flight_destination',$data['flight_destination_s'])->where('is_delete',1)->first();
                        if($flight_destination_s != '') {$data['flight_destination_s'] = $flight_destination_s->id; } else {  $statusdata=7;continue ;}



                        // get flight_name_T
                          $flight_name_t =  Flight::select('id')->where('flight_name',$data['flight_name_t'])->where('is_delete',1)->first();
                        if($flight_name_t != '') {   $data['flight_name_t'] = $flight_name_t->id; } elseif($data['flight_name_t']==""){$data['flight_name_t']= NULL;$data['flight_number_t']= NULL;} else {  $statusdata=8;continue ;}

                        // get flight_tource_T
                        $flight_source_t =  FlightSource::select('id')->where('flight_source',$data['flight_source_t'])->where('is_delete',1)->first();
                        if($flight_source_t != '') { $data['flight_source_t'] = $flight_source_t->id; } elseif($data['flight_source_t']==""){$data['flight_source_t']=NULL;}  else {  $statusdata=9;continue ;}

                        // get flight_destination_T
                        $flight_destination_t =  FlightDestination ::select('id')->where('flight_destination',$data['flight_destination_t'])->where('is_delete',1)->first();
                        if($flight_destination_t != '') {$data['flight_destination_t'] = $flight_destination_t->id; } elseif($data['flight_destination_t']==""){$data['flight_destination_t']=NULL;}  else {  $statusdata=10;continue ;}

                        if(!validateDate($data['flight_departure_date_f']))
                        {  $statusdata=11;
                            continue ;
                        }
                        if(!validateDate($data['flight_departure_date_s']))
                        {
                        $statusdata=12;
                            continue ;
                        }
                        if(!validateDate($data['flight_departure_date_t']) && !$data['flight_departure_date_t']=="")
                        {  $statusdata=13;

                            continue ;
                        }
                        
                     $save[]= $data;
                    }
                   
                }
                
                if($statusdata==1)
                        {
                           
                         
                       foreach($save as $data)
                            {
                                   $multiCity = new FlightMultiCity ();
                                   $multiCity->supplier_id = Sentinel::getuser()->id;
                                   $multiCity->flight_name_f = $data['flight_name_f'];//1
                                   $multiCity->flight_number_f = $data['flight_number_f'];//0
                                   $multiCity->flight_source_f = $data['flight_source_f'];//1
                                   $multiCity->flight_destination_f = $data['flight_destination_f'];//1
                                   $multiCity->flight_departure_date_f = $data['flight_departure_date_f'];//0
                                   $multiCity->flight_departure_time_f = $data['flight_departure_time_f'];//0
                                   $multiCity->flight_arrival_time_f = $data['flight_arrival_time_f'];//0
                                   $multiCity->flight_name_s = $data['flight_name_s'];//1
                                   $multiCity->flight_number_s = $data['flight_number_s'];
                                   $multiCity->flight_source_s = $data['flight_source_s'];//1
                                   $multiCity->flight_destination_s = $data['flight_destination_s'];//1
                                   $multiCity->flight_departure_date_s = $data['flight_departure_date_s'];//0
                                   $multiCity->flight_departure_time_s = $data['flight_departure_time_s'];//0
                                   $multiCity->flight_arrival_time_s = $data['flight_arrival_time_s'];//0
                                   $multiCity->flight_name_t = $data['flight_name_t'];//1
                                   $multiCity->flight_number_t = $data['flight_number_t'];//0
                                   $multiCity->flight_source_t = $data['flight_source_t'];//1
                                   $multiCity->flight_destination_t = $data['flight_destination_t'];//1
                                   $multiCity->flight_departure_date_t = $data['flight_departure_date_t'];//0
                                   $multiCity->flight_departure_time_t = $data['flight_departure_time_t'];//0
                                   $multiCity->flight_arrival_time_t = $data['flight_arrival_time_t'];//0
                                   $multiCity->flight_price = $data['flight_price'];//0
                                   $multiCity->supplier_price = $data['flight_price'];//0
                                   $multiCity->flight_pnr_no = $data['flight_pnr_no'];//0
                                   $multiCity->flight_pnr_no2 = $data['flight_pnr_no2'];//0
                                   $multiCity->flight_pnr_no3 = $data['flight_pnr_no3'];//0
                                   $multiCity->flight_via = $data['flight_via'];//0
                                   $multiCity->sold = $data['sold'];//0
                                   $multiCity->seat = $data['seat'];//0
                                   $multiCity->status = 2;//0
                                   $multiCity->is_delete = 1;//0
                                   $multiCity->created_at =  date("Y-m-d H:i:s");//0
                                   $multiCity->updated_at =  date("Y-m-d H:i:s");//0
                                   $multiCity->save();
                               
                                
                            }
                        }
                        else{
                            return $save;
                              
                           return redirect('multiCity_import')->with(['error'=>" Something went wrong. Please try again."]);
                        }

                fclose($handle);
                
              

                if(count($multiCity) == 0)
                {
                  //  return redirect('multiCity_import')->with(['error'=>" Something went wrong. Please try again."]);
                }
            }
            $data = [];
            return redirect('multiCity_import')->with(['success'=>" Successfully add data  "]);
        }
        catch(\Exception $e){
            return redirect('multiCity_import')->with(['error'=>" Something went wrong. Please try again."]);
        }
    }
}
