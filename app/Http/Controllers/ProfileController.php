<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Hash;
use App\User;
use App\Models\RoleUser;
use App\Models\Activation;

class ProfileController extends Controller
{
    
    public function updateProfile(Request $request,$id) {
      
        $this->validate($request, [
                'agency_name'  => 'required|max:255',
                'title_name' => 'required|max:255',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'agency_address'  => 'required|max:255',
                'city'          => 'required|max:20',
                'state'         => 'required|max:20',
                'country'       => 'required|max:20',
                'pincode'       => 'required|digits:6',
                'mobile'        => 'required|digits_between:10,13',
                'office_no'     => 'required|max:20',
                'gst_full_name'   => 'required|max:50',
                'gst_state'   => 'required|max:50',
                'pan_no'   => 'nullable |max:10',
                'gst_address'   => 'nullable |max:200',
                'gst_phone'   => 'nullable |min:10|max:13',
                'regi_gmail'   => 'nullable |email|max:254',
                'gst_no'   => 'required|min:15|max:15',
            ]);

        $id = $request->user_id;
        $user = User::find($id);

    if (!is_null($request->photo)) {
        $file = $request->file('photo');
        $destinationPath = './assets/images/user/';
        $filename = $file->getClientOriginalName();
        $file->move($destinationPath, $filename);
        $user->profile = $filename;
        }
         $user->first_name=$request->first_name;
         $user->last_name=$request->last_name;
         $user->title_name=$request->title_name;
        $user->agency_name=$request->agency_name;
        $user->agency_address=$request->agency_address;
        $user->mobile=$request->mobile;
        $user->office_no=$request->office_no;
        $user->city=$request->city;
        $user->state=$request->state;
        $user->country=$request->country;
        $user->pincode=$request->pincode;
        $user->gst_name=$request->gst_full_name;
        $user->gst_state=$request->gst_state;
        $user->pan_no=$request->pan_no;
        $user->gst_no=$request->gst_no;
        $user->gst_address=$request->gst_address;
        $user->gst_phone=$request->gst_phone;
        $user->regi_gmail=$request->regi_gmail;
        $user->save();

        return redirect()->back()->with(['success' => 'Your Profile updated successfully']);

     } 

     public function updatePassword(Request $request ,$id) {
          if (Sentinel::check()) {
            $id=Sentinel::getUser()->id;
            $this->validate($request, [
                'old_password'      =>'required|string|min:6',
                'new_password'      => 'required|string|min:6',
                'confirm_password'  => 'required|string|min:6|same:new_password',
            ]);

            $old_password=$request->old_password;
            $current_password = Sentinel::getUser()->password;
            if(Hash::check($old_password, $current_password))
            {   
                $user = User::find($id);
                $user->password=bcrypt($request->new_password);
                $user->save();
                // return redirect('admin/profile')->with('successpass',"Password updated successfully");
                return redirect()->back()->with(['success' => 'Your Password updated successfully']);
            }
            else {
                // return redirect('admin/profile')->with('error', "Invalid  Old password. Please try again");
                return redirect()->back()->with(['errorpass' => 'Invalid  Old password. Please try again']);
            }
        }
        else {
           // return redirect('login')->with('error',"login to change password ");
            return redirect()->back()->with(['errorpass' => 'login to change password ']);
        }
    } 


    public function updateProfileadmin(Request $request,$id) {
       
        $this->validate($request, [
                
                'title_name' => 'required|max:255',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                
            ]);

        $id = $request->user_id;
        $user = User::find($id);

    if (!is_null($request->photo)) {
        $file = $request->file('photo');
        $destinationPath = './assets/images/user/';
        $filename = $file->getClientOriginalName();
        $file->move($destinationPath, $filename);
        $user->profile = $filename;
        }
         $user->first_name=$request->first_name;
         $user->last_name=$request->last_name;
         $user->title_name=$request->title_name;
       
        $user->save();

        return redirect()->back()->with(['success' => 'Your Profile updated successfully']);

     } 
}
