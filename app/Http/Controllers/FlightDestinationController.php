<?php

namespace App\Http\Controllers;

use App\Models\FlightDestination;
use Illuminate\Http\Request;

class FlightDestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
<<<<<<< HEAD
        $flight=FlightDestination::where('is_delete','1')->orderBy('id', 'desc')->get();
=======
        $flight=FlightDestination::where('is_delete','1')->get();
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        return view('admin.flightDestination.index',compact('flight'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.flightDestination.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'flight_destination' =>'required|unique:flight_destinations'
        ]);

        $flight = new  FlightDestination;
        $flight->flight_destination=$request->flight_destination;
        $flight->save();
        return redirect()->route('admin-flightDestination.index')->with(['success'=>'Flight Destination added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FlightDestination  $flightDestination
     * @return \Illuminate\Http\Response
     */
    public function show(FlightDestination $flightDestination)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FlightDestination  $flightDestination
     * @return \Illuminate\Http\Response
     */
    public function edit(FlightDestination $flightDestination,$id)
    {
        $flight=FlightDestination::where('id',$id)->first();
        return view('admin.flightDestination.edit',compact('flight'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FlightDestination  $flightDestination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FlightDestination $flightDestination,$id)
    {
        $this->validate($request,[
            'flight_destination' =>'required'
        ]);
        $flight = FlightDestination::find($id);
        $flight->flight_destination=$request->flight_destination;
        $flight->save();
        return redirect()->route('admin-flightDestination.index')->with(['success'=>'Flight Destination Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FlightDestination  $flightDestination
     * @return \Illuminate\Http\Response
     */
    public function destroy(FlightDestination $flightDestination)
    {
        //
    }

    public function flightDestinationStatus($flight_id,$status)
    {
         FlightDestination::where('id',$flight_id)->update(['status'=>$status]);
         return redirect()->back()->with(['success' => 'Flight status updated successfully.']);
    }

    public function flightDestinationIsDelete($flight_id,$status)
    {
         FlightDestination::where('id',$flight_id)->update(['is_delete'=>$status]);
         return redirect()->back()->with(['success' => 'Deleted successfully.']);
    }
}
