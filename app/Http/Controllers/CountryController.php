<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country=country::where('is_delete','1')->get();
        return view('admin.country.index',compact('country'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.country.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'country_name' =>'required|max:50',
            'country_alphacode' =>'required|max:20'
        ]);

        $country = new  Country;
        $country->country_name=$request->country_name;
        $country->country_alphacode=$request->country_alphacode;
        $country->save();
        return redirect()->route('admin-country.index')->with(['success'=>'Country added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $countrym,$id)
    {
        $country=Country::where('id',$id)->first();
        return view('admin.country.edit',compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country,$id)
    {
        $this->validate($request,[
            'country_name' =>'required|max:50',
            'country_alphacode' =>'required|max:20'
        ]);
        
        $country = Country::find($id);
        $country->country_name=$request->country_name;
        $country->country_alphacode=$request->country_alphacode;
        $country->save();
        return redirect()->route('admin-country.index')->with(['success'=>'Country Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        //
    }


    public function countryStatus($country_id,$status)
    {
         Country::where('id',$country_id)->update(['status'=>$status]);
         return redirect()->back()->with(['success' => 'Country status updated successfully.']);
    }

    public function countryIsDelete($country_id,$status)
    {
         Country::where('id',$country_id)->update(['is_delete'=>$status]);
         return redirect()->back()->with(['success' => 'Deleted successfully.']);
    }
}
