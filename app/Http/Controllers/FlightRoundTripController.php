<?php

namespace App\Http\Controllers;

use App\Models\FlightRoundTrip;
use App\Models\FlightDestination;
use App\Models\Flight;
use App\Models\FlightSource;
use Illuminate\Http\Request;
use Sentinel;
<<<<<<< HEAD
use Mail;
use View;
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

class FlightRoundTripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Sentinel::getUser()->id;
        $flight=FlightRoundTrip::with('flight','flightSource','flightDestination','flightReturn','flightSourceReturn','flightDestinationReturn')->orderBy('created_at', 'DESC')->where('is_delete','1')->where('supplier_id',$id)->get();
        return view('supplier.flightroundtrip.index',compact('flight'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $flightSource=FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination=FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight=Flight::where('is_delete','1')->where('status','1')->get();
        return view('supplier.flightroundtrip.create',compact('flightSource','flightDestination','flight'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'source' =>'required',
            'destination' =>'required',
            'flight_name' =>'required',
<<<<<<< HEAD
            'flight_number' =>'required|max:50',
            'departure_date' =>'required',
            'departure_time' =>'date_format:"H:i"|required',
            'arrival_time' =>  'date_format:"H:i"|required',
            'return_flight_name' =>'required',
            'return_flight_number' =>'required|digits_between:1,10|numeric',
            'return_departure_date' =>'required|after_or_equal:departure_date',
            'return_departure_time' =>'date_format:"H:i"|required',
            'return_arrival_time' =>  'date_format:"H:i"|required',
            'flight_price' =>'required | numeric|min:1|max:1000000',
            'seat' =>'required | numeric|min:1|max:200',
            'flight_pnr_no' =>'nullable| digits_between:1,10',
            'flight_pnr_no2' =>'nullable| digits_between:1,10',
            'via' =>'nullable|max:200',
=======
            'flight_number' =>'required',
            'departure_date' =>'required',
            'departure_time' =>'required',
            'arrival_time' =>'required ',
            'return_flight_name' =>'required',
            'return_flight_number' =>'required',
            'return_departure_date' =>'required',
            'return_departure_time' =>'required',
            'return_arrival_time' =>'required ',
            'flight_price' =>'required | digits_between:1,10',
            'seat' =>'required | digits_between:1,10',
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

        ]);

        $flight = new  FlightRoundTrip;
        $flight->supplier_id=Sentinel::getUser()->id;
        $flight->flight_source=$request->source;
        $flight->flight_destination=$request->destination;
        $flight->return_flight_source=$request->destination;
        $flight->return_flight_destination=$request->source;
        $flight->flight_name=$request->flight_name;
        $flight->flight_number=$request->flight_number;
        $flight->flight_departure_date=$request->departure_date;
        $flight->flight_departure_time=$request->departure_time;
        $flight->flight_arrival_time=$request->arrival_time;
        $flight->return_flight_name=$request->return_flight_name;
        $flight->return_flight_number=$request->return_flight_number;
        $flight->return_departure_date=$request->return_departure_date;
        $flight->return_departure_time=$request->return_departure_time;
        $flight->return_arrival_time=$request->return_arrival_time;
        $flight->seat=$request->seat;
        $flight->flight_price=$request->flight_price;
<<<<<<< HEAD
        $flight->supplier_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
         $flight->flight_via=$request->via;
        $flight->save();


        $roundtripbook=FlightRoundTrip::with('flight','flightSource','flightDestination','flightReturn','flightSourceReturn','flightDestinationReturn')->find($flight->id);

        $user = Sentinel::getUser();
        $text = 'Supplier Add New  Block';
        $this->supplieradupdate($user,$text,$roundtripbook); // this is email sent to admin for new supplier


=======
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
        $flight->save();
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        return redirect()->route('supplier-FlightRoundTrip.index')->with(['success'=>'Flight added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FlightRoundTrip  $flightRoundTrip
     * @return \Illuminate\Http\Response
     */
    public function show(FlightRoundTrip $flightRoundTrip)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FlightRoundTrip  $flightRoundTrip
     * @return \Illuminate\Http\Response
     */
    public function edit(FlightRoundTrip $flightRoundTrip,$id)
    {
        $flightSource=FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination=FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight=Flight::where('is_delete','1')->where('status','1')->get();
        $flightroundtrip = FlightRoundTrip::where('id',$id)->first();
        return view('supplier.flightroundtrip.edit',compact('flightSource','flightDestination','flight','flightroundtrip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FlightRoundTrip  $flightRoundTrip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FlightRoundTrip $flightRoundTrip,$id)
    {

         $this->validate($request,[
<<<<<<< HEAD
             'source' =>'required',
             'destination' =>'required',
             'flight_name' =>'required',
             'flight_number' =>'required|max:50',
             'departure_date' =>'required',
             'departure_time' =>'date_format:"H:i"|required',
             'arrival_time' =>  'date_format:"H:i"|required',
             'return_flight_name' =>'required',
             'return_flight_number' =>'required|digits_between:1,10|numeric',
             'return_departure_date' =>'required|after_or_equal:departure_date',
             'return_departure_time' =>'date_format:"H:i"|required',
             'return_arrival_time' =>  'date_format:"H:i"|required',
             'flight_price' =>'nullable | numeric|min:1|max:1000000',
             'seat' =>'nullable|integer|min:1|max:200',
             'available' =>'nullable|integer|min:1|max:200',
             'flight_pnr_no' =>'nullable| digits_between:1,10',
             'flight_pnr_no2' =>'nullable| digits_between:1,10',
             'via' =>'nullable|max:200',
=======
            'source' =>'required',
            'destination' =>'required',
            'flight_name' =>'required',
            'flight_number' =>'required',
            'departure_date' =>'required',
            'departure_time' =>'required',
            'arrival_time' =>'required ',
            'return_flight_name' =>'required',
            'return_flight_number' =>'required',
            'return_departure_date' =>'required',
            'return_departure_time' =>'required',
            'return_arrival_time' =>'required ',
            'flight_price' =>'required | digits_between:1,10',
            'seat' =>'required | digits_between:1,10',
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

        ]);

        $flight = FlightRoundTrip::find($id);
        $flight->supplier_id=Sentinel::getUser()->id;
        $flight->flight_source=$request->source;
        $flight->flight_destination=$request->destination;
        $flight->return_flight_source=$request->destination;
        $flight->return_flight_destination=$request->source;
        $flight->flight_name=$request->flight_name;
        $flight->flight_number=$request->flight_number;
        $flight->flight_departure_date=$request->departure_date;
        $flight->flight_departure_time=$request->departure_time;
        $flight->flight_arrival_time=$request->arrival_time;
        $flight->return_flight_name=$request->return_flight_name;
        $flight->return_flight_number=$request->return_flight_number;
        $flight->return_departure_date=$request->return_departure_date;
        $flight->return_departure_time=$request->return_departure_time;
        $flight->return_arrival_time=$request->return_arrival_time;
<<<<<<< HEAD
        if($request->seatcount =='add'&& $request->available > 0) {
            $flight->increment('seat', $request->available);
        }
        elseif($request->seatcount =='minus' && $request->available > 0){
            $flight->decrement('seat', $request->available);
        }
        $flight->supplier_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
         $flight->flight_via=$request->via;
        $flight->save();


        $roundtripbook=FlightRoundTrip::with('flight','flightSource','flightDestination','flightReturn','flightSourceReturn','flightDestinationReturn')->find($flight->id);

        $user = Sentinel::getUser();
        $text = 'Supplier edit  block';
          $this->supplieradupdate($user,$text,$roundtripbook); // this is email sent to admin for new supplier
        // return view('emails.updateblockroundtrip',compact('user','roundtripbook','text'));
        
=======
        $flight->seat=$request->seat;
        $flight->flight_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
        $flight->save();
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        return redirect()->route('supplier-FlightRoundTrip.index')->with(['success'=>'Flight updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FlightRoundTrip  $flightRoundTrip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
       // return $request->all();

        FlightRoundTrip::wherein('id', $request->check)->update(['is_delete' => 0]);
        return redirect()->back()->with('success', 'Successfully Deleted ');
    }


     public function flightIsDelete($flight_id,$status)
    {
         FlightRoundTrip::where('id',$flight_id)->update(['is_delete'=>$status]);
         return redirect()->back()->with(['success' => 'Flight Deleted successfully.']);
    }


    /**  For Admin Function **/ 

    public function listRoundTripFlight()
    {
        $flight=FlightRoundTrip::with('flight','flightSource','flightDestination','flightReturn','flightSourceReturn','flightDestinationReturn')->orderBy('created_at', 'DESC')->where('is_delete','1')->get();
        return view('admin.flightroundtrip.index',compact('flight'));
    }

    public function createRoundTripFlight()
    {
        $flightSource=FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination=FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight=Flight::where('is_delete','1')->where('status','1')->get();
        return view('admin.flightroundtrip.create',compact('flightSource','flightDestination','flight'));
    }

     public function storeFlightRoundTrip(Request $request)
    {
        $this->validate($request,[
            'source' =>'required',
            'destination' =>'required',
            'flight_name' =>'required',
<<<<<<< HEAD
            'flight_number' =>'required|max:50',
            'departure_time' =>'date_format:"H:i"|required',
            'arrival_time' =>  'date_format:"H:i"|required',
            'arrival_time' =>'required ',
            'return_flight_name' =>'required',
            'return_flight_number' =>'required|max:50',
            'return_departure_date' =>'required',
            'flight_price' =>'required | numeric|min:1|max:1000000',
            'seat' =>'required | integer|min:1|max:200',
            'flight_pnr_no' =>'nullable| digits_between:1,10',
            'flight_pnr_no2' =>'nullable| digits_between:1,10',
            'via' =>'nullable|max:200',
=======
            'flight_number' =>'required',
            'departure_date' =>'required',
            'departure_time' =>'required',
            'arrival_time' =>'required ',
            'return_flight_name' =>'required',
            'return_flight_number' =>'required',
            'return_departure_date' =>'required',
            'return_departure_time' =>'required',
            'return_arrival_time' =>'required ',
            'flight_price' =>'required | digits_between:1,10',
            'seat' =>'required | digits_between:1,10',
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

        ]);

        $flight = new  FlightRoundTrip;
        $flight->supplier_id=Sentinel::getUser()->id;
        $flight->flight_source=$request->source;
        $flight->flight_destination=$request->destination;
        $flight->return_flight_source=$request->destination;
        $flight->return_flight_destination=$request->source;
        $flight->flight_name=$request->flight_name;
        $flight->flight_number=$request->flight_number;
        $flight->flight_departure_date=$request->departure_date;
        $flight->flight_departure_time=$request->departure_time;
        $flight->flight_arrival_time=$request->arrival_time;
        $flight->return_flight_name=$request->return_flight_name;
        $flight->return_flight_number=$request->return_flight_number;
        $flight->return_departure_date=$request->return_departure_date;
        $flight->return_departure_time=$request->return_departure_time;
        $flight->return_arrival_time=$request->return_arrival_time;
        $flight->seat=$request->seat;
        $flight->flight_price=$request->flight_price;
<<<<<<< HEAD
        $flight->supplier_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
         $flight->flight_via=$request->via;
=======
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        $flight->save();
        return redirect('admin/roundTripFlights')->with(['success'=>'Flight added successfully']);
    }

     public function editRoundTripFlight($id)
    {
        $flightSource=FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination=FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight=Flight::where('is_delete','1')->where('status','1')->get();
        $flightroundtrip = FlightRoundTrip::where('id',$id)->first();
        return view('admin.flightroundtrip.edit',compact('flightSource','flightDestination','flight','flightroundtrip'));
    }

     public function updateRoundTripFlight(Request $request,$id)
    {
        $this->validate($request,[
            'source' =>'required',
            'destination' =>'required',
            'flight_name' =>'required',
<<<<<<< HEAD
            'flight_number' =>'required|max:50',
            'departure_date' =>'required',
            'departure_time' =>'date_format:"H:i"|required',
            'arrival_time' =>  'date_format:"H:i"|required',
            'return_flight_name' =>'required',
            'return_flight_number' =>'required|digits_between:1,10|numeric',
            'return_departure_date' =>'required',
            'return_departure_time' =>'date_format:"H:i"|required',
            'return_arrival_time' =>  'date_format:"H:i"|required',
            'flight_price' =>'required | numeric|min:1|max:1000000',
            'available' =>'nullable | numeric|min:1|max:200',
            'flight_pnr_no' =>'nullable| digits_between:1,10',
            'flight_pnr_no2' =>'nullable| digits_between:1,10',
            'via' =>'nullable|max:200',
=======
            'flight_number' =>'required',
            'departure_date' =>'required',
            'departure_time' =>'required',
            'arrival_time' =>'required ',
            'return_flight_name' =>'required',
            'return_flight_number' =>'required',
            'return_departure_date' =>'required',
            'return_departure_time' =>'required',
            'return_arrival_time' =>'required ',
            'flight_price' =>'required | digits_between:1,10',
            'seat' =>'required | digits_between:1,10',
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

        ]);

        $flight = FlightRoundTrip::find($id);
        $flight->flight_source=$request->source;
        $flight->flight_destination=$request->destination;
        $flight->return_flight_source=$request->destination;
        $flight->return_flight_destination=$request->source;
        $flight->flight_name=$request->flight_name;
        $flight->flight_number=$request->flight_number;
        $flight->flight_departure_date=$request->departure_date;
        $flight->flight_departure_time=$request->departure_time;
        $flight->flight_arrival_time=$request->arrival_time;
        $flight->return_flight_name=$request->return_flight_name;
        $flight->return_flight_number=$request->return_flight_number;
        $flight->return_departure_date=$request->return_departure_date;
        $flight->return_departure_time=$request->return_departure_time;
        $flight->return_arrival_time=$request->return_arrival_time;
<<<<<<< HEAD
        if($request->seatcount =='add'&& $request->available > 0) {
            $flight->increment('seat', $request->available);
        }
        elseif($request->seatcount =='minus' && $request->available > 0){
            $flight->decrement('seat', $request->available);
        }

        $flight->flight_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
         $flight->flight_via=$request->via;
=======
        $flight->seat=$request->seat;
        $flight->flight_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        $flight->save();
        return redirect('admin/roundTripFlights')->with(['success'=>'Flight updated successfully']);
    }


     public function changeFlightStatus($flight_id,$status)
    {
         FlightRoundTrip::where('id',$flight_id)->update(['status'=>$status]);
         return redirect()->back()->with(['success' => 'Flight status updated successfully.']);
    }
<<<<<<< HEAD
    private function supplieradupdate($user,$text,$roundtripbook){
        $admin_email = Sentinel::findById(2)->email;
        Mail::send('emails.updateblockroundtrip',[
            'user' => $user,
            'text' => $text,
            'roundtripbook' => $roundtripbook,
        ],function($message) use ($user, $text,$admin_email,$roundtripbook) {
            $message->to($admin_email);
            $message->subject("$text");
        });
       
    }
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

}
