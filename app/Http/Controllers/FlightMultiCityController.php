<?php

namespace App\Http\Controllers;

use App\Models\FlightMultiCity;
use App\Models\FlightDestination;
use App\Models\Flight;
use App\Models\FlightSource;
use Illuminate\Http\Request;
use Sentinel;
<<<<<<< HEAD
use Mail;
use View;
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

class FlightMultiCityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Sentinel::getUser()->id;
        $flight=FlightMultiCity::with('flightF','flightS','flightT','flightSourceF','flightSourceS','flightSourceT','flightDestinationF','flightDestinationS','flightDestinationT')->orderBy('created_at', 'DESC')->where('is_delete','1')->where('supplier_id',$id)->get();
        return view('supplier.flightmulticity.index',compact('flight'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
<<<<<<< HEAD
        $flightSource = FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination = FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight = Flight::where('is_delete','1')->where('status','1')->get();
=======
        $flightSource=FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination=FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight=Flight::where('is_delete','1')->where('status','1')->get();
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        return view('supplier.flightmulticity.create',compact('flightSource','flightDestination','flight'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
<<<<<<< HEAD
        $messages = array(
            'required_with' => 'The :attribute field is required.',
            'after' => 'arrival time must be after departure time',
        );
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        $this->validate($request,[
            'source_f' =>'required',
            'destination_f' =>'required',
            'flight_name_f' =>'required',
<<<<<<< HEAD
            'flight_number_f' =>'required|digits_between:1,10',
            'departure_date_f' =>'required',
            'departure_time_f' => 'date_format:"H:i"|required',
            'arrival_time_f'   =>'date_format:"H:i"|required',
            'source_s' =>'required',
            'destination_s' =>'required',
            'flight_name_s' =>'required',
            'flight_number_s' =>'required|digits_between:1,10',
            'departure_date_s' =>'required|after:departure_date_f',
            'departure_time_s' => 'date_format:"H:i"|required',
            'arrival_time_s'   =>'date_format:"H:i"|required',


            'source_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t',
            'destination_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t',
            'flight_name_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t',
            'flight_number_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|nullable|digits_between:1,10',
            'departure_date_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|nullable|after:departure_date_s',
            'departure_time_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|date_format:"H:i"|nullable',
            'arrival_time_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|date_format:"H:i"|nullable',

            'flight_price' =>'nullable | numeric|min:1|max:1000000',
            'seat' =>'nullable | numeric|min:1|max:200',
            'available' =>'nullable | numeric|min:1|max:200',
            'pnr_no' =>'nullable|digits_between:1,10',
            'pnr_no2' =>'nullable|digits_between:1,10',
            'pnr_no3' =>'nullable|digits_between:1,10',
            'via' =>'nullable|max:200',


        ],$messages);
=======
            'flight_number_f' =>'required',
            'departure_date_f' =>'required',
            'departure_time_f' =>'required',
            'arrival_time_f' =>'required',
            'source_s' =>'required',
            'destination_s' =>'required',
            'flight_name_s' =>'required',
            'flight_number_s' =>'required',
            'departure_date_s' =>'required',
            'departure_time_s' =>'required',
            'arrival_time_s' =>'required',
            'source_t' =>'required',
            'destination_t' =>'required',
            'flight_name_t' =>'required',
            'flight_number_t' =>'required',
            'departure_date_t' =>'required',
            'departure_time_t' =>'required',
            'arrival_time_t' =>'required',
            'flight_price' =>'required | digits_between:1,10',
            'seat' =>'required | digits_between:1,10',

        ]);
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

        $flight = new  FlightMultiCity;
        $flight->supplier_id=Sentinel::getUser()->id;

        $flight->flight_source_f=$request->source_f;
        $flight->flight_destination_f=$request->destination_f;
        $flight->flight_name_f=$request->flight_name_f;
        $flight->flight_number_f=$request->flight_number_f;
        $flight->flight_departure_date_f=$request->departure_date_f;
        $flight->flight_departure_time_f=$request->departure_time_f;
        $flight->flight_arrival_time_f=$request->arrival_time_f;

        $flight->flight_source_s=$request->source_s;
        $flight->flight_destination_s=$request->destination_s;
        $flight->flight_name_s=$request->flight_name_s;
        $flight->flight_number_s=$request->flight_number_s;
        $flight->flight_departure_date_s=$request->departure_date_s;
        $flight->flight_departure_time_s=$request->departure_time_s;
        $flight->flight_arrival_time_s=$request->arrival_time_s;

        $flight->flight_source_t=$request->source_t;
        $flight->flight_destination_t=$request->destination_t;
        $flight->flight_name_t=$request->flight_name_t;
        $flight->flight_number_t=$request->flight_number_t;
        $flight->flight_departure_date_t=$request->departure_date_t;
        $flight->flight_departure_time_t=$request->departure_time_t;
        $flight->flight_arrival_time_t=$request->arrival_time_t;

        $flight->seat=$request->seat;
        $flight->flight_price=$request->flight_price;
<<<<<<< HEAD
        $flight->supplier_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
        $flight->flight_pnr_no3=$request->pnr_no3;
        $flight->flight_via=$request->via;
        $flight->save();

        $multicitybook=FlightMultiCity::with('flightF','flightS','flightT','flightSourceF','flightSourceS','flightSourceT','flightDestinationF','flightDestinationS','flightDestinationT')->find($flight->id);

        $user = Sentinel::getUser();
        $text = 'Supplier Add New Block';
       
        $this->supplieradupdate($user,$text,$multicitybook); // this is email sent to admin for new supplier


=======
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
        $flight->flight_pnr_no3=$request->pnr_no3;
        $flight->save();
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        return redirect()->route('supplier-FlightMultiCity.index')->with(['success'=>'Flight added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FlightMultiCity  $flightMultiCity
     * @return \Illuminate\Http\Response
     */
    public function show(FlightMultiCity $flightMultiCity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FlightMultiCity  $flightMultiCity
     * @return \Illuminate\Http\Response
     */
    public function edit(FlightMultiCity $flightMultiCity,$id)
    {
        $flightSource=FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination=FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight=Flight::where('is_delete','1')->where('status','1')->get();
        $flightCity = FlightMultiCity::where('id',$id)->first();
        return view('supplier.flightmulticity.edit',compact('flightSource','flightDestination','flight','flightCity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FlightMultiCity  $flightMultiCity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FlightMultiCity $flightMultiCity,$id)
    {
<<<<<<< HEAD
         $messages = array(
            'required_with' => 'The :attribute field is required.',
            'after' => 'arrival time must be after departure time',
        );
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        $this->validate($request,[
            'source_f' =>'required',
            'destination_f' =>'required',
            'flight_name_f' =>'required',
<<<<<<< HEAD
            'flight_number_f' =>'required|digits_between:1,10',
            'departure_date_f' =>'required',
            'departure_time_f' => 'date_format:"H:i"|required',
            'arrival_time_f'   =>'date_format:"H:i"|required',
            'source_s' =>'required',
            'destination_s' =>'required',
            'flight_name_s' =>'required',
            'flight_number_s' =>'required|digits_between:1,10',
            'departure_date_s' =>'required|after:departure_date_f',
            'departure_time_s' => 'date_format:"H:i"|required',
            'arrival_time_s'   =>'date_format:"H:i"|required',


            'source_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t',
            'destination_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t',
            'flight_name_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t',
            'flight_number_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|nullable|digits_between:1,10',
            'departure_date_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|nullable|after:departure_date_s',
            'departure_time_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|date_format:"H:i"|nullable',
            'arrival_time_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|date_format:"H:i"|nullable',

            'flight_price' =>'nullable | numeric|min:1|max:1000000',
            'seat' =>'nullable | numeric|min:1|max:200',
            'available' =>'nullable | numeric|min:1|max:200',
            'pnr_no' =>'nullable|digits_between:1,10',
            'pnr_no2' =>'nullable|digits_between:1,10',
            'pnr_no3' =>'nullable|digits_between:1,10',
            'via' =>'nullable|max:200',


        ],$messages);
=======
            'flight_number_f' =>'required',
            'departure_date_f' =>'required',
            'departure_time_f' =>'required',
            'arrival_time_f' =>'required',
            'source_s' =>'required',
            'destination_s' =>'required',
            'flight_name_s' =>'required',
            'flight_number_s' =>'required',
            'departure_date_s' =>'required',
            'departure_time_s' =>'required',
            'arrival_time_s' =>'required',
            'source_t' =>'required',
            'destination_t' =>'required',
            'flight_name_t' =>'required',
            'flight_number_t' =>'required',
            'departure_date_t' =>'required',
            'departure_time_t' =>'required',
            'arrival_time_t' =>'required',
            'flight_price' =>'required | digits_between:1,10',
            'seat' =>'required | digits_between:1,10',

        ]);
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

        $flight = FlightMultiCity::find($id);
        $flight->supplier_id=Sentinel::getUser()->id;

        $flight->flight_source_f=$request->source_f;
        $flight->flight_destination_f=$request->destination_f;
        $flight->flight_name_f=$request->flight_name_f;
        $flight->flight_number_f=$request->flight_number_f;
        $flight->flight_departure_date_f=$request->departure_date_f;
        $flight->flight_departure_time_f=$request->departure_time_f;
        $flight->flight_arrival_time_f=$request->arrival_time_f;

        $flight->flight_source_s=$request->source_s;
        $flight->flight_destination_s=$request->destination_s;
        $flight->flight_name_s=$request->flight_name_s;
        $flight->flight_number_s=$request->flight_number_s;
        $flight->flight_departure_date_s=$request->departure_date_s;
        $flight->flight_departure_time_s=$request->departure_time_s;
        $flight->flight_arrival_time_s=$request->arrival_time_s;

        $flight->flight_source_t=$request->source_t;
        $flight->flight_destination_t=$request->destination_t;
        $flight->flight_name_t=$request->flight_name_t;
        $flight->flight_number_t=$request->flight_number_t;
        $flight->flight_departure_date_t=$request->departure_date_t;
        $flight->flight_departure_time_t=$request->departure_time_t;
        $flight->flight_arrival_time_t=$request->arrival_time_t;
<<<<<<< HEAD
        if($request->seatcount =='add'&& $request->available > 0) {
            $flight->increment('seat', $request->available);
        }
        elseif($request->seatcount =='minus' && $request->available > 0){
            $flight->decrement('seat', $request->available);
        }

        $flight->supplier_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
        $flight->flight_pnr_no3=$request->pnr_no3;
        $flight->flight_via=$request->via;
        $flight->save();

        $multicitybook=FlightMultiCity::with('flightF','flightS','flightT','flightSourceF','flightSourceS','flightSourceT','flightDestinationF','flightDestinationS','flightDestinationT')->find($flight->id);

        $user = Sentinel::getUser();
        $text = 'Supplier edit  block';
        $this->supplieradupdate($user,$text,$multicitybook); // this is email sent to admin for new supplier

=======

        $flight->seat=$request->seat;
        $flight->flight_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
        $flight->flight_pnr_no3=$request->pnr_no3;
        $flight->save();
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        return redirect()->route('supplier-FlightMultiCity.index')->with(['success'=>'Flight Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FlightMultiCity  $flightMultiCity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        FlightMultiCity::wherein('id', $request->check)->update(['is_delete' => 0]);
        return redirect()->back()->with('success', 'Flight Deleted successfully');
    }


     public function flightIsDelete($flight_id,$status)
    {
         FlightMultiCity::where('id',$flight_id)->update(['is_delete'=>$status]);
         return redirect()->back()->with(['success' => 'Flight Deleted successfully.']);
    }


     /**  For Admin Function **/ 

    public function listMultiCityFlight()
    {
<<<<<<< HEAD
         $flight=FlightMultiCity::with('flightF','flightS','flightT','flightSourceF','flightSourceS','flightSourceT','flightDestinationF','flightDestinationS','flightDestinationT')->orderBy('created_at', 'DESC')->where('is_delete','1')->get();
=======
        $flight=FlightMultiCity::with('flightF','flightS','flightT','flightSourceF','flightSourceS','flightSourceT','flightDestinationF','flightDestinationS','flightDestinationT')->orderBy('created_at', 'DESC')->where('is_delete','1')->get();
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        return view('admin.flightmulticity.index',compact('flight'));
    }

    public function createMultiCityFlight()
    {
        $flightSource=FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination=FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight=Flight::where('is_delete','1')->where('status','1')->get();
        return view('admin.flightmulticity.create',compact('flightSource','flightDestination','flight'));
    }

     public function storeFlightMultiCity(Request $request)
    {
<<<<<<< HEAD
        $messages = array(
            'required_with' => 'The :attribute field is required.',
            'after' => 'arrival time must be after departure time',
        );

=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        $this->validate($request,[
            'source_f' =>'required',
            'destination_f' =>'required',
            'flight_name_f' =>'required',
<<<<<<< HEAD
            'flight_number_f' =>'required|digits_between:1,10',
            'departure_date_f' =>'required',
            'departure_time_f' => 'date_format:"H:i"|required',
            'arrival_time_f'   =>'date_format:"H:i"|required',
            'source_s' =>'required',
            'destination_s' =>'required',
            'flight_name_s' =>'required',
            'flight_number_s' =>'required|digits_between:1,10',
            'departure_date_s' =>'required|after:departure_date_f',
            'departure_time_s' => 'date_format:"H:i"|required',
            'arrival_time_s'   =>'date_format:"H:i"|required',


            'source_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t',
            'destination_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t',
            'flight_name_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t',
            'flight_number_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|nullable|digits_between:1,10',
            'departure_date_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|nullable|after:departure_date_s',
            'departure_time_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|date_format:"H:i"|nullable',
            'arrival_time_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|date_format:"H:i"|nullable',

            'flight_price' =>'nullable | numeric|min:1|max:1000000',
            'seat' =>'nullable | numeric|min:1|max:200',
            'available' =>'nullable | numeric|min:1|max:200',
            'pnr_no' =>'nullable|digits_between:1,10',
            'pnr_no2' =>'nullable|digits_between:1,10',
            'pnr_no3' =>'nullable|digits_between:1,10',
            'via' =>'nullable|max:200',

        ],$messages);
=======
            'flight_number_f' =>'required',
            'departure_date_f' =>'required',
            'departure_time_f' =>'required',
            'arrival_time_f' =>'required',
            'source_s' =>'required',
            'destination_s' =>'required',
            'flight_name_s' =>'required',
            'flight_number_s' =>'required',
            'departure_date_s' =>'required',
            'departure_time_s' =>'required',
            'arrival_time_s' =>'required',
            'source_t' =>'required',
            'destination_t' =>'required',
            'flight_name_t' =>'required',
            'flight_number_t' =>'required',
            'departure_date_t' =>'required',
            'departure_time_t' =>'required',
            'arrival_time_t' =>'required',
            'flight_price' =>'required | digits_between:1,10',
            'seat' =>'required | digits_between:1,10',

        ]);
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

        $flight = new  FlightMultiCity;
        $flight->supplier_id=Sentinel::getUser()->id;

        $flight->flight_source_f=$request->source_f;
        $flight->flight_destination_f=$request->destination_f;
        $flight->flight_name_f=$request->flight_name_f;
        $flight->flight_number_f=$request->flight_number_f;
        $flight->flight_departure_date_f=$request->departure_date_f;
        $flight->flight_departure_time_f=$request->departure_time_f;
        $flight->flight_arrival_time_f=$request->arrival_time_f;

        $flight->flight_source_s=$request->source_s;
        $flight->flight_destination_s=$request->destination_s;
        $flight->flight_name_s=$request->flight_name_s;
        $flight->flight_number_s=$request->flight_number_s;
        $flight->flight_departure_date_s=$request->departure_date_s;
        $flight->flight_departure_time_s=$request->departure_time_s;
        $flight->flight_arrival_time_s=$request->arrival_time_s;

        $flight->flight_source_t=$request->source_t;
        $flight->flight_destination_t=$request->destination_t;
        $flight->flight_name_t=$request->flight_name_t;
        $flight->flight_number_t=$request->flight_number_t;
        $flight->flight_departure_date_t=$request->departure_date_t;
        $flight->flight_departure_time_t=$request->departure_time_t;
        $flight->flight_arrival_time_t=$request->arrival_time_t;

<<<<<<< HEAD

        $flight->seat=$request->seat;
        $flight->supplier_price=$request->flight_price;
=======
        $flight->seat=$request->seat;
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        $flight->flight_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
        $flight->flight_pnr_no3=$request->pnr_no3;
<<<<<<< HEAD
        $flight->flight_via=$request->via;
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        $flight->save();
        return redirect('admin/multiCityFlights')->with(['success'=>'Flight added successfully']);
    }

    public function editMultiCityFlight(FlightMultiCity $flightMultiCity,$id)
    {
        $flightSource=FlightSource::where('is_delete','1')->where('status','1')->get();
        $flightDestination=FlightDestination::where('is_delete','1')->where('status','1')->get();
        $flight=Flight::where('is_delete','1')->where('status','1')->get();
        $flightCity = FlightMultiCity::where('id',$id)->first();
        return view('admin.flightmulticity.edit',compact('flightSource','flightDestination','flight','flightCity'));
    }

     public function updateMultiCityFlight(Request $request,$id)
    {
<<<<<<< HEAD
        $messages = array(
            'required_with' => 'The :attribute field is required.',
            'after' => 'arrival time must be after departure time',
        );
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        $this->validate($request,[
            'source_f' =>'required',
            'destination_f' =>'required',
            'flight_name_f' =>'required',
<<<<<<< HEAD
            'flight_number_f' =>'required|digits_between:1,10',
            'departure_date_f' =>'required',
            'departure_time_f' => 'date_format:"H:i"|required',
            'arrival_time_f'   =>'date_format:"H:i"|required',
            'source_s' =>'required',
            'destination_s' =>'required',
            'flight_name_s' =>'required',
            'flight_number_s' =>'required|digits_between:1,10',
            'departure_date_s' =>'required|after:departure_date_f',
            'departure_time_s' => 'date_format:"H:i"|required',
            'arrival_time_s'   =>'date_format:"H:i"|required',


            'source_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t',
            'destination_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t',
            'flight_name_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t',
            'flight_number_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|nullable|digits_between:1,10',
            'departure_date_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|nullable|after:departure_date_s',
            'departure_time_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|date_format:"H:i"|nullable',
            'arrival_time_t' =>'required_with:source_t,destination_t,flight_name_t,flight_number_t,departure_date_t|date_format:"H:i"|nullable',

            'flight_price' =>'nullable | numeric|min:1|max:1000000',
            'seat' =>'nullable | numeric|min:1|max:200',
            'available' =>'nullable | numeric|min:1|max:200',
            'pnr_no' =>'nullable|digits_between:1,10',
            'pnr_no2' =>'nullable|digits_between:1,10',
            'pnr_no3' =>'nullable|digits_between:1,10',
            'via' =>'nullable|max:200',
        ],$messages);
=======
            'flight_number_f' =>'required',
            'departure_date_f' =>'required',
            'departure_time_f' =>'required',
            'arrival_time_f' =>'required',
            'source_s' =>'required',
            'destination_s' =>'required',
            'flight_name_s' =>'required',
            'flight_number_s' =>'required',
            'departure_date_s' =>'required',
            'departure_time_s' =>'required',
            'arrival_time_s' =>'required',
            'source_t' =>'required',
            'destination_t' =>'required',
            'flight_name_t' =>'required',
            'flight_number_t' =>'required',
            'departure_date_t' =>'required',
            'departure_time_t' =>'required',
            'arrival_time_t' =>'required',
            'flight_price' =>'required | digits_between:1,10',
            'seat' =>'required | digits_between:1,10',

        ]);
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

        $flight = FlightMultiCity::find($id);

        $flight->flight_source_f=$request->source_f;
        $flight->flight_destination_f=$request->destination_f;
        $flight->flight_name_f=$request->flight_name_f;
        $flight->flight_number_f=$request->flight_number_f;
        $flight->flight_departure_date_f=$request->departure_date_f;
        $flight->flight_departure_time_f=$request->departure_time_f;
        $flight->flight_arrival_time_f=$request->arrival_time_f;

        $flight->flight_source_s=$request->source_s;
        $flight->flight_destination_s=$request->destination_s;
        $flight->flight_name_s=$request->flight_name_s;
        $flight->flight_number_s=$request->flight_number_s;
        $flight->flight_departure_date_s=$request->departure_date_s;
        $flight->flight_departure_time_s=$request->departure_time_s;
        $flight->flight_arrival_time_s=$request->arrival_time_s;

        $flight->flight_source_t=$request->source_t;
        $flight->flight_destination_t=$request->destination_t;
        $flight->flight_name_t=$request->flight_name_t;
        $flight->flight_number_t=$request->flight_number_t;
        $flight->flight_departure_date_t=$request->departure_date_t;
        $flight->flight_departure_time_t=$request->departure_time_t;
        $flight->flight_arrival_time_t=$request->arrival_time_t;

<<<<<<< HEAD
        if($request->seatcount =='add'&& $request->available > 0) {
            $flight->increment('seat', $request->available);
        }
        elseif($request->seatcount =='minus' && $request->available > 0){
            $flight->decrement('seat', $request->available);
        }
=======
        $flight->seat=$request->seat;
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        $flight->flight_price=$request->flight_price;
        $flight->flight_pnr_no=$request->pnr_no;
        $flight->flight_pnr_no2=$request->pnr_no2;
        $flight->flight_pnr_no3=$request->pnr_no3;
<<<<<<< HEAD
        $flight->flight_via=$request->via;
        $flight->save();

=======
        $flight->save();
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        return redirect('admin/multiCityFlights')->with(['success'=>'Flight Updated successfully']);
    }

     public function changeFlightStatus($flight_id,$status)
    {
         FlightMultiCity::where('id',$flight_id)->update(['status'=>$status]);
         return redirect()->back()->with(['success' => 'Flight status updated successfully.']);
    }

<<<<<<< HEAD
    private function supplieradupdate($user,$text,$multicitybook){
        $admin_email = Sentinel::findById(2)->email;
        Mail::send('emails.updateblockmulticity',[
            'user' => $user,
            'text' => $text,
            'multicitybook' => $multicitybook,
        ],function($message) use ($user, $text,$admin_email,$multicitybook) {
            $message->to($admin_email);
            $message->subject("$text");
        });
    }


=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

}
