<?php

namespace App\Http\Controllers;

use App\Models\FlightSource;
use Illuminate\Http\Request;

class FlightSourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
<<<<<<< HEAD
        $flight=FlightSource::where('is_delete','1')->orderBy('id', 'desc')->get();
=======
        $flight=FlightSource::where('is_delete','1')->get();
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
        return view('admin.flightSource.index',compact('flight'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.flightSource.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'flight_source' =>'required|unique:flight_sources'
        ]);

        $flight = new  FlightSource;
        $flight->flight_source=$request->flight_source;
        $flight->save();
        return redirect()->route('admin-flightSource.index')->with(['success'=>'Flight Source added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FlightSource  $flightSource
     * @return \Illuminate\Http\Response
     */
    public function show(FlightSource $flightSource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FlightSource  $flightSource
     * @return \Illuminate\Http\Response
     */
    public function edit(FlightSource $flightSource,$id)
    {
        $flight=FlightSource::where('id',$id)->first();
        return view('admin.flightSource.edit',compact('flight'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FlightSource  $flightSource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FlightSource $flightSource, $id)
    {
         $this->validate($request,[
            'flight_source' =>'required|unique:flight_sources'
        ]);
        $flight = FlightSource::find($id);
        $flight->flight_source=$request->flight_source;
        $flight->save();
        return redirect()->route('admin-flightSource.index')->with(['success'=>'Flight Source Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FlightSource  $flightSource
     * @return \Illuminate\Http\Response
     */
    public function destroy(FlightSource $flightSource)
    {
        //
    }


    public function flightSourceStatus($flight_id,$status)
    {
         FlightSource::where('id',$flight_id)->update(['status'=>$status]);
         return redirect()->back()->with(['success' => 'Flight status updated successfully.']);
    }

    public function flightSourceIsDelete($flight_id,$status)
    {
         FlightSource::where('id',$flight_id)->update(['is_delete'=>$status]);
         return redirect()->back()->with(['success' => 'Deleted successfully.']);
    }
}
