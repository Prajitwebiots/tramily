<?php

namespace App\Http\Controllers;

use App\Models\State;
use App\Models\Country;
use Illuminate\Http\Request;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $state=State::where('is_delete','1')->with('country')->get();
        return view('admin.state.index',compact('state'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country=Country::where('is_delete','1')->where('status','1')->get();
        return view('admin.state.create',compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'country_id' =>'required',
            'state_name' =>'required|max:20'
        ]);

        $state = new  State;
        $state->country_id=$request->country_id;
        $state->state_name=$request->state_name;
        $state->save();
        return redirect()->route('admin-state.index')->with(['success'=>'State added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit(State $state,$id)
    {
        $country=Country::where('is_delete','1')->where('status','1')->get();
        $state=State::where('id',$id)->first();
        return view('admin.state.edit',compact('country','state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, State $state,$id)
    {
        $this->validate($request,[
            'country_id' =>'required',
            'state_name' =>'required|max:20'
        ]);

        $state = State::find($id);;
        $state->country_id=$request->country_id;
        $state->state_name=$request->state_name;
        $state->save();
        return redirect()->route('admin-state.index')->with(['success'=>'State Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state)
    {
        //
    }


    public function stateStatus($state_id,$status)
    {
         State::where('id',$state_id)->update(['status'=>0]);
         return redirect()->back()->with(['success' => 'State status updated successfully.']);
    }

    public function stateIsDelete($state_id,$status)
    {
<<<<<<< HEAD

         State::where('id',$state_id)->update(['is_delete'=>0]);
         return redirect()->back()->with(['success' => 'Deleted successfully.']);
    }

=======
         State::where('id',$state_id)->update(['is_delete'=>$status]);
         return redirect()->back()->with(['success' => 'Deleted successfully.']);
    }
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
}
