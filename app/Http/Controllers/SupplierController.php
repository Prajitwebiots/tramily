<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Sentinel;
use Hash;
use App\User;
use App\Models\RoleUser;
use App\Models\Activation;
use App\Models\Bookings;
use App\Models\Country;
use App\Models\State;
use App\Models\City;

 use App\Models\FlightOneWay;
 use App\Models\FlightRoundTrip;
 use App\Models\FlightMultiCity;
use DB;

use DateTime;
use imagick;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('supplier.auth.login');
    }

    public function dashboard()
    {
        return view('supplier.dashboard');
    }


    public function profile()
    {

        $countrylist = Country::where('is_delete', 1)->where('status', 1)->get();
        $statelist = State::where('is_delete', 1)->where('status', 1)->get();
        $citylist = City::where('is_delete', 1)->where('status', 1)->get();

        $userdata = User::where('id', Sentinel::getUser()->id)->first();
        return view('supplier.profile', compact('userdata', 'countrylist', 'statelist', 'citylist'));

    }

    public function changePass()
    {
        return view('supplier.changePass');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function bookinghistory()
    {
        $id = Sentinel::getuser()->id;
        $history_oneway = Bookings::with('Oneway', 'Oneway.flight', 'Oneway.flightSource', 'Oneway.flightDestination')->where('ticket_type', 1)->where('supplier_id', $id)->orderBy('id', 'desc')->get();
        $history_round_trip = Bookings::with('round_trip', 'round_trip.flightDestination', 'round_trip.flightSourceReturn', 'round_trip.flightSource', 'round_trip.flightReturn', 'round_trip.flightSource', 'round_trip.flightDestinationReturn')->where('ticket_type', 2)->where('supplier_id', $id)->orderBy('id', 'desc')->get();
         $history_multicity = Bookings::with('multicity', 'multicity.flightF', 'multicity.flightS', 'multicity.flightT', 'multicity.flightSourceF', 'multicity.flightSourceS', 'multicity.flightSourceT', 'multicity.flightDestinationF', 'multicity.flightDestinationS', 'multicity.flightDestinationT')->where('ticket_type', 3)->where('supplier_id', $id)->orderBy('id', 'desc')->get();
        return view('supplier.booking_history', compact('history_oneway', 'history_round_trip', 'history_multicity'));
    }

    public function download()
    {
       
          
           FlightOneWay::where(DB::raw("UNIX_TIMESTAMP(STR_TO_DATE(flight_departure, '%d-%m-%Y'))"),'<', strtotime( date('d-m-Y', strtotime(date('d-m-Y'). ' - 1 days'))))->update(['status'=>0,'is_delete'=>0]);
           FlightRoundTrip::where(DB::raw("UNIX_TIMESTAMP(STR_TO_DATE(flight_departure_date, '%d-%m-%Y'))"),'<', strtotime( date('d-m-Y', strtotime(date('d-m-Y'). ' - 1 days'))))->update(['status'=>0,'is_delete'=>0]);
           FlightMultiCity::where(DB::raw("UNIX_TIMESTAMP(STR_TO_DATE(flight_departure_date_f, '%d-%m-%Y'))"),'<', strtotime( date('d-m-Y', strtotime(date('d-m-Y'). ' - 1 days'))))->update(['status'=>0,'is_delete'=>0]);
           
            // FlightRoundTrip::whereDate('flight_departure_date','<',date('d-m-y'))->update(['status'=>0,'is_delete'=>0]);
        // FlightMultiCity::whereDate('flight_departure_date_f','<',date('d-m-Y'))->update(['status'=>0,'is_delete'=>0]);


//         $html_code = "<h1>this is the body</h1>";


//         $my_img = imagecreate( 200, 80 );
//         $background = imagecolorallocate( $my_img, 0, 0, 255 );
//         $text_colour = imagecolorallocate( $my_img, 255, 255, 0 );
// $line_colour = imagecolorallocate( $my_img, 128, 255, 0 );
//         imagestring( $my_img, 4, 30, 25, "$html_code",$text_colour );
//         imagesetthickness ( $my_img, 5 );
//         imageline( $my_img, 30, 45, 165, 45, $line_colour );

// FlightOneWay::whereDate('flight_departure','<',date('d-m-Y'))->update(['status'=>3,'is_delete'=>0]);
//          FlightRoundTrip::whereDate('flight_departure_date','<',date('d-m-y'))->update(['status'=>3,'is_delete'=>0]);
//          FlightMultiCity::whereDate('flight_departure_date_f','<',date('d-m-Y'))->update(['status'=>3,'is_delete'=>0]);



//         header('Content-Type: image/jpeg');
//         imagejpeg($my_img);
//         file_put_contents(public_path('/assets/ticket/'.'test.jpeg'),imagejpeg($my_img));
//         imagedestroy( $my_img );

        

        
        

//        $view = view('404.index');
//        // Set the content-type
//
//
//// Create the image
//        $im = imagecreatetruecolor(900, 1200);
//
//// Create some colors
//        $white = imagecolorallocate($im, 255, 255, 255);
//        $grey = imagecolorallocate($im, 128, 128, 128);
//        $black = imagecolorallocate($im, 0, 0, 0);
//        imagefilledrectangle($im, 0, 0, 900, 1200, $white);
//
//// The text to draw
//        $text = $view;
//// Replace path by your own font path
//         $font =public_path('Roboto-Black.ttf');
//
//
//// Add the text
//        imagestring($im, 10, 0, 10, 20, $black, $font, $text);
//
//// Using imagepng() results in clearer text compared with imagejpeg()
    //        header('Content-Type: image/jpeg');
    //        imagejpeg($im);
//
//        file_put_contents(public_path('/assets/ticket/'.'test.jpeg'),imagejpeg($im));
//
//        imagedestroy($im);


    }
}