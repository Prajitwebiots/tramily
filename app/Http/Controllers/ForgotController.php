<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Sentinel;
use Activation;
use Reminder;
use Mail;

class ForgotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

          $user = User::whereEmail($request->email)->where('user_type',1)->first();
        if(count($user)>0){
            $sentinelUser = Sentinel::findById($user->id);
            if(count($user)==0)
                return redirect()->back()->with(['success'=>'Reset Code already sends  to your email.']);

            $reminder = Reminder::exists($sentinelUser) ? : Reminder::create($sentinelUser);
   
            $this->sendEmail($user,$reminder->code);

            return "Reset Code is send to your email";
        }
        else{

            return 'This email is not found please enter registered email.';
        }
    }
    public function storesupplier(Request $request)
    {
           $user = User::whereEmail($request->email)->where('user_type',2)->first();
        if(count($user)>0){
            $sentinelUser = Sentinel::findById($user->id);
            if(count($user)==0)
                return redirect()->back()->with(['success'=>'Reset Code already sends  to your email.']);

            $reminder = Reminder::exists($sentinelUser) ? : Reminder::create($sentinelUser);

            $this->sendEmail($user,$reminder->code);

            return "Reset Code is send to your email";
        }
        else{

            return 'This email is not found please enter registered email.';
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($email,$code)
    {
        $email = $email;
        $user = User::where('username',$email)->first();
            if($user)
                return view('auth.reset',compact('email','user','code'));
            else
                return view('auth.login')->with(['error'=>'Link is expired.']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'email' => 'required|max:255',
            'password'   => 'required|min:8|max:32',
            'confirm_password'  => 'required|same:password',
            'code'  => 'required',
        ]);
         $user = User::where('username',$request->email)->first();
        $sentinelUser = Sentinel::findById($user->id);

        if ($reminder = Reminder::complete($sentinelUser, $request->code, $request->password))
        {
           return redirect('/login')->with(['success'=>" Your Password Reset successfully !!!"]);
        }
        else
        {
           return redirect('/login')->with(['error'=>" Something went wrong. Please try again."]); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    // Forgot mail send
    private function sendEmail($user, $code){
        return Mail::send('emails.forgot-password',[
            'user' => $user,
            'code' => $code
            ],function ($message) use ($user) {
                $message->to($user->email);
                $message->subject("Reset Password");
            });
    }
}
