<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Activation;
use App\User;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use Mail;
use View;
use Reminder;
use Socialite;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

         $countrylist =Country::where('is_delete',1)->where('status',1)->get();
         $statelist =State::where('is_delete',1)->where('status',1)->get();
         $citylist =City::where('is_delete',1)->where('status',1)->get();

       return view('auth.register',compact('countrylist','statelist','citylist'));
    }

    public function supplierReg()
    {
        $countrylist =Country::where('is_delete',1)->where('status',1)->get();
        $statelist =State::where('is_delete',1)->where('status',1)->get();
        $citylist =City::where('is_delete',1)->where('status',1)->get();

<<<<<<< HEAD
        return view('supplier.auth.register',compact('countrylist','statelist','citylist'));
      
    }

=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
    
    public function store(Request $request)
    {

        try {
            $this->validate($request, [
                'agency_name'  => 'required|max:255',
                'title_name' => 'required|max:255',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'agency_address'  => 'required',
<<<<<<< HEAD
                'email'      => 'required|email|user_validation|max:255',
                'city'  => 'required|max:20',
                'state'  => 'required|max:20',
                'country'  => 'required|max:20',
                'pincode'  => 'required|digits:6',
                'mobile'  => 'required|digits_between:10,13',
                'office_no'  => 'required|max:255',
                 'password'   => 'required|min:8|max:32',
=======
//                'email'      => 'required|email|unique:users',
                'email'      => 'required|email|user_validation',
                'city'  => 'required',
                'state'  => 'required',
                'country'  => 'required',
                'pincode'  => 'required',
                'mobile'  => 'required',
                'office_no'  => 'required',
                'password'   => 'required',
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                'confirm_password'  => 'required|same:password',
                'gst_full_name'   => 'required|max:50',
                'gst_state'   => 'required|max:50',
                'gst_no'   => 'required|min:15|max:15',
                'gst_agency_address'   => 'nullable|max:500',
                'register_email'   => 'nullable|email',
                'gst_phone_number'   => 'nullable|digits_between:10,13',
                'pan_no'   =>'nullable|min:10|max:10',

            ]);

            $user =  Sentinel::register(array(
                'title_name'   => $request->title_name,
                'first_name'   => $request->first_name,
                'last_name'   => $request->last_name,
                'email'        => $request->email,
                'password'     => $request->password,
                'agency_name'  => $request->agency_name,
                'agency_address'  => $request->agency_address,
                'mobile'  => $request->mobile,
                'office_no'  => $request->office_no,
                'city'  => $request->city,
                'state'  => $request->state,
                'country'  => $request->country,
                'pincode'  => $request->pincode,
                'gst_name'  => $request->gst_full_name,
                'gst_state'  => $request->gst_state,
                'gst_no'  => $request->gst_no,
<<<<<<< HEAD
                'gst_address'  => $request->gst_agency_address,
                'gst_phone'  => $request->gst_phone_number,
                'regi_gmail'  => $request->register_email,
                'user_type'  => '1',
                'username' => 'S'.str_random('10'),
                 'pan_no'   =>$request->pan_no,
=======
                'user_type'  => '1',
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
            ));


            $activation = Activation::create($user); // will create activation record of registered user
            $role = Sentinel::findRoleById(2); // role 2 for user
            $role->users()->attach($user); // assign role to registered user
            $link = url('').'/activate/'.$request->email.'/'.$activation->code;  // account activation link
            $text = 'Your Account has been created.'; // email subjct

<<<<<<< HEAD
 //return view('emails.activation',compact('user','text','link'));
        $this->sendActivationEmail($user,$text,$link); // this is email sent to register user
         $this->newuserAdmin($user,$text,$link); // this is email admin for new user
=======
        $this->sendActivationEmail($user,$text,$link); // this is email sent to register user
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

           return redirect('login')->with(['success' => "Thank you. To complete your registration please check your email."]);

        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            return redirect()->back()->with(['error' => "You are Banned with $delay seconds"]);
        } catch (NotActivatedException $e) {
            return redirect()->back()->with(['error' => "You account is not activated!"]);
        }
    }


     public function Supplierstore(Request $request)
    {

        try {
            $this->validate($request, [
                'agency_name'  => 'required|max:255',
                'title_name' => 'required|max:255',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'agency_address'  => 'required',
<<<<<<< HEAD
                'email'      => 'required|email|supplier_validation|max:255',
                'city'  => 'required|max:20',
                'state'  => 'required|max:20',
                'country'  => 'required|max:20',
                'pincode'  => 'required|digits:6',
                'mobile'  => 'required|digits_between:10,13',
                'office_no'  => 'required|max:255',
                'password'   => 'required|min:8|max:32',
=======
                'email'      => 'required|email|supplier_validation',
                'city'  => 'required',
                'state'  => 'required',
                'country'  => 'required',
                'pincode'  => 'required',
                'mobile'  => 'required',
                'office_no'  => 'required',
                'password'   => 'required',
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
                'confirm_password'  => 'required|same:password',
                'gst_full_name'   => 'required|max:50',
                'gst_state'   => 'required|max:50',
                'gst_no'   => 'required|min:15|max:15',
                'gst_agency_address'   => 'nullable|max:500',
                'register_email'   => 'nullable|email',
                'gst_phone_number'   => 'nullable|digits_between:10,13',
                'pan_no'   =>'nullable|min:10|max:10',

            ]);

            $user =  Sentinel::register(array(
                'title_name'   => $request->title_name,
                'first_name'   => $request->first_name,
                'last_name'   => $request->last_name,
                'email'        => $request->email,
                'password'     => $request->password,
                'agency_name'  => $request->agency_name,
                'agency_address'  => $request->agency_address,
                'mobile'  => $request->mobile,
                'office_no'  => $request->office_no,
                'city'  => $request->city,
                'state'  => $request->state,
                'country'  => $request->country,
                'pincode'  => $request->pincode,
                'gst_name'  => $request->gst_full_name,
                'gst_state'  => $request->gst_state,
                'gst_no'  => $request->gst_no,
<<<<<<< HEAD
                'gst_address'  => $request->gst_agency_address,
                'gst_phone'  => $request->gst_phone_number,
                'regi_gmail'  => $request->register_email,
                'user_type'  => '2',
                'username' => 'S'.str_random('10'),
                'pan_no'   =>$request->pan_no,
=======
                'user_type'  => '2',
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc
            ));



            $activation = Activation::create($user); // will create activation record of registered user
            $role = Sentinel::findRoleById(3); // role 2 for user
            $role->users()->attach($user); // assign role to registered user
            $link = url('').'/activate/'.$request->email.'/'.$activation->code;  // account activation link
            $text = 'Your Account has been created.'; // email subjct

            $this->sendActivationEmail($user,$text,$link); // this is email sent to register user
<<<<<<< HEAD
             $this->newuserAdmin($user,$text,$link); // this is email sent to admin for new supplier
             
             
=======
>>>>>>> a4c94b4ecea0498d6873d20ed433a1495e86c9dc

            // return redirect()->back()->with(['success' => "Thank you. To complete your registration please check your email."]);
      return redirect('/supplier/login')->with(['success' => "Thank you. To complete your registration please check your email."]);
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            return redirect()->back()->with(['error' => "You are Banned with $delay seconds"]);
        } catch (NotActivatedException $e) {
            return redirect()->back()->with(['error' => "You account is not activated!"]);
        }
    }


     public function activate($email, $activationCode){
    
        $user = User::whereEmail($email)->first();
        $sentinelUser = Sentinel::findById($user->id);

        if(Activation::complete($sentinelUser, $activationCode)){
            return redirect('/login')->with(['success'=>" Your account is successfully Activated !!!"]);
        }
        else{
            return redirect('/login')->with(['error'=>" This link is expires. please try to login !!!"]);
        }
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    private function sendActivationEmail($user,$text,$link){
        Mail::send('emails.activation',[
            'user' => $user,
            'text' => $text,
            'link' => $link,
        ],function($message) use ($user, $text) {
            $message->to($user->email);
            $message->subject("Hello $user->title_name $user->first_name $user->last_name, $text");

        });
    }
    private function newuserAdmin($user,$text,$link){
        $admin = Sentinel::findById(2)->email;


        Mail::send('emails.newuser',[
            'user' => $user,
            'text' => $text,
            'link' => $link,
            'admin' => $admin,
        ],function($message) use ($user, $text,$admin) {
            $message->to($admin);

            $message->subject("new user Sign up in tramily ");
        });
    }

    public function state($country_id)
    { $list ="<option value=''>Select State</option>";
         $statelist =State::where('country_id',$country_id)->where('is_delete',1)->where('status',1)->get();

       foreach($statelist as  $state)
       {
           $list .= '<option value="'.$state->id.'">'.$state->state_name.'</option>';
       }
        return $list;


    }
    public function city($state_id)
    {
        $list ="<option value=''>Select City</option>";
        $citylist =City::where('state_id',$state_id)->where('is_delete',1)->where('status',1)->get();

        foreach($citylist as  $city)
        {
            $list .= '<option value="'.$city->id.'">'.$city->city_name.'</option>';
        }
        return $list;
    }
}
