<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class SupplierMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '3')
                return $next($request);
            else if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '2')
                return redirect('user/dashboard');

            else if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '1')
                return redirect('admin/dashboard');
            else
                return redirect('/supplier/login');
            }
        catch (\Exception $e) {
            return redirect('/supplier/login');
        }
    }
}
