<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '1')
                return redirect('admin/dashboard');
            else if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '2')
                return $next($request);
            else if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '3')
                return redirect('supplier/dashboard');
            else
                return redirect('/login');
        }
        catch (\Exception $e) {
            return redirect('/login');
        }
    }
}
