<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\FlightOneWay;
use App\Models\FlightRoundTrip;
use App\Models\FlightMultiCity;
use Illuminate\Support\Facades\Log;
use DB;

class StatusChanges extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:statuschanges';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::alert('Autotread');

        FlightOneWay::where(DB::raw("UNIX_TIMESTAMP(STR_TO_DATE(flight_departure, '%d-%m-%Y'))"),'<', strtotime( date('d-m-Y', strtotime(date('d-m-Y'). ' - 1 days'))))->update(['status'=>0,'is_delete'=>0]);
           FlightRoundTrip::where(DB::raw("UNIX_TIMESTAMP(STR_TO_DATE(flight_departure_date, '%d-%m-%Y'))"),'<', strtotime( date('d-m-Y', strtotime(date('d-m-Y'). ' - 1 days'))))->update(['status'=>0,'is_delete'=>0]);
           FlightMultiCity::where(DB::raw("UNIX_TIMESTAMP(STR_TO_DATE(flight_departure_date_f, '%d-%m-%Y'))"),'<', strtotime( date('d-m-Y', strtotime(date('d-m-Y'). ' - 1 days'))))->update(['status'=>0,'is_delete'=>0]);
        


        Log::alert('Autotread2');

    }
}
