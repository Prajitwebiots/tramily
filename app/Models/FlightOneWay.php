<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FlightOneWay extends Model
{
    public function flight()
    {
        return $this->hasOne('App\Models\Flight', 'id', 'flight_name');
    }

    public function flightSource()
    {
        return $this->hasOne('App\Models\FlightSource', 'id', 'flight_source');
    }

     public function flightDestination()
    {
        return $this->hasOne('App\Models\FlightDestination', 'id', 'flight_destination');
    }
    public function supplier()
    {
        return $this->hasOne('App\User', 'id', 'supplier_id');
    }
    public function supplier()
    {
        return $this->hasOne('App\User', 'id', 'supplier_id');
    }
}
