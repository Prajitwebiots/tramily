<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
	public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    
  
    public function country()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country');
    }
    public function state()
    {
        return $this->hasOne('App\Models\State', 'id', 'state');
    }
    public function city()
    {
        return $this->hasOne('App\Models\City', 'id', 'city');
    }
}
 