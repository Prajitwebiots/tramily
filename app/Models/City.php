<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
     public function country()
    {
        return $this->hasOne('App\Models\Country','id', 'country_id');
    }

     public function state()
    {
        return $this->hasOne('App\Models\State','id', 'state_id');
    }
}
