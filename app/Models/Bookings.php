<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    public function booking_details()
    {
        return $this->hasMany('App\Models\Booking_details', 'booking_id', 'id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function oneway()
    {
        return $this->hasOne('App\Models\FlightOneWay', 'id', 'ticket_id');
    }
    public function round_trip()
    {
        return $this->hasOne('App\Models\FlightRoundTrip', 'id', 'ticket_id');
    }
    public function multicity()
    {
        return $this->hasOne('App\Models\FlightMultiCity', 'id', 'ticket_id');
    }

}
