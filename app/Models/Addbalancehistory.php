<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Addbalancehistory extends Model
{
    protected $table = "add_balance_histories";

    public function userinfo()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function admininfo()
    {
        return $this->hasOne('App\User', 'id', 'admin_id');
    }
}
